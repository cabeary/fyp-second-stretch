﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using Supernova.Particles2D;
using Supernova.Particles2D.Modifiers.Alpha;
using Supernova.Samples.Windows.Utilities;
using GameStateManagement;
using System.Threading;
using Microsoft.Xna.Framework.Content;


namespace BrokenLands
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class CharacterCreationScreen : GameScreen
    {

        ContentManager content;
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SpriteFont spritefont, InconsolataFont;

        MouseState prevMouseState, currentMouseState;

        float FrameCounter, frameRate;
        Texture2D WorldMapTex, WorldMapUIOverlay;
        Rectangle WorldMapTexPos;

        Texture2D CancelTex;

        //List<InventoryItem> Items = new List<InventoryItem>();

        float[] ButtonAlpha;

        Texture2D SettingsIcon;

        String name;
        int[] AddedPts;
        int TotalPts;

        Rectangle[] StatPlusRects;
        Rectangle[] StatMinusRects;

        Rectangle MouseRect;
        Rectangle ConfirmationRect;
        Rectangle ChangeUnitRectLeft;
        Rectangle ChangeUnitRectRight;

        Random random;

        MouseController mousecontroller = new MouseController();

        public CharacterCreationScreen()
            : base()
        {
            this.ScreenState = ScreenState.Active;

        }



        public override void Activate(bool instancePreserved)
        {
            if (!instancePreserved)
            {
                if (content == null)
                    content = new ContentManager(ScreenManager.Game.Services, "Content");

                //TestCamera = new CameraObj(this, new Vector3(0, 0, 0), new Vector3(0, 0, 10), Vector3.Up, 1f, 1000f, 1f);
                FrameCounter = 0;
                prevMouseState = Mouse.GetState();
                currentMouseState = Mouse.GetState();

                //Loading
                spriteBatch = new SpriteBatch(ScreenManager.GraphicsDevice);

                //Textures
                WorldMapTex = content.Load<Texture2D>("UI/SkillScreen-UI");


                //Icons
                SettingsIcon = content.Load<Texture2D>("UI/Icons/Settings-Icon");
                CancelTex = content.Load<Texture2D>("UI/Icons/Cancel-Icon");

                //Fonts
                spritefont = content.Load<SpriteFont>("Times12");
                InconsolataFont = content.Load<SpriteFont>("Inconstella");

                //Models

                //Variables
                WorldMapTexPos = new Rectangle(0, 0, UIElement.viewport.Width, UIElement.viewport.Height);
                random = new Random();
                MouseRect = new Rectangle(Mouse.GetState().X, Mouse.GetState().Y, 10, 10);//Used for mouse Collision
                ButtonAlpha = new float[5];

                //Items = new List<InventoryItem>(40);

                //Temp to get in Units in
                Model Tile = content.Load<Model>("Tile/Tile");
                Texture2D tex = content.Load<Texture2D>("Gunbowman");

                StatPlusRects = new Rectangle[6];
                StatMinusRects = new Rectangle[6];

                for (int i = 0; i < 6; i++)
                {
                    StatMinusRects[i].X = (int)(0.6578125f * ScreenManager.GraphicsDevice.Viewport.Width);
                    StatMinusRects[i].Y = (int)((0.0930555f + 0.14861111f * i) * ScreenManager.GraphicsDevice.Viewport.Height);
                    StatMinusRects[i].Width = (int)(0.0265625f * ScreenManager.GraphicsDevice.Viewport.Width);
                    StatMinusRects[i].Height = (int)(0.0375 * ScreenManager.GraphicsDevice.Viewport.Height);
                }

                for (int i = 0; i < 6; i++)
                {
                    StatPlusRects[i].X = (int)(0.8953125f * ScreenManager.GraphicsDevice.Viewport.Width);
                    StatPlusRects[i].Y = (int)((0.0930555f + 0.14861111f * i) * ScreenManager.GraphicsDevice.Viewport.Height);
                    StatPlusRects[i].Width = (int)(0.0265625f * ScreenManager.GraphicsDevice.Viewport.Width);
                    StatPlusRects[i].Height = (int)(0.0375 * ScreenManager.GraphicsDevice.Viewport.Height);
                }

                ConfirmationRect = new Rectangle((int)(0.70859375f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.80555556f * ScreenManager.GraphicsDevice.Viewport.Height), (int)(0.16640625f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.0583333333f * ScreenManager.GraphicsDevice.Viewport.Height));
                ChangeUnitRectLeft = new Rectangle((int)(0.05859375f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.20138888889f * ScreenManager.GraphicsDevice.Viewport.Height), (int)(0.0548675f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.07361111f * ScreenManager.GraphicsDevice.Viewport.Height));
                ChangeUnitRectRight = new Rectangle((int)(0.52734375f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.20138888889f * ScreenManager.GraphicsDevice.Viewport.Height), (int)(0.0548675f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.07361111f * ScreenManager.GraphicsDevice.Viewport.Height));

                name = "";
                AddedPts = new int[6];
                for (int i = 0; i < 6; i++)
                {
                    AddedPts[i] = 10;
                }
                TotalPts = 60;

                // once the load has finished, we use ResetElapsedTime to tell the game's
                // timing mechanism that we have just finished a very long frame, and that
                // it should not try to catch up.
                ScreenManager.Game.ResetElapsedTime();
            }
        }

        //Every activate must have a deactivate
        public override void Deactivate()
        {
            base.Deactivate();
        }

        /// <summary>
        /// Unload graphics content used by the game.
        /// </summary>
        public override void Unload()
        {
            // content.Unload();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);

            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                ScreenManager.RemoveScreen(this);

            mousecontroller.MouseUpdate();

            //Rectangle[] prevLocations = Locations;
            currentMouseState = Mouse.GetState();

            //Update Mouse Position
            MouseRect.X = currentMouseState.X;
            MouseRect.Y = currentMouseState.Y;

            for (int i = 0; i < StatMinusRects.Length; i++)
            {
                if (MouseRect.Intersects(StatMinusRects[i]))
                {
                    if (mousecontroller.LeftMouseClick())
                    {
                        if (AddedPts[i] > 0)
                        {
                            AddedPts[i]--;
                            TotalPts--;
                            Console.WriteLine("Minused Points");
                        }
                    }
                }
            }

            for (int i = 0; i < StatPlusRects.Length; i++)
            {
                if (MouseRect.Intersects(StatPlusRects[i]))
                {
                    if (mousecontroller.LeftMouseClick())
                    {
                        if (TotalPts < 90)
                        {
                            AddedPts[i]++;
                            TotalPts++;
                            Console.WriteLine("Increased Points");
                        }
                    }
                }
            }

            if (MouseRect.Intersects(ConfirmationRect))
            {
                if (mousecontroller.LeftMouseClick())
                {
                    Player CustomPlayer = new Player(AddedPts[0], AddedPts[1], AddedPts[2], AddedPts[3], AddedPts[4], AddedPts[5], 0, name);
                }
            }

            if (MouseRect.Intersects(ChangeUnitRectLeft))
            {
                if (mousecontroller.LeftMouseClick())
                {
                }
            }

            if (MouseRect.Intersects(ChangeUnitRectRight))
            {
                if (mousecontroller.LeftMouseClick())
                {
                }
            }

            if (MouseRect.Intersects(new Rectangle((ScreenManager.GraphicsDevice.Viewport.Bounds.Right - CancelTex.Width) - 5, 7, CancelTex.Width, CancelTex.Height)))
            {
                if (mousecontroller.LeftMouseClick())
                {
                    //ScreenManager.RemoveScreen(this);
                }
            }

            // Reset prevMouseState
            prevMouseState = Mouse.GetState();

        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Draw(GameTime gameTime)
        {


            ScreenManager.FadeBackBufferToBlack(TransitionAlpha * 2 / 3);

            ScreenManager.GraphicsDevice.BlendState = BlendState.Opaque;
            ScreenManager.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            //NewParticle.Draw(gameTime, TestCamera);
            frameRate = (float)(1 / gameTime.ElapsedGameTime.TotalSeconds);

            float width = ScreenManager.GraphicsDevice.Viewport.Width;
            float scale = (width / 1920);
            Matrix scalingMatrix = Matrix.CreateScale(scale);
            spriteBatch.Begin(SpriteSortMode.Immediate, null, null, null, null, null, scalingMatrix);

            spriteBatch.Draw(WorldMapTex, new Vector2(ScreenManager.GraphicsDevice.Viewport.TitleSafeArea.X, ScreenManager.GraphicsDevice.Viewport.TitleSafeArea.Y), null, Color.White, 0f, new Vector2(0, 0), 1, SpriteEffects.None, 0f);

            spriteBatch.End();

            //UI DRAWING
            spriteBatch.Begin();
            #region Draw Stats
            DrawBorderedText(name,
                (int)(ScreenManager.GraphicsDevice.Viewport.Width / 3.047) - (int)(spritefont.MeasureString(players[0]._NAME).X / 2),
                (int)(ScreenManager.GraphicsDevice.Viewport.Height / 42.6667) + (int)(spritefont.MeasureString(players[0]._NAME).Y / 2),
                Color.White,
                spritefont);
            #endregion

            #region Draw Dialogue Box
            if (MouseRect.Intersects(new Rectangle((int)(0.68359375f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.05f * ScreenManager.GraphicsDevice.Viewport.Height), (int)(0.2109375f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.12083333f * ScreenManager.GraphicsDevice.Viewport.Height))) || MouseRect.Intersects(StatPlusRects[0]) || MouseRect.Intersects(StatMinusRects[0]))
            {
                DrawBorderedText("Melee : " + (players[CurrentPlayerNum].MyStatus.Skills.CalcMelee(0)).ToString("n2") + " --> " + (players[CurrentPlayerNum].MyStatus.Skills.CalcMelee(AddedPts[0])).ToString("n2"), (int)(0.0390625f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.7538888889f * ScreenManager.GraphicsDevice.Viewport.Height), Color.White, InconsolataFont);
                DrawBorderedText("Melee is the skill that affects physical non-combat actions,", (int)(0.0390625f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.7938888889f * ScreenManager.GraphicsDevice.Viewport.Height), Color.White, InconsolataFont);
                DrawBorderedText("such as forcing open doors", (int)(0.0390625f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.8238888889f * ScreenManager.GraphicsDevice.Viewport.Height), Color.White, InconsolataFont);
            }
            else if (MouseRect.Intersects(new Rectangle((int)(0.68359375f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.198611111f * ScreenManager.GraphicsDevice.Viewport.Height), (int)(0.2109375f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.12083333f * ScreenManager.GraphicsDevice.Viewport.Height))) || MouseRect.Intersects(StatPlusRects[1]) || MouseRect.Intersects(StatMinusRects[1]))
            {
                DrawBorderedText("Scavenge : " + (players[CurrentPlayerNum].MyStatus.Skills.CalcScavenge(0)).ToString("n2") + " --> " + (players[CurrentPlayerNum].MyStatus.Skills.CalcScavenge(AddedPts[1])).ToString("n2"), (int)(0.0390625f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.7538888889f * ScreenManager.GraphicsDevice.Viewport.Height), Color.White, InconsolataFont);
                DrawBorderedText("Scavenge is the skill to search. The higher the Scavenge,", (int)(0.0390625f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.7938888889f * ScreenManager.GraphicsDevice.Viewport.Height), Color.White, InconsolataFont);
                DrawBorderedText("the more likely you are to get more quantity of items", (int)(0.0390625f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.8238888889f * ScreenManager.GraphicsDevice.Viewport.Height), Color.White, InconsolataFont);
            }
            else if (MouseRect.Intersects(new Rectangle((int)(0.68359375f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.34722222f * ScreenManager.GraphicsDevice.Viewport.Height), (int)(0.2109375f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.12083333f * ScreenManager.GraphicsDevice.Viewport.Height))) || MouseRect.Intersects(StatPlusRects[2]) || MouseRect.Intersects(StatMinusRects[2]))
            {
                DrawBorderedText("Lockpick : " + (players[CurrentPlayerNum].MyStatus.Skills.CalcLockpick(0)).ToString("n2") + " --> " + (players[CurrentPlayerNum].MyStatus.Skills.CalcLockpick(AddedPts[2])).ToString("n2"), (int)(0.0390625f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.7538888889f * ScreenManager.GraphicsDevice.Viewport.Height), Color.White, InconsolataFont);
                DrawBorderedText("Lockpick is the skill to open doors and chests. A higher lockpick level", (int)(0.0390625f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.7938888889f * ScreenManager.GraphicsDevice.Viewport.Height), Color.White, InconsolataFont);
                DrawBorderedText("means less chance of breaking lock(Disable lockpick) and easier to open", (int)(0.0390625f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.8238888889f * ScreenManager.GraphicsDevice.Viewport.Height), Color.White, InconsolataFont);
                DrawBorderedText("lock itself", (int)(0.0390625f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.8538888889f * ScreenManager.GraphicsDevice.Viewport.Height), Color.White, InconsolataFont);
            }
            else if (MouseRect.Intersects(new Rectangle((int)(0.68359375f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.49583333f * ScreenManager.GraphicsDevice.Viewport.Height), (int)(0.2109375f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.12083333f * ScreenManager.GraphicsDevice.Viewport.Height))) || MouseRect.Intersects(StatPlusRects[3]) || MouseRect.Intersects(StatMinusRects[3]))
            {
                DrawBorderedText("Barter : " + (players[CurrentPlayerNum].MyStatus.Skills.CalcBarter(0)).ToString("n2") + " --> " + (players[CurrentPlayerNum].MyStatus.Skills.CalcBarter(AddedPts[3])).ToString("n2"), (int)(0.0390625f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.7538888889f * ScreenManager.GraphicsDevice.Viewport.Height), Color.White, InconsolataFont);
                DrawBorderedText("Barter is the skill to negotiate. Higher barter means you get more value", (int)(0.0390625f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.7938888889f * ScreenManager.GraphicsDevice.Viewport.Height), Color.White, InconsolataFont);
                DrawBorderedText("from trading and having a better chance to escape combat by negotiation", (int)(0.0390625f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.8238888889f * ScreenManager.GraphicsDevice.Viewport.Height), Color.White, InconsolataFont);
            }
            else if (MouseRect.Intersects(new Rectangle((int)(0.68359375f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.64444444f * ScreenManager.GraphicsDevice.Viewport.Height), (int)(0.2109375f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.12083333f * ScreenManager.GraphicsDevice.Viewport.Height))) || MouseRect.Intersects(StatPlusRects[4]) || MouseRect.Intersects(StatMinusRects[4]))
            {
                DrawBorderedText("Heal : " + (players[CurrentPlayerNum].MyStatus.Skills.CalcHeal(0)).ToString("n2") + " --> " + (players[CurrentPlayerNum].MyStatus.Skills.CalcHeal(AddedPts[4])).ToString("n2"), (int)(0.0390625f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.7538888889f * ScreenManager.GraphicsDevice.Viewport.Height), Color.White, InconsolataFont);
                DrawBorderedText("Heal is the skill that affects how much % does a salve or potion heal.", (int)(0.0390625f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.7938888889f * ScreenManager.GraphicsDevice.Viewport.Height), Color.White, InconsolataFont);
                DrawBorderedText("Higher heal levels mean a great % healed", (int)(0.0390625f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.8238888889f * ScreenManager.GraphicsDevice.Viewport.Height), Color.White, InconsolataFont);
                DrawBorderedText("Note : Heal is also affected by Target's charisma", (int)(0.0390625f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.8538888889f * ScreenManager.GraphicsDevice.Viewport.Height), Color.White, InconsolataFont);
            }
            else
            {
                DrawBorderedText("Note : Adding 1 Growth Point in a Skill adds slightly more than 1 point", (int)(0.0390625f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.7638888889f * ScreenManager.GraphicsDevice.Viewport.Height), Color.White, InconsolataFont);
                DrawBorderedText("to the actual value. Actual Value is based on Character Base Stats", (int)(0.0390625f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.8038888889f * ScreenManager.GraphicsDevice.Viewport.Height), Color.White, InconsolataFont);
            }
            #endregion
            
            #region Draw Skills & Added values
            DrawBorderedText("Pts : " + (90 - TotalPts), (int)(0.5244375f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.097222222f * ScreenManager.GraphicsDevice.Viewport.Height), Color.White, InconsolataFont);
            DrawBorderedText(" Strength     : " + AddedPts[0], (int)(0.7159375f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.0980555f * ScreenManager.GraphicsDevice.Viewport.Height), Color.White, InconsolataFont);
            DrawBorderedText(" Dexterity    : " + AddedPts[1], (int)(0.7159375f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.2466667f * ScreenManager.GraphicsDevice.Viewport.Height), Color.White, InconsolataFont);
            DrawBorderedText(" Intelligence : " + AddedPts[2], (int)(0.7159375f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.3952778f * ScreenManager.GraphicsDevice.Viewport.Height), Color.White, InconsolataFont);
            DrawBorderedText(" Charisma     : " + AddedPts[3], (int)(0.7159375f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.54388889f * ScreenManager.GraphicsDevice.Viewport.Height), Color.White, InconsolataFont);
            DrawBorderedText(" Endurance    : " + AddedPts[4], (int)(0.7159375f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.6925f * ScreenManager.GraphicsDevice.Viewport.Height), Color.White, InconsolataFont);
            DrawBorderedText(" Agility      : " + AddedPts[5], (int)(0.7159375f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.6925f * ScreenManager.GraphicsDevice.Viewport.Height), Color.White, InconsolataFont);
            DrawBorderedText("Confirm", (int)(0.7659375f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.82255556f * ScreenManager.GraphicsDevice.Viewport.Height), Color.White, InconsolataFont);
            #endregion

            if (MouseRect.Intersects(ChangeUnitRectLeft))
            {
            }
            else if (MouseRect.Intersects(ChangeUnitRectRight))
            {
            }

            //  spriteBatch.Draw(players[0].Texture, new Vector2(70, 60) - new Vector2(0, 806) * 0.2f, null, Color.White, 0f, Vector2.Zero, 0.6f, SpriteEffects.None, 0f);
            spriteBatch.Draw(CancelTex, new Vector2((ScreenManager.GraphicsDevice.Viewport.Bounds.Right - CancelTex.Width / 2) - 3, 7 + CancelTex.Height / 2), null, Color.White, 0f, new Vector2(CancelTex.Width / 2, CancelTex.Height / 2), 0.7f, SpriteEffects.None, 0f);
            spriteBatch.DrawString(spritefont, "Mouse X : " + MouseRect.X + " Mouse Y : " + MouseRect.Y, new Vector2(50, 100), Color.White);
            spriteBatch.End();


            base.Draw(gameTime);
        }


        private void DrawModel(Model model, Matrix world, CameraObj camera)
        {
            Matrix[] transforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(transforms);

            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    //effect.SpecularColor = Color.WhiteSmoke.ToVector3();
                    ///effect.SpecularPower = 100.0f;
                    //effect.FogEnabled = true;
                    //effect.FogColor = Color.White.ToVector3();
                    //effect.FogStart = 999999.0f;
                    //effect.FogEnd = 1000000.0f;


                    effect.EnableDefaultLighting();
                    effect.DiffuseColor = new Vector3(0.7f, 0.7f, 0.7f);
                    effect.World = transforms[mesh.ParentBone.Index] * world;
                    // Use the matrices provided by the chase camera
                    effect.View = camera.view;
                    effect.Projection = camera.proj;
                }
                mesh.Draw();
            }
        }

        public void DrawBorderedText(string text, int PosX, int PosY, Color color, SpriteFont font)
        {
            spriteBatch.DrawString(font, text, new Vector2(PosX + 1, PosY + 1), Color.Black * 0.5f);
            spriteBatch.DrawString(font, text, new Vector2(PosX + 1, PosY - 1), Color.Black * 0.5f);
            spriteBatch.DrawString(font, text, new Vector2(PosX - 1, PosY + 1), Color.Black * 0.5f);
            spriteBatch.DrawString(font, text, new Vector2(PosX - 1, PosY - 1), Color.Black * 0.5f);
            spriteBatch.DrawString(font, text, new Vector2(PosX, PosY), color);

        }

        public void ResetPoints()
        {
            for (int i = 0; i < AddedPts.Length; i++)
            {
                AddedPts[i] = 0;
            }
            TotalPts = 0;
        }
    }
}

