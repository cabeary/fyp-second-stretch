﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using BrokenLands;

namespace BrokenLands
{
    class Map
    {

        //implement fragment sorting:might not be required
        //finalise the quest
        //implement metbefore bool into actors
        //overlays are being drawn in the drawlist. put them into the overlaylist

        public string mapName = null;
        public bool rerender = false;

        public MapCell[,] gridNodes;
        public Texture2D floorTexture;
        public FloorObject floorObject;
        //list of obstacles
        public List<Obstacle> obstacleList = new List<Obstacle>();
        public List<testObject> mapexitList = new List<testObject>();
        public List<testObject> debugList = new List<testObject>();
        //current character


        //actual values
        public List<Enemy> enemList = new List<Enemy>();
        public List<Player> playerList = new List<Player>();
        public List<Entity> drawList = new List<Entity>();
        public List<SceneObject> sceneList = new List<SceneObject>();
        public List<Actor> actorList = new List<Actor>();
        public List<DungeonAI> aiList = new List<DungeonAI>();
        public List<SceneObject> overlayList = new List<SceneObject>();
        public List<SceneObject> underlayList = new List<SceneObject>();
        public List<Vector2> sectionNodes = new List<Vector2>();
        public MapGenerator mapGen=null;
        int enemyCount = 0;
        public List<Entity> objectListInZone;



        //prerender  floor->(underlay)-> mid -> scene->(overlay) -> top
        Texture2D Scenelayer;
        Texture2D Floorlayer;
        Texture2D postProcessed;
        List<Entity> toplayer = new List<Entity>();
        List<Entity> midlayer = new List<Entity>();
        Vector3 anchor = Vector3.Zero;
        Vector3 target = Vector3.Zero;
        Effect layerEffect, scenelayerEffect,shadowEffect;

        //debug
        bool run = true;

        //battle arena thing
        Texture2D battletile;
        List<Entity> tiles = new List<Entity>();

        public void setShaders(Effect layer,Effect scene,Effect shadow) {
            layerEffect = layer;
            scenelayerEffect = scene;
            shadowEffect = shadow;
        }

        public void dynamicSort() {
            Pathfinder path = GameSession._pather;

            toplayer.Clear();
            midlayer.Clear();

            for (int i = 0; i < drawList.Count;i++ ) {
                if (!drawList[i].draw)
                    drawList.RemoveAt(i);
            }

            foreach(Entity e in drawList){
                if (path.CheckEntitiesInRadius(e.MyLocation, 2)) //checks area between front and bottom
                {
                    midlayer.Add(e);
                }

                else
                    toplayer.Add(e);
            }

            sortDraw(midlayer);
            sortDraw(toplayer);
        }

        public void prerender(SpriteBatch spritebatch, CameraObj camera, GraphicsDevice device)
        {
            //only repopulate the lists when necessary. MAYBE TODO(Not keeping track of the lists probs saves memory)

            float dist =  0;

            RenderTarget2D sceneTarget, floortarget;
            List<Entity> floorlayerList = new List<Entity>();
            List<Entity> scenelayerList = new List<Entity>();

            //floor layer
            foreach(SceneObject s in underlayList){
                if(camera.inView(s.MyLocation.getBox()))
                floorlayerList.Add(s);
            }
            foreach(Entity e in floorlayerList){
                e.UpdateLocation(device.Viewport,camera);
            }

            //capture the floor

            PresentationParameters pp = device.PresentationParameters;
            floortarget = new RenderTarget2D(device, pp.BackBufferWidth, pp.BackBufferHeight, true, device.DisplayMode.Format, DepthFormat.Depth24);

            device.SetRenderTarget(floortarget);
            device.Clear(Color.Black);
            spritebatch.Begin();
            foreach(Entity e in floorlayerList){
                e.Draw(spritebatch, device.Viewport, camera);
            }

           
            spritebatch.End();

            device.SetRenderTarget(null);
            Floorlayer = (Texture2D)floortarget;


           // scenelayer -> sceneobjects and actors
            foreach(Actor a in actorList){
                 dist=Vector3.Distance(a.MyLocation.getPositon(), camera.target);
                 if ((camera.inView(a.MyLocation.getBox()) || dist < 500))
                    scenelayerList.Add(a);
            }

            foreach(SceneObject s in sceneList){
                if (camera.inView(s.MyLocation.getBox()) || dist < 500)
                scenelayerList.Add(s);
            }

            foreach(Unit u in enemList){
                if (camera.inView(u.MyLocation.getBox()) || dist < 500)
                {
                    if(u.draw)
                    scenelayerList.Add(u);
                }
            }

            //foreach(Entity e in scenelayerList){
            //    e.UpdateLocation(device.Viewport,camera);
            //}

            sortDraw(scenelayerList);
            //capture the scene
            sceneTarget = new RenderTarget2D(device, (int)(pp.BackBufferWidth * 1), (int)(pp.BackBufferHeight * 1), false, device.DisplayMode.Format, DepthFormat.Depth24);
            
            device.SetRenderTarget(sceneTarget);
            device.Clear(Color.Black);
            spritebatch.Begin();
            foreach (Entity e in scenelayerList)
            {
                e.UpdateLocation(device.Viewport, camera);
                e.Draw(spritebatch, device.Viewport, camera);
            }


            foreach (SceneObject s in overlayList)
            {
                s.UpdateLocation(device.Viewport, camera);
                s.Draw(spritebatch, device.Viewport, camera);
            }

            spritebatch.End();

            device.SetRenderTarget(null);
            Scenelayer = (Texture2D)sceneTarget;
        }
         

        public bool enemInZone(List<List<MapCell>> zone)
        {
            foreach (List<MapCell> lmc in zone)
            {
                foreach (MapCell mc in lmc)
                {
                    if (mc.Ent != null)
                    {
                        if (mc.Ent.MyType == Entity.uType.Enemy)
                            return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Returns a list of entities in a particular zone
        /// </summary>
        /// <param name="zone"></param>
        /// <returns></returns>
        public List<Entity> objectsInZone(List<List<MapCell>> zone)
        {
            List<Entity> objects = new List<Entity>();
            foreach (List<MapCell> listmc in zone)
            {
                foreach (MapCell mc in listmc)
                {
                    if (mc.Ent != null)
                    {
                        objects.Add(mc.Ent);
                    }
                }
            }
            return objects;
        }
        /// <summary>
        /// Check if the mapcell is in any zone, true if it is
        /// </summary>
        /// <param name="mc"></param>
        /// <param name="zone">Returns the zone that the mapcell is in, if any</param>
        /// <returns></returns>
        public bool isInAnyZone(MapCell mc, out List<List<MapCell>> zone)
        {
            zone = getZone(mc);
            if (zone.Count > 0)
            {
                return true;
            }
            return false;

        }

        /// <summary>
        /// Check if the mapcell is in a particular zone
        /// </summary>
        /// <param name="mc"></param>
        /// <param name="zone"></param>
        /// <returns></returns>
        public bool isInZone(MapCell mc, List<List<MapCell>> zone)
        {
            if (
            (mc.Location.X >= zone[0][0].Location.X && mc.Location.X <= zone[zone.Count - 1][zone[0].Count - 1].Location.X) &&
            (mc.Location.Y >= zone[0][0].Location.Y && mc.Location.Y <= zone[zone.Count - 1][zone[0].Count - 1].Location.Y)
            )
            {
                return true;

            }
            return false;


        }
        /*
        public MapCell[,] getZone1(Player p)
        {
            MapCell[,] zone = new MapCell[0, 0];

            for (int i = 0; i < sectionNodes.Count; i += 2)
            {
                //find pair that the player is in
                if (
                    (p.MyLocation.Location.X >= sectionNodes[i].X && p.MyLocation.Location.X <= sectionNodes[i + 1].X) &&
                    (p.MyLocation.Location.Y >= sectionNodes[i].Y && p.MyLocation.Location.Y <= sectionNodes[i + 1].Y)
            for (int i = 0; i < sectionNodes.Count; i += 2)
            {
                //find pair that the player is in
                if (
                    (p.MyLocation.Location.X > sectionNodes[i].X && p.MyLocation.Location.X < sectionNodes[i + 1].X) &&
                    (p.MyLocation.Location.Y > sectionNodes[i].Y && p.MyLocation.Location.Y < sectionNodes[i + 1].Y)
                    )
                {
                    //get the length of the zone
                    Vector2 length = Vector2.Zero;
                    length.X = sectionNodes[i + 1].X - sectionNodes[i].X;
                    length.Y = sectionNodes[i + 1].Y - sectionNodes[i].Y;
                    zone = new MapCell[(int)length.X, (int)length.Y];
                    List<MapCell> cellList = new List<MapCell>();


                    for (int x = (int)sectionNodes[i].X; x < sectionNodes[i + 1].X; x++)
                    {
                        for (int z = (int)sectionNodes[i].Y; z < sectionNodes[i + 1].Y; z++)
                        {
                            cellList.Add(gridNodes[x, z]);
                        }
                    }

                    for (int x = 0; x < zone.GetLength(0); x++)
                    {
                        for (int z = 0; z < zone.GetLength(1); z++)
                        {
                            zone[x, z] = cellList[x * (zone.GetLength(0)) + z];
                        }
                    }

                    break;
                }
            }

            return zone;
        }*/

        public List<MapCell> drawZone() {
            //MapCell[,] zone = new MapCell[0, 0];
            List<MapCell> cellList = new List<MapCell>();
            for (int i = 0; i < sectionNodes.Count; i += 2)
            {



                    for (int x = (int)sectionNodes[i].X; x < sectionNodes[i + 1].X; x++)
                    {
                        for (int z = (int)sectionNodes[i].Y; z < sectionNodes[i + 1].Y; z++)
                        {
                            cellList.Add(gridNodes[x, z]);
                        }
                    //break;
                }
            }

            return cellList;
        }
        /// <summary>
        /// Returns the zone that the Player is in
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public List<List<MapCell>> getZone(Player p)
        {
            //MapCell[,] zone = new MapCell[0, 0];
            List<List<MapCell>> cellList = new List<List<MapCell>>();

            for (int i = 0; i < sectionNodes.Count; i += 2)
            {
                //find pair that the player is in
                if ((p.MyLocation.Location.X >= sectionNodes[i].X && p.MyLocation.Location.X <= sectionNodes[i + 1].X) &&
                    (p.MyLocation.Location.Y >= sectionNodes[i].Y && p.MyLocation.Location.Y <= sectionNodes[i + 1].Y)
                    )
                {
                    //get the length of the zone
                    //Vector2 length = Vector2.Zero;
                    //length.X = sectionNodes[i + 1].X - sectionNodes[i].X;
                    //length.Y = sectionNodes[i + 1].Y - sectionNodes[i].Y;
                    //zone = new MapCell[(int)length.X, (int)length.Y];
                    for (int x = (int)sectionNodes[i].X; x < sectionNodes[i + 1].X; x++)
                    {

                        List<MapCell> thisrow = new List<MapCell>();
                        for (int z = (int)sectionNodes[i].Y; z < sectionNodes[i + 1].Y; z++)
                        {
                            thisrow.Add(gridNodes[x, z]);
                        }
                        cellList.Add(thisrow);
                        //cellList.Add(gridNodes[x, z]);

                    }

                    //for (int x = 0; x < zone.GetLength(0); x++)
                    //{
                    //    for (int z = 0; z < zone.GetLength(1); z++)
                    //    {
                    //        zone[x, z] = cellList[x * (zone.GetLength(0)) + z];
                    //    }
                    //}

                    break;
                }
            }

            return cellList;
        }
        public List<List<MapCell>> getZone(MapCell mc)
        {
            //MapCell[,] zone = new MapCell[0, 0];
            List<List<MapCell>> cellList = new List<List<MapCell>>();

            for (int i = 0; i < sectionNodes.Count; i += 2)
            {
                //find pair that the player is in
                if ((mc.Location.X >= sectionNodes[i].X && mc.Location.X <= sectionNodes[i + 1].X) &&
                    (mc.Location.Y >= sectionNodes[i].Y && mc.Location.Y <= sectionNodes[i + 1].Y)
                    )
                {
                    //get the length of the zone
                    //Vector2 length = Vector2.Zero;
                    //length.X = sectionNodes[i + 1].X - sectionNodes[i].X;
                    //length.Y = sectionNodes[i + 1].Y - sectionNodes[i].Y;
                    //zone = new MapCell[(int)length.X, (int)length.Y];
                    for (int x = (int)sectionNodes[i].X; x < sectionNodes[i + 1].X; x++)
                    {

                        List<MapCell> thisrow = new List<MapCell>();
                        for (int z = (int)sectionNodes[i].Y; z < sectionNodes[i + 1].Y; z++)
                        {
                            thisrow.Add(gridNodes[x, z]);
                        }
                        cellList.Add(thisrow);
                        //cellList.Add(gridNodes[x, z]);

                    }

                    //for (int x = 0; x < zone.GetLength(0); x++)
                    //{
                    //    for (int z = 0; z < zone.GetLength(1); z++)
                    //    {
                    //        zone[x, z] = cellList[x * (zone.GetLength(0)) + z];
                    //    }
                    //}

                    break;
                }
            }

            return cellList;
        }

        public Vector2 getMousePosition()
        {
            Point mousePos = new Point(Mouse.GetState().X, Mouse.GetState().Y);
            return new Vector2(mousePos.X, mousePos.Y);
        }

        public Map(Texture2D defaultFloor,Texture2D battletile)
        {
            floorObject = new FloorObject(defaultFloor, 0);
            floorTexture = defaultFloor;
            this.battletile = battletile;
        }

        public void updateActor(MouseController controller)
        {

            for (int i = 0; i < actorList.Count; i++)
            {
                if (actorList[i] != null)
                    actorList[i].UpdateChat(controller);
            }
        }

        public void Update(GameTime gameTime)
        {
            System.Diagnostics.Debug.WriteLine("EnemyListmap:" +enemList.Count);

            if (enemyCount < enemList.Count)
                enemyCount = enemList.Count;

            //if (enemyCount != 0 && enemList.Count == 0)
            //{
            //    QuestReactor.reaction(new Action(10, 0), 0);//hard coded for now
            //}

            foreach (Unit u in enemList)
            {
                u.Update(gameTime);
            }

            foreach (Unit u in playerList)
            {
                u.Update(gameTime);
            }
            foreach (Actor a in actorList)
            {
                a.Update(gameTime);
            }
        }


        public void sortDraw(List<Entity> list) {

            list.Sort(delegate(Entity a, Entity b)
            {
                float sumA = -a.MyLocation.getPositon().X + a.MyLocation.getPositon().Z;
                float sumB = -b.MyLocation.getPositon().X + b.MyLocation.getPositon().Z;


                if (sumA < sumB)
                    return -1;
                else
                    return 0;
            });
        }

        public void debugDraw(SpriteBatch spritebatch, CameraObj camera, GraphicsDevice device)
        {

            //draw grid

            device.DepthStencilState = DepthStencilState.Default;
     

            for (int x = 0; x < gridNodes.GetLength(0); x++)
            {

                for (int z = 0; z < gridNodes.GetLength(1); z++)
                {
                    if (camera.inView(gridNodes[x, z].getBox()))
                    {
                        gridNodes[x, z].Draw(camera, device);
                    }
                }
            }



            // sort the enemies by depth
            enemList.Sort(delegate(Enemy a, Enemy b)
            {
                float sumA = -a.MyLocation.getPositon().X + a.MyLocation.getPositon().Z;
                float sumB = -b.MyLocation.getPositon().X + b.MyLocation.getPositon().Z;


                if (sumA < sumB)
                    return -1;
                else
                    return 0;



            });
            actorList.Sort(delegate(Actor a, Actor b)
            {
                float sumA = -a.MyLocation.getPositon().X + a.MyLocation.getPositon().Z;
                float sumB = -b.MyLocation.getPositon().X + b.MyLocation.getPositon().Z;


                if (sumA < sumB)
                    return -1;
                else
                    return 0;



            });


            sceneList.Sort(delegate(SceneObject a, SceneObject b)
            {
                float sumA = -a.MyLocation.getPositon().X + a.MyLocation.getPositon().Z;
                float sumB = -b.MyLocation.getPositon().X + b.MyLocation.getPositon().Z;


                if (sumA < sumB)
                    return -1;
                else
                    return 0;



            });



            spritebatch.Begin();


            if (GameSession.currentGameState == GameSession.GameState.Dungeon)
            {
                //draw players and enemy


                foreach (SceneObject s in underlayList)
                {
                    s.Draw(spritebatch, device.Viewport, camera);
                    s.UpdateLocation(device.Viewport, camera);
                }

                foreach (Unit t in enemList)
                {
                    t.Draw(spritebatch, device.Viewport, camera);
                    t.UpdateLocation(device.Viewport, camera);
                }

                foreach (Actor a in actorList)
                {
                    a.UpdateLocation(device.Viewport, camera);
                    a.Draw(spritebatch, device.Viewport, camera);
                }

                //draw debug
                foreach (testObject t in debugList)
                {
                    t.Draw(camera);
                }

                //draw scene

                foreach (SceneObject s in sceneList)
                {
                    s.UpdateLocation(device.Viewport, camera);

                    if (Vector2.Distance(s.getPosition(), getMousePosition()) < 125)
                    {
                        s.Draw(spritebatch, device.Viewport, camera, 0.3f);
                    }

                    else
                    {
                        s.Draw(spritebatch, device.Viewport, camera);

                    }
                }

                foreach (SceneObject s in overlayList)
                {
                    s.Draw(spritebatch, device.Viewport, camera);
                    s.UpdateLocation(device.Viewport, camera);
                }



            }
            else if (GameSession.currentGameState == GameSession.GameState.Combat)
            {
                List<Entity> list = objectsInZone(GameSession.combatZone);
                foreach (Entity e in list)
                {
                    e.UpdateLocation(device.Viewport, camera);
                    e.Draw(spritebatch, device.Viewport, camera);
                }
            }
            spritebatch.End();
        }


        private void DrawModel(Model model, Matrix world, CameraObj camera)
        {//16.0f
            world = Matrix.Identity * Matrix.CreateTranslation(Vector3.Zero) * Matrix.CreateScale(16);
            Matrix[] transforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(transforms);

            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.EnableDefaultLighting();
                    effect.Texture = floorTexture;
                    effect.DiffuseColor = new Vector3(0.7f, 0.7f, 0.7f);
                    effect.World = transforms[mesh.ParentBone.Index] * world;
                    effect.View = camera.view;
                    effect.Projection = camera.proj;
                }
                mesh.Draw();
            }
        }


        public void Draw(Model tile,SpriteBatch spritebatch, CameraObj camera, GraphicsDevice device, SpriteFont font) {
            //draw grid     ||   CellStraightLineCheck() from pathfinder (GameSession._pather)


            scenelayerEffect.Parameters["MousePosition"].SetValue(getMousePosition());
            scenelayerEffect.Parameters["Width"].SetValue(device.Viewport.Width);
            scenelayerEffect.Parameters["Height"].SetValue(device.Viewport.Height);

            shadowEffect.Parameters["PlayerPosition"].SetValue(getMousePosition());
            shadowEffect.Parameters["Width"].SetValue(device.Viewport.Height);
            shadowEffect.Parameters["Height"].SetValue(device.Viewport.Height);


            for (int i = 0; i < enemList.Count; i++)
            {
                if(!enemList[i].draw){
                    enemList.RemoveAt(i);
                }
            }

            if (camera.target != target || rerender)
            {
                prerender(spritebatch, camera, device);
                rerender = false;
            }

            target = camera.target;

            device.DepthStencilState = DepthStencilState.Default;
            


            //in combat only
            if (GameSession.currentGameState == GameSession.GameState.Combat)
            {

                DrawModel(tile, Matrix.Identity, camera);
                sortDraw(tiles);


                spritebatch.Begin();

                foreach(Entity e in tiles){
                    e.UpdateLocation(device.Viewport,camera);
                    e.Draw(spritebatch,device.Viewport,camera);
                }

                spritebatch.End();

                foreach (List<MapCell> lmc in GameSession.combatZone)
                {
                    foreach (MapCell mc in lmc)
                    {

                        if (tiles.Count != (lmc.Count * GameSession.combatZone.Count))
                        {
                            tiles.Add(new SceneObject(battletile,-1,mc));
                        }

                        if (true)//camera.inView(GameSession.combatZone[x, z].getBox()))
                        {
                            mc.Draw(camera, device);  //instance draw these
                        }
                    }

                }


                /*
            //-----debug-------
            if (run)
            {
                GameSession.combatZone = drawZone();
                run = false;
                }
                foreach (MapCell mc in GameSession.combatZone)
                {
                    if (true)//camera.inView(GameSession.combatZone[x, z].getBox()))
                    {
                        mc.Draw(camera, device);  //instance draw these
                    }
                }
            //-----------------
                */
                List<Entity> list = objectsInZone(GameSession.combatZone);
                foreach (Entity e in list)
                {
                    spritebatch.Begin();

                    e.UpdateLocation(device.Viewport, camera);
                    e.Draw(spritebatch, device.Viewport, camera);

                    spritebatch.End();
                }
            }

            else { // outside of combat

                //PresentationParameters pp = device.PresentationParameters;
                //RenderTarget2D post = new RenderTarget2D(device, pp.BackBufferWidth, pp.BackBufferHeight, true, device.DisplayMode.Format, DepthFormat.Depth24);
                tiles.Clear();

                dynamicSort();

                //device.SetRenderTarget(post);
                DrawModel(tile, Matrix.Identity, camera);
                spritebatch.Begin(0, BlendState.AlphaBlend, null, null, null, layerEffect);
                //draw list in a buffer to hold objects that has to be sorted dynamically
                //draw floor layer
                spritebatch.Draw(Floorlayer, Vector2.Zero, Color.White);

                //draw midlayer
                foreach(Entity e in midlayer){
                    e.UpdateLocation(device.Viewport,camera);
                    e.Draw(spritebatch,device.Viewport,camera);
                }

                //draw scenelayer
                spritebatch.End();

                spritebatch.Begin(0, BlendState.AlphaBlend, null, null, null, scenelayerEffect);
                spritebatch.Draw(Scenelayer,Vector2.Zero,Color.White);
                spritebatch.End();


                if (GameSession.currentlySelectedTile != null)
                    GameSession.currentlySelectedTile.Draw(camera, device);

                //draw top layer
                spritebatch.Begin();
                foreach(Entity e in toplayer){
                    if(camera.inView(e.MyLocation.getBox())){
                    e.UpdateLocation(device.Viewport,camera);
                    e.Draw(spritebatch,device.Viewport,camera);
                    }
                }

                
                spritebatch.End();

               // device.SetRenderTarget(null);

              //  postProcessed = (Texture2D)post;

                //spritebatch.Begin(0, BlendState.AlphaBlend, null, null, null, shadowEffect);
                //spritebatch.Begin();
                ////draw the post processed image
                //spritebatch.Draw(postProcessed,Vector2.Zero,Color.White);

                //spritebatch.End();

                spritebatch.Begin();

                foreach (Actor a in actorList)
                {
                    a.DrawChat(spritebatch, font);
                }

                spritebatch.End();
            }

            
        }


        public BoundingBox getTileBounds(Vector2 nodeDimension)
        {
            return gridNodes[(int)nodeDimension.X, (int)nodeDimension.Y].getBox();
        }

        public Vector3 getTilePosition(Vector2 nodeDimension)
        {
            return gridNodes[(int)nodeDimension.X, (int)nodeDimension.Y].getPositon();
        }


    }
}
