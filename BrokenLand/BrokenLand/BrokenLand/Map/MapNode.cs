﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace BrokenLands
{
    class MapNode
    {
        public Rectangle Location, InitialLocation;
        public Texture2D currentTex;
        public int id;
        public bool discoveredBool;
        public List<int> linkedNodes;
        public string nodeName,nodeDesc;
        public int mapInt;

        public MapNode(int id)
        {
            this.id = id;
            this.discoveredBool = false;
            linkedNodes = new List<int>();
        }
        public MapNode[] discoverNode(MapNode[] nodes, int index)
        {
            this.discoveredBool = true;
            foreach (int i in linkedNodes)
            {
                nodes[i].discoveredBool = true;
            }
            return nodes;
        }

        //public bool checkID(MapNode[] mapnodes)
        //{
        //    bool check = true;
        //    for (int i = 0; i < mapnodes.Length; i++)
        //    {
        //        for (int e = 0; e < mapnodes.Length; e++)
        //        {
        //            if (mapnodes[i] == mapnodes[e])
        //            {
        //                break;
        //            }
        //            else if (mapnodes[i].id == mapnodes[e].id)
        //            {
        //                check = false;
        //                return check;
        //            }

        //        }
        //    }
        //    return check;
        //}

        public void setInitLocation(Rectangle Loc)
        {
            Location = Loc;
            InitialLocation = Loc;
        }
        public void DrawName(SpriteBatch spriteBatch,SpriteFont spriteFont)
        {
            GameSession.GS.DrawBorderedText(spriteBatch, this.nodeName, Location.X-(int)(spriteFont.MeasureString(this.nodeName).X/2.5), Location.Y + 50, Color.White, spriteFont);
        }


    }
}
