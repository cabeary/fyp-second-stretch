﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace BrokenLands
{
    class Node
    {
        private Vector3 Position;
        Matrix world;
        public Model tile;
        public Model tile2;
       //public Model Model { get { return tile; } set { tile = value; } }
        BoundingBox box;
        float scale;
        //bool occupied; object: eg player/enemy/obstacle
        testObject test;
        public bool useTex = false;
        public bool selected = false;
        public Texture2D tex;
        public Texture2D modelOriginalTex;
        //debug
        bool draw= true;
        public Node() { }
        public Node(Vector3 position,Model model,float scale) {
            this.Position = position;
            tile = model;
            box = GenerateBounds.CreateBoundingBox(model, Position, scale);
            modelOriginalTex = ((BasicEffect)model.Meshes[0].Effects[0]).Texture;
            this.scale = scale;

           
        }

        public BoundingBox getBox() {
            return box;
        }

        public void Draw(CameraObj camera,GraphicsDevice device) {
            world = Matrix.CreateScale(scale) * Matrix.CreateTranslation(Position);
            if (draw)
            {
                //if (!useTex)
                DrawModel(tile, world, camera);
            }

               // else {
               //     DrawModel(tile2, world, camera);
               // }
                
            
            //FIXME: drawing works fine but sometimes crashes. creating the buffer each round kills performance.
            //not dedicating global var to it cos not required for base game
            //if (device != null)
            //{
            //    
            //BoundingBoxBuffers buffer = GenerateBounds.CreateBoundingBoxBuffers(box, device);
            
            //    BasicEffect lineEffect = new BasicEffect(device);
            //    lineEffect.LightingEnabled = false;
            //    lineEffect.TextureEnabled = false;
            //    lineEffect.VertexColorEnabled = true;
            //    GenerateBounds.DrawBoundingBox(buffer, lineEffect, device, camera.view, camera.proj);
            //}
                
            
        }

        public void Update() {
            //if mouse raycast intersects. color = green
        }

        private void DrawModel(Model model, Matrix world, CameraObj camera)
        {
            Matrix[] transforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(transforms);

            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.EnableDefaultLighting();
                    effect.DiffuseColor = new Vector3(0.7f, 0.7f, 0.7f);
                    effect.World = transforms[mesh.ParentBone.Index] * world;
                    // Use the matrices provided by the chase camera
                    effect.View = camera.view;
                    effect.Projection = camera.proj;
                    if (GameSession.currentlySelectedTile == this) {
                        effect.Texture = MapCell.selectedTile;
                        if (GameSession.currentlySelectedTile.Ent != null)
                        {
                            if (GameSession.currentlySelectedTile.Ent.MyType == Entity.uType.Actor)
                                effect.Texture = MapCell.selectedInteractibleTile;
                        }
                       
                            
                    }
                    else
                    if (useTex)
                    {
                       // effect.TextureEnabled = true;
                        effect.Texture = tex;
                    }
                    else {
                       // effect.TextureEnabled = true;
                        effect.Texture = modelOriginalTex;
                    }
                }
                mesh.Draw();
            }
        }

        public Vector3 getPositon() {
            return Position;
        }

        public Vector2 getUnprojectedPositon(Viewport viewport, CameraObj  camera) {
             Vector3 projected = viewport.Project(Position, camera.proj, camera.view, Matrix.Identity);
            Vector2 unprojected = Vector2.Zero;
            unprojected.X = projected.X;
            unprojected.Y = projected.Y;
            return unprojected;
        }

        //Debugging tools
        public void setDraw(bool draw) {
            this.draw = draw;
        }

        public void setTile(Model model) {
            tile = model;
        }





    };
};
