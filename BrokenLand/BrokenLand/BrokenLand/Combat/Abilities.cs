﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrokenLands
{
    class Abilities
    {
        public List<PlayerAbility> AbilityList = new List<PlayerAbility>();
        
        /// <summary>
        /// Initialization of Abilities class with default abilities
        /// </summary>
        public Abilities()
        {
            PlayerAbility newAbility;
            newAbility = new PlayerAbility(0, "Stand Behind Me!", "Moves up to an ally, pushing him/her one unit back and taking the original spot", true);
            AbilityList.Add(newAbility);
            newAbility = new PlayerAbility(1, "Dash", "Gains X extra AP this turn but loses next. Amount depends on ability level.", false);
            AbilityList.Add(newAbility);
            newAbility = new PlayerAbility(2, "Trick", "Disable target from acting this turn.", true);
            AbilityList.Add(newAbility);
            newAbility = new PlayerAbility(3, "Lightfooted", "Able to get past enemy units with X AP. Amount depends on ability level.", true);
            AbilityList.Add(newAbility);
            newAbility = new PlayerAbility(4, "Help!", "Allies within X range move 1 block towards self. Amount depends on ability level.", false);
            AbilityList.Add(newAbility);
            newAbility = new PlayerAbility(5, "Helping Hand", "Gains X extra AP based on each nearby ally. Range depends on ability level.", false);
            AbilityList.Add(newAbility);
            newAbility = new PlayerAbility(6, "Distraction", "Enemies withi n X range move 1 block towards self. Amount depends on ability level.", false);
            AbilityList.Add(newAbility);
            newAbility = new PlayerAbility(7, "Shove", "Pushes target X units away, or until it hits a wall. Amount depends on ability level.", true);
            AbilityList.Add(newAbility);
            newAbility = new PlayerAbility(8, "Telekinesis", "Move a unit that is X units away. Amount depends on ability level.", true);
            AbilityList.Add(newAbility);
            newAbility = new PlayerAbility(9, "Inspire", "Use 1 AP to grant allies in range 1 AP. Amount depends on ability level.", false);
            AbilityList.Add(newAbility);
            newAbility = new PlayerAbility(10, "Taunt", "Consumes AP to increase threat meter of self", false);
            AbilityList.Add(newAbility);
            newAbility = new PlayerAbility(11, "Pulse", "Reduce all enemies' AP by 1 in a radius. Range depends on ability level.", false);
            AbilityList.Add(newAbility);
            newAbility = new PlayerAbility(12, "Cocoon", "Consumes all remaining AP to heal X health per AP. Amount depends on ability level.", false);
            AbilityList.Add(newAbility);
        }

        /// <summary>
        /// Returns Ability. Parse in AbilityID to identify which Ability
        /// </summary>
        /// <param name="AbilityID"></param>
        /// <returns></returns>
        public PlayerAbility ReturnPlayerAbility(int AbilityID)
        {
            if (AbilityID < AbilityList.Count)
                return AbilityList[AbilityID];
            else
                return null;
        }

        /// <summary>
        /// Levels up ability. Use when character hits certain level marks
        /// </summary>
        /// <param name="AbilityID"></param>
        public void LevelupAbility(int AbilityID)
        {
            if (AbilityID < AbilityList.Count) //If ability exists
            {
                if (AbilityList[AbilityID].Level < 5)//Max level
                    AbilityList[AbilityID].LevelUpAbility();
            }
        }

        /// <summary>
        /// Uses ability. Based off character's ID (Given ability)
        /// </summary>
        /// <param name="AbilityID"></param>
        /// <param name="user"></param>
        /// <param name="target"></param>
        public void UseAbility(Player user, Unit target)
        {
            int AbilityID = user.MyStatus.AbilityID;
            if (!AbilityList[AbilityID].Used)// || AbilityList[AbilityID].Used)
            {
                AbilityList[AbilityID].Used = true;
                int distance = 1;
                Pathfinder.Direction newDirection = Pathfinder.Direction.None;
                int AbilityLevel = AbilityList[AbilityID].Level;
                if (target != null)
                {
                    newDirection = GameSession._pather.checkInLineDirection(user.MyLocation, target.MyLocation, out distance);
                    //distance = GameSession._pather.getManhattanDistance(user.MyLocation, target.MyLocation);
                }
                switch (AbilityID)
                {
                    case 0:
                        //Works, but needs further testing (With actual player)
                        if (distance <= AbilityLevel + 2)
                        {
                            if (target.MyType == user.MyType)
                            {
                                //No need to check if exit bounds
                                MapCell toCheckNode;
                                if (newDirection == Pathfinder.Direction.Left)
                                    toCheckNode = GameSession.map.gridNodes[(int)target.MyLocation.Location.X - 1, (int)target.MyLocation.Location.Y];
                                else if (newDirection == Pathfinder.Direction.Right)
                                    toCheckNode = GameSession.map.gridNodes[(int)target.MyLocation.Location.X + 1, (int)target.MyLocation.Location.Y];
                                else if (newDirection == Pathfinder.Direction.Up)
                                    toCheckNode = GameSession.map.gridNodes[(int)target.MyLocation.Location.X, (int)target.MyLocation.Location.Y - 1];
                                else// if (newDirection == Pathfinder.Direction.Down)
                                    toCheckNode = GameSession.map.gridNodes[(int)target.MyLocation.Location.X, (int)target.MyLocation.Location.Y + 1];

                                //Check if node is empty (Available to do action)
                                if (toCheckNode.Ent == null)
                                {
                                    /*/Getting path for user
                                    List<MapCell> tempUserPath = new List<MapCell>();
                                    MapCell tempNode = target.MyLocation;
                                    for (int i = 0; i < 10; i++)
                                    {
                                        tempUserPath.Add(tempNode);
                                        if (newDirection == Pathfinder.Direction.Left)
                                            tempNode = GameSession.map.gridNodes[(int)tempNode.Location.X + 1, (int)tempNode.Location.Y];
                                        else if (newDirection == Pathfinder.Direction.Right)
                                            tempNode = GameSession.map.gridNodes[(int)tempNode.Location.X - 1, (int)tempNode.Location.Y];
                                        else if (newDirection == Pathfinder.Direction.Up)
                                            tempNode = GameSession.map.gridNodes[(int)tempNode.Location.X, (int)tempNode.Location.Y + 1];
                                        else// if (newDirection == Pathfinder.Direction.Down)
                                            tempNode = GameSession.map.gridNodes[(int)tempNode.Location.X, (int)tempNode.Location.Y - 1];
                                        if (tempNode == user.MyLocation)
                                        {
                                            break;
                                        }
                                    }*/

                                    //Getting path for target
                                    //List<MapCell> tempTargetPath = new List<MapCell>();
                                    //tempTargetPath.Add(toCheckNode);

                                    CombatActions userAction = new CombatActions(user, CombatActions.ActionType.MOVE, target.MyLocation);
                                    CombatActions targetAction = new CombatActions(target, CombatActions.ActionType.MOVE, toCheckNode);
                                    GameSession._combat.PerformAction(targetAction);
                                    GameSession._combat.PerformAction(userAction);
                                    user.MyStatus.CurrentAP--;
                                    //user.MyLocation.Location = target.MyLocation.Location;
                                    //target.MyLocation.Location = toCheckNode.Location;
                                }
                                else
                                {
                                    //Write that you can't do that
                                }
                            }
                        }
                        break;
                    case 1:
                        //Gains additional AP now, but loses ability to act next turn
                        user.MyStatus.CurrentAP += AbilityLevel;
                        user.Disabled = true;
                        break;
                    case 2:
                        //Disable target from acting next turn
                        if (distance < AbilityLevel + 2)
                        {
                            if (target != null)
                            {
                                target.MyStatus.CurrentAP = 0;
                                //target.Disabled = true;
                                user.MyStatus.CurrentAP--;
                            }
                        }
                        break;
                    case 3:
                        //Able to run through unit
                        //Note: It is possible to use more than your max AP in this case, but you cannot act afterwards
                        if (distance <= user.MyStatus.CurrentAP)
                        {
                            MapCell toCheckNode;
                            if (newDirection == Pathfinder.Direction.Left)
                                toCheckNode = GameSession.map.gridNodes[(int)target.MyLocation.Location.X - 1, (int)target.MyLocation.Location.Y];
                            else if (newDirection == Pathfinder.Direction.Right)
                                toCheckNode = GameSession.map.gridNodes[(int)target.MyLocation.Location.X + 1, (int)target.MyLocation.Location.Y];
                            else if (newDirection == Pathfinder.Direction.Up)
                                toCheckNode = GameSession.map.gridNodes[(int)target.MyLocation.Location.X, (int)target.MyLocation.Location.Y - 1];
                            else// if (newDirection == Pathfinder.Direction.Down)
                                toCheckNode = GameSession.map.gridNodes[(int)target.MyLocation.Location.X, (int)target.MyLocation.Location.Y + 1];

                            if (toCheckNode.Ent == null)
                            {
                                List<MapCell> tempPath = new List<MapCell>();
                                MapCell tempNode = toCheckNode;

                                /*for (int i = 0; i < 10; i++)
                                {
                                    tempPath.Add(toCheckNode);
                                    if (newDirection == Pathfinder.Direction.Left)
                                        tempNode = GameSession.map.gridNodes[(int)tempNode.Location.X + 1, (int)tempNode.Location.Y];
                                    else if (newDirection == Pathfinder.Direction.Right)
                                        tempNode = GameSession.map.gridNodes[(int)tempNode.Location.X - 1, (int)tempNode.Location.Y];
                                    else if (newDirection == Pathfinder.Direction.Up)
                                        tempNode = GameSession.map.gridNodes[(int)tempNode.Location.X, (int)tempNode.Location.Y + 1];
                                    else// if (newDirection == Pathfinder.Direction.Down)
                                        tempNode = GameSession.map.gridNodes[(int)tempNode.Location.X, (int)tempNode.Location.Y - 1];
                                    if (tempNode == user.MyLocation)
                                    {
                                        break;
                                    }
                                }*/

                                CombatActions newAction = new CombatActions(user, CombatActions.ActionType.MOVE, toCheckNode);
                                GameSession._combat.PerformAction(newAction);
                                user.MyStatus.CurrentAP -= (5 - AbilityLevel) + distance; //+ Actual movement cost
                            }
                        }
                        break;
                    case 4:
                        //Works, but has problem where does not immediately update
                        //Calls allies towards self
                        user.MyStatus.CurrentAP--;
                        foreach (Unit u in GameSession.map.playerList)
                        {
                            int distanceFromUnit;
                            Pathfinder.Direction newerDirection = GameSession._pather.checkInLineDirection(u.MyLocation, user.MyLocation, out distanceFromUnit);
                            if (distanceFromUnit <= AbilityLevel + 1 && distanceFromUnit > 1)
                            {
                                MapCell moveToNode;
                                if (newerDirection == Pathfinder.Direction.Left)
                                    moveToNode = GameSession.map.gridNodes[(int)u.MyLocation.Location.X - 1, (int)u.MyLocation.Location.Y];
                                else if (newerDirection == Pathfinder.Direction.Right)
                                    moveToNode = GameSession.map.gridNodes[(int)u.MyLocation.Location.X + 1, (int)u.MyLocation.Location.Y];
                                else if (newerDirection == Pathfinder.Direction.Up)
                                    moveToNode = GameSession.map.gridNodes[(int)u.MyLocation.Location.X, (int)u.MyLocation.Location.Y - 1];
                                else //if (newerDirection == Pathfinder.Direction.Left)
                                    moveToNode = GameSession.map.gridNodes[(int)u.MyLocation.Location.X, (int)u.MyLocation.Location.Y + 1];
                                if (moveToNode.Ent == null)
                                {
                                    CombatActions newAction = new CombatActions(u, CombatActions.ActionType.MOVE, moveToNode);
                                    GameSession._combat.PerformAction(newAction);
                                }
                            }
                        }
                        break;
                    case 5:
                        //Gains AP based on nearby ally units
                        user.MyStatus.CurrentAP--; //To offset using self to add AP
                        foreach (Unit u in GameSession.map.playerList)
                        {
                            int newDistance = GameSession._pather.getManhattanDistance(u.MyLocation, user.MyLocation);
                            if (newDistance < (AbilityLevel + 1) * 2)
                                user.MyStatus.CurrentAP++;
                        }
                        break;
                    case 6:
                        //Makes enemy move towards unit
                        user.MyStatus.CurrentAP--;
                        foreach (Unit u in GameSession.map.enemList)
                        {
                            int distanceFromUnit;
                            Pathfinder.Direction newerDirection = GameSession._pather.checkInLineDirection(u.MyLocation, user.MyLocation, out distanceFromUnit);
                            if (distanceFromUnit <= AbilityLevel + 100 && distanceFromUnit > 1)
                            {
                                MapCell moveToNode;
                                if (newerDirection == Pathfinder.Direction.Left)
                                    moveToNode = GameSession.map.gridNodes[(int)u.MyLocation.Location.X - 1, (int)u.MyLocation.Location.Y];
                                else if (newerDirection == Pathfinder.Direction.Right)
                                    moveToNode = GameSession.map.gridNodes[(int)u.MyLocation.Location.X + 1, (int)u.MyLocation.Location.Y];
                                else if (newerDirection == Pathfinder.Direction.Up)
                                    moveToNode = GameSession.map.gridNodes[(int)u.MyLocation.Location.X, (int)u.MyLocation.Location.Y - 1];
                                else //if (newerDirection == Pathfinder.Direction.Left)
                                    moveToNode = GameSession.map.gridNodes[(int)u.MyLocation.Location.X, (int)u.MyLocation.Location.Y + 1];
                                if (moveToNode.Ent == null)
                                {
                                    CombatActions newAction = new CombatActions(u, CombatActions.ActionType.MOVE, moveToNode);
                                    GameSession._combat.PerformAction(newAction);
                                }
                            }
                        }
                        break;
                    case 7:
                        //Shoves enemy away
                        if (distance == 1)
                        {
                            //Check to see if they do not exit map/go past units
                            MapCell toCheckNode;
                            if (newDirection == Pathfinder.Direction.Right)
                            {
                                int locationX;
                                if (target.MyLocation.Location.X + AbilityLevel >= GameSession.map.gridNodes.GetLength(0) - 1)
                                {
                                    locationX = GameSession.map.gridNodes.GetLength(0) - 1;
                                }
                                else
                                {
                                    locationX = (int)target.MyLocation.Location.X + AbilityLevel;
                                }
                                toCheckNode = GameSession.map.gridNodes[locationX, (int)target.MyLocation.Location.Y];
                            }
                            else if (newDirection == Pathfinder.Direction.Left)
                            {
                                int locationX;
                                if (target.MyLocation.Location.X - AbilityLevel <= 0)
                                {
                                    locationX = 0;
                                }
                                else
                                {
                                    locationX = (int)target.MyLocation.Location.X - AbilityLevel;
                                }
                                toCheckNode = GameSession.map.gridNodes[locationX, (int)target.MyLocation.Location.Y];
                            }
                            else if (newDirection == Pathfinder.Direction.Up)
                            {
                                int locationY;
                                if (target.MyLocation.Location.Y - AbilityLevel <= 0)
                                {
                                    locationY = 0;
                                }
                                else
                                {
                                    locationY = (int)target.MyLocation.Location.Y - AbilityLevel;
                                }
                                toCheckNode = GameSession.map.gridNodes[(int)target.MyLocation.Location.X, locationY];
                            }
                            else// if (newDirection == Pathfinder.Direction.Down)
                            {
                                int locationY;
                                if (target.MyLocation.Location.Y + AbilityLevel >= GameSession.map.gridNodes.GetLength(1) - 1)
                                {
                                    locationY = (int)GameSession.map.gridNodes.GetLength(1) - 1;
                                }
                                else
                                {
                                    locationY = (int)target.MyLocation.Location.Y + AbilityLevel;
                                }
                                toCheckNode = GameSession.map.gridNodes[(int)target.MyLocation.Location.X, locationY];
                            }

                            MapCell tempNode = target.MyLocation;
                            //Keeps falling back until hits a wall/unit or max distance
                            while (tempNode.Ent == null || tempNode.Ent == target)
                            {
                                if (tempNode == toCheckNode)
                                    break;
                                else
                                {
                                    if (newDirection == Pathfinder.Direction.Up)
                                        tempNode = GameSession.map.gridNodes[(int)tempNode.Location.X, (int)tempNode.Location.Y - 1];
                                    else if (newDirection == Pathfinder.Direction.Down)
                                        tempNode = GameSession.map.gridNodes[(int)tempNode.Location.X, (int)tempNode.Location.Y + 1];
                                    else if (newDirection == Pathfinder.Direction.Left)
                                        tempNode = GameSession.map.gridNodes[(int)tempNode.Location.X - 1, (int)tempNode.Location.Y];
                                    else// if (newDirection == Pathfinder.Direction.Right)
                                        tempNode = GameSession.map.gridNodes[(int)tempNode.Location.X + 1, (int)tempNode.Location.Y];
                                }
                            }

                            if (tempNode != target.MyLocation)
                            {
                                List<MapCell> tempPath = new List<MapCell>();
                                MapCell pathNode = tempNode;

                                for (int i = 0; i < 10; i++)
                                {
                                    tempPath.Add(toCheckNode);
                                    if (newDirection == Pathfinder.Direction.Left)
                                        pathNode = GameSession.map.gridNodes[(int)pathNode.Location.X + 1, (int)pathNode.Location.Y];
                                    else if (newDirection == Pathfinder.Direction.Right)
                                        pathNode = GameSession.map.gridNodes[(int)pathNode.Location.X - 1, (int)pathNode.Location.Y];
                                    else if (newDirection == Pathfinder.Direction.Up)
                                        pathNode = GameSession.map.gridNodes[(int)pathNode.Location.X, (int)pathNode.Location.Y + 1];
                                    else// if (newDirection == Pathfinder.Direction.Down)
                                        pathNode = GameSession.map.gridNodes[(int)pathNode.Location.X, (int)pathNode.Location.Y - 1];
                                    if (pathNode == user.MyLocation)
                                    {
                                        break;
                                    }
                                }

                                CombatActions newAction = new CombatActions(target, CombatActions.ActionType.MOVE, pathNode);
                                GameSession._combat.PerformAction(newAction);
                                //target.MyLocation = tempNode;
                                user.MyStatus.CurrentAP--;
                            }
                        }
                        break;
                    case 8:
                        //Telekinesis to push items 1 unit away
                        if (distance <= AbilityLevel + 2)
                        {
                            //Check to see if they do not exit map/go past units
                            MapCell toCheckNode;
                            if (newDirection == Pathfinder.Direction.Left)
                                toCheckNode = GameSession.map.gridNodes[(int)target.MyLocation.Location.X - 1, (int)target.MyLocation.Location.Y];
                            else if (newDirection == Pathfinder.Direction.Right)
                                toCheckNode = GameSession.map.gridNodes[(int)target.MyLocation.Location.X + 1, (int)target.MyLocation.Location.Y];
                            else if (newDirection == Pathfinder.Direction.Up)
                                toCheckNode = GameSession.map.gridNodes[(int)target.MyLocation.Location.X, (int)target.MyLocation.Location.Y - 1];
                            else// if (newDirection == Pathfinder.Direction.Down)
                                toCheckNode = GameSession.map.gridNodes[(int)target.MyLocation.Location.X, (int)target.MyLocation.Location.Y + 1];

                            if (toCheckNode.Ent == null)
                            {
                                CombatActions newAction = new CombatActions(target, CombatActions.ActionType.MOVE, toCheckNode);
                                GameSession._combat.PerformAction(newAction);
                                user.MyStatus.CurrentAP--;
                            }
                        }
                        break;
                    case 9:
                        //Rallies all units
                        user.MyStatus.CurrentAP--;
                        foreach (Unit u in GameSession.map.playerList)
                        {
                            if (GameSession._pather.getManhattanDistance(u.MyLocation, user.MyLocation) < AbilityLevel + 2)
                                u.MyStatus.CurrentAP += 2;
                        }
                        break;
                    case 10:
                        //Increases threat meter so target will more likely focus this target
                        break;
                    case 11:
                        //Reduce all units in an area by 1AP
                        user.MyStatus.CurrentAP--;
                        foreach (Unit u in GameSession.map.enemList)
                        {
                            if (GameSession._pather.getManhattanDistance(u.MyLocation, user.MyLocation) < AbilityLevel + 2)
                                u.MyStatus.CurrentAP--;
                        }
                        break;
                    case 12:
                        user.health += user.MyStatus.CurrentAP * 3 + 2;
                        if (user.health > user.MyStatus.calcHP())
                            user.health = user.MyStatus.calcHP();
                        user.MyStatus.CurrentAP = 0;
                        break;
                }
            }
        }

        /// <summary>
        /// Inputs ability ID to check if ability is already used
        /// </summary>
        /// <param name="AbilityID"></param>
        /// <returns></returns>
        public bool CheckUsed(int AbilityID)
        {
            return AbilityList[AbilityID].Used;
        }

        /// <summary>
        /// Inputs character to check if ability is already used
        /// </summary>
        /// <param name="AbilityID"></param>
        /// <returns></returns>
        public bool CheckUsed(Unit unit)
        {
            if (unit.MyStatus.AbilityID != null)
                return AbilityList[unit.MyStatus.AbilityID].Used;
            else
                return true;
        }

        /// <summary>
        /// Resets all ability Usage so all abilities can be re-used
        /// </summary>
        public void ResetAbilityUse()
        {
            for (int i = 0; i < AbilityList.Count; i++)
            {
                AbilityList[i].Used = false;
            }
        }

        /// <summary>
        /// Resets specific Ability use so ability can be reused
        /// </summary>
        /// <param name="AbilityID"></param>
        public void ResetAbilityUse(int AbilityID)
        {
            AbilityList[AbilityID].Used = false;
        }

        /// <summary>
        /// Resets specific Ability use so ability can be reused
        /// </summary>
        /// <param name="Unit unit"></param>
        public void ResetAbilityUse(Unit unit)
        {
            if (unit.MyStatus.AbilityID != null)
                AbilityList[unit.MyStatus.AbilityID].Used = false;
        }
    }
}
