﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrokenLands
{
    class PlayerAbility
    {
        int CharacterID;
        String AbilityTitle;
        String AbilityDesc;
        bool AbilityUsed;
        int AbilityLevel;
        bool isTargettedAbility;

        public int ID
        {
            get { return CharacterID; }
            set { CharacterID = value; }
        }
        public String Title
        {
            get { return AbilityTitle; }
            set { AbilityTitle = value; }
        }
        public String Desc
        {
            get { return AbilityDesc; }
            set { AbilityDesc = value; }
        }
        public bool Used
        {
            get { return AbilityUsed; }
            set { AbilityUsed = value; }
        }
        public int Level
        {
            get { return AbilityLevel; }
            set { AbilityLevel = value; }
        }
        public bool Targetable
        {
            get { return isTargettedAbility; }
            set { isTargettedAbility = value; }
        }

        /// <summary>
        /// Default Constructor
        /// </summary>
        public PlayerAbility()
        {
            CharacterID = -1;
            AbilityTitle = "default";
            AbilityDesc = "default desc";
            AbilityUsed = false;
            AbilityLevel = 1;
        }

        /// <summary>
        /// Constructor that takes in ID, Title and Desc
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="Title"></param>
        /// <param name="Desc"></param>
        public PlayerAbility(int ID, String Title, String Desc, bool targetable)
        {
            CharacterID = ID;
            AbilityTitle = Title;
            AbilityDesc = Desc;
            Targetable = targetable;
            AbilityUsed = false;
            AbilityLevel = 1;
        }

        //Unused
        /// <summary>
        /// Returns range of ability
        /// </summary>
        /// <returns></returns>
        /*public int GetAbilityRange()
        {
            return AbilityLevel;
        }*/

        /// <summary>
        /// Increases level. Max cap at 5
        /// </summary>
        public void LevelUpAbility()
        {
            if (AbilityLevel <  5)
                AbilityLevel++;
        }

    }
}
