﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace BrokenLands
{
    class CombatEngine
    {

        public CombatEngine(GameSession gs)
        {
            this.gs = gs;
        }

        GameSession gs;
        Dictionary<CombatActions.ActionType, int> actionRangeDict = new Dictionary<CombatActions.ActionType, int>();
        //public bool isUnitPerformingAction = false;

        private void SetRangeDictionary(Unit unit)
        {
            actionRangeDict.Add(CombatActions.ActionType.MOVE, unit.MyStatus.CurrentAP);
            actionRangeDict.Add(CombatActions.ActionType.ATTACK1, unit.MyStatus.Range);
            actionRangeDict.Add(CombatActions.ActionType.ATTACK2, unit.MyStatus.Range);
            actionRangeDict.Add(CombatActions.ActionType.BLOCK, 10000);
            actionRangeDict.Add(CombatActions.ActionType.RALLY, 10000);
            actionRangeDict.Add(CombatActions.ActionType.IDLE, 10000);
        }

        public int getRange(CombatActions.ActionType a)
        {
            int _RANGE;
            actionRangeDict.TryGetValue(a, out _RANGE);
            return _RANGE;
        }

        public List<List<CombatActions>> GetCombatActionsByLocation(Unit unit, MapCell loc)
        {

            List<List<CombatActions>> acts = new List<List<CombatActions>>();
            int i = 0;
            if (unit.MyStatus.CurrentAP > 0)
            {
                foreach (CombatActions.ActionType a in Enum.GetValues(typeof(CombatActions.ActionType)))
                {

                    acts.Add(new List<CombatActions>());
                    switch (a)
                    {
                        case CombatActions.ActionType.MOVE:
                            List<MapCell> movable = GameSession._pather.getPossibleMoveLocations(unit);
                            foreach (MapCell mc in movable)
                            {
                                acts[i].Add(new CombatActions(unit, a, mc));
                            }

                            break;
                        case CombatActions.ActionType.ATTACK1:
                            List<MapCell> attackTargets = GameSession._pather.getPossibleStationaryAttackLocations(unit);
                            foreach (MapCell mc in attackTargets)
                            {
                                acts[i].Add(new CombatActions(unit, a, mc));
                            }
                            break;
                        case CombatActions.ActionType.BLOCK:

                            foreach (MapCell mc in GameSession._pather.getNeighbours(loc))
                            {
                                acts[i].Add(new CombatActions(unit, a, mc));
                            }

                            break;
                        case CombatActions.ActionType.RALLY:
                            if (true)
                            {
                                acts[i].Add(new CombatActions(unit, a, unit.MyLocation));
                            }
                            break;
                        case CombatActions.ActionType.IDLE:
                            acts[i].Add(new CombatActions(unit, a, unit.MyLocation));
                            break;
                    }

                    i++;

                }
            }
            else {
                //System.Diagnostics.Debug.WriteLine("WHY ARE YOU CALLING THIS!!?!??!?!");
            }

            return acts;
        }
        /*
        public void PerformMeleeAttackAction(Entity a,Entity b) {
            System.Diagnostics.Debug.WriteLine(a._NAME + " slashes " + b._NAME);
            b.health -= (a.attack - b.defence);
        
        }
        public void PerformRangedAttackAction(Entity a, Entity b) {
            System.Diagnostics.Debug.WriteLine(a._NAME + " shoots at " + b._NAME);
            b.health -= (a.attack - b.defence/2);
        }

        */
        public void PerformAction(CombatActions act, List<MapCell> path, int costOfTravel) {

            act.Unit.isPerformingAction = true;
            act.Unit.MyStatus.CurrentAP -= costOfTravel;//_pathfinder.CalcPathCost(_pathfinder.getPath(act.Unit.MyLocation, act.Target));
            //act.Unit.MyLocation.Ent = null;
            //act.Target.Ent = act.Unit;
            act.Unit.SetPath(path, true);
          
        }
        public float calcHitChance(float hitRate, float evade) {
            return ((hitRate / 100) * (1 - (evade / 100))) * 100;
        
        }
        List<MapCell> path = new List<MapCell>();
        public void PerformAction(CombatActions act)
        {
            act.Unit.isPerformingAction = true;

            if (act.Target == null) {
               // int costOfTravel;
                //path = GameSession._pather.getPath(act.Unit.MyLocation, act.Target, out costOfTravel);
                try
                {

                    act.Unit.MyStatus.CurrentAP -= GameSession._pather.CalcPathCost(act.path);
                }
                catch (Exception e) {
                   // gs.game1.ScreenManager.console.WriteLine(e+"");
                    //Console.WriteLine(e); 
                }
                act.Unit.SetPath(act.path, true);
                return;
            }
            //System.Diagnostics.Debug.WriteLine("STARTPERF: " + GameSession.sw.Elapsed.TotalSeconds);
          
            
            switch (act.Action)
            {
                case CombatActions.ActionType.MOVE:
                    //System.Diagnostics.Debug.WriteLine(act.Unit._NAME + act.Action.ToString() + " TO " + act.Target.Location);
                    
                    int costOfTravel;
                    path = GameSession._pather.getPath(act.Unit.MyLocation, act.Target, out costOfTravel);
            

                    act.Unit.MyStatus.CurrentAP -= costOfTravel;
                    act.Unit.stopMove = false;
                    act.Unit.SetPath(path, true);
                    break;
                case CombatActions.ActionType.ATTACK1:
                case CombatActions.ActionType.ATTACK2:
                    bool leftHandedAtk = false;
                    
                    if (act.Action == CombatActions.ActionType.ATTACK1)
                    {
                        leftHandedAtk = true;
                        act.Unit.MyStatus.CurrentAP -= act.Unit.MyStatus.leftWeapon.apCost;
                    }
                    else {
                        leftHandedAtk = false;
                        act.Unit.MyStatus.CurrentAP -= act.Unit.MyStatus.rightWeapon.apCost;
                    }
                    act.Unit.state = Entity.AnimationState.Attacking;
                    //act.Unit.MyStatus.leftWeapon.tex;
                    //act.Unit.MyStatus.
                    //System.Diagnostics.Debug.WriteLine(act.Unit._NAME + " " + act.Action.ToString() + " " + act.Target.Ent._NAME);
                    if (act.Unit.MyType == Entity.uType.Player)
                    {
                     //  GameSession.gameAudioEngine.playSound("GunFire");
                    }
                    else {
                    //    GameSession.gameAudioEngine.playSound("GorillaRoar");
                    }
                    Random random = new Random();
                    bool miss = false;
                    
                    int tempRand = random.Next(0, 100);
                    float hitChance = act.Unit.MyStatus.calcAccuracy();
                   
                    float dodgeChance = ((Unit)(act.Target.Ent)).MyStatus.calcEvade();


                    if (tempRand > calcHitChance(hitChance,dodgeChance)) {
                        miss = true;
                    }


                    if (!miss)
                    {
                        int hitCount;
                        int damage = (int)(act.Unit.MyStatus.calcMeleeAttack(out hitCount, leftHandedAtk)  * ( ((Unit)(act.Target.Ent)).MyStatus.calcDefence() * 3 / 100));
                        
                        act.Target.Ent.health -=  ((int)damage * hitCount);
                        act.Target.Ent.state = Entity.AnimationState.Flinch;
                        if (hitCount > 1)
                        {
                            gs.uiNumbers.Add(new UINumbers(2
                                , "-" + (int)damage + " x " + hitCount, Color.Red, (Unit)act.Target.Ent));
                        }
                        else {
                            gs.uiNumbers.Add(new UINumbers(2
                                    , "-" + (int)damage, Color.Red, (Unit)act.Target.Ent));
                        }
                        ((Unit)act.Target.Ent).CheckHealth();
                    }
                    else {
                        gs.uiNumbers.Add(new UINumbers(2
                            , "MISSED!", Color.Red, (Unit)act.Target.Ent));
                    
                    }
                    
                   
                    
                    act.Unit.isPerformingAction = false;
                    
                    break;
                case CombatActions.ActionType.BLOCK:
                    act.Unit.MyStatus.CurrentAP -= act.Cost;
                    act.Unit.isPerformingAction = false;
                    break;
                case CombatActions.ActionType.RALLY:
                    act.Unit.MyStatus.CurrentAP -= act.Cost;
                    act.Unit.isPerformingAction = false;
                    break;
                case CombatActions.ActionType.IDLE:
                    act.Unit.MyStatus.CurrentAP -= act.Cost;
                    act.Unit.isPerformingAction = false;
                    break;


            }
        }

    }
}
