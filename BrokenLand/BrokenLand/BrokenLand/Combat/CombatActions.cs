﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
namespace BrokenLands
{
    class CombatActions
    {
        //unit performing the action
        Unit unit;
        //target of action, for example ATTACK 0,1
        MapCell target;
        public List<MapCell> path;
        //actual action
        ActionType action;
        int cost;
        
        public int Cost { get { return getCost(this.action); } set { cost = value; } }
        public ActionType Action { get { return action; } set { action = value; } }
        public MapCell Target { get { return target; } set { target = value; } }
        public Unit Unit { get { return unit; } set { unit = value; } }

        Dictionary<ActionType, int> actionCostDict = new Dictionary<ActionType, int>();
        private void SetDictionary(Unit unit)
        {
            actionCostDict.Add(ActionType.MOVE, 1);
            if (unit.equipList["LeftWeapon"] != null)
            {
                actionCostDict.Add(ActionType.ATTACK1, ((Weapon)unit.equipList["LeftWeapon"]).apCost);
            }
            if (unit.equipList["RightWeapon"] != null)
            {
                actionCostDict.Add(ActionType.ATTACK2, ((Weapon)unit.equipList["RightWeapon"]).apCost);
            }
            actionCostDict.Add(ActionType.IDLE, 10);
            //actionCostDict.Add(ActionType.BLOCK, 10);
            //actionCostDict.Add(ActionType.RALLY, 10);
            

        }



        public int getCost(ActionType a)
        {
            int _COST;
            actionCostDict.TryGetValue(a, out _COST);
            return _COST;
        }

        

        public enum ActionType
        {
            MOVE = 0,
            ATTACK1,
            ATTACK2,
            BLOCK,
            RALLY,
            IDLE

        }

        /// <summary>
        /// Actions carried out by characters
        /// </summary>
        /// <param name="target">Target of action</param>
        /// <param name="act">Type of action</param>
        public CombatActions(Unit unit, ActionType act, MapCell target)
        {
            SetDictionary(unit);
            this.unit = unit;
            this.target = target;
            this.action = act;
            actionCostDict.TryGetValue(act, out cost);

            
        }

        public CombatActions(Unit unit, ActionType act, List<MapCell> path)
        {
            SetDictionary(unit);
            this.unit = unit;
            this.path = path;
            this.action = act;
            actionCostDict.TryGetValue(act, out cost);


        }

        public void PrintAction(){
            string message = "";

            message += action.ToString() ;
            message += "\nName: " + unit._NAME;
            message += ", \nTargetlocation: " + target.Location;
            if (target.Ent != null)
            {
                message += "\nTarget: " + target.Ent._NAME;
            }
            message += "\nCost: " + cost;
            System.Diagnostics.Debug.WriteLine(message);
        }
        /*
        public void PerformAction()
        {

            switch (action)
            {
                case CombatActions.ActionType.MOVE:
                    unit.MyLocation = target;
                    unit.MyStatus.currentAP;
                    break;
                case CombatActions.ActionType.ATTACK:
                    break;
                case CombatActions.ActionType.BLOCK:
                    break;
                case CombatActions.ActionType.RALLY:
                    break;
                case CombatActions.ActionType.IDLE:
                    break;


            }}
        */



    }
}
