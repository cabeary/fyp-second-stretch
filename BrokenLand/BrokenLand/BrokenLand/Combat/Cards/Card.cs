﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace BrokenLands
{
    class Card
    {
        String CardTitle;
        String CardDesc;
        bool isEventCard; //True for Event, False for Condition;
        Texture2D CardTex;
        
        List<List<MapCell>> Zone;

        //If Event
        //0 for No effect, 1 for Enemy invade (Add new monsters to dungeon), 2 for NPC taking shleter (Trade)
        //If Condition
        //0 for No condition, 1 for speed, 2 for damage minimalization, 3 for specific weapon types
        int CardEffectType;
        int CardSeverity; //10 for max severity, 1 for min
        Random random = new Random();

        public String Title
        {
            get { return CardTitle; }
            set { CardTitle = value; }
        }
        public String Desc
        {
            get { return CardDesc; }
            set { CardDesc = value; }
        }
        public bool isEvent
        {
            get { return isEventCard; }
            set { isEventCard = value; }
        }
        public int EffectType
        {
            get { return CardEffectType; }
            set { CardEffectType = value; }
        }
        public int Severity
        {
            get { return CardSeverity; }
            set { CardSeverity = value; }
        }
        public List<List<MapCell>> SpawnZone
        {
            get { return Zone; }
        }

        /// <summary>
        /// Constructor to generate default class
        /// </summary>
        public Card()
        {
            CardTitle = "default";
            CardDesc = "default desc";
            isEventCard = true;
            //CardTex = GameSession.GS.apeframe;
            CardEffectType = 0;
            CardSeverity = 0;
        }
        public Card(bool isEvent, int Type, int Severity)
        {
            //CardTex = GameSession.GS.apeframe;
            if (isEvent)
            {
                isEventCard = true;
                if (Type == 0)
                {
                    Title = "Silence into the night";
                    Desc = "Things are tense, but peaceful. You figure you got one night off.";
                    //Severity = 0;
                    //No effect
                }
                else if (Type == 1)
                {
                    if (Severity == 0)
                    {
                        Title = "Sounds in the bush";
                        Desc = "People were around here. Were. Now, you don't know where they are";
                        //No Effect
                    }
                    else if (Severity < 4)
                    {
                        Title = "Scouting party";
                        Desc = "Enemy raiders have sent a scouting party to check in the surrounding area. Be careful.";
                    }
                    else if (Severity < 8)
                    {
                        Title = "Skirmish";
                        Desc = "Enemy raiders have sent a sizeable amount of units to rob and steal. Beware.";
                    }
                    else if (Severity < 10)
                    {
                        Title = "Invasion!";
                        Desc = "Enemy raiders have sent their full force upon your party! Fight or flight!";
                    }
                    else
                    {
                        Title = "Hostage";
                        Desc = "Raiders have attacked this party, and only one is left. Can you save him/her in time?";
                    }
                }
                /*else if (Type == 2)
                {
                    if (Severity == 0)
                    {
                        Title = "Sounds in the bush";
                        Desc = "People were around here. Were. Now, you don't know where they are";
                        //No Effect
                    }
                    else if (Severity < 4)
                    {
                        Title = "Wanderer";
                        Desc = "A wanderer has been spotted in the dungeon. He may be able to help us, if we provide something he might want.";
                    }
                    else if (Severity < 8)
                    {
                        Title = "Party";
                        Desc = "A small group has been spotted in the dungeon. They are wary, but if you don't mean harm, they can assist you.";
                    }
                    else if (Severity < 10)
                    {
                        Title = "Company";
                        Desc = "Survivors! They might be willing to trade. After all, in this new world, you need all the help you get";
                    }
                    else
                    {
                        Title = "A new hope";
                        Desc = "Someone has been waiting for a group like yours. If you convince him/her, you may have an extra pair of helping hands.";
                    }
                }*/
                else //if (Type == 3)
                {
                    if (Severity == 0)
                    {
                        Title = "Dud traps";
                        Desc = "There are traps around - but nothing that will hinder you.";
                    }
                    else if (Severity < 4)
                    {
                        Title = "Touch and go";
                        Desc = "You might not want to stay. There might be something here you do not want to touch.";
                    }
                    else if (Severity < 8)
                    {
                        Title = "Thread lightly";
                        Desc = "You suspect traps. Mainly due to the pool of acid, or maybe the explosion marks over there.";
                    }
                    else if (Severity < 10)
                    {
                        Title = "Minefield";
                        Desc = "You might want to step away. Very far away.";
                    }
                    else
                    {
                        Title = "Thin ice";
                        Desc = "Someone is here - but so are many, many traps.";
                    }
                }
            }
            else
            {
                isEventCard = false;
                if (Type == 0)
                {
                    Title = "Just another day";
                    Desc = "Another day of scavenging and looking.";
                }
                else if (Type == 1)
                {
                    if (Severity == 0)
                    {
                        Title = "Pace";
                        Desc = "Time is not of the essence. You'll be fine";
                    }
                    else if (Severity < 4)
                    {
                        Title = "Steady";
                        Desc = "Slow and steady...Actually, remove the slow bit. You'll need to be quicker.";
                    }
                    else if (Severity < 8)
                    {
                        Title = "Swift";
                        Desc = "Time is of the essence. You'll have to hurry.";
                    }
                    else
                    {
                        Title = "Timed";
                        Desc = "Staying a second longer might prove fatal. Faster!";
                    }
                }
                else if (Type == 2)
                {
                    if (Severity < 4)
                    {
                        Title = "Damage Sustained";
                        Desc = "It is hard to avoid all those bullets, but you might as well try.";
                    }
                    else if (Severity < 8)
                    {
                        Title = "Damage Mitigation";
                        Desc = "You really want to avoid getting hit. Much. At all.";
                    }
                    else
                    {
                        Title = "No Harm Be Done";
                        Desc = "At least, that is what you hope would happen. This is going to be tough to scrape out.";
                    }
                }
                else //if (Type == 3)
                {
                    if (Severity == 0)
                    {
                        Title = "Anything goes";
                        Desc = "Any weapon, any armor. Anything goes, really.";
                    }
                    else if (Severity < 4)
                    {
                        Title = "Silence of the lambs";
                        Desc = "And monkeys and apes and all those monstrosity. No guns, or they would come rushing onto you.";
                    }
                    else if (Severity < 8)
                    {
                        Title = "Ammo conservation";
                        Desc = "A bullet into their brain might be satisfying, but you getting eaten alive the next day isn't. Save up and hit them.";
                    }
                    else
                    {
                        Title = "Let them come";
                        Desc = "Those big guys? You probably don't want to have a direct fight. Stay ranged, stay safe.";
                    }
                }
            }

            CardTitle = Title;
            CardDesc = Desc;
            CardEffectType = Type;
            CardSeverity = Severity;
        }
        public Card(String title, String desc, bool isEvent, int Type, int Severity)
        {
            CardTitle = title;
            CardDesc = desc;
            isEventCard = isEvent;
            CardEffectType = Type;
            CardSeverity = Severity;
        }

        /// <summary>
        /// Prints values of card
        /// </summary>
        public void PrintValues()
        {
            Console.WriteLine(CardTitle + "\n" + CardDesc + "\nType: " + CardEffectType + "\nSeverity: " + CardSeverity);
        }

        /// <summary>
        /// Activates cards ability
        /// </summary>
        public void Activate()
        {
            try
            {
                Zone = GameSession.map.getZone(GameSession.players[0].MyLocation);
            }
            catch
            {
                Console.WriteLine("Error loading zone");
                return;
            }
            if (isEvent)
            {
                GameSession.GS.CardCondition = -1;
                if (EffectType == 0)
                {
                    //Nothing happens
                }
                else if (EffectType == 1) //Extra enemy units
                {
                    if (Severity == 0)
                    {
                        //Too low severity, nothing happens
                    }
                    else
                    {
                        float enemyCount = GameSession.map.enemList.Count * Severity / 15.0f;
                        if (enemyCount < 1 && enemyCount > 0)
                        {
                            enemyCount = 1;
                        }

                        SpawnEnem(enemyCount, Zone);

                        if (Severity == 10) //Spawn recruitable unit
                        {
                            //SpawnPlayer();
                        }
                    }
                }/*
                else if (EffectType == 2) //Extra neutral units
                {
                    SpawnPlayer();
                }*/
                else //if (EffectType == 3) //Spawn obstacle
                {
                    int MapSize = Zone.Count * Zone[0].Count;//GameSession.map.gridNodes.GetLength(0) * GameSession.map.gridNodes.GetLength(1);
                    int SpawnCount = (int)(Math.Sqrt(MapSize / 10)) * Severity;
                    if (SpawnCount > 25)
                    {
                        SpawnCount = 25;
                    }
                    Console.WriteLine("Obstacles spawned : " + SpawnCount);

                    for (int i = 0; i < SpawnCount; i++)
                    {
                        MapCell spawnLoc = GetSpawnLocation(Zone);

                        if (GameSession.map.gridNodes[(int)spawnLoc.Location.X, (int)spawnLoc.Location.Y].Ent == null)
                        {
                            bool isDamage;
                            int isDamageInt = random.Next(0, 2);
                            if (isDamageInt == 0)
                                isDamage = false;
                            else
                                isDamage = true;
                            int cost = random.Next(0, 10);

                            Obstacle newObstacle = new Obstacle(GameSession.GS.yellowtile, spawnLoc, 0, cost, isDamage, "newObs");
                            GameSession.GS.addObstacle(newObstacle, newObstacle.MyLocation);
                        }
                        else
                        {
                            i--;
                        }
                    }
                }
            }
            else
            {
                if (EffectType == 0)
                {
                    //Nothing happens
                }
                else if (EffectType == 1)
                {
                    //Time based condition
                    if (Severity > 0)
                    {
                        float TimeGiven = (15 - Severity) * 2.5f;
                        GameSession.GS.CardCondition = (int)TimeGiven;
                    }
                }
                else// if (EffectType == 2)
                {
                    //Damage-mitigation based condition
                    float MaxChanceDamageTaken = 0;
                    MaxChanceDamageTaken = ((15 - Severity) * 1.5f);
                    GameSession.GS.CardCondition = (int)MaxChanceDamageTaken;
                }
                /*else // if (EffectType == 3)
                {
                    //Range based condition
                    float newRange = (11 - Severity) / 2;
                    if (newRange < 1)
                        newRange = 1;
                    foreach (Player playerUnit in GameSession.players)
                    {
                        if (playerUnit.MyStatus.Range > newRange)
                            playerUnit.MyStatus.Range = (int)newRange;
                    }
                }*/
            }
        }

        /// <summary>
        /// Spawns Player unit
        /// </summary>
        public void SpawnPlayer()
        {
            Player newPlayer = new Player(10, 15, 14, 20, 11, 15, GetSpawnLocation(GameSession.map.enemList[0].MyLocation.Location), GameSession.players[0].Texture, 0, "Recruit");
            //newPlayer.MyStatus.Range = 1;
            newPlayer.MyStatus.AbilityID = 1;
            GameSession.GS.addPlayer(newPlayer, newPlayer.MyLocation);
            Console.WriteLine("Player spawn at " + newPlayer.MyLocation.Location);
        }

        public void SpawnEnem(float Count, List<List<MapCell>> Zone)
        {
            for (int i = 0; i < Count; i++)
            {
                Enemy newEnemy = new Enemy(random.Next(7, 10), random.Next(7, 10), random.Next(7, 10), random.Next(7, 10), random.Next(7, 10), random.Next(7, 10), GetSpawnLocation(Zone), GameSession.GS.enemies[0].Texture, 0, "EventSpawn", 0);
                newEnemy.Size = GameSession.map.enemList[0].Size;
                GameSession.GS.addEnemy(newEnemy, newEnemy.MyLocation);
            }
        }

        /// <summary>
        /// Finds a suitable, non-occupied block to spawn in
        /// </summary>
        /// <returns></returns>
        private MapCell GetSpawnLocation(List<List<MapCell>> CurrentZone)
        {
            MapCell spawnLoc = null;

            while (spawnLoc == null || spawnLoc.Ent != null)
            {
                int x = random.Next(0, CurrentZone.Count - ((int)GameSession.map.enemList[0].Size.X - 1));//GameSession.map.gridNodes.GetLength(0));
                int y = random.Next(0, CurrentZone[0].Count - ((int)GameSession.map.enemList[0].Size.Y - 1));//GameSession.map.gridNodes.GetLength(1));

                spawnLoc = CurrentZone[x][y];

                //Check for size overlap
                if (GameSession.map.enemList[0].Size != new Vector2(1, 1))
                {
                    for (int i = 0; i < GameSession.map.enemList[0].Size.X; i++)
                    {
                        for (int j = 0; j < GameSession.map.enemList[0].Size.Y; j++)
                        {
                            if (CurrentZone[x + i][y + j].Ent != null)
                            {
                                spawnLoc = null;
                                break;
                            }
                        }
                        //If changed to no suitable unit
                        if (spawnLoc == null)
                            break;
                    }
                }
            }

            return spawnLoc;
        }

        /// <summary>
        /// Finds a suitable, non-occupied block to spawn in close to the original start
        /// </summary>
        /// <returns></returns>
        private MapCell GetSpawnLocation(Vector2 startPoint)
        {
            MapCell spawnLoc = GameSession.map.gridNodes[(int)startPoint.X, (int)startPoint.Y];
            while (spawnLoc.Ent != null)
            {
                int randomAddX = random.Next(-2, 2);
                int randomAddY = random.Next(-2, 2);
                spawnLoc = GameSession.map.gridNodes[randomAddX + (int)spawnLoc.Location.X, randomAddY + (int)spawnLoc.Location.Y];
            }

            return spawnLoc;
        }

        public void Draw(SpriteBatch spriteBatch, SpriteFont font, Rectangle MouseRect)
        {
            //if (MouseRect.Intersects())
            //{
                spriteBatch.Draw(GameSession.GS.CardTex, new Rectangle((int)(GameSession.GS.game1.ScreenManager.GraphicsDevice.Viewport.Width * 0.02f), (int)(GameSession.GS.game1.ScreenManager.GraphicsDevice.Viewport.Height * 0.24f), (int)(GameSession.GS.game1.ScreenManager.GraphicsDevice.Viewport.Width * 0.235f), (int)(GameSession.GS.game1.ScreenManager.GraphicsDevice.Viewport.Height * 0.45f)), Color.White);
                spriteBatch.DrawString(GameSession.GS.InconsolataFont, GameSession.GS.CurrentCard.Title, new Vector2((int)(GameSession.GS.game1.ScreenManager.GraphicsDevice.Viewport.Width * 0.1275f - (GameSession.GS.InconsolataFont.MeasureString(GameSession.GS.CurrentCard.Title).X / 2)), (int)(GameSession.GS.game1.ScreenManager.GraphicsDevice.Viewport.Height * 0.28f)), Color.White);
                spriteBatch.Draw(GameSession.GS.apeframe, new Rectangle((int)((GameSession.GS.game1.ScreenManager.GraphicsDevice.Viewport.Width * 0.1275f) - (GameSession.GS.game1.ScreenManager.GraphicsDevice.Viewport.Height * 0.065f)), (int)(GameSession.GS.game1.ScreenManager.GraphicsDevice.Viewport.Height * 0.36f), (int)(GameSession.GS.game1.ScreenManager.GraphicsDevice.Viewport.Height * 0.13f), (int)(GameSession.GS.game1.ScreenManager.GraphicsDevice.Viewport.Height * 0.13f)), Color.White);
            //}
            String infoText = "";
            if (isEvent)
                infoText += "Event";
            else
                infoText += "Condition";
            infoText += "\nType: " + CardEffectType + "\nSeverity: " + CardSeverity;
            if (GameSession.GS.CardCondition >= 0)
                infoText += "\nTurns Left: " + GameSession.GS.CardCondition;
             
            if (MouseRect.Intersects(new Rectangle((int)(GameSession.GS.game1.ScreenManager.GraphicsDevice.Viewport.Width * 0.02f), (int)(GameSession.GS.game1.ScreenManager.GraphicsDevice.Viewport.Height * 0.24f), (int)(GameSession.GS.game1.ScreenManager.GraphicsDevice.Viewport.Width * 0.235f), (int)(GameSession.GS.game1.ScreenManager.GraphicsDevice.Viewport.Height * 0.45f))))
            {
                infoText = GameSession.GS.StringTrim(CardDesc, 18);
            }

            spriteBatch.DrawString(font, infoText, new Vector2(0.06f * GameSession.GS.game1.ScreenManager.GraphicsDevice.Viewport.Width, 0.5f * GameSession.GS.game1.ScreenManager.GraphicsDevice.Viewport.Height), Color.White);
        }
    }
}
