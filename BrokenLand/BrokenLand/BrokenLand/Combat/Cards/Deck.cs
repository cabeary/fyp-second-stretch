﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrokenLands
{
    class Deck
    {
        List<Card> DeckList;
        int currentCardID;
        Random randomGen = new Random();

        /// <summary>
        /// Constructor for deck that puts in a random set of 30 cards
        /// </summary>
        public Deck()
        {
            DeckList = new List<Card>();
            currentCardID = 0;
            for (int i = 0; i < 30; i++)
            {
                bool isEvent = false;//For Events/Conditions
                int boolVal = randomGen.Next(0, 2);
                if (boolVal == 0)
                    isEvent = true;
                int Type = randomGen.Next(0, 3);
                int Severity = randomGen.Next(0, 11);

                isEvent = true;
                //Type = 1;
                //Severity = 1;

                Card newCard = new Card(isEvent, Type, Severity);
                DeckList.Add(newCard);
            }
        }

        /// <summary>
        /// Constructor for deck that puts in a random set of cards. Specify card amount
        /// </summary>
        public Deck(int cardNum)
        {
            DeckList = new List<Card>();
            currentCardID = 0;
            for (int i = 0; i < cardNum; i++)
            {
                bool isTrue = false;
                int boolVal = randomGen.Next(0, 2);
                if (boolVal == 0)
                    isTrue = true;
                int Type = randomGen.Next(0, 3);
                int Severity = randomGen.Next(0, 11);

                Card newCard = new Card(isTrue, Type, Severity);
                DeckList.Add(newCard);
            }
        }

        /// <summary>
        /// Retrieves a card 
        /// </summary>
        /// <returns></returns>
        public Card GetCard()
        {
            return DeckList[currentCardID];
        }

        /// <summary>
        /// Retrieves a card and move to next card (Does not actually delete old card
        /// </summary>
        /// <returns></returns>
        public Card RemoveCard()
        {
            Card cardToRemove = DeckList[currentCardID];
            currentCardID++;
            if (currentCardID >= DeckList.Count)
                currentCardID = 0;
            return cardToRemove;
        }

        /// <summary>
        /// Prints value of entire decklist
        /// </summary>
        public void PrintValues()
        {
            for (int i = 0; i < DeckList.Count; i++)
            {
                DeckList[i].PrintValues();
                Console.WriteLine("\n");
            }
        }

        /// <summary>
        /// Reorders cards using the Fisher-Yates shuffle
        /// </summary>
        public void ShuffleDeck()
        {
            int cardCount = DeckList.Count;
            while (cardCount > 1)
            {
                cardCount--;
                int randomValue = randomGen.Next(cardCount + 1);
                Card tempCard = DeckList[randomValue];
                DeckList[randomValue] = DeckList[cardCount];
                DeckList[cardCount] = tempCard;
            }

            /*List<Card> newDeckList = new List<Card>();
            List<bool> isTaken = new List<bool>();

            for (int i = 0; i < DeckList.Count; i++)
            {
                isTaken.Add(false);
            }

            for (int i = 0; i < DeckList.Count; i++)
            {
                int nextCardToAdd = randomGen.Next(0, DeckList.Count);
                if (!isTaken[i]) //If card is not taken yet
                {
                    newDeckList.Add(DeckList[nextCardToAdd]);
                    isTaken[i] = true;
                }
                else
                {
                    i--;
                }
            }*/
        }
    }
}
