using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using GameStateManagement;
using BrokenLands;


namespace BrokenLands
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class MainGameEntry : Game
    {
        GraphicsDeviceManager graphics;

        ContentManager content;
        SpriteBatch spriteBatch;
        SpriteFont spriteFont;
        GameSession gs;
        ScreenManager screenManager;
        ScreenFactory screenFactory;
        KeyboardState oldkeystate;

        public MainGameEntry()
            : base()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
<<<<<<< HEAD

            graphics.PreferredBackBufferWidth = 1920;//1920 ;//1280;
            graphics.PreferredBackBufferHeight = 1080;// 1080;// 720;
            graphics.IsFullScreen = false;

            TargetElapsedTime = TimeSpan.FromTicks(333333);
            IntPtr hWnd = this.Window.Handle;
            var control = System.Windows.Forms.Control.FromHandle(hWnd);
            var form = control.FindForm();
            form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;

=======
            graphics.PreferredBackBufferWidth = 1280;
            graphics.PreferredBackBufferHeight = 720;
            //graphics.PreferredBackBufferWidth = 1920 ;//1280;
            //graphics.PreferredBackBufferHeight = 1080;// 720;
            //graphics.IsFullScreen = true;
>>>>>>> list_of_list_implementation

            TargetElapsedTime = TimeSpan.FromTicks(333333);

            gs = GameSession.GS;
 

            // Create the screen factory and add it to the Services
            screenFactory = new ScreenFactory();
            Services.AddService(typeof(IScreenFactory), screenFactory);
            this.IsFixedTimeStep = false;
            // Create the screen manager component.
            screenManager = new ScreenManager(this);
            Components.Add(screenManager);

            AddInitialScreens();

        }

        private void AddInitialScreens()
        {
            // Activate the first screens.
            //screenManager.AddScreen(new BackgroundScreen(), null);
            //screenManager.AddScreen(new WorldMapScreen(), null);
            screenManager.AddScreen(new MainMenuScreen(), null);
            //screenManager.AddScreen(new Game1(), null);
        }
        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization code here
            if (content == null)
                content = new ContentManager(screenManager.Game.Services, "Content");

            spriteBatch = new SpriteBatch(GraphicsDevice);
            spriteFont = content.Load<SpriteFont>("Times12");

            Texture2D tex = content.Load<Texture2D>("Gunbowman");



            base.Initialize();
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
           // MouseController.MouseUpdate();
            // TODO: Add your update code here
            if (screenManager.input.CurrentKeyboardStates[0].IsKeyDown(Keys.D1))
            {
                if (oldkeystate.IsKeyUp(Keys.D2))
                {
                    screenManager.RemoveScreen(screenManager.GetScreens()[0]);
                    screenManager.AddScreen(new WorldMapScreen(), null);

                }
                
            }
            if (screenManager.input.CurrentKeyboardStates[0].IsKeyDown(Keys.D2))
            {

                if (oldkeystate.IsKeyUp(Keys.D2))
                {
                    screenManager.RemoveScreen(screenManager.GetScreens()[0]);
                    GameSession.currentGameState = GameSession.GameState.Dungeon;
                    screenManager.AddScreen(new Game1(), null);

                    
                }
            }

            oldkeystate = screenManager.input.CurrentKeyboardStates[0];
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        protected override void Draw(GameTime gameTime)
        {
            //graphics.GraphicsDevice.Clear(Color.White);
            if (spriteBatch != null)
            {
                spriteBatch.Begin();
                spriteBatch.DrawString(spriteFont, "Press 1 for World Map    Press 2 for Battle Arena", new Vector2(50, 50), Color.Black);
                spriteBatch.End();
            }
                // The real drawing happens inside the screen manager component.
            base.Draw(gameTime);
        }

    }
}
