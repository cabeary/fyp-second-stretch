using Microsoft.Xna.Framework;

namespace Supernova.Particles2D.Modifiers.Movement.Gravity
{
    /// <summary>
    /// Modifier which will pull particles towards a gravitational point
    /// </summary>
    public class GravityPoint : IModifier
    {
        public void Update(float particleAge, double totalMilliseconds, double elapsedSeconds, Particle2D particle)
        {
            Vector3 distance = new Vector3(Vector3.Subtract(Position, particle.Position).X, Vector3.Subtract(Position, particle.Position).Y, Vector3.Subtract(Position, particle.Position).Z);

            if (distance.LengthSquared() < Radius * Radius)
            {
                Vector3 force = Vector3.Normalize(distance);
                force = Vector3.Multiply(force, Strength);
                force = Vector3.Multiply(force, (float) elapsedSeconds);

                particle.Affect(ref force);
            }
        }

        /// <summary>
        /// Gets or sets the center of the gravity point
        /// </summary>
        public Vector3 Position { get; set; }

        /// <summary>
        /// Gets or sets the radius of influence
        /// </summary>
        public float Radius { get; set; }

        /// <summary>
        /// Gets or sets the strength of the pull of gravity
        /// </summary>
        public float Strength { get; set; }
    }
}
