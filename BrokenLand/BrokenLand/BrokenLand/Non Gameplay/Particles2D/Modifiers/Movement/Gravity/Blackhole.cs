using Microsoft.Xna.Framework;

namespace Supernova.Particles2D.Modifiers.Movement.Gravity
{
    /// <summary>
    /// Modifier which will pull particles towards a gravitational point and kill them when within a specified radius
    /// </summary>
    public class Blackhole : IModifier
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public Blackhole()
        {
            RemovalRadius = 15f;
            GravityPoint = new GravityPoint();
            GravityPoint.Strength = -15;
            GravityPoint.Radius = 10;
            GravityPoint.Position = new Vector3(0,0,-100f);
        }

        public void Update(float particleAge, double totalMilliseconds, double elapsedSeconds, Particle2D particle)
        {
            GravityPoint.Update(particleAge, totalMilliseconds, elapsedSeconds, particle);

            Vector3 distance = Vector3.Subtract(GravityPoint.Position, particle.Position);

            float test = distance.LengthSquared();

            if (test < RemovalRadius * RemovalRadius)
                particle.IsAlive = false;
        }

        /// <summary>
        /// Gets or sets the radius in which particles will be killed
        /// </summary>
        public float RemovalRadius { get; set; }

        /// <summary>
        /// Gets or sets the GravityPoint for the black hole
        /// </summary>
        public GravityPoint GravityPoint { get; set; }
    }
}
