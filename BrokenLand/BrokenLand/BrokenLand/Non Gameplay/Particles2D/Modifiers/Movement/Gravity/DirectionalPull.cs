using Microsoft.Xna.Framework;

namespace Supernova.Particles2D.Modifiers.Movement.Gravity
{
    /// <summary>
    /// Modifier to gradually pull particles in a particular direction
    /// </summary>
    public class DirectionalPull : IModifier
    {
        public DirectionalPull(Vector2 grav)
        {
            Gravity = grav;
        }
        public void Update(float particleAge, double totalMilliseconds, double elapsedSeconds, Particle2D particle)
        {
            Vector3 deltaGrav = new Vector3(Vector2.Multiply(Gravity, (float)elapsedSeconds).X, Vector2.Multiply(Gravity, (float)elapsedSeconds).Y,0f);
            particle.Affect(ref deltaGrav);
        }

        /// <summary>
        /// Gets or sets the gravitational pull on particles
        /// </summary>
        public Vector2 Gravity { get; set; }
    }
}
