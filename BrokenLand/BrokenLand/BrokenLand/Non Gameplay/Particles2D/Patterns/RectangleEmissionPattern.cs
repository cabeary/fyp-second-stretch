﻿using System;
using Microsoft.Xna.Framework;

namespace Supernova.Particles2D.Patterns
{
    public class RectangleEmissionPattern : IEmissionPattern
    {
        private readonly int width;
        private readonly int height;

        public RectangleEmissionPattern(int width, int height)
        {
            this.width = width;
            this.height = height;
        }

        public Vector3 CalculateParticlePosition(Random random, Vector3 emitPosition)
        {
            float halfWidth = width / 2f;
            float halfHeight = height / 2f;

            Vector3 offset = Vector3.Zero;
            offset.X = (float)((halfWidth - (-halfWidth)) * random.NextDouble() + (-halfWidth));
            offset.Y = (float)((halfHeight - (-halfHeight)) * random.NextDouble() + (-halfHeight));
            offset.Z = (float)((halfWidth - (-halfWidth)) * random.NextDouble() + (-halfWidth));

            return Vector3.Add(emitPosition, offset);
        }
    }
}