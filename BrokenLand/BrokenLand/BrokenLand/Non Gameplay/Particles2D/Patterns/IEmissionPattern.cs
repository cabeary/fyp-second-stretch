﻿using System;
using Microsoft.Xna.Framework;

namespace Supernova.Particles2D.Patterns
{
    public interface IEmissionPattern
    {
        Vector3 CalculateParticlePosition(Random random, Vector3 emitPosition);
    }
}