﻿using System;
using Microsoft.Xna.Framework;

namespace Supernova.Particles2D.Patterns
{
    public class CircleEmissionPattern : IEmissionPattern
    {
        private readonly float radius;

        public CircleEmissionPattern(float radius)
        {
            this.radius = radius;
        }

        public Vector3 CalculateParticlePosition(Random random, Vector3 emitPosition)
        {
            float rads = (float)(random.NextDouble() * MathHelper.TwoPi);
            Vector3 offset = new Vector3((float)Math.Cos(rads) * radius, (float)Math.Sin(rads) * radius, (float)Math.Sin(rads) * radius);
            
            return Vector3.Add(emitPosition, offset);
        }
    }
}