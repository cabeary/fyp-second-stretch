﻿using System;
using Microsoft.Xna.Framework;

namespace Supernova.Particles2D.Patterns
{
    public class ConeEmissionPattern : IEmissionPattern
    {
        private readonly float angle;

        public ConeEmissionPattern(float angle)
        {
            this.angle = angle;
        }

        public Vector3 CalculateParticlePosition(Random random, Vector3 emitPosition)
        {
            float rads = (float)(random.NextDouble() * MathHelper.TwoPi);
            Vector3 offset = new Vector3((float)Math.Cos(rads) * angle, (float)Math.Sin(rads) * angle, (float)Math.Sin(rads) * angle);
            offset.Y = MathHelper.Clamp(offset.Y, 0, 1);

            offset.X = MathHelper.Clamp(offset.X, 0, 1);
    
            return Vector3.Add(emitPosition, offset);
        }
    }
}