﻿using BrokenLands;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using GameStateManagement;

namespace Supernova.Particles2D
{
    public class Particle2D
    {
        private Vector2 origin;
        private Vector3 position = Vector3.Zero;
        private Vector2 scale = Vector2.One;
        private Vector3 velocity = Vector3.Zero;
        private Vector3 projectedPosition = Vector3.Zero;
        private float rotation;

        private GraphicsDevice graphics;
        private Texture2D texture;
        private Color color = Color.White;
        private float alpha = 1f;

        private float lifespan;
        private float inceptionTime;
        private bool isAlive;

        public void Initialize(Texture2D particleTexture, Vector3 emitPosition, Vector3 initialVelocity, Color particleColor, float particleLifespan, float totalMilliseconds,GraphicsDevice graphics)
        {
            this.texture = particleTexture;
            origin = new Vector2(particleTexture.Width / 2f, particleTexture.Height / 2f);
            this.graphics = graphics;
            isAlive = true;
            alpha = 1f;
            inceptionTime = totalMilliseconds;
            position = emitPosition;
            color = particleColor;
            lifespan = particleLifespan;
            velocity = initialVelocity;
        }

        internal void Update(double totalMilliseconds)
        {
            if (lifespan < (totalMilliseconds - inceptionTime))
            {
                isAlive = false;
            }
            else
            { 
                position += velocity;
                projectedPosition = position + velocity;
            }
        }

        internal void Affect(ref Vector3 attraction)
        {
            velocity = Vector3.Add(velocity, attraction);
        }

        internal void Draw(SpriteBatch spriteBatch,CameraObj camera,ParticleBase particle,GraphicsDevice graphicsDev)
        {
            graphicsDev.Viewport.Unproject(new Vector3(this.position.X,this.position.Y,0f), camera.proj, camera.view, Matrix.Identity);
            spriteBatch.Draw(texture, ToScreenCoordinates(projectedPosition, camera, graphicsDev), null, color * alpha, rotation, origin, particle.SetScale(camera.VCameraPosition), SpriteEffects.None, 0);
        }

        public Vector3 Position
        {
            get { return position; }
            set { position = value; }
        }

        public Vector3 ProjectedPosition
        {
            get { return projectedPosition; }
        }

        public Vector2 Scale
        {
            get { return scale; }
            set { scale = value; }
        }

        public Vector3 Velocity
        {
            get { return velocity; }
            set { velocity = value; }
        }

        public float Rotation
        {
            get { return rotation; }
            set { rotation = value; }
        }

        public Color Color
        {
            get { return color; }
            set { color = value; }
        }

        public float Alpha
        {
            get { return alpha; }
            set { alpha = value; }
        }

        public Texture2D Texture
        {
            get { return texture; }
            set { texture = value; }
        }

        public float Lifespan
        {
            get { return lifespan; }
            set { lifespan = value; }
        }

        public float InceptionTime
        {
            get { return inceptionTime; }
            set { inceptionTime = value; }
        }

        public bool IsAlive
        {
            get { return isAlive; }
            set { isAlive = value; }
        }

        Vector2 ToScreenCoordinates(Vector3 worldCoords, CameraObj Camera,GraphicsDevice graphics)
        {
            var screenPositon = graphics.Viewport.Project(worldCoords,
                    Camera.proj, Camera.view, Matrix.Identity);
            return new Vector2(screenPositon.X, screenPositon.Y);
        }
    }
}
