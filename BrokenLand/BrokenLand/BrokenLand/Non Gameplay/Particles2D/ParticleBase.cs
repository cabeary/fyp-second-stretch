﻿using System;
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Supernova.Particles2D;
using Supernova.Samples.Windows.Utilities;
using Supernova.Particles2D.Modifiers.Alpha;
using Supernova.Particles2D.Modifiers.Movement.Gravity;
using Supernova.Particles2D.Modifiers.Movement;
using Supernova.Particles2D.Patterns;

namespace BrokenLands
{
    public class ParticleBase
    {
        public Vector4 BillboardPos_World;
        public Vector3 ParticlePosW;
        Vector4 BillboardPos_Screen;
        public Texture2D TSprite;
        float FLifetime;
        public Vector3 FVelocity, FInitialVelocity, FTerminalVelocity;
        float FSize;

        private ParticleEffect2D particleEffect;


        public ParticleBase(Matrix proj,Matrix view,Vector4 ParticlePos,GraphicsDevice graphics,Texture2D texture,float size = 0,Vector3 Velocity = new Vector3())
        {
            BillboardPos_World = ParticlePos;
            ParticlePosW = new Vector3(ParticlePos.X, ParticlePos.Y, ParticlePos.Z);
            BillboardPos_Screen = Vector4.Transform(BillboardPos_World,proj);
            BillboardPos_Screen = Vector4.Transform(BillboardPos_Screen,view);
            BillboardPos_Screen /= BillboardPos_Screen.W;
            SetVelocity(Velocity);
            FSize = size;
            TSprite = texture;
            particleEffect = ParticleEffect2DFactory.Initialize(1000, 2000)
                                                    .SetMaxParticleSpeed(0.1f)
                                                    .SetEmitAmount(5)
                                                    .AddTexture(TSprite)
                                                    .AddTexture(TSprite)
                                                    //.SetEmissionPattern(new RectangleEmissionPattern(100,100))
                                                    //.SetEmissionPattern(new ConeEmissionPattern(0))
                                                    .AddModifier(new DirectionalPull(new Vector2(0,0)))
                                                    .AddModifier(new AlphaAgeTransform())
                                                    .Create();
            
        }

        public void Update(GraphicsDevice graphics,GameTime gameTime,int Pattern)
        {
            BillboardPos_World = Vector4.Add(BillboardPos_World, new Vector4(FInitialVelocity.X, FInitialVelocity.Y, FInitialVelocity.Z, 1.0f));
            //particleEffect.Modifiers.
            Vector3 emitPosition = new Vector3(0, 0,0);
            
            particleEffect.Emit((float)gameTime.TotalGameTime.TotalMilliseconds, emitPosition,Pattern,graphics);
            particleEffect.Update((float)gameTime.TotalGameTime.TotalMilliseconds, (float)gameTime.ElapsedGameTime.TotalSeconds);

        }

        public void Draw(SpriteBatch spriteBatch,CameraObj camera,GraphicsDeviceManager graphics,GraphicsDevice graphicsdev)
        {
            particleEffect.Draw(spriteBatch,camera,this,graphicsdev);
        }

        public void SetSize(float size)
        {
            FSize = size;
        }

        public void SetVelocity(Vector3 Velocity)
        {
            FInitialVelocity = Velocity;
        }

        public float SetScale(Vector3 CameraPos)
        {
            float scale = 1.0f;
            float dist = Vector3.Distance(CameraPos, ParticlePosW);
            scale*= FSize;
            scale = scale - (0.002f * dist);
            if (scale <= 0.1f)
            {
                scale = 0.1f;
            }
            return scale;
        }

        public void SetSprite(Texture2D sprite)
        {
            TSprite = sprite;
        }
    }
}
