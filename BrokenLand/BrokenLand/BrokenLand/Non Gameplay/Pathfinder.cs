﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace BrokenLands
{
    class Pathfinder
    {
        //Lists to determine nodes
        //List<List<MapCell>> nodeCells = new List<List<MapCell>>();
        List<List<MapCell>> nodeCells = new List<List<MapCell>>();
        List<MapCell> pathway = new List<MapCell>();

        MapCell currentNodeCell;
        //MapCell startNode, goalNode;
        //int startNodeID;
        int mapSizeX;
        int mapSizeZ;
        Vector2 zoneOffset;

        /// <summary>
        /// Constructor. Generates List from Map, sets the value of mapsize
        /// </summary>
        /// <param name="map"></param>
        /// 
        public Pathfinder() { }
        private Map map;
        public Pathfinder(Map map)
        {
            this.map = map;
            SetMap(map.gridNodes);
        }

        /*public Pathfinder(List<List<MapCell>> map)
        {
            nodeCells = map;
            mapSizeX = map.Count;//map.GetLength(0);
            mapSizeZ = map[0].Count;//map.GetLength(1);
        }*/

        /// <summary>
        /// Find the path from A to B (If applicable) and sets path (Access via getPath())
        /// </summary>
        /// <param name="nodeStart"></param>
        /// <param name="nodeEnd"></param>
        private void pathFind(MapCell nodeStart, MapCell nodeEnd)
        {
            MapCell goalNode = nodeEnd;
            MapCell startNode = nodeStart;
            List<MapCell> openList = new List<MapCell>();
            List<MapCell> closedList = new List<MapCell>();
            pathway = new List<MapCell>();

            resetValues();
            startNode.updateDistance(0, getManhattanDistance(startNode, goalNode));

            openList.Add(startNode);

            while (openList.Count > 0)
            {
                if (openList.Count > 1)
                    openList = sortNodes(openList);
                currentNodeCell = openList[0];//getBestNode(openList); //Get node with lowest f Value

                if (currentNodeCell.Location == goalNode.Location)
                {
                    //Reached, get path by searching through parent nodes of last child
                    setPath(currentNodeCell);
                    break;
                }
                openList.RemoveAt(0);
                closedList.Add(currentNodeCell); // Add node to confirmed row

                List<MapCell> neighbours = getNeighbours(currentNodeCell, startNode);

                foreach (MapCell node in neighbours)
                {
                    float g = currentNodeCell.DistanceMoved + node.Cost;
                    if (node.Ent != null)
                    {
                        if (node.Ent.MyType == Entity.uType.Obstacle)
                        {
                            Obstacle currentObs = (Obstacle)node.Ent;
                            g += currentObs.TravelCost;
                        }
                    }
                    float h = getManhattanDistance(node, goalNode);
                    float f = g + h;

                    //If checked nodes list has node and f value higher than current
                    if (closedList.Contains(node))// && f >= node.DistanceTotal)
                    {
                        continue; //Skips the loop
                    }

                    if (!openList.Contains(node) || f < node.DistanceTotal) //If open list does not have node or totalDistance is less than current
                    {
                        node.ParentLocation = currentNodeCell;
                        node.updateDistance((int)g, (int)h);
                        if (!openList.Contains(node)) //If open list does not have node ONLY
                        {
                            //add to list
                            openList.Add(node);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Returns a list of node starting from the endNode to the startNode. Use pathFind() first
        /// </summary>
        /// <returns></returns>
        public List<MapCell> getPath(MapCell start, MapCell end)
        {
            pathFind(start, end);
            return pathway;
        }

        /// <summary>
        /// Returns a list of node starting from the endNode to the startNode. Use pathFind() first. Out returns cost of travel
        /// </summary>
        /// <returns></returns>
        public List<MapCell> getPath(MapCell start, MapCell end, out int cost)
        {
            pathFind(start, end);
            cost = CalcPathCost(pathway);
            return pathway;
        }

        /// <summary>
        /// Returns a list of nodes which indicates locations that the player can move to
        /// </summary>
        /// <param name="nodeStart"></param>
        /// <param name="playerUnit"></param>
        /// <returns></returns>
        public List<MapCell> getPossibleMoveLocations(Unit playerUnit)
        {
            List<MapCell> possibleMoveLocations = new List<MapCell>();
            MapCell startNode = playerUnit.MyLocation;

            resetValues();
            List<MapCell> openListLoc = new List<MapCell>();
            List<MapCell> closedListLoc = new List<MapCell>();
            openListLoc.Add(startNode);

            while (openListLoc.Count > 0)
            {
                currentNodeCell = getBestNode(openListLoc); //Get node with lowest f Value
                //if (currentNodeCell != null) //Nodepath exists
                //{
                if (currentNodeCell.DistanceTotal > playerUnit.MyStatus.CurrentAP)
                {
                    break;
                }
                if (openListLoc.Count > 1)
                    openListLoc = sortNodes(openListLoc);
                openListLoc.RemoveAt(0);
                closedListLoc.Add(currentNodeCell); // Add node to confirmed row

                List<MapCell> neighbours = getNeighbours(currentNodeCell, startNode);

                foreach (MapCell node in neighbours)
                {
                    float g = currentNodeCell.DistanceMoved + node.Cost;
                    if (node.Ent != null)
                    {
                        if (node.Ent.MyType == Entity.uType.Obstacle)
                        {
                            Obstacle currentObs = (Obstacle)node.Ent;
                            g += currentObs.TravelCost;
                        }
                    }
                    float h = 0;
                    float f = g + h;

                    if (closedListLoc.Contains(node) && f >= node.DistanceTotal) //If confirmed path has node and f value higher than current
                    {
                        continue; //Skips the loop
                    }

                    if (!openListLoc.Contains(node) || f < node.DistanceTotal) //If open list does not have node or totalDistance is less than current
                    {
                        node.ParentLocation = currentNodeCell;
                        node.updateDistance((int)g, (int)h);
                        if (!openListLoc.Contains(node)) //If open list does not have node ONLY
                        {
                            //add to list
                            openListLoc.Add(node);
                        }
                    }
                }
            }

            for (int i = 0; i < closedListLoc.Count; i++)
            {
                if (playerUnit.Size == new Vector2(1, 1))
                {
                    if (closedListLoc[i].Ent != null)
                    {
                        closedListLoc.RemoveAt(i);
                        i--;
                    }
                }
                else
                {
                    List<MapCell> OccupiedNodes = playerUnit.GetOccupied(closedListLoc[i]);
                    bool nodeOccupied = false;

                    for (int j = 0; j < OccupiedNodes.Count; j++)
                    {
                        if (closedListLoc[i].Ent != null && closedListLoc[i].Ent != (Entity)playerUnit)
                        {
                            nodeOccupied = true;
                            break;
                        }
                    }
                    if (nodeOccupied)
                    {
                        closedListLoc.RemoveAt(i);
                        i--;
                    }
                }
            }
            possibleMoveLocations = closedListLoc;
            return possibleMoveLocations;
        }

        /// <summary>
        /// Returns a list of nodes which indicates locations that the player can move to, with a parameter of how much AP to use
        /// </summary>
        /// <param name="nodeStart"></param>
        /// <param name="playerUnit"></param>
        /// <returns></returns>
        public List<MapCell> getPossibleMoveLocations(Unit playerUnit, int APUse)
        {
            List<MapCell> possibleMoveLocations = new List<MapCell>();
            MapCell startNode = playerUnit.MyLocation;

            resetValues();
            List<MapCell> openListLoc = new List<MapCell>();
            List<MapCell> closedListLoc = new List<MapCell>();
            openListLoc.Add(startNode);

            if (playerUnit.MyStatus.CurrentAP < APUse)
                APUse = playerUnit.MyStatus.CurrentAP;

            while (openListLoc.Count > 0)
            {
                currentNodeCell = getBestNode(openListLoc); //Get node with lowest f Value
                //if (currentNodeCell != null) //Nodepath exists
                //{
                if (currentNodeCell.DistanceTotal > APUse)
                {
                    break;
                }
                if (openListLoc.Count > 1)
                    openListLoc = sortNodes(openListLoc);
                openListLoc.RemoveAt(0);
                closedListLoc.Add(currentNodeCell); // Add node to confirmed row

                List<MapCell> neighbours = getNeighbours(currentNodeCell, startNode);

                foreach (MapCell node in neighbours)
                {
                    float g = currentNodeCell.DistanceMoved + node.Cost;
                    if (node.Ent != null)
                    {
                        if (node.Ent.MyType == Entity.uType.Obstacle)
                        {
                            Obstacle currentObs = (Obstacle)node.Ent;
                            g += currentObs.TravelCost;
                        }
                    }
                    float h = 0;
                    float f = g + h;

                    if (closedListLoc.Contains(node) && f >= node.DistanceTotal) //If confirmed path has node and f value higher than current
                    {
                        continue; //Skips the loop
                    }

                    if (!openListLoc.Contains(node) || f < node.DistanceTotal) //If open list does not have node or totalDistance is less than current
                    {
                        node.ParentLocation = currentNodeCell;
                        node.updateDistance((int)g, (int)h);
                        if (!openListLoc.Contains(node)) //If open list does not have node ONLY
                        {
                            //add to list
                            openListLoc.Add(node);
                        }
                    }
                }
            }

            for (int i = 0; i < closedListLoc.Count; i++)
            {
                if (playerUnit.Size == new Vector2(1, 1))
                {
                    if (closedListLoc[i].Ent != null)
                    {
                        closedListLoc.RemoveAt(i);
                        i--;
                    }
                }
                else
                {
                    List<MapCell> OccupiedNodes = playerUnit.GetOccupied(closedListLoc[i]);
                    bool nodeOccupied = false;

                    for (int j = 0; j < OccupiedNodes.Count; j++)
                    {
                        if (closedListLoc[i].Ent != null)
                        {
                            nodeOccupied = true;
                            break;
                        }
                    }
                    if (nodeOccupied)
                    {
                        closedListLoc.RemoveAt(i);
                        i--;
                    }
                }
            }

            possibleMoveLocations = closedListLoc;
            return possibleMoveLocations;
        }

        /// <summary>
        /// Returns all possible locations that unit can attack current enemy
        /// </summary>
        /// <param name="nodeStart"></param>
        /// <param name="playerUnit"></param>
        /// <returns></returns>
        public List<MapCell> getTargetPossibleAttackLocations(MapCell attackTarget, Unit unit)
        {
            MapCell startNode = attackTarget;

            //Used for getting nodes in unavailable rectangle
            MapCell TopLeftAnchor, BtmRightAnchor, CheckAreaTLAnchor, CheckAreaBRAnchor;

            int currentX = 0;
            int currentY = 0;
            if (startNode.Location.X - (unit.Size.X - 1) - zoneOffset.X >= 0)
            {
                currentX = (int)(startNode.Location.X - (unit.Size.X - 1) - zoneOffset.X);
            }
            else
            {
                currentX = 0;
            }
            if (startNode.Location.Y - (unit.Size.Y - 1) - zoneOffset.Y >= 0)
            {
                currentY = (int)(startNode.Location.Y - (unit.Size.Y - 1) - zoneOffset.Y);
            }
            else
            {
                currentY = 0;
            }
            TopLeftAnchor = nodeCells[currentX][currentY];

            if (startNode.Location.X + startNode.Ent.Size.X - 1 - zoneOffset.X < nodeCells.Count)
            {
                currentX = (int)(startNode.Location.X + startNode.Ent.Size.X - 1 - zoneOffset.X);
            }
            else
            {
                currentX = nodeCells.Count - 1 - (int)zoneOffset.X;
            }
            if (startNode.Location.Y + startNode.Ent.Size.Y - 1 - zoneOffset.Y < nodeCells[0].Count)
            {
                currentY = (int)(startNode.Location.Y + startNode.Ent.Size.Y - 1 - zoneOffset.Y);
            }
            else
            {
                currentY = nodeCells[0].Count - 1 - (int)zoneOffset.Y;
            }
            BtmRightAnchor = nodeCells[currentX][currentY];

            if (TopLeftAnchor.Location.X - unit.MyStatus.Range - (int)zoneOffset.X >= 0)
            {
                currentX = (int)(TopLeftAnchor.Location.X - unit.MyStatus.Range - (int)zoneOffset.X);
            }
            else
            {
                currentX = 0;
            }
            if (TopLeftAnchor.Location.Y - unit.MyStatus.Range - (int)zoneOffset.Y >= 0)
            {
                currentY = (int)(TopLeftAnchor.Location.Y - unit.MyStatus.Range - (int)zoneOffset.Y);
            }
            else
            {
                currentY = 0;
            }
            CheckAreaTLAnchor = nodeCells[currentX][currentY];

            if (BtmRightAnchor.Location.X + unit.MyStatus.Range - (int)zoneOffset.X < nodeCells.Count)
            {
                currentX = (int)(BtmRightAnchor.Location.X + unit.MyStatus.Range - (int)zoneOffset.X);
            }
            else
            {
                currentX = nodeCells.Count - 1;// -(int)zoneOffset.X;
            }
            if (BtmRightAnchor.Location.Y + unit.MyStatus.Range - (int)zoneOffset.Y < nodeCells[0].Count)
            {
                currentY = (int)(BtmRightAnchor.Location.Y + unit.MyStatus.Range - (int)zoneOffset.Y);
            }
            else
            {
                currentY = nodeCells[0].Count - 1;// -(int)zoneOffset.Y;
            }
            CheckAreaBRAnchor = nodeCells[currentX][currentY];

            /*Console.WriteLine("TL : " + TopLeftAnchor.Location);
            Console.WriteLine("BR : " + BtmRightAnchor.Location);
            Console.WriteLine("TLBounds : " + CheckAreaTLAnchor.Location);
            Console.WriteLine("BRBounds : " + CheckAreaBRAnchor.Location);*/

            List<MapCell> possibleAttackLocations = new List<MapCell>();
            List<MapCell> toCheckMapCells = new List<MapCell>();

            for (int x = (int)CheckAreaTLAnchor.Location.X - (int)zoneOffset.X; x <= CheckAreaBRAnchor.Location.X - (int)zoneOffset.X; x++)
            {
                for (int y = (int)CheckAreaTLAnchor.Location.Y - (int)zoneOffset.Y; y <= CheckAreaBRAnchor.Location.Y - (int)zoneOffset.Y; y++)
                {
                    if (!((x >= TopLeftAnchor.Location.X - (int)zoneOffset.X && x <= BtmRightAnchor.Location.X - (int)zoneOffset.X) &&
                        (y >= TopLeftAnchor.Location.Y - (int)zoneOffset.Y && y <= BtmRightAnchor.Location.Y - (int)zoneOffset.Y))) //If not within the inner Rectangle
                    {
                        toCheckMapCells.Add(nodeCells[x][y]);
                    }
                }
            }

            bool diagonalEnabled = false;
            for (int i = 0; i < toCheckMapCells.Count; i++)
            {
                bool passable = true;

                if (diagonalEnabled)
                {
                    if (toCheckMapCells[i].Location.X == CheckAreaBRAnchor.Location.X && toCheckMapCells[i].Location.Y == CheckAreaBRAnchor.Location.Y)
                        passable = false;
                    else if (toCheckMapCells[i].Location.X == CheckAreaBRAnchor.Location.X && toCheckMapCells[i].Location.Y == CheckAreaTLAnchor.Location.Y)
                        passable = false;
                    else if (toCheckMapCells[i].Location.X == CheckAreaTLAnchor.Location.X && toCheckMapCells[i].Location.Y == CheckAreaBRAnchor.Location.Y)
                        passable = false;
                    else if (toCheckMapCells[i].Location.X == CheckAreaTLAnchor.Location.X && toCheckMapCells[i].Location.Y == CheckAreaTLAnchor.Location.Y)
                        passable = false;
                }
                if (passable)
                {
                    for (int j = 0; j < unit.Size.X; j++)
                    {
                        for (int k = 0; k < unit.Size.Y; k++)
                        {
                            if (toCheckMapCells[i].Location.X + j < nodeCells.Count && toCheckMapCells[i].Location.Y + k < nodeCells[0].Count)
                            {
                                MapCell tempCell = nodeCells[(int)(toCheckMapCells[i].Location.X + j)][(int)(toCheckMapCells[i].Location.Y + k)];
                                if (tempCell != null)
                                {
                                    if (tempCell.Ent != null)
                                    {
                                        if (tempCell.Ent.MyType != unit.MyType)
                                        {
                                            passable = false;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if (passable)
                {
                    if (getManhattanDistance(toCheckMapCells[i], attackTarget) <= unit.MyStatus.Range)
                        possibleAttackLocations.Add(toCheckMapCells[i]);
                    //Console.WriteLine(toCheckMapCells[i].Location);
                }
            }
            return possibleAttackLocations;
        }

        /// <summary>
        /// Returns all attackable locations of node in range 
        /// </summary>
        /// <param name="startNode"></param>
        /// <param name="playerUnit"></param>
        /// <returns></returns>
        public List<MapCell> getPossibleStationaryAttackLocations(Unit unit)
        {
            List<MapCell> possibleAttackTargets = new List<MapCell>();
            if (unit.MyType == Entity.uType.Player)
            {
                foreach (Unit u in map.enemList)
                {
                    if (u.IsAlive)
                    {
                        List<MapCell> occupiedMapCells = unit.GetOccupied();
                        List<MapCell> attackedMapCells = u.GetOccupied();
                        //check whether u is in range of unit
                        for (int i = 0; i < occupiedMapCells.Count; i++)
                        {
                            for (int j = 0; j < attackedMapCells.Count; j++)
                            {
                                if (getManhattanDistance(u.MyLocation, occupiedMapCells[i]) <= unit.MyStatus.Range)
                                {
                                    //Check if straight line
                                    //if (isInLine(unit, u))
                                    possibleAttackTargets.Add(attackedMapCells[j]);
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                foreach (Unit u in map.playerList)
                {
                    if (u.IsAlive)
                    {
                        List<MapCell> occupiedMapCells = unit.GetOccupied();
                        List<MapCell> attackedMapCells = u.GetOccupied();
                        //check whether u is in range of unit
                        for (int i = 0; i < occupiedMapCells.Count; i++)
                        {
                            for (int j = 0; j < attackedMapCells.Count; j++)
                            {
                                if (getManhattanDistance(attackedMapCells[j], occupiedMapCells[i]) <= unit.MyStatus.Range)
                                {
                                    //Check if straight line
                                    //if (isInLine(unit, u))
                                    possibleAttackTargets.Add(attackedMapCells[j]);
                                    //Console.WriteLine(attackedMapCells[i].Location);
                                }
                            }
                        }
                    }
                }
            }
            return possibleAttackTargets;
        }

        private List<MapCell> getPossibleStationaryAttackLocations(MapCell nodeStart, Unit unit)
        {
            List<MapCell> possibleAttackTargets = new List<MapCell>();
            if (unit.MyType == Entity.uType.Player)
            {
                foreach (Unit u in map.enemList)
                {
                    if (u.IsAlive)
                    {
                        List<MapCell> occupiedMapCells = unit.GetOccupied(nodeStart);
                        List<MapCell> attackedMapCells = u.GetOccupied();
                        //check whether u is in range of unit
                        for (int i = 0; i < occupiedMapCells.Count; i++)
                        {
                            for (int j = 0; j < attackedMapCells.Count; j++)
                            {
                                if (getManhattanDistance(u.MyLocation, nodeStart) <= unit.MyStatus.Range)
                                {
                                    //if (isInLine(u, nodeStart, unit.Size))
                                    possibleAttackTargets.Add(attackedMapCells[j]);
                                }

                            }
                        }
                    }
                }
            }
            else
            {
                foreach (Unit u in map.playerList)
                {
                    if (u.IsAlive)
                    {
                        List<MapCell> occupiedMapCells = unit.GetOccupied(nodeStart);
                        List<MapCell> attackedMapCells = u.GetOccupied();
                        //check whether u is in range of unit
                        for (int i = 0; i < occupiedMapCells.Count; i++)
                        {
                            for (int j = 0; j < attackedMapCells.Count; j++)
                            {
                                if (getManhattanDistance(attackedMapCells[j], nodeStart) <= unit.MyStatus.Range)
                                {
                                    //if (isInLine(u, nodeStart, unit.Size))
                                    possibleAttackTargets.Add(attackedMapCells[j]);
                                }

                            }
                        }
                    }
                }
            }
            return possibleAttackTargets;
        }

        /// <summary>
        /// Returns all attackable targets' location within range (With movement)
        /// </summary>
        /// <param name="startNode"></param>
        /// <param name="playerUnit"></param>
        /// <returns></returns>
        public List<MapCell> getPossibleMoveAttackLocations(Unit playerUnit)
        {
            if (playerUnit.MyStatus.CurrentAP == 1)
            {
                return getPossibleStationaryAttackLocations(playerUnit);
            }

            List<MapCell> possibleMoveAttackLocations = new List<MapCell>();
            List<MapCell> possibleMoveLocations = getPossibleMoveLocations(playerUnit, playerUnit.MyStatus.CurrentAP - 1);
            possibleMoveLocations.Add(playerUnit.MyLocation);

            for (int i = 0; i < possibleMoveLocations.Count; i++) //For every moveable location (save 1 AP to attack at least)
            {
                List<MapCell> possibleAttackLocations =
                    getPossibleStationaryAttackLocations(possibleMoveLocations[i], playerUnit);

                //For every MapCell in attackable locations
                foreach (MapCell mapLocation in possibleAttackLocations)
                {
                    //If not in list, add in
                    if (!possibleMoveAttackLocations.Contains(mapLocation))
                    {
                        mapLocation.ParentLocation = possibleMoveLocations[i];
                        possibleMoveAttackLocations.Add(mapLocation);

                    }
                }
            }

            return possibleMoveAttackLocations;
        }

        /// <summary>
        /// Takes in a list of pathways and determines which one is the best path before returning it.
        /// </summary>
        /// <param name="pathwayList"></param>
        /// <param name="APLeft"></param>
        /// <returns></returns>
        public List<MapCell> getBestPath(List<List<MapCell>> pathwayList, int currentAP)
        {
            List<MapCell> bestPath = new List<MapCell>();
            List<List<MapCell>> secondCheck = new List<List<MapCell>>();
            int pathLength = 10000;
            int pathID = -1;

            for (int i = 0; i < pathwayList.Count; i++)
            {
                if (pathwayList[i].Count <= pathLength)
                {
                    if (pathwayList[i].Count < pathLength)
                    {
                        secondCheck = new List<List<MapCell>>();
                        secondCheck.Add(pathwayList[i]);
                        pathID = i;
                        pathLength = pathwayList[i].Count;
                    }
                    else //If pathwayList[i].Count == pathLength
                    {
                        secondCheck.Add(pathwayList[i]);
                    }
                }
            }

            if (secondCheck.Count == 1) //If there is only one path left
            {
                bestPath = secondCheck[0];
            }
            else //If more than 1 pathway with same length
            {
                List<List<MapCell>> randomChoice = new List<List<MapCell>>();

                for (int j = 0; j < secondCheck.Count; j++)
                {
                    if (secondCheck[j][secondCheck.Count - currentAP].Ent == null)
                    {
                        randomChoice.Add(secondCheck[j]);
                    }
                }

                if (randomChoice.Count == 0) //If all paths are blocked
                {
                    randomChoice = secondCheck;
                }

                Random RNGesus = new Random();
                int randomInt = RNGesus.Next(randomChoice.Count);
                bestPath = randomChoice[randomInt];
            }
            return bestPath;
        }

        /// <summary>
        /// Finds best node within list (with the lowest movement cost value)
        /// </summary>
        /// <param name="openList"></param>
        /// <returns></returns>
        private MapCell getBestNode(List<MapCell> openList)
        {
            List<MapCell> pathNodes = new List<MapCell>();

            //int lowestDistanceX = -1;
            //int lowestDistanceY = -1;
            int lowestDistanceID = -1;
            int lowestDistanceTotal = 10000;
            int distanceLeft = 10000;
            for (int i = 0; i < openList.Count; i++)
            {
                if (openList[i].DistanceTotal < lowestDistanceTotal)
                {
                    lowestDistanceTotal = openList[i].DistanceTotal;
                    lowestDistanceID = i;
                    distanceLeft = openList[i].DistanceLeft;
                    //lowestDistanceX = (int)(openList[i].Location.X);
                    //lowestDistanceY = (int)(openList[i].Location.Y);
                }
                else if (openList[i].DistanceTotal == lowestDistanceTotal) //If 2 nodes are similar in DistanceTotal
                {
                    if (openList[i].DistanceLeft < distanceLeft) //Check DistanceLeft
                    {
                        lowestDistanceTotal = openList[i].DistanceTotal;
                        lowestDistanceID = i;
                        distanceLeft = openList[i].DistanceLeft;
                    }
                }
            }

            if (lowestDistanceID == -1)
            {
                return null;
            }
            else
            {
                return openList[lowestDistanceID];
            }
        }

        /// <summary>
        /// Get neighbours of current node, regardless of condition
        /// </summary>
        /// <param name="currentNode"></param>
        /// <returns></returns>
        public List<MapCell> getNeighbours(MapCell currentNode)
        {
            List<MapCell> pathNodes = new List<MapCell>();

            for (int i = -1; i < 2; i++)
            {
                for (int j = -1; j < 2; j++)
                {
                    if (i == 0 && j == 0) //If not center
                    {
                        continue;
                    }
                    if (i != j && i != -j) //If not diagonals
                    {
                        if (!(currentNode.Location.X - zoneOffset.X == 0 && i < 0) && //If NOT at left-est row and moves left
                            !(currentNode.Location.X - zoneOffset.X == (mapSizeX - 1) && i > 0) && // If NOT at right-est row and moves right
                            !(currentNode.Location.Y - zoneOffset.Y == 0 && j < 0) && //If NOT at top row and moves up
                            !(currentNode.Location.Y - zoneOffset.Y == (mapSizeZ - 1) && j > 0)) //If NOT at btm row and moves down
                        {
                            //int newNeighbourID = (int)(currentNode.Location.X + currentNode.Location.Y * mapSizeZ)
                            //  + i + (j * mapSizeZ);
                            int locationX = (int)currentNode.Location.X + i - (int)zoneOffset.X;
                            int locationY = (int)currentNode.Location.Y + j - (int)zoneOffset.Y;
                            pathNodes.Add(nodeCells[locationX][locationY]);
                        }
                    }
                }
            }
            return pathNodes;
        }

        /// <summary>
        /// Get neighbours of current node. Auto-detects and ignore blocked/enemy nodes
        /// </summary>
        /// <param name="currentNode"></param>
        /// <returns></returns>
        private List<MapCell> getNeighbours(MapCell currentNode, MapCell startNode)
        {
            List<MapCell> pathNodes = new List<MapCell>();
            if (startNode.Ent != null)
            {
                if (startNode.Ent.Size != new Vector2(1, 1))
                {
                    for (int i = -1; i < 2; i++)
                    {
                        for (int j = -1; j < 2; j++)
                        {
                            if (i == 0 && j == 0) //If not center
                            {
                                continue;
                            }
                            if (i != j && i != -j) //If not diagonals
                            {
                                if ((!(currentNode.Location.X - zoneOffset.X == 0 && i < 0)) && //If NOT at left-est row and moves left
                                    (!(currentNode.Location.X - zoneOffset.X == (mapSizeX - startNode.Ent.Size.X) && i > 0)) && // If NOT at right-est row and moves right
                                    (!(currentNode.Location.Y - zoneOffset.Y == 0 && j < 0)) && //If NOT at top row and moves up
                                    (!(currentNode.Location.Y - zoneOffset.Y == (mapSizeZ - startNode.Ent.Size.Y) && j > 0))) //If NOT at btm row and moves down
                                {

                                    int locationX = (int)currentNode.Location.X + i - (int)zoneOffset.X;
                                    int locationY = (int)currentNode.Location.Y + j - (int)zoneOffset.Y;

                                    List<MapCell> NodesToCheck = startNode.Ent.GetOccupied(nodeCells[locationX][locationY]);
                                    bool nodeOccupied = false;

                                    for (int k = 0; k < NodesToCheck.Count; k++)
                                    {
                                        if (NodesToCheck[k].Ent != null) //If is not empty
                                        {
                                            if (NodesToCheck[k].Ent.MyType != startNode.Ent.MyType) //If not allies
                                            {
                                                if (NodesToCheck[k].Ent.MyType != Entity.uType.Obstacle) //If it is not an obstacle, allows movement		                                                nodeOccupied = true;
                                                {
                                                    nodeOccupied = true;
                                                    break;
                                                }
                                            }
                                        }
                                    }

                                    if (!nodeOccupied)
                                    {
                                        pathNodes.Add(nodeCells[locationX][locationY]);
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    for (int i = -1; i < 2; i++)
                    {
                        for (int j = -1; j < 2; j++)
                        {
                            if (i == 0 && j == 0) //If not center
                            {
                                continue;
                            }
                            if (i != j && i != -j) //If not diagonals
                            {
                                if ((!(currentNode.Location.X - zoneOffset.X == 0 && i < 0)) && //If NOT at left-est row and moves left
                                    (!(currentNode.Location.X - zoneOffset.X == (mapSizeX - 1) && i > 0)) && // If NOT at right-est row and moves right
                                    (!(currentNode.Location.Y - zoneOffset.Y == 0 && j < 0)) && //If NOT at top row and moves up
                                    (!(currentNode.Location.Y - zoneOffset.Y == (mapSizeZ - 1) && j > 0))) //If NOT at btm row and moves down
                                {
                                    //int newNeighbourID = (int)(currentNode.Location.X + currentNode.Location.Y * mapSizeZ)
                                    //  + i + (j * mapSizeZ);

                                    int locationX = (int)currentNode.Location.X + i - (int)zoneOffset.X;
                                    int locationY = (int)currentNode.Location.Y + j - (int)zoneOffset.Y;


                                    //If empty
                                    if (nodeCells[locationX][locationY].Ent == null)
                                    {
                                        //if (nodeCells[locationX][locationY].Marked == false) //If not marked
                                        pathNodes.Add(nodeCells[locationX][locationY]);
                                    }
                                    else if (startNode.Ent != null) //Checks if invalid operation
                                    {
                                        //Else if same type
                                        if (nodeCells[locationX][locationY].Ent.MyType == startNode.Ent.MyType)
                                        {
                                            pathNodes.Add(nodeCells[locationX][locationY]);
                                        }
                                        else if (nodeCells[locationX][locationY].Ent.MyType == Entity.uType.Obstacle)
                                        {
                                            pathNodes.Add(nodeCells[locationX][locationY]);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                Console.WriteLine("No neighbours found");
            }
            return pathNodes;
        }

        /// <summary>
        /// Get neighbours of current node. If ignoreOccupied, gets all neighbours, 
        /// if !ignoreOccupied, gets only neighbours that are not occupied and allies
        /// </summary>
        /// <param name="currentNode"></param>
        /// <param name="ignoreOccupied"></param>
        /// <returns></returns>
        private List<MapCell> getNeighbours(MapCell currentNode, MapCell startNode, bool ignoreOccupied)
        {
            List<MapCell> pathNodes = new List<MapCell>();

            if (startNode.Ent != null)
            {
                if (startNode.Ent.Size != new Vector2(1, 1))
                {
                    for (int i = -1; i < 2; i++)
                    {
                        for (int j = -1; j < 2; j++)
                        {
                            if (i == 0 && j == 0) //If not center
                            {
                                continue;
                            }
                            if (i != j && i != -j) //If not diagonals
                            {
                                if (!(currentNode.Location.X == 0 && i < 0) && //If NOT at left-est row and moves left
                                    !(currentNode.Location.X == (mapSizeX - startNode.Ent.Size.X) && i > 0) && // If NOT at right-est row and moves right
                                    !(currentNode.Location.Y == 0 && j < 0) && //If NOT at top row and moves up
                                    !(currentNode.Location.Y == (mapSizeZ - startNode.Ent.Size.Y) && j > 0)) //If NOT at btm row and moves down
                                {

                                    int locationX = (int)currentNode.Location.X + i;
                                    int locationY = (int)currentNode.Location.Y + j;

                                    //If not ignoring, check occupied and Ent type. If ignoring, move on
                                    if (ignoreOccupied == true)
                                    {
                                        pathNodes.Add(nodeCells[locationX][locationY]);
                                    }
                                    else
                                    {
                                        List<MapCell> NodesToCheck = startNode.Ent.GetOccupied(nodeCells[locationX][locationY]);
                                        bool nodeOccupied = false;

                                        for (int k = 0; k < NodesToCheck.Count; k++)
                                        {
                                            if (!(NodesToCheck[k].Ent == startNode.Ent ||
                                                NodesToCheck[k].Ent == null ||
                                                NodesToCheck[k].Ent.MyType == Entity.uType.Obstacle))
                                            {
                                                nodeOccupied = true;
                                                break;
                                            }
                                        }

                                        if (!nodeOccupied)
                                        {
                                            pathNodes.Add(nodeCells[locationX][locationY]);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    for (int i = -1; i < 2; i++)
                    {
                        for (int j = -1; j < 2; j++)
                        {
                            if (i == 0 && j == 0) //If not center
                            {
                                continue;
                            }
                            if (i != j && i != -j) //If not diagonals
                            {
                                if (!(currentNode.Location.X == 0 && i < 0) && //If NOT at left-est row and moves left
                                    !(currentNode.Location.X == (mapSizeX - 1) && i > 0) && // If NOT at right-est row and moves right
                                    !(currentNode.Location.Y == 0 && j < 0) && //If NOT at top row and moves up
                                    !(currentNode.Location.Y == (mapSizeZ - 1) && j > 0)) //If NOT at btm row and moves down
                                {
                                    //int newNeighbourID = (int)(currentNode.Location.X + currentNode.Location.Y * mapSizeZ)
                                    //  + i + (j * mapSizeZ);

                                    int locationX = (int)currentNode.Location.X + i;
                                    int locationY = (int)currentNode.Location.Y + j;
                                    //If not ignoring, check occupied and Ent type. If ignoring, move on
                                    if (ignoreOccupied == true)
                                    {
                                        pathNodes.Add(nodeCells[locationX][locationY]);
                                    }
                                    else
                                    {
                                        //If empty
                                        if (nodeCells[locationX][locationY].Ent == null)
                                        {
                                            pathNodes.Add(nodeCells[locationX][locationY]);
                                        } //Or same type
                                        else if (nodeCells[locationX][locationY].Ent.MyType == startNode.Ent.MyType)
                                        {
                                            pathNodes.Add(nodeCells[locationX][locationY]);
                                        } //Or if obstacle
                                        else if (nodeCells[locationX][locationY].Ent.MyType == Entity.uType.Obstacle)
                                        {
                                            pathNodes.Add(nodeCells[locationX][locationY]);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                Console.WriteLine("No neighbours found");
            }
            return pathNodes;
        }

        /// <summary>
        /// Calculates the direct distance between two nodes (ignore obstacles)
        /// </summary>
        /// <param name="node1"></param>
        /// <param name="node2"></param>
        /// <returns></returns>
        public int getManhattanDistance(MapCell node1, MapCell node2)
        {
            int distanceFromEndX = Math.Abs(1 * //1 represents energy required to pass through 
              (int)((node1.Location.X) - (node2.Location.X))); //May need change to check each node and add value
            int distanceFromEndY = Math.Abs(1 *
              (int)((node1.Location.Y) - (node2.Location.Y)));

            int manhattanDistance = distanceFromEndX + distanceFromEndY;//getDistancesCost(node1, node2)[2];
            return manhattanDistance;
        }

        /// <summary>
        /// Checks between 2 units to see if they are within range
        /// </summary>
        /// <param name="unit"></param>
        /// <returns></returns>
        public bool proximityCheck(Unit unit, Entity target, int distance)
        {
            bool inRange = false;
            if (getManhattanDistance(unit.MyLocation, target.MyLocation) <= distance)
                inRange = true;
            return inRange;
        }

        /// <summary>
        /// Checks based on unit in range, on type of unit that unit searches for
        /// Input an int to represent the % of map in proximity check. 1 indicates 10%, 10 indicates 100%. 
        /// Use 0 to detect directly next to you.
        /// </summary>
        /// <param name="rangeSize"></param>
        /// <returns></returns>
        public bool proximityCheck(Unit unit, Unit.uType checkType, int rangeSize)
        {
            bool inRange = false;
            if (checkType == Entity.uType.Enemy)
            {
                for (int i = 0; i < GameSession.GS.enemies.Count; i++)
                {
                    if (getManhattanDistance(unit.MyLocation, GameSession.GS.enemies[i].MyLocation) <= (mapSizeX * mapSizeZ) * rangeSize / 10)
                    {
                        inRange = true;
                        break;
                    }
                }
            }
            else if (checkType == Entity.uType.Player)
            {
                for (int i = 0; i < GameSession.players.Count; i++)
                {
                    if (getManhattanDistance(unit.MyLocation, GameSession.players[i].MyLocation) <= (mapSizeX * mapSizeZ) * rangeSize / 10)
                    {
                        inRange = true;
                        break;
                    }
                }
            }
            else if (checkType == Entity.uType.Obstacle)
            {
                for (int i = 0; i < GameSession.map.obstacleList.Count; i++)
                {
                    if (getManhattanDistance(unit.MyLocation, GameSession.map.obstacleList[i].MyLocation) <= (mapSizeX * mapSizeZ) * rangeSize / 10)
                    {
                        inRange = true;
                        break;
                    }
                }
            }
            else if (checkType == Entity.uType.Actor)
            {
                for (int i = 0; i < GameSession.map.actorList.Count; i++)
                {
                    if (getManhattanDistance(unit.MyLocation, GameSession.map.obstacleList[i].MyLocation) <= (mapSizeX * mapSizeZ) * rangeSize / 10)
                    {
                        inRange = true;
                        break;
                    }
                }
            }
            else
            {
                //Default : Check player
                for (int i = 0; i < GameSession.players.Count; i++)
                {
                    if (getManhattanDistance(unit.MyLocation, GameSession.players[i].MyLocation) <= (mapSizeX * mapSizeZ) * rangeSize / 10)
                    {
                        inRange = true;
                        break;
                    }
                }
            }
            return inRange;
        }

        /// <summary>
        /// Sets the path based on parentNodes (Child -> Parent -> Parent2 -> ... -> ParentX = pathway). Only access by pathFind()
        /// </summary>
        /// <param name="lastNode"></param>
        private void setPath(MapCell lastNode)
        {
            List<MapCell> nodeList = new List<MapCell>();
            while (lastNode != null && lastNode.ParentLocation != null)//nodeCells[startNodeID])
            {
                nodeList.Add(lastNode);
                lastNode = lastNode.ParentLocation;
            }

            pathway = nodeList;
        }

        //Unused
        /// <summary>
        /// Remove node from list
        /// </summary>
        /// <param name="nodeToRemove"></param>
        /// <param name="nodeList"></param>
        private List<MapCell> removeNodeFromList(MapCell nodeToRemove, List<MapCell> nodeList)
        {
            for (int count = 0; count < nodeList.Count; count++)
            {
                if (nodeList[count] == nodeToRemove)
                {
                    nodeList.RemoveAt(count);
                }
            }
            return nodeList;
        }

        /// <summary>
        /// Rearranges the nodes of a list based on their distanceTotal(Distance moved + distance left) value
        /// </summary>
        /// <param name="nodeList"></param>
        /// <returns></returns>
        private List<MapCell> sortNodes(List<MapCell> nodeList)
        {
            List<MapCell> sortedList = new List<MapCell>();
            List<bool> boolList = new List<bool>();
            List<int> nodefValueList = new List<int>();
            for (int i = 0; i < nodeList.Count; i++)
            {
                nodefValueList.Add(nodeList[i].DistanceTotal);
                boolList.Add(false);
            }
            nodefValueList.Sort(); //F values of nodes sorted in ascending order
            int j = 0;
            while (sortedList.Count != nodeList.Count) //Keep adding until sortedList is full
            {
                for (int i = 0; i < nodeList.Count; i++)
                {
                    if (nodefValueList[j] == nodeList[i].DistanceTotal//If the f value in list matches f value of current node checked
                        && boolList[i] == false)//And node not used before
                    {
                        sortedList.Add(nodeList[i]); //Add node to sorted list
                        boolList[i] = true;
                        j++; //Now checks for next f value in line
                        break;
                    }
                }
            }
            return sortedList;
        }

        /// <summary>
        /// Sets value for startNode (Unused)
        /// </summary>
        /// <param name="start"></param>
        /*public void setStartNode(MapCell start)
        {
            startNode = start;
        }*/

        /// <summary>
        /// Change all values back to 0 to be ready for the next pathFind
        /// </summary>
        private void resetValues()
        {
            //pathway = new List<MapCell>(); Only resets when calling new path

            for (int i = 0; i < nodeCells.Count; i++)
            {
                for (int j = 0; j < nodeCells[i].Count; j++)
                {
                    nodeCells[i][j].updateDistance(0, 0);
                    nodeCells[i][j].ParentLocation = null;

                }
            }
        }

        /// <summary>
        /// Sets map size
        /// </summary>
        /// <param name="mapSize"></param>
        public void setMapSize(Vector2 mapSize)
        {
            mapSizeX = (int)mapSize.X;
            mapSizeZ = (int)mapSize.Y;
        }

        /// <summary>
        /// Retrieve cost of travelling a path
        /// </summary>
        /// <param name="startNode"></param>
        /// <param name="endNode"></param>
        /// <returns></returns>
        private List<int> getDistancesCost(List<MapCell> toCheckPathway, out int damageTaken)
        {
            List<int> distanceCostList = new List<int>();
            damageTaken = 0;
            int totalDistance = 0;
            int horizontalDistance = 0;
            int verticalDistance = 0;

            if (toCheckPathway.Count > 0) //If pathway exists
            {
                MapCell lastNode = toCheckPathway[0];//startNode;

                for (int i = 0; i < toCheckPathway.Count; i++)
                {
                    bool obsBlock = false;
                    int extraCost = 0;
                    if (toCheckPathway[i].Ent != null)
                    {
                        if (toCheckPathway[i].Ent.MyType == Entity.uType.Obstacle)
                        {
                            Obstacle currentObs = (Obstacle)toCheckPathway[i].Ent;
                            if (currentObs.isDamageTile)
                            {
                                damageTaken += currentObs.TravelCost;
                            }
                            else
                            {
                                obsBlock = true;
                                extraCost = currentObs.TravelCost;
                            }
                        }
                    }
                    if (lastNode.Location.X == toCheckPathway[i].Location.X)
                    {
                        verticalDistance += pathway[i].Cost;
                        if (obsBlock)
                            verticalDistance += extraCost;
                    }
                    else
                    {
                        horizontalDistance += pathway[i].Cost;
                        if (obsBlock)
                            horizontalDistance += extraCost;
                    }
                    totalDistance += pathway[i].Cost;
                    if (obsBlock)
                        totalDistance += extraCost;
                    lastNode = toCheckPathway[i];
                }
            }

            distanceCostList.Add(horizontalDistance);
            distanceCostList.Add(verticalDistance);
            distanceCostList.Add(totalDistance);
            return distanceCostList;
        }

        /// <summary>
        /// Calculates cost of travelling path
        /// </summary>
        /// <param name="toCheckPathway"></param>
        /// <returns></returns>
        public int CalcPathCost(List<MapCell> toCheckPathway)
        {
            int tempStorage;
            int totalCost = getDistancesCost(toCheckPathway, out tempStorage)[getDistancesCost(toCheckPathway, out tempStorage).Count - 1];
            return totalCost;
        }

        public enum Direction
        {
            Left,
            Right,
            Up,
            Down,
            None
        }

        /*public void move(MapCell startNode, MapCell endNode, Character character)
        {
            //pathFind(startNode, endNode);
            if (character.getAPValue() * 2 > pathway.Count)
            {
                character.setCharPosition(endNode.Location);
            }
        }*/

        /// <summary>
        /// Gets an array of node coordinates that are x size away from currentNode
        /// </summary>
        /// <param name="idStartNode"></param>
        /// <param name="radiusSize"></param>
        /// <returns></returns>
        public Vector2[] getRadiusNodes(int idStartNode, int radiusSize)
        {
            List<Vector2> vectorList = new List<Vector2>();
            int size = radiusSize;
            int drawOnX = (idStartNode % mapSizeX) - size;
            int drawOnY = (idStartNode / mapSizeZ);
            int increaseX = -1;
            int increaseY = -1;
            for (int i = 0; i < size * 4; i++)
            {
                if (drawOnX < mapSizeX && drawOnX >= 0 && drawOnY < mapSizeZ && drawOnY >= 0)
                {
                    vectorList.Add(new Vector2(drawOnX, drawOnY));
                }
                if (drawOnX == (idStartNode % mapSizeX) - 2 || drawOnX == (idStartNode % mapSizeX) + 2)
                {
                    increaseX = -increaseX;
                }
                if (drawOnY == (idStartNode / mapSizeZ) - 2 || drawOnY == (idStartNode / mapSizeZ) + 2)
                {
                    increaseY = -increaseY;
                }
                drawOnX += increaseX;
                drawOnY += increaseY;
            }

            Vector2[] vectorArray = new Vector2[vectorList.Count];
            for (int i = 0; i < vectorList.Count; i++)
            {
                vectorArray[i] = vectorList[i];
            }
            return vectorArray;
        }

        /// <summary>
        /// Changes value of 2D array to list
        /// </summary>
        public List<List<MapCell>> arrayToList(MapCell[,] nodeArray)
        {
            List<List<MapCell>> nodeCellList = new List<List<MapCell>>();

            //Add nodes to List via 2D Array
            for (int row = 0; row < nodeArray.GetLength(0); row++)
            {
                List<MapCell> newCol = new List<MapCell>();
                for (int col = 0; col < nodeArray.GetLength(1); col++)
                {
                    newCol.Add(nodeArray[row, col]);
                    //nodeCellList[i][j] = nodeArray[i, j];
                }
                nodeCellList.Add(newCol);
            }

            return nodeCellList;
        }

        /// <summary>
        /// Sets a new map (replaces existing one, if any)
        /// </summary>
        /// <param name="nodeArray"></param>
        public void SetMap(MapCell[,] nodeArray)
        {
            nodeCells = arrayToList(nodeArray);
            mapSizeX = nodeArray.GetLength(0);
            mapSizeZ = nodeArray.GetLength(1);
            zoneOffset = nodeArray[0, 0].Location;
        }

        /// <summary>
        /// Sets a new map (replaces existing one, if any)
        /// </summary>
        /// <param name="map"></param>
        public void SetMap(List<List<MapCell>> map)
        {
            nodeCells = map;
            mapSizeX = map.Count;//map.GetLength(0);
            mapSizeZ = map[0].Count;//map.GetLength(1);
            zoneOffset = map[0][0].Location;
        }

        /// <summary>
        /// Trims the pathway to a suitable size based on currentAP
        /// </summary>
        /// <param name="pathway"></param>
        /// <param name="currentAP"></param>
        /// <returns></returns>
        public List<MapCell> TrimPath(List<MapCell> pathway, int currentAP)
        {
            List<MapCell> newPath = new List<MapCell>();
            if (currentAP >= pathway.Count)
                newPath = pathway;
            else
            {
                //Adds in as many nodes as current AP, backtracking from middle to start
                for (int i = currentAP; i > 0; i--)
                {
                    newPath.Add(pathway[pathway.Count - i]);
                }
            }

            if (newPath.Count > 0)
            {
                //While last node in path is blocked
                while (newPath[0].Ent != null)
                {
                    //Remove it
                    newPath.RemoveAt(0);
                    if (newPath.Count == 0)
                        break;
                }
            }

            return newPath;
        }
        public List<MapCell> TrimPath(List<MapCell> pathway, int currentAP, Unit unit)
        {
            List<MapCell> newPath = new List<MapCell>();
            if (currentAP >= pathway.Count)
                newPath = pathway;
            else
            {
                //Adds in as many nodes as current AP, backtracking from middle to start
                for (int i = currentAP; i > 0; i--)
                {
                    newPath.Add(pathway[pathway.Count - i]);
                }
            }

            while (newPath.Count > 0)
            {
                //While last node in path is blocked
                if (newPath[0].Ent != null)
                {
                    if (newPath[0].Ent == (Entity)unit)
                    {
                        return newPath;
                    }
                    else
                    {
                        newPath.RemoveAt(0);
                    }
                }
                else
                {
                    return newPath;
                }
            }

            return newPath;
        }

        /// <summary>
        /// Check which direction is a node from another. Read it as EndNode is on StartNode's "Insert Direction Here"
        /// </summary>
        /// <param name="startNode"></param>
        /// <param name="endNode"></param>
        /// <param name="distanceFromStart"></param>
        /// <returns></returns>
        public Direction checkInLineDirection(MapCell startNode, MapCell endNode, out int distanceFromStart)
        {
            Direction newDirection = Direction.None;
            distanceFromStart = 0;
            if (startNode.Location.X == endNode.Location.X)
            {
                distanceFromStart = Math.Abs((int)(startNode.Location.Y - endNode.Location.Y));
                if (startNode.Location.Y >= endNode.Location.Y)
                    newDirection = Direction.Up;
                else
                    newDirection = Direction.Down;
            }
            else if (startNode.Location.Y == endNode.Location.Y)
            {
                distanceFromStart = Math.Abs((int)(startNode.Location.X - endNode.Location.X));
                if (startNode.Location.X >= endNode.Location.X)
                    newDirection = Direction.Left;
                else
                    newDirection = Direction.Right;
            }
            else
            {
                distanceFromStart = getManhattanDistance(startNode, endNode);
                if (Math.Abs((int)(startNode.Location.Y - endNode.Location.Y)) >= Math.Abs((int)(startNode.Location.X - endNode.Location.X)))
                {
                    if (startNode.Location.Y >= endNode.Location.Y)
                        newDirection = Direction.Up;
                    else
                        newDirection = Direction.Down;
                }
                else
                {
                    if (startNode.Location.X >= endNode.Location.X)
                        newDirection = Direction.Left;
                    else
                        newDirection = Direction.Right;
                }
            }
            return newDirection;
        }

        /// <summary>
        /// Checks if two units are connected by a single straight line
        /// </summary>
        /// <param name="attacker"></param>
        /// <param name="defender"></param>
        /// <returns></returns>
        public bool isInLine(Unit attacker, Unit defender)
        {
            bool inLine = false;
            Vector2 attTLCords, attBRCords, defTLCords, defBRCords;
            attTLCords = attacker.MyLocation.Location;
            attBRCords = new Vector2(attacker.MyLocation.Location.X + attacker.Size.X - 1, attacker.MyLocation.Location.Y + attacker.Size.Y - 1);
            defTLCords = defender.MyLocation.Location;
            defBRCords = new Vector2(defender.MyLocation.Location.X + defender.Size.X - 1, defender.MyLocation.Location.Y + defender.Size.Y - 1);

            for (int i = (int)attTLCords.X; i <= attBRCords.X; i++)
            {
                for (int j = (int)defTLCords.X; j <= defBRCords.X; j++)
                {
                    if (i == j)
                    {
                        inLine = true;
                        break;
                    }
                }
                if (inLine == true)
                    break;
            }

            if (!inLine)
            {
                for (int i = (int)attTLCords.Y; i <= attBRCords.Y; i++)
                {
                    for (int j = (int)defTLCords.Y; j <= defBRCords.Y; j++)
                    {
                        if (i == j)
                        {
                            inLine = true;
                            break;
                        }
                    }
                    if (inLine == true)
                        break;
                }
            }

            return inLine;
        }

        public bool isInLine(Unit unit, MapCell location, Vector2 size)
        {
            bool inLine = false;
            Vector2 attTLCords, attBRCords, defTLCords, defBRCords;
            attTLCords = unit.MyLocation.Location;
            attBRCords = new Vector2(unit.MyLocation.Location.X + unit.Size.X - 1, unit.MyLocation.Location.Y + unit.Size.Y - 1);
            defTLCords = location.Location;
            defBRCords = new Vector2(location.Location.X + size.X - 1, location.Location.Y + size.Y - 1);

            for (int i = (int)attTLCords.X; i <= attBRCords.X; i++)
            {
                for (int j = (int)defTLCords.X; j <= defBRCords.X; j++)
                {
                    if (i == j)
                    {
                        inLine = true;
                        break;
                    }
                }
                if (inLine == true)
                    break;
            }

            if (!inLine)
            {
                for (int i = (int)attTLCords.Y; i <= attBRCords.Y; i++)
                {
                    for (int j = (int)defTLCords.Y; j <= defBRCords.Y; j++)
                    {
                        if (i == j)
                        {
                            inLine = true;
                            break;
                        }
                    }
                    if (inLine == true)
                        break;
                }
            }

            return inLine;
        }
        public List<MapCell> CellStraightLineCheck (MapCell cell, int range){
            Vector2 cellCoords = cell.Location;
            List<MapCell> toReturn = new List<MapCell>();
            /*
            for (int i = 0; i <= range; i++)
            {
                for (int j = 0; j <= range; j++)
                {
                    if (j + i <= range)
                        if (cellCoords.X - i > 0 && cellCoords.Y + i < map.gridNodes.GetLength(1))
                            toReturn.Add(map.gridNodes[(int)cellCoords.X - i, (int)cellCoords.Y + j]);
                }
            }*/


            
            for (int i = 0; i <= range; i++) {

                if (cellCoords.X - i > 0) {
                    toReturn.Add(map.gridNodes[(int)cellCoords.X - i , (int)cellCoords.Y]);
                }
                if (cellCoords.X + i < map.gridNodes.GetLength(0)){
                    toReturn.Add(map.gridNodes[(int)cellCoords.X + i, (int)cellCoords.Y]);
                }

                if (cellCoords.Y - i > 0)
                {
                    toReturn.Add(map.gridNodes[(int)cellCoords.X, (int)cellCoords.Y - i]);
                }
                if (cellCoords.Y + i < map.gridNodes.GetLength(1))
                {
                    toReturn.Add(map.gridNodes[(int)cellCoords.X, (int)cellCoords.Y + i]);
                }

            }


            return toReturn;
        }
        public List<MapCell> CheckEntitiesInRadiusTest(MapCell cell, int radius)
        {
            Vector2 cellCoords = cell.Location;
            List<MapCell> toReturn = new List<MapCell>();
            for (int i = 0; i <= radius; i++)
            {
                for (int j = 0; j <= radius; j++)
                {
                    if (j + i <= radius) //radius check
                    {
                        if (cellCoords.X - i > 0 && cellCoords.Y + i < map.gridNodes.GetLength(1)){
                            toReturn.Add(map.gridNodes[(int)cellCoords.X - i, (int)cellCoords.Y + j]);
                        }
                    }
                }

            }
            return toReturn;
            /*Vector2 cellCoords = cell.Location;
            List<MapCell> toReturn = new List<MapCell>();
            for (int i = 0; i <= radius; i++)
            {
                for (int j = 0; j <= radius; j++)
                {
                    if (j + i <= radius)
                        if (cellCoords.X - i > 0 && cellCoords.Y + i < map.gridNodes.GetLength(1))
                            if ((map.gridNodes[(int)cellCoords.X - i, (int)cellCoords.Y + j]).Ent != null)
                                if ((map.gridNodes[(int)cellCoords.X - i, (int)cellCoords.Y + j]).Ent != cell.Ent)
                                    return true;
                }

            }
            return false;*/
        }

        public bool CheckEntitiesInRadius(MapCell cell, int radius)
        {
            Vector2 cellCoords = cell.Location;
            List<MapCell> toReturn = new List<MapCell>();
            for (int i = 0; i <= radius; i++)
            {
                for (int j = 0; j <= radius; j++) 
                {
                    if (j + i <= radius) //radius check
                        if (cellCoords.X - i > 0 && cellCoords.Y + j < map.gridNodes.GetLength(1)) 
                            if ((map.gridNodes[(int)cellCoords.X - i, (int)cellCoords.Y + j]).Ent != null) //check if occuppied
                                if ((map.gridNodes[(int)cellCoords.X - i, (int)cellCoords.Y + j]).Ent != cell.Ent && ((map.gridNodes[(int)cellCoords.X - i, (int)cellCoords.Y + j]).Ent.MyType)!= Entity.uType.Player) //check agaisnt itself & ignore players for sorting
                                    if (((map.gridNodes[(int)cellCoords.X - i, (int)cellCoords.Y + j]).Ent.MyType) != Entity.uType.Catch )
                                    return true;
                }

            }
            return false;
            /*Vector2 cellCoords = cell.Location;
            List<MapCell> toReturn = new List<MapCell>();
            for (int i = 0; i <= radius; i++)
            {
                for (int j = 0; j <= radius; j++)
                {
                    if (j + i <= radius)
                        if (cellCoords.X - i > 0 && cellCoords.Y + i < map.gridNodes.GetLength(1))
                            if ((map.gridNodes[(int)cellCoords.X - i, (int)cellCoords.Y + j]).Ent != null)
                                if ((map.gridNodes[(int)cellCoords.X - i, (int)cellCoords.Y + j]).Ent != cell.Ent)
                                    return true;
                }

            }
            return false;*/
        }

        public bool CellSortCheck(MapCell cell, int range)
        {
            Vector2 cellCoords = cell.Location;
            List<MapCell> toReturn = new List<MapCell>();
            for (int i = 0; i <= range; i++)
            {

                if (cellCoords.X - i > 0)
                {
                    if ((map.gridNodes[(int)cellCoords.X - i, (int)cellCoords.Y]).Ent != null)
                    {
                        if ((map.gridNodes[(int)cellCoords.X - i, (int)cellCoords.Y]).Ent != cell.Ent)
                            return true;
                    }
                }
                if (cellCoords.Y + i < map.gridNodes.GetLength(1))
                {
                    if ((map.gridNodes[(int)cellCoords.X, (int)cellCoords.Y + i]).Ent != null)
                    {
                        if ((map.gridNodes[(int)cellCoords.X, (int)cellCoords.Y + i]).Ent != cell.Ent)
                            return true;
                    }
                }

            }

            return false;
            //return toReturn;
        }
        public List<MapCell> getUnitsInRadius(MapCell nodeStart, int radiusSize)
        {
            List<MapCell> unitLocations = new List<MapCell>();
            MapCell startNode = nodeStart;

            resetValues();
            List<MapCell> openListLoc = new List<MapCell>();
            List<MapCell> closedListLoc = new List<MapCell>();
            openListLoc.Add(startNode);

            while (openListLoc.Count > 0)
            {
                currentNodeCell = getBestNode(openListLoc); //Get node with lowest f Value
                //if (currentNodeCell != null) //Nodepath exists
                //{
                if (currentNodeCell.DistanceTotal > radiusSize) //Cannot attack past max range
                {
                    break;
                }
                if (openListLoc.Count > 1)
                    openListLoc = sortNodes(openListLoc);
                openListLoc.RemoveAt(0);
                closedListLoc.Add(currentNodeCell); // Add node to confirmed row

                List<MapCell> neighbours = getNeighbours(currentNodeCell, startNode, true);

                foreach (MapCell node in neighbours)
                {
                    float g = currentNodeCell.DistanceMoved + (1 * node.Cost);
                    float h = 0;
                    float f = g + h;

                    if (closedListLoc.Contains(node) && f >= node.DistanceTotal) //If confirmed path has node and f value higher than current
                    {
                        continue; //Skips the loop
                    }

                    if (!openListLoc.Contains(node) || f < node.DistanceTotal) //If open list does not have node or totalDistance is less than current
                    {
                        node.ParentLocation = currentNodeCell;
                        node.updateDistance((int)g, (int)h);
                        if (!openListLoc.Contains(node)) //If open list does not have node ONLY
                        {
                            //add to list
                            openListLoc.Add(node);
                        }
                    }
                }
            }

            for (int i = 0; i < closedListLoc.Count; i++)
            {
                if (closedListLoc[i].Ent != null)
                {
                    unitLocations.Add(closedListLoc[i]);
                }
            }

            unitLocations.Remove(startNode);
            return unitLocations;

        }
    }
}