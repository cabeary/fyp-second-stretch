﻿
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameStateManagement;

namespace BrokenLands
{
    public class CameraObj
    {
        public Vector3 VCameraOffset,VCameraPosition,VCameraDirection,VCameraUp;
        public float FCameraSpeed;
        public Matrix world,view, proj;
        public float FCameraNearPlane,FCameraFarPlane;
        public bool orthographic;
        public Vector3 target;
        public float scale = 1;
        public Vector3 RotatedTarget {
            get
            {
                
                Vector3 rotated = Vector3.Transform(target, Matrix.CreateRotationY(-MathHelper.PiOver4));
                // VCameraPosition = rotated;

                return rotated;
            }
        }
        public Vector3 RotatedPosDiff {
            get
            {
                Vector3 rotated = Vector3.Transform(posdiff, Matrix.CreateScale(scale));
                rotated = Vector3.Transform(posdiff, Matrix.CreateRotationY(-MathHelper.PiOver4));
                
                // VCameraPosition = rotated;

                return rotated;
            }
        }
        public Vector3 distance;
        public Vector3 RotatedCameraView
        {
            get
            {
                return VCameraPosition;
                Vector3 rotated = Vector3.Transform(VCameraPosition, Matrix.CreateRotationY(-MathHelper.PiOver4));
                // VCameraPosition = rotated;

                return rotated;
            }
        }
        public void PanCamera(Vector3 vec) {
            //VCameraPosition += vec;
            target += vec;
        }
        public void ChangeTarget(Vector3 target) {
            this.target = Vector3.Transform(target, Matrix.CreateRotationY(MathHelper.PiOver4));;
            //Vector3 temp;
            //temp = posdiff + target ;
            //VCameraPosition = Vector3.Transform(temp, Matrix.CreateRotationY(MathHelper.PiOver4));
        }
        public Vector3 posdiff;
        public CameraObj(GameScreen game, Vector3 pos, Vector3 target, Vector3 up,float Near,float Far,float speed, bool orthographic)
        {
            this.orthographic = orthographic;
            this.target = target;
            posdiff = pos - target;
            VCameraPosition = pos - target;
            if (orthographic)
                VCameraDirection = target - RotatedCameraView;
            else
                VCameraDirection = pos - target;
            VCameraDirection.Normalize();
            VCameraUp = up;
            FCameraNearPlane = Near;
            FCameraFarPlane = Far;
            FCameraSpeed = speed;
            world = Matrix.Identity;


            distance = target - VCameraPosition;

            if (orthographic)
            {
                view = Matrix.CreateLookAt(target + posdiff, target, VCameraUp);
                proj = Matrix.CreateOrthographic(game.ScreenManager.GraphicsDevice.Viewport.Width * 0.11f, game.ScreenManager.GraphicsDevice.Viewport.Height * 0.11f, FCameraNearPlane, FCameraFarPlane);

            }
            else
            {
                view = Matrix.CreateLookAt(VCameraPosition, target, VCameraUp);
                proj = Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4, game.ScreenManager.GraphicsDevice.Viewport.AspectRatio, FCameraNearPlane, FCameraFarPlane);
            }
        }

        public bool inView(BoundingBox boundingBox)
        {
            return new BoundingFrustum(view * proj).Intersects(boundingBox);
        }


        public void CreateLookAt()
        {

            if (orthographic)
                {
                    // maintain the camera angle at 30 degree, 45 degree


                    //VCameraPosition.Z = (float)((VCameraPosition.Y - target.Y) / (Math.Tan(MathHelper.ToRadians(30))));

                    view = Matrix.CreateLookAt(RotatedTarget + RotatedPosDiff, RotatedTarget, VCameraUp);
                }
                else
                {
                    view = Matrix.CreateLookAt(VCameraPosition, VCameraPosition + VCameraDirection, VCameraUp);
                }
        }



    }
    

}
