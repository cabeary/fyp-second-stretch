﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace BrokenLands
{
    public class SpriteSheetReader
    {
        //this class is used to convert frame numbers to the spritesheet frame positions eg: No:0 = pos:(0,0)
        //for the sake of convenience, a method to handle animations playing/speed/loop/etc will be added here as well

        public int frameWidth, frameHeight;
        public Texture2D texture, texture2;
        public bool drawTexture2 = false;
        public string animationName = "";
        public float delay;
        public int[] currAnimation; // the current animation
        public int frameX, frameY;
        public int frameIndex;//frameIndex
        Rectangle source;
        public bool looping = true;
        float elapsed;//time elapsed
        int lastframeX;//Max Frame No X 
        int lastframeY;
        public bool pause = false;

        public SpriteSheetReader(Texture2D texture,int framewidth,int frameheight,int lengthX,int lengthY) {
            this.texture = texture;
            frameWidth = framewidth;
            frameHeight = frameheight;
            lastframeX = lengthX;
            lastframeY = lengthY;
            elapsed = 0;
        }

        public void setAnimation(int[] animation,float delay,bool loop){
            currAnimation=animation;
            looping=loop;
            this.delay = delay;
        }

        public void Update(GameTime gameTime)
        {
            if(!pause)
            elapsed += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
            framepos(currAnimation[frameIndex]);
            PlayAnimation();

            source = new Rectangle(frameWidth * frameX, frameHeight * frameY, frameWidth, frameHeight);
        }
        public void Draw(SpriteBatch spritebatch,Vector2 pos,Color color,float rotation,Vector2 origin,float scale, SpriteEffects effect)
        {
            
            spritebatch.Draw(texture, pos, source, color, rotation, origin, scale, effect, 0f);
            if (drawTexture2) {
                spritebatch.Draw(texture2, pos, source, color, rotation, origin, scale, effect, 0f);
            }
        }
        

        void framepos(int frameNo)
        {
            
            int frame = frameNo;
            frameX = 0;
            frameY = 0;
            for (int i = 0; i <= lastframeY; i++)
            {
                if (frame <= lastframeX)
                {
                    frameX = frame;
                    frameY = i;
                    break;
                }
                else
                {
                    frame -= lastframeX;
                    frame--;
                }
            }

        }
        Dictionary<int, string> frameSound = new Dictionary<int, string>();
        public void AddSoundAtFrame(int frame, string soundClipName) {
            frameSound.Add(frame, soundClipName);
        }

        void PlayAnimation()
        {
            if (looping && frameIndex == currAnimation.Length - 1)
            {
                frameIndex = 0;
            }
            if (elapsed > delay&&!pause)
            {
                if (frameIndex < currAnimation.Length - 1)
                {
                    frameIndex++;
                    foreach(KeyValuePair<int,string> frameSounds in frameSound){
                        if (frameSounds.Key == frameIndex) {
                            GameSession.gameAudioEngine.playSound(frameSounds.Value);
                        }
                    }
                    //if (frameIndex == 1) GameSession.gameAudioEngine.playSound("GunFire");
                
                
                }
                elapsed = 0;
            }

        }
    }
}
