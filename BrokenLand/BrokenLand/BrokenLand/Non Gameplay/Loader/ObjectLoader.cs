﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.IO;


//TODO: Write an AI number
namespace BrokenLands
{
    class ObjectLoader
    {
        //add the write routine for updated actors
        //add reward loading for quests

        //Check if the current tile is already occupied.

        //HIW-> HOW IT WORKS

        //****Description****
        //takes in metadata
        //METADATA: INT TYPE, INT LINE NO,VEC2 NODEPOSITION
        //HIW(Type):Tells it the number of lines to retrieve and which file to retrieve from (enemy.data,etc)
        //HIW(Line):Which line to start at (object property position)
        //HIW(Node positon):The position to place the object in the map (NA to scenes)
        //spits out object classes to the handle
        MapGenerator handle;
        public static string path = "../../../../../brokenland/brokenlandcontent/";

        public ObjectLoader(MapGenerator handle)
        {
            this.handle = handle;
        }

        public Entity fetchObject(int lineNo, int type, MapCell Location)
        {
            if (type == 0)
            {
                //return enemy 
                //load enemies
                //format texture, rotattion, strength, dexterity, intelligence,charisma, endurance, agility, node
                //skip linoNo,take x number of lines,order->first
                string modelName = File.ReadLines(path + "ObjectData/enemies.data").Skip(lineNo - 1).Take(1).First();
                int rotation = int.Parse(File.ReadLines(path + "ObjectData/enemies.data").Skip(lineNo).Take(1).First());

                int XSize = int.Parse(File.ReadLines(path + "ObjectData/enemies.data").Skip(lineNo + 1).Take(1).First());
                int YSize = int.Parse(File.ReadLines(path + "ObjectData/enemies.data").Skip(lineNo + 2).Take(1).First());

                int strength = int.Parse(File.ReadLines(path + "ObjectData/enemies.data").Skip(lineNo + 3).Take(1).First());
                int dext = int.Parse(File.ReadLines(path + "ObjectData/enemies.data").Skip(lineNo + 4).Take(1).First());
                int intelligence = int.Parse(File.ReadLines(path + "ObjectData/enemies.data").Skip(lineNo + 5).Take(1).First());
                int charisma = int.Parse(File.ReadLines(path + "ObjectData/enemies.data").Skip(lineNo + 6).Take(1).First());
                int endurance = int.Parse(File.ReadLines(path + "ObjectData/enemies.data").Skip(lineNo + 7).Take(1).First());
                int agility = int.Parse(File.ReadLines(path + "ObjectData/enemies.data").Skip(lineNo + 8).Take(1).First());
                int AItype = int.Parse(File.ReadLines(path + "ObjectData/enemies.data").Skip(lineNo + 9).Take(1).First());

                System.Diagnostics.Debug.WriteLine(modelName);

                //model = handle.game.Content.Load<Model>(modelName);
                Texture2D texture = handle.game.Content.Load<Texture2D>("Units/"+modelName);
                MapCell node = Location;

                Vector3 position = node.getPositon();
                Enemy enemy = new Enemy(strength, dext, intelligence, charisma, endurance, agility, node, texture, lineNo, modelName, AItype);
                enemy.Size = new Vector2(XSize, YSize);
                
                return enemy;

            }
            else if (type > 0)
            {
                //return overlay
                string modelName = File.ReadLines(path + "ObjectData/overlays.data").Skip(lineNo - 1).Take(1).First();
                System.Diagnostics.Debug.WriteLine(modelName);

                Texture2D texture2 = handle.game.Content.Load<Texture2D>("MapObjects/"+modelName);

                //get middleNode
                MapCell node = Location;

                //vect position2d = node.getPositon();
                return new SceneObject(texture2, lineNo, node);
            }
            else
            {
                return null;
            }

        }

        public void loadObject(int lineNo, int type, Vector2 nodePos)
        {
            string modelName = "";
            MapCell node;
            Vector3 position;
            Model model;


            switch (type)
            {
                //scenes take in vec2 while everything else uses nodes

                case 0:
                    //load enemies
                    //format texture, rotattion, strength, dexterity, intelligence,charisma, endurance, agility, node
                    //skip linoNo,take x number of lines,order->first


                    modelName = File.ReadLines(path + "ObjectData/enemies.data").Skip(lineNo - 1).Take(1).First();
                    int rotation = int.Parse(File.ReadLines(path + "ObjectData/enemies.data").Skip(lineNo).Take(1).First());

                    //string test = File.ReadLines(path + "ObjectData/enemies.data").Skip(lineNo + 1).Take(1).First();


                    int XSize = int.Parse(File.ReadLines(path + "ObjectData/enemies.data").Skip(lineNo + 1).Take(1).First());
                    int YSize = int.Parse(File.ReadLines(path + "ObjectData/enemies.data").Skip(lineNo + 2).Take(1).First());

                    int strength = int.Parse(File.ReadLines(path + "ObjectData/enemies.data").Skip(lineNo + 3).Take(1).First());
                    int dext = int.Parse(File.ReadLines(path + "ObjectData/enemies.data").Skip(lineNo + 4).Take(1).First());
                    int intelligence = int.Parse(File.ReadLines(path + "ObjectData/enemies.data").Skip(lineNo + 5).Take(1).First());
                    int charisma = int.Parse(File.ReadLines(path + "ObjectData/enemies.data").Skip(lineNo + 6).Take(1).First());
                    int endurance = int.Parse(File.ReadLines(path + "ObjectData/enemies.data").Skip(lineNo + 7).Take(1).First());
                    int agility = int.Parse(File.ReadLines(path + "ObjectData/enemies.data").Skip(lineNo + 8).Take(1).First());
                    int AItype = int.Parse(File.ReadLines(path + "ObjectData/enemies.data").Skip(lineNo + 9).Take(1).First());

                    System.Diagnostics.Debug.WriteLine(modelName);

                    //model = handle.game.Content.Load<Model>(modelName);

                    Texture2D texture = handle.game.Content.Load<Texture2D>("Units/"+modelName);
                    node = handle.handle.gridNodes[(int)nodePos.X, (int)nodePos.Y];

                    position = node.getPositon();
                    Enemy enemy = new Enemy(strength, dext, intelligence, charisma, endurance, agility, node, texture, lineNo, modelName, AItype);
                    //EXPERIMENTAL
                    enemy.Size = new Vector2(1, 1);
                    handle.handle.enemList.Add(enemy);
                    break;
                case 1:
                    //load obstacles

                    modelName = File.ReadLines(path + "ObjectData/objects.data").Skip(lineNo - 1).Take(1).First();
                    System.Diagnostics.Debug.WriteLine(modelName);

                    //int health = int.Parse(File.ReadLines(path + "ObjectData/objects.data").Skip(lineNo).Take(1).First());
                    //int defence = int.Parse(File.ReadLines(path + "ObjectData/objects.data").Skip(lineNo + 1).Take(1).First());

                    texture = handle.game.Content.Load<Texture2D>("MapObjects/"+modelName);
                    node = handle.handle.gridNodes[(int)nodePos.X, (int)nodePos.Y];

                    position = node.getPositon();
                    Vector2 position2D = new Vector2(position.X, position.Z);
                    handle.handle.obstacleList.Add(new Obstacle(texture, position2D, lineNo));
                    break;


                case 2:
                    //load scene. Scene now refers to deco object in a map

                    modelName = File.ReadLines(path + "ObjectData/scenes.data").Skip(lineNo - 1).Take(1).First();
                    int rot = int.Parse(File.ReadLines(path + "ObjectData/scenes.data").Skip(lineNo).Take(1).First());
                    int XSize2 = int.Parse(File.ReadLines(path + "ObjectData/scenes.data").Skip(lineNo+1).Take(1).First());
                    int YSize2 = int.Parse(File.ReadLines(path + "ObjectData/scenes.data").Skip(lineNo + 2).Take(1).First());
                    System.Diagnostics.Debug.WriteLine(modelName);

                    Texture2D Scenetexture = handle.game.Content.Load<Texture2D>("MapObjects/"+modelName);

                    //get middleNode
                    node = node = handle.handle.gridNodes[(int)nodePos.X, (int)nodePos.Y];

                    //vect position2d = node.getPositon();
                    SceneObject scene = new SceneObject(Scenetexture, lineNo, node);
                    scene.Size = new Vector2(XSize2, YSize2);

                    if (rot == 0)
                        scene.MyType = Entity.uType.Other;
                    else
                        scene.MyType = Entity.uType.Catch;

                    handle.handle.sceneList.Add(scene);
                    break;

                case 3:
                    //load playernodes
                    if (handle.playerNode.Count() < 3)
                    {
                        handle.playerNode.Add(nodePos);
                        System.Diagnostics.Debug.WriteLine("Player added:" + nodePos);
                    }
                    else
                    {
                        System.Diagnostics.Debug.WriteLine("Max number of players achieved");
                    }
                    break;

                case 4://load actor -> dialogtree -> dialogs ->groups
                    //load actor texture and position

                    modelName = File.ReadLines(path + "ObjectData/actors.data").Skip(lineNo - 1).Take(1).First();

                    System.Diagnostics.Debug.WriteLine(modelName);

                    //model = handle.game.Content.Load<Model>(modelName);
                    Texture2D texture2 = handle.game.Content.Load<Texture2D>("Actors/" + modelName);
                    node = handle.handle.gridNodes[(int)nodePos.X, (int)nodePos.Y];

                    position = node.getPositon();

                    rotation = int.Parse(File.ReadLines(path + "ObjectData/actors.data").Skip(lineNo - 1 + 1).Take(1).First());

                    int XSize1 = int.Parse(File.ReadLines(path + "ObjectData/actors.data").Skip(lineNo - 1 + 2).Take(1).First());
                    int YSize1 = int.Parse(File.ReadLines(path + "ObjectData/actors.data").Skip(lineNo - 1 + 3).Take(1).First());

                    int questID = int.Parse(File.ReadLines(path + "ObjectData/actors.data").Skip(lineNo - 1 + 4).Take(1).First());
                    int loadID = int.Parse(File.ReadLines(path + "ObjectData/actors.data").Skip(lineNo - 1 + 5).Take(1).First());
                    //string test = File.ReadLines(path + "ObjectData/actors.data").Skip(lineNo - 1 + 4).Take(1).First();
                    int loadtype = int.Parse(File.ReadLines(path + "ObjectData/actors.data").Skip(lineNo - 1 + 6).Take(1).First());

                    System.Diagnostics.Debug.WriteLine("Condition:" + loadID + " Quest:" + questID);
                    Entity loadObject = fetchObject(loadID, loadtype, node);  //temp value 0. replace w true/false
                    
                    Actor actor = new Actor(modelName, questID, loadObject, texture2, node, lineNo);
                    actor.Size = new Vector2(XSize1, YSize1);

                    //string name,int questID,int conditionID,Texture2D texture, Vector2 position,int lineNo
                    //load dialogs -> no of messages -> rest of the stuff
                    //Dialog("What do you want,punk? ", new Lock(-1, 0, true, 0), 0, 1, new Action(0, 0, 0, 0), true)



                    string dialogfile = File.ReadLines(path + "ObjectData/actors.data").Skip(lineNo - 1 + 7).Take(1).First();
                    
                    
                    System.Diagnostics.Debug.WriteLine("Dialogfile:" + dialogfile);

                    actor.fileName = dialogfile;

                    string dialogpath;

                    if (handle.newmap)
                        dialogpath = "ObjectData/Dialog/";
                    else
                        dialogpath = "ObjectData/Dialog/SavedDialog/";

                    using (StreamReader reader = new StreamReader(path + dialogpath + dialogfile + ".dialog"))
                    {
                        int dialogcount = int.Parse(reader.ReadLine());

                        for (int i = 0; i < dialogcount; i++)
                        {

                            //    //load message
                            string message = reader.ReadLine();


                            //    //load lock
                            //type req met value
                            string test = reader.ReadLine();
                            int locktype = int.Parse(test);
                            int requirement = int.Parse(reader.ReadLine());
                            bool met = bool.Parse(reader.ReadLine());
                            int value = int.Parse(reader.ReadLine());

                            //    //load group and next group
                            int group = int.Parse(reader.ReadLine());
                            int nextgroup = int.Parse(reader.ReadLine());

                            //    //load action
                            //Action(int action, int outcome, int value, int condition)
                            int action = int.Parse(reader.ReadLine());
                            int value1 = int.Parse(reader.ReadLine());


                            //    //load is teller
                            bool teller = bool.Parse(reader.ReadLine());

                            Dialog d = new Dialog(message, new Lock(locktype, requirement, met, value), group, nextgroup, new Action(action, value1), teller);

                            if (handle.newmap && d.locK.getMet())
                                d.locK.prevmet = true;

                            //    //add dialog 
                            actor.dialogtree.dialogList.Add(d);

                        }
                        actor.printDialog();
                        //load group
                        int groupcount = int.Parse(reader.ReadLine());
                        for (int i = 0; i < groupcount; i++)
                        {
                            //    //load lock
                            //type req met value
                            int locktype = int.Parse(reader.ReadLine());
                            int requirement = int.Parse(reader.ReadLine());
                            bool met = bool.Parse(reader.ReadLine());
                            int value = int.Parse(reader.ReadLine());

                            //load group no
                            int group = int.Parse(reader.ReadLine());
                            //add group

                            
                            Group g = new Group(new Lock(locktype, requirement, met, value), group);

                            if (met && handle.newmap)
                                g.locK.prevmet = true;

                            actor.dialogtree.defaultgroupList.Add(g);

                            
                        }

                    }



                    //add actor to list
                    handle.handle.actorList.Add(actor);
                    break;

                case 5:
                    //refine the quest objects for desc and additional info
                    //load quest -> load conditions QuestObject(string title,int lineNo)

                    //load quest name
                    string questName = File.ReadLines(path + "ObjectData/quests.data").Skip(lineNo - 1).Take(1).First();
                    //quest number = lineNo

                    QuestObject quest = new QuestObject(questName, lineNo);

                    //open file by name
                    using (StreamReader reader = new StreamReader(path + "ObjectData/Quest/" + questName + ".quest"))
                    {
                        //load quest desc
                        string desc = reader.ReadLine();
                        quest.desc = desc;

                        //fetch condition count
                        int conditionCount = int.Parse(reader.ReadLine());
                        //loop condition
                        for (int i = 0; i < conditionCount; i++)
                        {
                            string conditionName = reader.ReadLine();
                            //conditionID will be assigned by list index
                            Condition condition = new Condition(conditionName);
                            //add condition to quest                            
                            quest.conditionList.Add(condition);
                        }
                    }

                    //add in section for rewards(weapons,reward,experience,etc)

                    //add quest
                    quest.printQuest();
                    QuestManager.questList.Add(quest);
                    break;

                case 6: //load active quests
                    break;
                case 7: // dungeon AI
                    modelName = File.ReadLines(path + "ObjectData/ais.data").Skip(lineNo - 1).Take(1).First();
                    string map = File.ReadLines(path + "ObjectData/ais.data").Skip(lineNo).Take(1).First();

                    System.Diagnostics.Debug.WriteLine(modelName);

                    //model = handle.game.Content.Load<Model>(modelName);
                    texture2 = handle.game.Content.Load<Texture2D>("Units/"+modelName);
                    node = handle.handle.gridNodes[(int)nodePos.X, (int)nodePos.Y];

                    handle.handle.aiList.Add(new DungeonAI(texture2, lineNo, node, map));
                    break;
                case 8: //floor
                    modelName = File.ReadLines(path + "ObjectData/floors.data").Skip(lineNo - 1).Take(1).First();

                    System.Diagnostics.Debug.WriteLine(modelName);

                    //model = handle.game.Content.Load<Model>(modelName);
                    texture2 = handle.game.Content.Load<Texture2D>("MapObjects/" + modelName);

                    handle.handle.floorTexture = texture2;
                    handle.handle.floorObject = new FloorObject(texture2, lineNo);
                    break;

                case 9:
                    modelName = File.ReadLines(path + "ObjectData/overlays.data").Skip(lineNo - 1).Take(1).First();
                    System.Diagnostics.Debug.WriteLine(modelName);

                    texture2 = handle.game.Content.Load<Texture2D>("MapObjects/" + modelName);

                    //get middleNode
                    node = node = handle.handle.gridNodes[(int)nodePos.X, (int)nodePos.Y];

                    //vect position2d = node.getPositon();
                    handle.handle.overlayList.Add(new SceneObject(texture2, lineNo, node));
                    break;

                case 10: // underlay
                    modelName = File.ReadLines(path + "ObjectData/underlays.data").Skip(lineNo - 1).Take(1).First();
                    System.Diagnostics.Debug.WriteLine(modelName);

                    texture2 = handle.game.Content.Load<Texture2D>("MapObjects/"+modelName);

                    //get middleNode
                    node = node = handle.handle.gridNodes[(int)nodePos.X, (int)nodePos.Y];

                    //vect position2d = node.getPositon();
                    handle.handle.underlayList.Add(new SceneObject(texture2, lineNo, node));
                    break;
                case 11://load loot
                    modelName = File.ReadLines(path + "ObjectData/loot.data").Skip(lineNo - 1).Take(1).First();
                    string lootFile = File.ReadLines(path + "ObjectData/loot.data").Skip(lineNo).Take(1).First();
                    System.Diagnostics.Debug.WriteLine(modelName);

                    texture2 = handle.game.Content.Load<Texture2D>("MapObjects/"+modelName);

                    //get middleNode
                    node = node = handle.handle.gridNodes[(int)nodePos.X, (int)nodePos.Y];

                    //vect position2d = node.getPositon();
                    LootContainer loot = new LootContainer(texture2, lineNo, node);

                    string lootpath;
                    if (handle.newmap)
                        lootpath = "ObjectData/Loot/";
                    else
                        lootpath = "ObjectData/Loot/SavedLoot/";

                    loot.fileName = lootFile;

                    //load the loot items
                    using (StreamReader reader = new StreamReader(path + lootpath + lootFile + ".loot"))
                    {
                        //will have to use chu's item loader
                        int count = int.Parse(reader.ReadLine());

                        for (int i = 0; i < count; i++)
                        {
                            // Item(int itemID, String itemName, String desc, int itemType, float val, Texture2D itemTexture = null)

                            int itemID = int.Parse(reader.ReadLine());



                            Item item = EquipmentLoader.fetchItem(itemID);


                            if (item != null)
                                loot.itemList.Add(item);

                            else
                            {
                                System.Diagnostics.Debug.WriteLine("Item is not found");
                            }
                        }
                    }
                    loot.debugLoot();
                    handle.handle.sceneList.Add(loot);

                    break;
                case 12:
                    //Loading full CharacterList
                    List<Player> PlayerList = new List<Player>();

                    using (StreamReader reader = new StreamReader(path + "objectdata/PlayerCharacters.data"))
                    {
                        int PlayerCount = int.Parse(reader.ReadLine());
                        
                        for (int i = 0; i < PlayerCount; i++)
                        {
                            String name = reader.ReadLine();
                            int Str = int.Parse(reader.ReadLine());
                            int Dex = int.Parse(reader.ReadLine());
                            int Int = int.Parse(reader.ReadLine());
                            int Cha = int.Parse(reader.ReadLine());
                            int End = int.Parse(reader.ReadLine());
                            int Agi = int.Parse(reader.ReadLine());
                            int line = int.Parse(reader.ReadLine());
                            int Mel = int.Parse(reader.ReadLine());
                            int Sca = int.Parse(reader.ReadLine());
                            int Loc = int.Parse(reader.ReadLine());
                            int Bar = int.Parse(reader.ReadLine());
                            int Heal = int.Parse(reader.ReadLine());
                            float GPt = float.Parse(reader.ReadLine());
                            int AbilityID = int.Parse(reader.ReadLine());
                            int TextureID = int.Parse(reader.ReadLine());
                            
                            Player newPlayer = new Player(Str, Dex, Int, Cha, End, Agi, line, name);
                            newPlayer.MyStatus.Skills.MeleePts = Mel;
                            newPlayer.MyStatus.Skills.ScavengePts = Sca;
                            newPlayer.MyStatus.Skills.LockpickPts = Loc;
                            newPlayer.MyStatus.Skills.BarterPts = Bar;
                            newPlayer.MyStatus.Skills.HealPts = Heal;
                            newPlayer.MyStatus.Skills.GrowthPts = GPt;
                            newPlayer.MyStatus.AbilityID = AbilityID;
                            
                            if (TextureID < GameSession.GS.CharacterTextureList.Count && TextureID >= 0)
                                newPlayer.Texture = GameSession.GS.CharacterTextureList[TextureID];
                            else
                                newPlayer.Texture = GameSession.GS.CharacterTextureList[0];
                            newPlayer.MyLocation = new MapCell(1, new Vector2(0), new Vector3(0, 0, 0), GameSession.GS.defaultModel, 1);

                            PlayerList.Add(newPlayer);
                            //add party, addweapons
                            //GameSession.party.members.Add(newPlayer);
                            int equipCount = int.Parse(reader.ReadLine());
                            for (int j = 0; j < equipCount; j++)
                            {
                                Equipment eq = EquipmentLoader.fetchEquipment(int.Parse(reader.ReadLine()));//(Equipment)EquipmentLoader.fetchItem(1);//19
                                PlayerList[PlayerList.Count - 1].Equip(eq);
                            }
                            
                            GameSession.map.drawList.Add(newPlayer);
                        }

                        GameSession.players = PlayerList;
                    }
                    break;
                case 13:
                    //Adding individual units
                    using (StreamReader reader = new StreamReader(path + "objectdata/RecruitableCharacters.data"))
                    {
                        int PlayerCount = int.Parse(reader.ReadLine());

                        for (int i = 0; i < PlayerCount; i++)
                        {
                            String name = reader.ReadLine();
                            int Str = int.Parse(reader.ReadLine());
                            int Dex = int.Parse(reader.ReadLine());
                            int Int = int.Parse(reader.ReadLine());
                            int Cha = int.Parse(reader.ReadLine());
                            int End = int.Parse(reader.ReadLine());
                            int Agi = int.Parse(reader.ReadLine());
                            int line = int.Parse(reader.ReadLine());
                            int Mel = int.Parse(reader.ReadLine());
                            int Sca = int.Parse(reader.ReadLine());
                            int Loc = int.Parse(reader.ReadLine());
                            int Bar = int.Parse(reader.ReadLine());
                            int Heal = int.Parse(reader.ReadLine());
                            float GPt = float.Parse(reader.ReadLine());
                            int AbilityID = int.Parse(reader.ReadLine());
                            int TextureID = int.Parse(reader.ReadLine());

                            //lineNo value is the same, add the player, otherwise, move on to next character
                            if (lineNo == i)
                            {
                                Player newPlayer = new Player(Str, Dex, Int, Cha, End, Agi, line, name);
                                newPlayer.MyStatus.Skills.MeleePts = Mel;
                                newPlayer.MyStatus.Skills.ScavengePts = Sca;
                                newPlayer.MyStatus.Skills.LockpickPts = Loc;
                                newPlayer.MyStatus.Skills.BarterPts = Bar;
                                newPlayer.MyStatus.Skills.HealPts = Heal;
                                newPlayer.MyStatus.Skills.GrowthPts = GPt;
                                newPlayer.MyStatus.AbilityID = AbilityID;

                                if (TextureID < GameSession.GS.CharacterTextureList.Count && TextureID >= 0)
                                    newPlayer.Texture = GameSession.GS.CharacterTextureList[TextureID];
                                else
                                    newPlayer.Texture = GameSession.GS.CharacterTextureList[0];
                                newPlayer.MyLocation = new MapCell(1, new Vector2(0), new Vector3(0, 0, 0), GameSession.GS.defaultModel, 1);

                                GameSession.players.Add(newPlayer);

                                //add party, addweapons
                                GameSession.party.members.Add(newPlayer);
                                Equipment eq = EquipmentLoader.equipList[1];//(Equipment)EquipmentLoader.fetchItem(1);//19
                                GameSession.players[GameSession.players.Count - 1].Equip(eq);
                                GameSession.GS.addPlayer(newPlayer, newPlayer.MyLocation);
                                GameSession.map.drawList.Add(newPlayer);
                                break;
                            }
                        }
                    }
                    
                    break;
                
                case 14:
                    using (StreamReader reader = new StreamReader(path + "objectdata/ShopItems.data"))
                    {
                        GameSession.GS.shopItems = new Inventory();
                        int ShopCount = int.Parse(reader.ReadLine());

                        for (int ShopInt = 0; ShopInt < ShopCount; ShopInt++)
                        {
                            int ShopItemCount = int.Parse(reader.ReadLine());
                            for (int ShopItem = 0; ShopItem < ShopItemCount; ShopItem++)
                            {
                                int itemID = int.Parse(reader.ReadLine());

                                GameSession.GS.shopItems.activeList.Add(EquipmentLoader.fetchItem(itemID));
                                
                            }
                        }
                    }
                    break;
                case 15:
                    //Load inventory
                    Inventory newInventory = new Inventory();
                    using (StreamReader reader = new StreamReader(path + "objectdata/Inventory.data"))
                    {
                        int money = int.Parse(reader.ReadLine());
                        newInventory.money = money;
                        int itemCount = int.Parse(reader.ReadLine());
                        for (int i = 0; i < itemCount; i++)
                        {
                            int ID = int.Parse(reader.ReadLine());
                            Item newItem = EquipmentLoader.fetchItem(ID);
                            if (newItem == null) {
                                newItem = new Item (EquipmentLoader.fetchEquipment(ID));
                            }
                            newInventory.activeList.Add(newItem);
                        }
                        GameSession.GS.inventory = newInventory;
                    }
                    break;
                default:

                    //load exit nodes  ---> replace it to grid nodes. Actors can replace this
                    // handle.borderNodes.Add(nodePos);
                    handle.handle.sectionNodes.Add(nodePos);
                   // handle.borderNodes.Add(nodePos);
                    System.Diagnostics.Debug.WriteLine("Exit added");

                    //load exit nodes
                    //handle.mapexit.Add(nodePos);
                    //System.Diagnostics.Debug.WriteLine("Exit added");
                    break;
            }
        }
    }
}
