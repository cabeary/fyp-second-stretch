﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.IO;
using BrokenLands;

namespace BrokenLands
{
    //TODO: build players and exits from node positons

    //split this to map and map generator
    class MapGenerator
    {
        Model defaultTile;

        public bool newmap = false;

        //Node[,] gridNodes; //nodes that hold the actual grid. nodes here will be used for 2x2 objects



        //************************************************************************************
        public ObjectLoader loader; //takes in metadata and spits out objects ->list files

        List<Object> scene;

        //List<Object> obstacleList = new List<Object>();

        public List<Vector2> playerNode = new List<Vector2>();
        public List<Vector2> mapexit = new List<Vector2>();
        public Map handle;

        public Game game;
        //************************************************************************************

        public MapGenerator(Game game, Model model, Map map)
        {
            this.handle = map;
            defaultTile = model;
            loader = new ObjectLoader(this);
            this.game = game;



        }

        //spacing between each node. size of the whole map: in squares
        public void NewMap(float spacing, int size)
        {
            spacing = spacing / 2.0f;
            //this is isometric map for now
            //plane will come later
            MapCell[,] gridNodes = new MapCell[size, size]; //list of each of our nodes

            Vector3 pos = Vector3.Zero; //position for each node
            Vector3 nodePos = Vector3.Zero;

            pos.X = -1 * spacing * (size / 2);
            pos.Z = -1 * spacing * (size / 2);
            float resetZ = pos.Z;
            for (int x = 0; x < size; x++)
            {
                pos.Z = resetZ;
                for (int z = 0; z < size; z++)
                {
                    //int cost, Vector2 location, Vector3 position,Model model, float scale
                    gridNodes[x, z] = new MapCell(1, new Vector2(x, z), pos, defaultTile, 0.15f);//0.05f

                    pos.Z += spacing;
                }
                pos.X += spacing;
            }

            handle.gridNodes = gridNodes;
        }


        public void AddPlayers(List<Player> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                list[i].MyLocation.Ent = null;
                list[i].MyLocation = handle.gridNodes[(int)playerNode[i].X, (int)playerNode[i].Y];
                list[i].SetLocation(list[i].MyLocation);
                //list[i].MyLocation.Ent = list[i];
            }
            handle.playerList = list;
        }

        public void addtoDraw()
        {            //foreach (Unit en in handle.enemList)  //only render dynamically during combat stage
            //{
            //    handle.drawList.Add(en);
            //}


            foreach (Unit p in handle.playerList) //PLAYERS NEED TO BE DYNAMICALLY SORTED
            {
                handle.drawList.Add(p);
            }
        }


        public void setTile(Vector2 nodeDimension, Model model)
        {
            handle.gridNodes[(int)nodeDimension.X, (int)nodeDimension.Y].setTile(model);
        }

        public void debugPlayerNodes()
        {
            //add player to objectList w default texture
            Model defaultPlayer = game.Content.Load<Model>("alienSpike"); //default player model
            foreach (Vector2 player in playerNode)
            {

                handle.debugList.Add(new testObject(defaultPlayer, handle.getTilePosition(player), 0));

            }
        }
        public void debugExits()
        {

            Model defaultExit = game.Content.Load<Model>("alienSpike"); //default exit model
            foreach (Vector2 exit in mapexit)
            {

                handle.debugList.Add(new testObject(defaultExit, handle.getTilePosition(exit), 0));

            }
        }
        //add exits to obstacle list with exit params

        private void createPlayers()
        {
            //add player with player defined texture
        }

        public void getStateFile() {
        }

        //loading
        public void ReadMap(string path,string filename)
        {
            handle.mapName = filename;

            int lineNo = 0, type = 0;
            Vector2 nodePos = Vector2.Zero;
            using (StreamReader reader = new StreamReader(path))
            {

                //call the object loader

                //read grid dimensions
                // NewMap(12f, int.Parse(reader.ReadLine()));
                NewMap(13f, int.Parse(reader.ReadLine()));

                //read file
                string input = "";
                
                while (input != "end")
                {
                    input = reader.ReadLine();

                    if (input != "end")
                    {
                        lineNo = int.Parse(input);

                        type = int.Parse(reader.ReadLine());

                        nodePos.X = int.Parse(reader.ReadLine());
                        nodePos.Y = int.Parse(reader.ReadLine());
                        
                        
                        
                        loader.loadObject(lineNo, type, nodePos);
                     
                    }

                }


            }


        }

        public void SaveActors() {
            //write all actors to a dedicated file
            foreach(Actor a in handle.actorList){

                using (StreamWriter writer = new StreamWriter(ObjectLoader.path+"ObjectData/Dialog/SavedDialog/"+a.fileName+".dialog"))
                {
                    //write dialog count
                    writer.WriteLine(a.dialogtree.dialogList.Count);

                    foreach (BrokenLands.Dialog d in a.dialogtree.dialogList)
                    {
                        //write message
                        writer.WriteLine(d.message);
                        //locktype
                        writer.WriteLine(d.locK.type);
                        //req
                        writer.WriteLine(d.locK.requirement);
                        //met
                        writer.WriteLine(d.locK.getMet());
                        //value
                        writer.WriteLine(d.locK.value);
                        //group
                        writer.WriteLine(d.group);
                        //next group
                        writer.WriteLine(d.getNextGroup());
                        //action type
                        writer.WriteLine(d.getAction().action);
                        //action value
                        writer.WriteLine(d.getAction().value);
                        //teller
                        writer.WriteLine(d.teller);
                    }

                    //write group count
                    writer.WriteLine(a.dialogtree.defaultgroupList.Count);

                    foreach (BrokenLands.Group g in a.dialogtree.defaultgroupList)
                    {
                        //locktype
                        writer.WriteLine(g.locK.type);
                        //req
                        writer.WriteLine(g.locK.requirement);
                        //met
                        writer.WriteLine(g.locK.getMet());
                        //value
                        writer.WriteLine(g.locK.value);
                        //group
                        writer.WriteLine(g.getGroup());
                    }
                }
            }
        }

        public void SaveLootContainers() {

            foreach(SceneObject s in handle.sceneList){

                if (s.MyType == Entity.uType.Loot) {
                    LootContainer l = (LootContainer)s;
                    using (StreamWriter writer = new StreamWriter(ObjectLoader.path + "ObjectData/Loot/SavedLoot/" + l.fileName + ".loot"))
                    {
                        //will have to use chu's item loader
                        writer.WriteLine(l.itemList.Count);

                        foreach(Item i in l.itemList){
                            writer.WriteLine(i.id);
                        }
                    }
                }



            }



        }

        public void WriteMap(string filename)
        {
            System.Diagnostics.Debug.WriteLine(filename);
            using (StreamWriter write = new StreamWriter(filename))
            {
                //int lineNo, int type, Vector2 nodePos

                //write gridsize
                write.WriteLine(handle.gridNodes.GetLength(0));

                //enemy
                foreach (Unit o in handle.enemList)
                {
                    write.WriteLine(o.lineNo); //lineNo
                    write.WriteLine(0); //type
                    Vector2 nodePos = Vector2.Zero;
                    bool stop = false;
                    for (int x = 0; x < handle.gridNodes.GetLength(0); x++)
                    {

                        for (int z = 0; z < handle.gridNodes.GetLength(1); z++)
                        {
                            if (new Vector2(x, z) == o.MyLocation.Location)
                            {
                                nodePos = new Vector2(x, z);
                                stop = true;
                                break;
                            }
                        }

                        if (stop)
                            break;
                    }

                    //write node pos
                    write.WriteLine(nodePos.X.ToString());
                    write.WriteLine(nodePos.Y.ToString());
                }

                //write obstacle
                

                //write scene
                foreach (SceneObject o in handle.sceneList)
                {

                    write.WriteLine(o.lineNo);
                    if (o.MyType == Entity.uType.Loot)
                        write.WriteLine(11);
                    else
                        write.WriteLine(2);
                    Vector2 nodePos = Vector2.Zero;

                    bool stop = false;
                    for (int x = 0; x < handle.gridNodes.GetLength(0); x++)
                    {

                        for (int z = 0; z < handle.gridNodes.GetLength(1); z++)
                        {
                            if (new Vector2(x, z) == o.MyLocation.Location)
                            {
                                nodePos = new Vector2(x, z);
                                stop = true;
                                break;
                            }
                        }

                        if (stop)
                            break;
                    }

                    //write node pos
                    write.WriteLine(nodePos.X.ToString());
                    write.WriteLine(nodePos.Y.ToString());
                }


                //write ai
                foreach (DungeonAI d in handle.aiList)
                {

                    write.WriteLine(d.lineNo);
                    write.WriteLine(7);
                    Vector2 nodePos = Vector2.Zero;

                    bool stop = false;
                    for (int x = 0; x < handle.gridNodes.GetLength(0); x++)
                    {

                        for (int z = 0; z < handle.gridNodes.GetLength(1); z++)
                        {
                            if (new Vector2(x, z) == d.MyLocation.Location)
                            {
                                nodePos = new Vector2(x, z);
                                stop = true;
                                break;
                            }
                        }

                        if (stop)
                            break;
                    }

                    //write node pos
                    write.WriteLine(nodePos.X.ToString());
                    write.WriteLine(nodePos.Y.ToString());

                }
                //write floor
                write.WriteLine(handle.floorObject.lineNo);
                write.WriteLine(8);
                write.WriteLine(0);
                write.WriteLine(0);

                //write overlay
                foreach (SceneObject s in handle.overlayList)
                {

                    write.WriteLine(s.lineNo);
                    write.WriteLine(9);
                    Vector2 nodePos = Vector2.Zero;

                    bool stop = false;
                    for (int x = 0; x < handle.gridNodes.GetLength(0); x++)
                    {

                        for (int z = 0; z < handle.gridNodes.GetLength(1); z++)
                        {
                            if (new Vector2(x, z) == s.MyLocation.Location)
                            {
                                nodePos = new Vector2(x, z);
                                stop = true;
                                break;
                            }
                        }

                        if (stop)
                            break;
                    }

                    //write node pos
                    write.WriteLine(nodePos.X.ToString());
                    write.WriteLine(nodePos.Y.ToString());
                }


                foreach (Actor a in handle.actorList)
                {

                    write.WriteLine(a.lineNo);
                    write.WriteLine(4);
                    Vector2 nodePos = Vector2.Zero;

                    bool stop = false;
                    for (int x = 0; x < handle.gridNodes.GetLength(0); x++)
                    {

                        for (int z = 0; z < handle.gridNodes.GetLength(1); z++)
                        {
                            if (new Vector2(x, z) == a.MyLocation.Location)
                            {
                                nodePos = new Vector2(x, z);
                                stop = true;
                                break;
                            }
                        }

                        if (stop)
                            break;
                    }

                    //write node pos
                    write.WriteLine(nodePos.X.ToString());
                    write.WriteLine(nodePos.Y.ToString());
                }

                //write underlay
                foreach (SceneObject s in handle.underlayList)
                {
                    write.WriteLine(s.lineNo);
                    write.WriteLine(10);
                    Vector2 nodePos = Vector2.Zero;

                    bool stop = false;
                    for (int x = 0; x < handle.gridNodes.GetLength(0); x++)
                    {

                        for (int z = 0; z < handle.gridNodes.GetLength(1); z++)
                        {
                            if (new Vector2(x, z) == s.MyLocation.Location)
                            {
                                nodePos = new Vector2(x, z);
                                stop = true;
                                break;
                            }
                        }

                        if (stop)
                            break;
                    }

                    //write node pos
                    write.WriteLine(nodePos.X.ToString());
                    write.WriteLine(nodePos.Y.ToString());
                }

                //write region

                //write player
                //lineNo, int type, Vector2 nodePos
                foreach (Vector2 p in playerNode)
                {
                    write.WriteLine(0); //lineNo -> player has no lineNo
                    write.WriteLine(3); //type
                    write.WriteLine(p.X.ToString()); //nodepos
                    write.WriteLine(p.Y.ToString());
                }

                //write exits
                foreach (Vector2 e in handle.sectionNodes)
                {
                    write.WriteLine(0); //lineNo -> player has no lineNo
                    write.WriteLine(-1); //type
                    write.WriteLine(e.X.ToString()); //nodepos
                    write.WriteLine(e.Y.ToString());
                }

                write.WriteLine("end");
            }


        }

        /// <summary>
        /// To Save Quest Title, Condition count and individual completion for each condition
        /// </summary>
        /// <param name="filename"></param>
        public void SaveQuestCondition()
        {
            List<String> mapNames = new List<String>();

            //Adds all map names into a List to write
            foreach (QuestObject obj in QuestManager.activeList)
            {
                if (!mapNames.Contains(obj.location))
                {
                    mapNames.Add(obj.location);
                }
            }

            foreach (QuestObject obj in QuestManager.questList)
            {
                if (!mapNames.Contains(obj.location))
                {
                    mapNames.Add(obj.location);
                }
            }

            for (int i = 0; i < mapNames.Count; i++)
            {
                using (StreamWriter writer = new StreamWriter("../../../../BrokenLandContent/ObjectData/Quest/" + mapNames[i] + ".condition"))
                {
                    //Iterates through both lists to check which quests have the same title and writes it in
                    foreach (QuestObject q in QuestManager.activeList)
                    {
                        if (q.location == mapNames[i])
                        {
                            int conditionCount = q.conditionList.Count;
                            foreach (Condition c in q.inactiveConditionList)
                            {
                                if (!q.conditionList.Contains(c))
                                {
                                    conditionCount++;
                                }
                            }
                            
                            writer.WriteLine(q.title);
                            writer.WriteLine(conditionCount);
                            writer.WriteLine(QuestManager.activeList.Count - 1);

                            foreach (Condition c in q.conditionList)
                            {
                                writer.WriteLine(c.name);
                                writer.WriteLine(c.condition);
                            }
                            foreach (Condition c in q.inactiveConditionList)
                            {
                                if (!q.conditionList.Contains(c))
                                {
                                    writer.WriteLine(c.name);
                                    writer.WriteLine(c.condition);
                                }
                            }
                        }
                    }

                    foreach (QuestObject q in QuestManager.questList)
                    {
                        if (q.location == mapNames[i])
                        {
                            int conditionCount = q.conditionList.Count;
                            foreach (Condition c in q.inactiveConditionList)
                            {
                                if (!q.conditionList.Contains(c))
                                {
                                    conditionCount++;
                                }
                            }

                            writer.WriteLine(q.title);
                            writer.WriteLine(conditionCount);
                            writer.WriteLine(QuestManager.questList.Count - 1);

                            foreach (Condition c in q.conditionList)
                            {
                                writer.WriteLine(c.name);
                                writer.WriteLine(c.condition);
                            }
                            foreach (Condition c in q.inactiveConditionList)
                            {
                                if (!q.conditionList.Contains(c))
                                {
                                    writer.WriteLine(c.name);
                                    writer.WriteLine(c.condition);
                                }
                            }
                        }
                    }
                }
            }
        }

        public void SaveCharacters()
        {
            using (StreamWriter writer = new StreamWriter("../../../../BrokenLandContent/ObjectData/PlayerCharacters.data"))
            {
                writer.WriteLine(GameSession.players.Count);
                foreach (Player p in GameSession.players)
                {
                    writer.WriteLine(p._NAME);
                    writer.WriteLine(p.MyStatus.BaseStr);
                    writer.WriteLine(p.MyStatus.BaseDex);
                    writer.WriteLine(p.MyStatus.BaseInt);
                    writer.WriteLine(p.MyStatus.BaseCha);
                    writer.WriteLine(p.MyStatus.BaseEnd);
                    writer.WriteLine(p.MyStatus.BaseAgi);
                    writer.WriteLine(p.lineNo);
                    writer.WriteLine(p.MyStatus.Skills.MeleePts);
                    writer.WriteLine(p.MyStatus.Skills.ScavengePts);
                    writer.WriteLine(p.MyStatus.Skills.LockpickPts);
                    writer.WriteLine(p.MyStatus.Skills.BarterPts);
                    writer.WriteLine(p.MyStatus.Skills.HealPts);
                    writer.WriteLine(p.MyStatus.Skills.GrowthPts.ToString("n3"));
                    writer.WriteLine(p.MyStatus.AbilityID);
                    writer.WriteLine(GameSession.GS.CharacterTextureList.IndexOf(p.Texture));
                    writer.WriteLine(p.MyStatus.equippedItems.Count);
                    p.MyStatus.UpdateEquips();
                    foreach (Equipment e in p.MyStatus.equippedItems) {
                        writer.WriteLine(e.itemID);
                    }
                    
                }
            }

        }

        public void SaveInventory()
        {
            using (StreamWriter writer = new StreamWriter("../../../../BrokenLandContent/ObjectData/Inventory.data"))
            {
                writer.WriteLine(GameSession.GS.inventory.money);
                writer.WriteLine(GameSession.GS.inventory.activeList.Count);
                foreach (Item i in GameSession.GS.inventory.activeList)
                {
                    writer.WriteLine(i.id);
                }
            }
        }

        //deprecated

    };
};
