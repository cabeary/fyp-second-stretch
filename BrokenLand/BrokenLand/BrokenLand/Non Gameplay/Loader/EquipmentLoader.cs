﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
using System.IO;

namespace BrokenLands
{
    static class EquipmentLoader
    {
        public static readonly List<Item> itemList = new List<Item>();
        public static readonly List<Equipment> equipList = new List<Equipment>();
        public static readonly List<Texture2D> equipTexture = new List<Texture2D>();
        public static readonly List<Texture2D> equipSpriteSheet = new List<Texture2D>();
        public static string itemTexPath = "Item/";//../../../../BrokenLandContent/Item/";
        public static string spriteTexPath = "Units/Weapons/";//../../../../BrokenLandContent/Item/";

        public static Equipment fetchEquipment(int equipID) {
            foreach (Equipment e in equipList)
            {
                if (equipID == e.itemID)
                {
                    return e;
                }
            }
            return null;
        
        }
        public static Item fetchItem(int itemID) {

            foreach(Item i in itemList){

                if (itemID == i.id)
                    return i;

            }
           

            return null;

        }

        public static void LoadEquipments(ContentManager content)
        {

            using (StreamReader streamReader = new StreamReader("../../../../BrokenLandContent/ObjectData/equipments.txt"))
            {
                string line = "";
                Texture2D texture;
                while (line!= "end"){
                    line = streamReader.ReadLine();
                    string readType = line.Split(':').First();
                    switch (readType)
                    {
                            //TODO
                        
                        case "inventory_equipment":
                            Equipment e = new Equipment();
                            String nextLine = "";
                            while (nextLine != "!")
                            {
                                nextLine = streamReader.ReadLine();
                                string[] attriblist = nextLine.Split('@');
                                foreach (string s in attriblist)
                                {
                                    string[] t = s.Split(null);
                                    string attribute = s.Split(null).First();//nextLine.Substring(nextLine.IndexOf('@'), nextLine.IndexOf(' '));
                                    string value = "";
                                    for (int i = 1; i < t.Count(); i++)
                                    {
                                        if (i > 1)
                                        {
                                            value += " ";
                                        }

                                        value += t[i];
                                    }
                                    switch (attribute)
                                    {
                                        case "spritesheet":
                                            e.spritesheet_t = value;
                                            texture = content.Load<Texture2D>(spriteTexPath + value);
                                            e.spriteSheet = texture;
                                            equipSpriteSheet.Add(texture);
                                            break;
                                        case "type":
                                            if (value == "Weapon")
                                            {
                                                e = new Weapon();
                                            }
                                            e.SetType(value);
                                            break;
                                        case "range":
                                            ((Weapon)e).range = int.Parse(value);
                                            break;
                                        case "speed":
                                            ((Weapon)e).baseSpeed = int.Parse(value);
                                            break;
                                        case "counter":
                                            //((Weapon)e).counterRate = int.Parse(value);
                                            break;
                                        case "damage":
                                            ((Weapon)e).baseDamage = int.Parse(value);
                                            break;
                                        case "hitcount":
                                            ((Weapon)e).hitCount = int.Parse(value);
                                            break;
                                        case "apcost":
                                            ((Weapon)e).apCost = int.Parse(value);
                                            break;
                                       // case "weight":
                                       //     ((Weapon)e).baseWeight = int.Parse(value);
                                       //     break;
                                        case "prefix":
                                            e.prefix = value;
                                            break;
                                        case "itemid":
                                            e.itemID = int.Parse(value);
                                            break;
                                        case "name":
                                            e.name = value;
                                            break;
                                        case "desc":
                                            e.desc = value;
                                            break;
                                        case "atk":
                                            e.baseAttack = int.Parse(value);
                                            break;
                                        case "def":
                                            e.baseDefence = int.Parse(value);
                                            break;
                                        //case "magic":
                                          //  e.baseMagic = int.Parse(value);
                                          //  break;
                                        case "weight":
                                            e.baseWeight = int.Parse(value);
                                            break;
                                        //case "speed":
                                          //  e.baseSpeed = int.Parse(value);
                                           // break;
                                        case "value":
                                            e.shopValue = int.Parse(value);
                                            break;
                                        case "texture":
                                            e.sprite_t = value;
                                            texture = content.Load<Texture2D>(itemTexPath + value);
                                            e.tex = texture;
                                            equipTexture.Add(texture);
                                            break;

                                    }
                                }
                                
                            }

                            equipList.Add(e);

                            break;
                        case "inventory_item":
                            Item item = new Item();
                            string nextLiner = "";
                            while (nextLiner != "!")
                            {
                                nextLiner = streamReader.ReadLine();
                                string[] attriblist = nextLiner.Split('@');
                                foreach (string s in attriblist)
                                {
                                    string[] t = s.Split(null);
                                    string attribute = s.Split(null).First();//nextLine.Substring(nextLine.IndexOf('@'), nextLine.IndexOf(' '));
                                    string value = "";
                                    for (int i = 1; i < t.Count(); i++)
                                    {
                                        if (i > 1)
                                        {
                                            value += " ";
                                        }

                                        value += t[i];
                                    }
                                    switch (attribute)
                                    {
                                        
                                        case "itemid":
                                            item.id = int.Parse(value);
                                            break;
                                        case "name":
                                            item.name = value;
                                            break;
                                        case "desc":
                                            item.desc = value;
                                            break;
                                        case "type":
                                            item.type = (Item.ItemType)int.Parse(value);
                                            break;
                                        case "value":
                                            item.value = int.Parse(value);
                                            break;
                                        case "texture":
                                            texture = content.Load<Texture2D>(itemTexPath + value);
                                            item.texture = texture;
                                            break;
                                    }
                                }
                            }
                            itemList.Add(item);
                            //GameSession.inactiveInventory.AddItem(item);
                            break;

                        default: continue;
                    }
                    
                } 
            }

        }
    }
}
