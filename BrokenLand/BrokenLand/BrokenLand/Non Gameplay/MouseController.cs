﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
namespace BrokenLands
{
   class MouseController
    {
        public Ray raycast;
        //Texture2D defaultPointer;
        MouseState state;
        MouseState prevstate = new MouseState();
        public int mouseScrollValue = 0;
        public static Texture2D defaultPointer;

        public void setTextures(Texture2D defaultPointer) {
            MouseController.defaultPointer = defaultPointer;
        }

        public void Update(Viewport viewport, CameraObj camera)
        {
            mouseScrollValue = Mouse.GetState().ScrollWheelValue;

            prevstate = state;
            state = Mouse.GetState();

            //get mouse mouse position
            Point mousePos = new Point(Mouse.GetState().X,Mouse.GetState().Y);

            //mouse position in 3d space (at clipping plane)
            Vector3 pos1 = viewport.Unproject(new Vector3(mousePos.X, mousePos.Y, 0), camera.proj, camera.view, Matrix.Identity);


            ////mouse position + 1. project the position forward. Like camera.forward
            Vector3 pos2 = viewport.Unproject(new Vector3(mousePos.X, mousePos.Y, 1), camera.proj, camera.view, Matrix.Identity);
            //// direction to cast to
            Vector3 dir = Vector3.Normalize(pos2 - pos1);

            raycast = new Ray(pos1, dir);

        }

        public void MouseUpdate() {
           
            prevstate = state;
            state = Mouse.GetState();
        }

        public void MouseDraw(SpriteBatch batch) {
            Rectangle source = new Rectangle(0,0,19,19); //hardcoded for now
            batch.Draw(defaultPointer, getPosition(), source, Color.White, 0, new Vector2(0,0), 1, SpriteEffects.None, 0f);
            //batch.Draw(defaultPointer,getPosition(),Color.White);
        }

        public bool Selected(Rectangle rect) {

            Point mousePos = new Point(Mouse.GetState().X, Mouse.GetState().Y);
            return rect.Contains(mousePos);
        }

        public Vector2 getPosition(){

            Point mousePos = new Point(Mouse.GetState().X, Mouse.GetState().Y);
            return new Vector2(mousePos.X, mousePos.Y);
        }

        public bool Selected(BoundingBox box)
        {
            //if ray intersects w box
            return raycast.Intersects(box) > 0;
        }

        public bool RightMouseClick()
        {
           
            return (state.RightButton == ButtonState.Pressed && prevstate.RightButton == ButtonState.Released);
        }

        public bool LeftMouseClick()
        {

            return (state.LeftButton == ButtonState.Pressed && prevstate.LeftButton == ButtonState.Released);
        }

    }
}
