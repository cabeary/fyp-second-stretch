﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace GameStateManagement
{
    class Audio
    {
        string name;
        //bool isStarting;
        //bool isPlaying;
        //bool isLooping;

        /// <summary>
        /// Constructor. Takes in Name, Duration and Type(music or sound)
        /// </summary>
        /// <param name="audioName"></param>
        /// <param name="audioDuration"></param>
        /// <param name="type"></param>
        public Audio(string audioName)
        {
            name = audioName;
            //isStarting = false;
            //isPlaying = false;
            //isLooping = false;
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        /*public Cue cue
        {
            get { return audioCue; }
            set { audioCue = value; }
        }
        public float Duration
        {
            get { return duration; }
            set { duration = value; }
        }
        public float StartTime
        {
            get { return startTime; }
            set { startTime = value; }
        }
        public bool Looping
        {
            get { return isLooping; }
            set { isLooping = value; }
        }
        public int RepeatCount
        {
            get { return repeatCount; }
            set { repeatCount += value; }
        }
        /// <summary>
        /// Allows code in GameAudioEngine to play sound if sound is setStarting(true)
        /// </summary>
        /// <param name="starting"></param>
        public void setStarting(bool starting)
        {
            isStarting = starting;
        }
        /// <summary>
        /// Resets elapsed Time and isPlaying to ensure audioEngine will not play
        /// </summary>
        public void stopPlaying()
        {
            isPlaying = false;
            startTime = 36000000;
        }
        /// <summary>
        /// Checks if audio is ready to play (Is not playing, but is starting). If ready, starts playing and resets Starting 
        /// </summary>
        /// <returns></returns>
        public bool readyToPlay()
        {
            if (isStarting && !isPlaying)
            {
                isPlaying = true;
                isStarting = false;
                return true;
            }
            else
                return false;
        }
        /// <summary>
        /// Checks if audio is ready to restart (IsPlaying and IsStarting). If ready, remains playing, but resets Starting
        /// </summary>
        /// <returns></returns>
        public bool readyToRestart()
        {
            if (isStarting && isPlaying)
            {
                isStarting = false;
                return true;
            }
            else
                return false;
        }*/
    }
}
