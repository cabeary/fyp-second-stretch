﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using Supernova.Particles2D;
using Supernova.Particles2D.Modifiers.Alpha;
using Supernova.Samples.Windows.Utilities;
using GameStateManagement;
using System.Threading;
using Microsoft.Xna.Framework.Content;
using BrokenLands;
using BrokenLands;


namespace BrokenLands
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class QuestScreen : GameScreen
    {

        ContentManager content;
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SpriteFont spritefont, InconsolataFont;

        KeyboardState lastKeyboardState;
        MouseState prevMouseState, currentMouseState;

        float FrameCounter, frameRate;
        Texture2D WorldMapTex, WorldMapUIOverlay,QuestButtonTex;
        Rectangle WorldMapTexPos;
        Rectangle[] QuestButtonRect = new Rectangle[5];
        int SelectedRect;
        String ConditionText;
        String DescriptionText;

        Texture2D CancelTex;

        float[] ButtonAlpha;

        Texture2D SettingsIcon;

        Rectangle MouseRect;

        Random random;

        MouseController mousecontroller = new MouseController();

        public QuestScreen()
            : base()
        {
            this.ScreenState = ScreenState.Active;
        }

        public override void Activate(bool instancePreserved)
        {
            if (!instancePreserved)
            {
                if (content == null)
                    content = new ContentManager(ScreenManager.Game.Services, "Content");

                //TestCamera = new CameraObj(this, new Vector3(0, 0, 0), new Vector3(0, 0, 10), Vector3.Up, 1f, 1000f, 1f);
                FrameCounter = 0;
                prevMouseState = Mouse.GetState();
                currentMouseState = Mouse.GetState();
                lastKeyboardState = Keyboard.GetState();

                //Loading
                spriteBatch = new SpriteBatch(ScreenManager.GraphicsDevice);

                //Textures
                WorldMapTex = content.Load<Texture2D>("UI/QuestUI");
                QuestButtonTex = content.Load<Texture2D>("UI/QuestButton");

                //Icons
                SettingsIcon = content.Load<Texture2D>("UI/Icons/Settings-Icon");
                CancelTex = content.Load<Texture2D>("UI/CancelButton");

                //Fonts
                spritefont = content.Load<SpriteFont>("Fonts/Times12");
                InconsolataFont = content.Load<SpriteFont>("Fonts/Inconstella");

                //Models

                //Variables
                WorldMapTexPos = new Rectangle(0, 0, UIElement.viewport.Width, UIElement.viewport.Height);
                random = new Random();
                MouseRect = new Rectangle(Mouse.GetState().X, Mouse.GetState().Y, 10, 10);//Used for mouse Collision
                ButtonAlpha = new float[5];
                for (int i = 0; i < QuestButtonRect.Length; i++)
                {
                    QuestButtonRect[i] = new Rectangle((int)(ScreenManager.GraphicsDevice.Viewport.Width * 0.168f)
                        , (int)((ScreenManager.GraphicsDevice.Viewport.Height * 0.248f)+i*ScreenManager.GraphicsDevice.Viewport.Height*0.112f), QuestButtonTex.Width, QuestButtonTex.Height);
                    ButtonAlpha[i] = 0.7f;
                }

                //Items = new List<InventoryItem>(40);

                //Temp to get in Units in
                
                Texture2D tex = content.Load<Texture2D>("Units/Gunbowman");

                SelectedRect = 0;
                DescriptionText = "";
                ConditionText = "";

                ScreenManager.Game.ResetElapsedTime();
            }
        }

        //Every activate must have a deactivate
        public override void Deactivate()
        {
            base.Deactivate();
        }

        /// <summary>
        /// Unload graphics content used by the game.
        /// </summary>
        public override void Unload()
        {
            // content.Unload();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);

            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape) || (Keyboard.GetState().IsKeyDown(Keys.Tab) && lastKeyboardState.IsKeyUp(Keys.Tab)))
                ScreenManager.RemoveScreen(this);

            mousecontroller.MouseUpdate();

            //Rectangle[] prevLocations = Locations;
            currentMouseState = Mouse.GetState();

            //Update Mouse Position
            MouseRect.X = currentMouseState.X;
            MouseRect.Y = currentMouseState.Y;

            for (int i = 0; i < QuestButtonRect.Length; i++)
            {
                if (MouseRect.Intersects(QuestButtonRect[i]))
                {
                    ButtonAlpha[i] = 1.0f;
                    if (mousecontroller.LeftMouseClick())
                    {
                        SelectedRect = i;
                        ConditionText = "";

                        //Text modifier
                        List<string> queststringList = new List<string>();

                        if (QuestManager.activeList.Count > i)
                        {
                            foreach (Condition c in QuestManager.activeList[SelectedRect].conditionList)
                            {
                                if (c.condition > 0)
                                {
                                    queststringList.Add(c.name + " Done(" + c.condition.ToString() + ")");
                                }
                                else
                                {
                                    queststringList.Add(c.name);
                                }
                            }

                            foreach (string s in queststringList)
                            {
                                string currentStr;
                                if (s.Length > 36)
                                {
                                    currentStr = GameSession.GS.StringTrim(s, 36);//s.Substring(0, 18) + "...";
                                }
                                else
                                {
                                    currentStr = s;//.Substring(0, s.Length - 1);
                                }
                                ConditionText += currentStr + "\n";
                            }

                            DescriptionText = GameSession.GS.StringTrim(QuestManager.activeList[SelectedRect].desc, 36);
                        }
                    }
                }
                else
                {
                    ButtonAlpha[i] = 0.7f;
                }
            }

            if (MouseRect.Intersects(new Rectangle((ScreenManager.GraphicsDevice.Viewport.Bounds.Right - CancelTex.Width) - 5, 7, CancelTex.Width, CancelTex.Height)))
            {
                if (mousecontroller.LeftMouseClick())
                {
                    ScreenManager.RemoveScreen(this);
                }
            }
            GameSession.gameAudioEngine.Update(gameTime);
            // Reset prevMouseState
            prevMouseState = Mouse.GetState();
            lastKeyboardState = Keyboard.GetState();
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Draw(GameTime gameTime)
        {


            ScreenManager.FadeBackBufferToBlack(TransitionAlpha * 2 / 3);

            ScreenManager.GraphicsDevice.BlendState = BlendState.Opaque;
            ScreenManager.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            //NewParticle.Draw(gameTime, TestCamera);
            frameRate = (float)(1 / gameTime.ElapsedGameTime.TotalSeconds);

            float width = ScreenManager.GraphicsDevice.Viewport.Width;
            float scale = (width / 1920);
            Matrix scalingMatrix = Matrix.CreateScale(scale);
            spriteBatch.Begin(SpriteSortMode.Immediate, null, null, null, null, null, scalingMatrix);

            spriteBatch.Draw(WorldMapTex, new Vector2(ScreenManager.GraphicsDevice.Viewport.TitleSafeArea.X, ScreenManager.GraphicsDevice.Viewport.TitleSafeArea.Y), null, Color.White, 0f, new Vector2(0, 0), 1, SpriteEffects.None, 0f);

            spriteBatch.End();

            //UI DRAWING
            spriteBatch.Begin();

          //  spriteBatch.Draw(players[0].Texture, new Vector2(70, 60) - new Vector2(0, 806) * 0.2f, null, Color.White, 0f, Vector2.Zero, 0.6f, SpriteEffects.None, 0f);
            spriteBatch.Draw(CancelTex, new Vector2((ScreenManager.GraphicsDevice.Viewport.Bounds.Right - CancelTex.Width / 2) - 3, 7 + CancelTex.Height / 2), null, Color.White, 0f, new Vector2(CancelTex.Width / 2, CancelTex.Height / 2), 0.7f, SpriteEffects.None, 0f);
#if DEBUG
            spriteBatch.DrawString(spritefont, "Mouse X : " + MouseRect.X + " Mouse Y : " + MouseRect.Y, new Vector2(50, 100), Color.White);
#endif
            for(int i = 0; i < QuestButtonRect.Length; i++)
            {
                spriteBatch.Draw(QuestButtonTex,new Vector2(QuestButtonRect[i].X,QuestButtonRect[i].Y), null, Color.White*ButtonAlpha[i],0f,Vector2.Zero,scale,SpriteEffects.None,0f);
                if(QuestManager.questList!=null)
                {
                    if (i < QuestManager.activeList.Count)
                    {
                        spriteBatch.DrawString(InconsolataFont, QuestManager.activeList[i].title, new Vector2(QuestButtonRect[i].X + (int)(0.03f * ScreenManager.GraphicsDevice.Viewport.Width), QuestButtonRect[i].Y + (int)(0.035f * ScreenManager.GraphicsDevice.Viewport.Height)), Color.White);
                    }
                    else
                    {
                        spriteBatch.DrawString(InconsolataFont, "< EMPTY >", new Vector2(QuestButtonRect[i].X + (QuestButtonRect[i].Width /4), QuestButtonRect[i].Y + (int)(0.035f * ScreenManager.GraphicsDevice.Viewport.Height)), Color.White);
                    }
                }
            }

            spriteBatch.DrawString(InconsolataFont, ConditionText, new Vector2(0.5421875f * ScreenManager.GraphicsDevice.Viewport.Width, 0.593055556f * ScreenManager.GraphicsDevice.Viewport.Height), Color.White);

            spriteBatch.End();


            base.Draw(gameTime);
        }



        public void DrawBorderedText(string text, int PosX, int PosY, Color color, SpriteFont font)
        {
            spriteBatch.DrawString(font, text, new Vector2(PosX + 1, PosY + 1), Color.Black * 0.5f);
            spriteBatch.DrawString(font, text, new Vector2(PosX + 1, PosY - 1), Color.Black * 0.5f);
            spriteBatch.DrawString(font, text, new Vector2(PosX - 1, PosY + 1), Color.Black * 0.5f);
            spriteBatch.DrawString(font, text, new Vector2(PosX - 1, PosY - 1), Color.Black * 0.5f);
            spriteBatch.DrawString(font, text, new Vector2(PosX, PosY), color);

        }
    }
}

