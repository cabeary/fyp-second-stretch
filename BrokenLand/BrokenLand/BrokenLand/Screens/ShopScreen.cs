﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using Supernova.Particles2D;
using Supernova.Particles2D.Modifiers.Alpha;
using Supernova.Samples.Windows.Utilities;
using GameStateManagement;
using System.Threading;
using Microsoft.Xna.Framework.Content;


namespace BrokenLands
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    class ShopScreen : GameScreen
    {

        ContentManager content;
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SpriteFont spritefont, InconsolataFont;

        MouseState prevMouseState, currentMouseState;

        float FrameCounter, frameRate;
        Texture2D WorldMapTex, WorldMapUIOverlay;
        Rectangle WorldMapTexPos;

        Texture2D CancelTex;
        Texture2D Tick;

        //List<InventoryItem> Items = new List<InventoryItem>();
        Inventory shopInventory = new Inventory();

        float[] ButtonAlpha;

        Texture2D SettingsIcon;

        Rectangle[] itemHolderRects;
        Rectangle[] shopHolderRects;
        Rectangle ConfirmRect;

        Rectangle MouseRect;

        List<Player> players = new List<Player>();
        List<int> SelectedInventoryItems = new List<int>();
        List<int> SelectedShopItems = new List<int>();

        int discount = 0;
        int barterValue = 0;
        float TotalCostBuy = 0;
        float TotalCostSell = 0;
        String displayText = "";
        
        Random random;

        MouseController mousecontroller = new MouseController();

        public ShopScreen(int Discount, int BarterValue, List<Item> ShopItems)
            : base()
        {
            players = GameSession.players;
            discount = Discount;
            barterValue = (int)players[0].MyStatus.Skills.BarterValue(BarterValue);

            for(int i = 0; i < ShopItems.Count; i++)
            {
                shopInventory.AddItem(ShopItems[i]);
            }
            
            this.ScreenState = ScreenState.Active;

        }

        public override void Activate(bool instancePreserved)
        {
            if (!instancePreserved)
            {
                if (content == null)
                    content = new ContentManager(ScreenManager.Game.Services, "Content");

                //TestCamera = new CameraObj(this, new Vector3(0, 0, 0), new Vector3(0, 0, 10), Vector3.Up, 1f, 1000f, 1f);
                FrameCounter = 0;
                prevMouseState = Mouse.GetState();
                currentMouseState = Mouse.GetState();

                //Loading
                spriteBatch = new SpriteBatch(ScreenManager.GraphicsDevice);

                //Textures
                WorldMapTex = content.Load<Texture2D>("UI/Shop-UI");


                //Icons
                SettingsIcon = content.Load<Texture2D>("UI/Icons/Settings-Icon");
                CancelTex = content.Load<Texture2D>("UI/Icons/Cancel-Icon");
                Tick = content.Load<Texture2D>("UI/Tick");

                //Fonts
                spritefont = content.Load<SpriteFont>("Fonts/Times12");
                InconsolataFont = content.Load<SpriteFont>("Fonts/Inconstella");

                //Models

                //Variables
                WorldMapTexPos = new Rectangle(0, 0, UIElement.viewport.Width, UIElement.viewport.Height);
                random = new Random();
                MouseRect = new Rectangle(Mouse.GetState().X, Mouse.GetState().Y, 10, 10);//Used for mouse Collision
                ButtonAlpha = new float[5];
                /*
                BarterRect = new Rectangle((int)(0.05184375f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.504444444f * ScreenManager.GraphicsDevice.Viewport.Height), (int)(0.13046875f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.91666667f * ScreenManager.GraphicsDevice.Viewport.Height));
                DiscountRect = new Rectangle((int)(0.05184375f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.38222222f * ScreenManager.GraphicsDevice.Viewport.Height), (int)(0.13046875f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.91666667f * ScreenManager.GraphicsDevice.Viewport.Height));
                CurrentMoneyRect = new Rectangle((int)(0.48203125f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.255555556f * ScreenManager.GraphicsDevice.Viewport.Height), (int)(0.13046875f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.91666667f * ScreenManager.GraphicsDevice.Viewport.Height));
                CostRect = new Rectangle((int)(0.48203125f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.3763888889f * ScreenManager.GraphicsDevice.Viewport.Height), (int)(0.13046875f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.91666667f * ScreenManager.GraphicsDevice.Viewport.Height)); 
                */
                ConfirmRect = new Rectangle((int)(0.70078125f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.775f * ScreenManager.GraphicsDevice.Viewport.Height), (int)(0.059375f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.0486111111f * ScreenManager.GraphicsDevice.Viewport.Height));

                //Temp to get in Units in
                Model Tile = content.Load<Model>("Tile/Tile");
                Texture2D tex = content.Load<Texture2D>("Units/Gunbowman");

                //players[0].Texture = tex;

                itemHolderRects = new Rectangle[40];
                for (int i = 0; i < 40; i++)
                {
                    Item item = new Item(i, "Poop", "Fecal Material", 1, 1f, new Vector2(ScreenManager.GraphicsDevice.Viewport.Width * 0.278125f + (i % 5) * ScreenManager.GraphicsDevice.Viewport.Width * 0.04375f,
                        ScreenManager.GraphicsDevice.Viewport.Height * 0.1980666667f + (i / 5) * ScreenManager.GraphicsDevice.Viewport.Height * 0.07777777778f), SettingsIcon);
                    itemHolderRects[i] = item.rectangle;
                }
                //Inventory Positioning
                for (int i = 0; i < GameSession.GS.inventory.activeList.Count; i++)
                {
                    //NEED TO FIX POSITIONING
                    GameSession.GS.inventory.activeList[i].position =
                        new Vector2((ScreenManager.GraphicsDevice.Viewport.Width * 0.278125f) +
                            (i % 5)
                    * (ScreenManager.GraphicsDevice.Viewport.Width * 0.04375f),
                    (ScreenManager.GraphicsDevice.Viewport.Height * 0.1980666667f)
                    + (i / 5) * (ScreenManager.GraphicsDevice.Viewport.Height * 0.0777777778f));
                    //(i / ScreenManager.GraphicsDevice.Viewport.Height * 0.006944444f) 
                    //* ScreenManager.GraphicsDevice.Viewport.Height * 0.10694444f);
                    GameSession.GS.inventory.activeList[i].rectangle = new Rectangle((int)GameSession.GS.inventory.activeList[i].position.X, (int)GameSession.GS.inventory.activeList[i].position.Y,
                        GameSession.GS.inventory.activeList[i].texture.Width, GameSession.GS.inventory.activeList[i].texture.Height);
                }

                shopHolderRects = new Rectangle[25];
                for (int i = 0; i < 25; i++)
                {
                    Item item = new Item(i, "Poop", "Fecal Material", 1, 1f, new Vector2(ScreenManager.GraphicsDevice.Viewport.Width * 0.5515625f + (i % 5) * ScreenManager.GraphicsDevice.Viewport.Width * 0.04375f,
                        ScreenManager.GraphicsDevice.Viewport.Height * 0.1980666667f + (i / 5) * ScreenManager.GraphicsDevice.Viewport.Height * 0.07777777778f), SettingsIcon);
                    itemHolderRects[i] = item.rectangle;
                }
                
                //Shop Inventory Positioning
                for (int i = 0; i < shopInventory.activeList.Count; i++)
                {
                    //NEED TO FIX POSITIONING
                    shopInventory.activeList[i].position =
                        new Vector2((ScreenManager.GraphicsDevice.Viewport.Width * 0.5515625f) +
                            (i % 5)
                    * (ScreenManager.GraphicsDevice.Viewport.Width * 0.04375f),
                    (ScreenManager.GraphicsDevice.Viewport.Height * 0.1980666667f)
                    + (i / 5) * (ScreenManager.GraphicsDevice.Viewport.Height * 0.0777777778f));
                    shopInventory.activeList[i].rectangle = new Rectangle((int)shopInventory.activeList[i].position.X, (int)shopInventory.activeList[i].position.Y,
                        shopInventory.activeList[i].texture.Width, shopInventory.activeList[i].texture.Height);
                }

                // once the load has finished, we use ResetElapsedTime to tell the game's
                // timing mechanism that we have just finished a very long frame, and that
                // it should not try to catch up.
                ScreenManager.Game.ResetElapsedTime();
            }
        }

        //Every activate must have a deactivate
        public override void Deactivate()
        {
            base.Deactivate();
        }

        /// <summary>
        /// Unload graphics content used by the game.
        /// </summary>
        public override void Unload()
        {
            // content.Unload();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);

            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                ScreenManager.RemoveScreen(this);



            mousecontroller.MouseUpdate();

            //Rectangle[] prevLocations = Locations;
            currentMouseState = Mouse.GetState();

            //Update Mouse Position
            MouseRect.X = currentMouseState.X;
            MouseRect.Y = currentMouseState.Y;

            if (MouseRect.Intersects(ConfirmRect))
            {
                if (mousecontroller.LeftMouseClick())
                {
                    float buyRate, sellRate;
                    //if (barterValue > 100)
                        buyRate = (2.3f - barterValue / 100.0f) * (130 - discount) / 100;
                    //else
                      //  buyRate = (barterValue / 100.0f) * (130 - discount) / 100;
                    sellRate = barterValue / 100.0f;
                    float cost = ((buyRate * TotalCostBuy) - (sellRate * TotalCostSell));
                    //If rich enough & enough item slots
                    if (GameSession.GS.inventory.money >= cost && GameSession.GS.inventory.activeList.Count + SelectedShopItems.Count - SelectedInventoryItems.Count <= 40)
                    {
                        //Sell Items
                        List<Item> ItemList = new List<Item>();
                        foreach (int i in SelectedInventoryItems)
                        {
                            ItemList.Add(GameSession.GS.inventory.activeList[i]);
                        }
                        foreach (Item j in ItemList)
                        {
                            shopInventory.AddItem(j);
                            GameSession.GS.inventory.activeList.Remove(j);
                        }

                        //Buy Items
                        foreach (int i in SelectedShopItems)
                        {
                            GameSession.GS.inventory.AddItem(shopInventory.activeList[i]);
                            shopInventory.RemoveItem(shopInventory.activeList[i]);
                        }

                        //Positioning

                        for (int i = 0; i < GameSession.GS.inventory.activeList.Count; i++)
                        {
                            //NEED TO FIX POSITIONING
                            GameSession.GS.inventory.activeList[i].position =
                                new Vector2((ScreenManager.GraphicsDevice.Viewport.Width * 0.278125f) +
                                    (i % 5)
                            * (ScreenManager.GraphicsDevice.Viewport.Width * 0.04375f),
                            (ScreenManager.GraphicsDevice.Viewport.Height * 0.1980666667f)
                            + (i / 5) * (ScreenManager.GraphicsDevice.Viewport.Height * 0.0777777778f));
                            //(i / ScreenManager.GraphicsDevice.Viewport.Height * 0.006944444f) 
                            //* ScreenManager.GraphicsDevice.Viewport.Height * 0.10694444f);
                            GameSession.GS.inventory.activeList[i].rectangle = new Rectangle((int)GameSession.GS.inventory.activeList[i].position.X, (int)GameSession.GS.inventory.activeList[i].position.Y,
                                GameSession.GS.inventory.activeList[i].texture.Width, GameSession.GS.inventory.activeList[i].texture.Height);
                        }

                        for (int i = 0; i < shopInventory.activeList.Count; i++)
                        {
                            //NEED TO FIX POSITIONING
                            shopInventory.activeList[i].position =
                                new Vector2((ScreenManager.GraphicsDevice.Viewport.Width * 0.5515625f) +
                                    (i % 5)
                            * (ScreenManager.GraphicsDevice.Viewport.Width * 0.04375f),
                            (ScreenManager.GraphicsDevice.Viewport.Height * 0.1980666667f)
                            + (i / 5) * (ScreenManager.GraphicsDevice.Viewport.Height * 0.0777777778f));
                            shopInventory.activeList[i].rectangle = new Rectangle((int)shopInventory.activeList[i].position.X, (int)shopInventory.activeList[i].position.Y,
                                shopInventory.activeList[i].texture.Width, shopInventory.activeList[i].texture.Height);
                        }

                        GameSession.GS.inventory.money -= (int)cost;
                        SelectedInventoryItems = new List<int>();
                        SelectedShopItems = new List<int>();
                        TotalCostBuy = 0;
                        TotalCostSell = 0;
                    }
                    else if (GameSession.GS.inventory.money < cost)
                    {
                        ScreenManager.notifier.setNewText("Not enough money", "You do not have at least $" + (int)cost);
                        ScreenManager.notifier.StartNotify();
                    }
                }
            }

            for (int i = 0; i < GameSession.GS.inventory.activeList.Count; i++)
            {
                GameSession.GS.inventory.activeList[i].Update(MouseRect);
                if (mousecontroller.LeftMouseClick())
                {
                    if (GameSession.GS.inventory.activeList[i].rectangle.Intersects(MouseRect))
                    {
                        if (GameSession.GS.inventory.activeList[i].quantity > 1)
                        {
                            int ItemQuanCount = 0;
                            while (SelectedInventoryItems.Contains(i))
                            {
                                ItemQuanCount++;
                                SelectedInventoryItems.Remove(i);
                            }
                            if (GameSession.GS.inventory.activeList[i].quantity > ItemQuanCount)
                            {
                                SelectedInventoryItems.Add(i);
                                ItemQuanCount++;
                            }
                            for (int j = 0; j < ItemQuanCount; j++)
                            {
                                SelectedInventoryItems.Add(i);
                            }
                        }
                        else //if (GameSession.GS.inventory.activeList[i].quantity == 1)
                        {
                            if (SelectedInventoryItems.Contains(i))
                            {
                                SelectedInventoryItems.Remove(i);
                                TotalCostSell -= GameSession.GS.inventory.activeList[i].value;
                            }
                            else
                            {
                                SelectedInventoryItems.Add(i);
                                TotalCostSell += GameSession.GS.inventory.activeList[i].value;
                            }
                        }
                        return;
                    }
                }
                else if (mousecontroller.RightMouseClick())
                {
                    //Right mouse click always remove 1 instance of item
                    SelectedInventoryItems.Remove(i);
                }
            }

            for (int i = 0; i < shopInventory.activeList.Count; i++)
            {
                shopInventory.activeList[i].Update(MouseRect);
                if (mousecontroller.LeftMouseClick())
                {
                    if (shopInventory.activeList[i].rectangle.Intersects(MouseRect))
                    {
                        if (shopInventory.activeList[i].quantity > 1)
                        {
                            int ItemQuanCount = 0;
                            while (SelectedShopItems.Contains(i))
                            {
                                ItemQuanCount++;
                                SelectedShopItems.Remove(i);
                            }
                            if (shopInventory.activeList[i].quantity > ItemQuanCount)
                            {
                                SelectedShopItems.Add(i);
                                ItemQuanCount++;
                            }
                            for (int j = 0; j < ItemQuanCount; j++)
                            {
                                SelectedShopItems.Add(i);
                            }
                        }
                        else //if (shopInventory.activeList[i].quantity == 1)
                        {
                            if (SelectedShopItems.Contains(i))
                            {
                                SelectedShopItems.Remove(i);
                                TotalCostBuy -= shopInventory.activeList[i].value;
                            }
                            else
                            {
                                SelectedShopItems.Add(i);
                                TotalCostBuy += shopInventory.activeList[i].value;
                            }
                        }
                        return;
                    }
                }
                else if (mousecontroller.RightMouseClick())
                {
                    //Right mouse click always remove 1 instance of item
                    SelectedInventoryItems.Remove(i);
                }
            }

            if (MouseRect.Intersects(new Rectangle((int)(0.45f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.83472222f * ScreenManager.GraphicsDevice.Viewport.Height), (int)(0.1421875f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.065277778f * ScreenManager.GraphicsDevice.Viewport.Height))))
            {
                if (mousecontroller.LeftMouseClick())
                {
                    GameSession.GS.shopItems.activeList = shopInventory.activeList;
                    ScreenManager.RemoveScreen(this);
                }
            }

            if (MouseRect.Intersects(new Rectangle((ScreenManager.GraphicsDevice.Viewport.Bounds.Right - CancelTex.Width) - 5, 7, CancelTex.Width, CancelTex.Height)))
            {
                if (mousecontroller.LeftMouseClick())
                {
                    GameSession.GS.shopItems.activeList = shopInventory.activeList;
                    ScreenManager.RemoveScreen(this);
                }
            }

            UpdateDisplayText();
            GameSession.gameAudioEngine.Update(gameTime);
            // Reset prevMouseState
            prevMouseState = Mouse.GetState();
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Draw(GameTime gameTime)
        {
            ScreenManager.FadeBackBufferToBlack(TransitionAlpha * 2 / 3);

            ScreenManager.GraphicsDevice.BlendState = BlendState.Opaque;
            ScreenManager.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            //NewParticle.Draw(gameTime, TestCamera);
            frameRate = (float)(1 / gameTime.ElapsedGameTime.TotalSeconds);

            float width = ScreenManager.GraphicsDevice.Viewport.Width;
            float scale = (width / 1920);
            Matrix scalingMatrix = Matrix.CreateScale(scale);
            spriteBatch.Begin(SpriteSortMode.Immediate, null, null, null, null, null, scalingMatrix);

            spriteBatch.Draw(WorldMapTex, new Vector2(ScreenManager.GraphicsDevice.Viewport.TitleSafeArea.X, ScreenManager.GraphicsDevice.Viewport.TitleSafeArea.Y), null, Color.White, 0f, new Vector2(0, 0), 1, SpriteEffects.None, 0f);

            spriteBatch.End();

            //UI DRAWING
            spriteBatch.Begin();
            
            DrawBorderedText(displayText, (int)(0.56015625f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.673611111f * ScreenManager.GraphicsDevice.Viewport.Height), Color.White, InconsolataFont);
            
            foreach (Item item in GameSession.GS.inventory.activeList)
            {
                item.Draw(spriteBatch, InconsolataFont);
            }
            //Reactivate when has a proper inventory to test
            foreach (Item item in shopInventory.activeList)
            {
                item.Draw(spriteBatch, InconsolataFont);
            }

            foreach (int i in SelectedInventoryItems)
            {
                spriteBatch.Draw(Tick, GameSession.GS.inventory.activeList[i].position, Color.White);
            }
            foreach (int i in SelectedShopItems)
            {
                spriteBatch.Draw(Tick, shopInventory.activeList[i].position, Color.White);
            }
            foreach (Item item in shopInventory.activeList)
            {
                item.DrawToolTip(spriteBatch, InconsolataFont);
            }
            foreach (Item item in GameSession.GS.inventory.activeList)
            {
                item.DrawToolTip(spriteBatch, InconsolataFont);
            }

            //  spriteBatch.Draw(players[0].Texture, new Vector2(70, 60) - new Vector2(0, 806) * 0.2f, null, Color.White, 0f, Vector2.Zero, 0.6f, SpriteEffects.None, 0f);
            spriteBatch.Draw(CancelTex, new Vector2((ScreenManager.GraphicsDevice.Viewport.Bounds.Right - CancelTex.Width / 2) - 3, 7 + CancelTex.Height / 2), null, Color.White, 0f, new Vector2(CancelTex.Width / 2, CancelTex.Height / 2), 0.7f, SpriteEffects.None, 0f);
#if DEBUG
            spriteBatch.DrawString(spritefont, "Mouse X : " + MouseRect.X + " Mouse Y : " + MouseRect.Y, new Vector2(50, 100), Color.White);
#endif
            spriteBatch.End();


            base.Draw(gameTime);
        }

        private void DrawModel(Model model, Matrix world, CameraObj camera)
        {
            Matrix[] transforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(transforms);

            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    //effect.SpecularColor = Color.WhiteSmoke.ToVector3();
                    ///effect.SpecularPower = 100.0f;
                    //effect.FogEnabled = true;
                    //effect.FogColor = Color.White.ToVector3();
                    //effect.FogStart = 999999.0f;
                    //effect.FogEnd = 1000000.0f;


                    effect.EnableDefaultLighting();
                    effect.DiffuseColor = new Vector3(0.7f, 0.7f, 0.7f);
                    effect.World = transforms[mesh.ParentBone.Index] * world;
                    // Use the matrices provided by the chase camera
                    effect.View = camera.view;
                    effect.Projection = camera.proj;
                }
                mesh.Draw();
            }
        }

        public void DrawBorderedText(string text, int PosX, int PosY, Color color, SpriteFont font)
        {
            spriteBatch.DrawString(font, text, new Vector2(PosX + 1, PosY + 1), Color.Black * 0.5f);
            spriteBatch.DrawString(font, text, new Vector2(PosX + 1, PosY - 1), Color.Black * 0.5f);
            spriteBatch.DrawString(font, text, new Vector2(PosX - 1, PosY + 1), Color.Black * 0.5f);
            spriteBatch.DrawString(font, text, new Vector2(PosX - 1, PosY - 1), Color.Black * 0.5f);
            spriteBatch.DrawString(font, text, new Vector2(PosX, PosY), color);

        }

        public void UpdateDisplayText()
        {
            String tempString;
            float buyRate, sellRate;
            //if (barterValue < 100)
                buyRate = (2.3f - barterValue / 100.0f) * (130 - discount) / 100;
            //else
              //  buyRate = (barterValue / 100.0f) * (130 - discount) / 100;
            sellRate = barterValue / 100.0f;
            float cost = ((buyRate * TotalCostBuy) - (sellRate * TotalCostSell));
            
            if (cost >= 0)
                tempString = "Cost: $" + (int)cost;
            else
                tempString = "Gain: $" + (int)(-1 * cost);
            displayText = "Money: $" + GameSession.GS.inventory.money + "\n" + tempString + "\nDiscount: " + discount + "%\nRate: " + barterValue + "%";
        }
    }
}

