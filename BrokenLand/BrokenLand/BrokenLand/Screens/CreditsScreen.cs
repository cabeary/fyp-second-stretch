﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using Supernova.Particles2D;
using Supernova.Particles2D.Modifiers.Alpha;
using Supernova.Samples.Windows.Utilities;
using GameStateManagement;
using System.Threading;
using Microsoft.Xna.Framework.Content;
using BrokenLands;
using System.Text.RegularExpressions;


namespace BrokenLands
{
    public class CreditsScreen : GameScreen
    {
        ContentManager content;
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SpriteFont spritefont, InconsolataFont;

        MouseState prevMouseState, currentMouseState;

        List<string> storyStrings = new List<string>();
        Vector2[] storyVectors;

        Rectangle MouseRect;
        Texture2D CancelTex;
        int currentString = 0;

        float FrameCounter, frameRate;

        bool Exiting = false;
        
        MouseController mousecontroller = new MouseController();
        string test;
        public CreditsScreen()
            : base()
        {
            this.ScreenState = ScreenState.Active;

        }



        public override void Activate(bool instancePreserved)
        {
            if (!instancePreserved)
            {
                if (content == null)
                    content = new ContentManager(ScreenManager.Game.Services, "Content");

                //TestCamera = new CameraObj(this, new Vector3(0, 0, 0), new Vector3(0, 0, 10), Vector3.Up, 1f, 1000f, 1f);
                FrameCounter = 0;
                prevMouseState = Mouse.GetState();
                currentMouseState = Mouse.GetState();

                //Loading
                spriteBatch = new SpriteBatch(ScreenManager.GraphicsDevice);

                //Textures
                CancelTex = content.Load<Texture2D>("UI/Icons/Cancel-Icon");


                //Icons


                //Fonts
                spritefont = content.Load<SpriteFont>("Fonts/Times12");
                InconsolataFont = content.Load<SpriteFont>("Fonts/BigNoodle");

                //Variables
                storyStrings.Add("Lead Programmer Net Lim Chu Yan");
                storyStrings.Add("Programmer & Level Designer Net Edrick Hong");
                storyStrings.Add("Lead Art & UI Designer Net Adam Amin");
                storyStrings.Add("Project Manager & Programmer Net Terence Chan");
                storyStrings.Add("Base Art Assets Net GameTextures Net CGTextures");
                storyStrings.Add("Music Net AudioMicro");

                storyVectors = new Vector2[storyStrings.Count];
                for (int i = 0; i < storyStrings.Count; i++)
                {
                    storyVectors[i] = new Vector2(ScreenManager.GraphicsDevice.Viewport.Width * 0.1f, 200 * (i + 1));
                }

                MouseRect = new Rectangle(Mouse.GetState().X, Mouse.GetState().Y, 10, 10);//Used for mouse Collision

                
                
                ScreenManager.Game.ResetElapsedTime();
            }
        }

        //Every activate must have a deactivate
        public override void Deactivate()
        {
            
            base.Deactivate();
            
        }

        public static string SpliceText(string text, int lineLength)
        {
            return Regex.Replace(text, "N.t", Environment.NewLine);
        }
        /// <summary>
        /// Unload graphics content used by the game.
        /// </summary>
        public override void Unload()
        {
            content.Unload();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
            bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);
            
            
            mousecontroller.MouseUpdate();

            //Rectangle[] prevLocations = Locations;
            currentMouseState = Mouse.GetState();

            //Update Mouse Position
            MouseRect.X = currentMouseState.X;
            MouseRect.Y = currentMouseState.Y;
            for (int i = 0; i<storyVectors.Length;i++ )
            {
                storyVectors[i].Y -= 1 * gameTime.ElapsedGameTime.Milliseconds/10;
            }
            if (storyVectors[storyVectors.Length - 1].Y < -100)
            {
                ExitScreen();
            }
            if (MouseRect.Intersects(new Rectangle((ScreenManager.GraphicsDevice.Viewport.Bounds.Right - CancelTex.Width) - 5, 7, CancelTex.Width, CancelTex.Height)))
            {
                if (mousecontroller.LeftMouseClick())
                {
                    ScreenManager.RemoveScreen(this);
                }
            }

            prevMouseState = Mouse.GetState();
            GameSession.gameAudioEngine.Update(gameTime);
        }
        
        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Draw(GameTime gameTime)
        {


            ScreenManager.FadeBackBufferToBlack(TransitionAlpha * 2 / 3);

            ScreenManager.GraphicsDevice.BlendState = BlendState.Opaque;
            ScreenManager.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            frameRate = (float)(1 / gameTime.ElapsedGameTime.TotalSeconds);

            spriteBatch.Begin();
            for(int i = 0;i<storyStrings.Count;i++)
            { 
                spriteBatch.DrawString(InconsolataFont,SpliceText(storyStrings[i],0),storyVectors[i],Color.White);
            }
            spriteBatch.Draw(CancelTex, new Vector2((ScreenManager.GraphicsDevice.Viewport.Bounds.Right - CancelTex.Width / 2) - 3, 7 + CancelTex.Height / 2), null, Color.White, 0f, new Vector2(CancelTex.Width / 2, CancelTex.Height / 2), 0.7f, SpriteEffects.None, 0f);
            spriteBatch.End();


            base.Draw(gameTime);
        }

    }
}

