﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using Supernova.Particles2D;
using Supernova.Particles2D.Modifiers.Alpha;
using Supernova.Samples.Windows.Utilities;
using GameStateManagement;
using System.Threading;
using Microsoft.Xna.Framework.Content;


namespace BrokenLands
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class CharacterCreationScreen : GameScreen
    {

        ContentManager content;
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SpriteFont spritefont, InconsolataFont;

        MouseState prevMouseState, currentMouseState;
        KeyboardState lastKeyboardState;

        float FrameCounter, frameRate;
        Texture2D WorldMapTex, WorldMapUIOverlay;
        Rectangle WorldMapTexPos;

        //Texture2D CancelTex;
        int CurrentTex;
        Player TempPlayer = new Player(10, 10, 10, 10, 10, 10, 1, "Insert Name");

        //List<InventoryItem> Items = new List<InventoryItem>();

        float[] ButtonAlpha;

        Texture2D SettingsIcon;

        int[] AddedPts;
        int TotalPts;

        Rectangle[] StatPlusRects;
        Rectangle[] StatMinusRects;

        Rectangle MouseRect;
        Rectangle ConfirmationRect;
        Rectangle ChangeUnitRectLeft;
        Rectangle ChangeUnitRectRight;

        bool isNameSelected;
        String name;
        Random random;

        MouseController mousecontroller = new MouseController();

        public CharacterCreationScreen()
            : base()
        {
            this.ScreenState = ScreenState.Active;

        }



        public override void Activate(bool instancePreserved)
        {
            if (!instancePreserved)
            {
                if (content == null)
                    content = new ContentManager(ScreenManager.Game.Services, "Content");

                //TestCamera = new CameraObj(this, new Vector3(0, 0, 0), new Vector3(0, 0, 10), Vector3.Up, 1f, 1000f, 1f);
                FrameCounter = 0;
                prevMouseState = Mouse.GetState();
                currentMouseState = Mouse.GetState();

                //Loading
                spriteBatch = new SpriteBatch(ScreenManager.GraphicsDevice);

                //Textures
                WorldMapTex = content.Load<Texture2D>("UI/CharacterCreationScreen-UI");


                //Icons
                SettingsIcon = content.Load<Texture2D>("UI/Icons/Settings-Icon");
                //CancelTex = content.Load<Texture2D>("UI/Icons/Cancel-Icon");

                //Fonts
                spritefont = content.Load<SpriteFont>("Fonts/Times12");
                InconsolataFont = content.Load<SpriteFont>("Fonts/Inconstella");

                //Character Textures;
                CurrentTex = 0;
                TempPlayer.Texture = GameSession.GS.CharacterTextureList[CurrentTex];
                
                //Variables
                WorldMapTexPos = new Rectangle(0, 0, UIElement.viewport.Width, UIElement.viewport.Height);
                random = new Random();
                MouseRect = new Rectangle(Mouse.GetState().X, Mouse.GetState().Y, 10, 10);//Used for mouse Collision
                ButtonAlpha = new float[5];

                //Items = new List<InventoryItem>(40);

                //Temp to get in Units in
                Model Tile = content.Load<Model>("Tile/Tile");

                StatPlusRects = new Rectangle[6];
                StatMinusRects = new Rectangle[6];

                for (int i = 0; i < 6; i++)
                {
                    StatMinusRects[i].X = (int)(0.65625f * ScreenManager.GraphicsDevice.Viewport.Width);
                    StatMinusRects[i].Y = (int)((0.0930556f + 0.12083333f * i) * ScreenManager.GraphicsDevice.Viewport.Height);
                    StatMinusRects[i].Width = (int)(0.0265625f * ScreenManager.GraphicsDevice.Viewport.Width);
                    StatMinusRects[i].Height = (int)(0.03333 * ScreenManager.GraphicsDevice.Viewport.Height);
                }

                for (int i = 0; i < 6; i++)
                {
                    StatPlusRects[i].X = (int)(0.8953125f * ScreenManager.GraphicsDevice.Viewport.Width);
                    StatPlusRects[i].Y = (int)((0.0930556f + 0.12083333f * i) * ScreenManager.GraphicsDevice.Viewport.Height);
                    StatPlusRects[i].Width = (int)(0.0265625f * ScreenManager.GraphicsDevice.Viewport.Width);
                    StatPlusRects[i].Height = (int)(0.03333 * ScreenManager.GraphicsDevice.Viewport.Height);
                }

                ConfirmationRect = new Rectangle((int)(0.70859375f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.80555556f * ScreenManager.GraphicsDevice.Viewport.Height), (int)(0.16640625f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.0583333333f * ScreenManager.GraphicsDevice.Viewport.Height));
                ChangeUnitRectLeft = new Rectangle((int)(0.05859375f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.20138888889f * ScreenManager.GraphicsDevice.Viewport.Height), (int)(0.0548675f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.07361111f * ScreenManager.GraphicsDevice.Viewport.Height));
                ChangeUnitRectRight = new Rectangle((int)(0.52734375f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.20138888889f * ScreenManager.GraphicsDevice.Viewport.Height), (int)(0.0548675f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.07361111f * ScreenManager.GraphicsDevice.Viewport.Height));

                AddedPts = new int[6];
                for (int i = 0; i < 6; i++)
                {
                    AddedPts[i] = 10;
                }
                TotalPts = 60;
                isNameSelected = false;
                name = "";

                // once the load has finished, we use ResetElapsedTime to tell the game's
                // timing mechanism that we have just finished a very long frame, and that
                // it should not try to catch up.

                foreach (QuestObject q in QuestManager.questList)
                {
                    //If any condition is complete, means it was a completed quest - reset it
                    try
                    {
                        if (q.conditionList[0].condition > 0 || q.inactiveConditionList[0].condition > 0)
                        {
                            foreach (Condition c in q.conditionList)
                            {
                                c.condition = 0;
                            }
                            foreach (Condition c in q.inactiveConditionList)
                            {
                                c.condition = 0;
                            }
                        }
                    }
                    catch (Exception e) { }
                }

                QuestManager.activeList.Clear();

                ScreenManager.Game.ResetElapsedTime();
                ScreenManager.fader.FadeInScreen(2000);
            }
        }

        //Every activate must have a deactivate
        public override void Deactivate()
        {
            base.Deactivate();
        }

        /// <summary>
        /// Unload graphics content used by the game.
        /// </summary>
        public override void Unload()
        {
            // content.Unload();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);



            mousecontroller.MouseUpdate();

            //Rectangle[] prevLocations = Locations;
            currentMouseState = Mouse.GetState();

            //Update Mouse Position
            MouseRect.X = currentMouseState.X;
            MouseRect.Y = currentMouseState.Y;

            for (int i = 0; i < StatMinusRects.Length; i++)
            {
                if (MouseRect.Intersects(StatMinusRects[i]))
                {
                    if (mousecontroller.LeftMouseClick())
                    {
                        if (AddedPts[i] > 10)
                        {
                            AddedPts[i]--;
                            TotalPts--;
                            Console.WriteLine("Minused Points");
                        }
                    }
                }
            }

            for (int i = 0; i < StatPlusRects.Length; i++)
            {
                if (MouseRect.Intersects(StatPlusRects[i]))
                {
                    if (mousecontroller.LeftMouseClick())
                    {
                        if (TotalPts < 90 && AddedPts[i] < 20)
                        {
                            AddedPts[i]++;
                            TotalPts++;
                            Console.WriteLine("Increased Points");
                        }
                    }
                }
            }

            if (MouseRect.Intersects(ConfirmationRect))
            {
                if (mousecontroller.LeftMouseClick())
                {
                    if (TotalPts == 90)
                    {
                        if (name == "" || name == "Name")
                        {
                            name = "NewGuy";
                        }
                        Player newPlayer = new Player(AddedPts[0], AddedPts[1], AddedPts[2], AddedPts[3], AddedPts[4], AddedPts[5], 1, name);
                        newPlayer.Texture = GameSession.GS.CharacterTextureList[CurrentTex];
                        newPlayer.MyStatus.AbilityID = 9;
                        //newPlayer.MyStatus.Range = 4;
                        GameSession.players = new List<Player>();
                        GameSession.players.Add(newPlayer);
                        Equipment eq = EquipmentLoader.fetchEquipment(19);//newPlayer.equipList["LeftWeapon"];
                       // eq.type = Equipment.EquipType.Weapon;
                        GameSession.players[GameSession.players.Count - 1].Equip(eq);
                        GameSession.party = new DungeonParty(new List<Entity> { GameSession.players[0] });
                        ScreenManager.fader.FadeOutScreen(1000);
                    }
                }
            }
            else if (MouseRect.Intersects(ChangeUnitRectLeft))
            {
                if (mousecontroller.LeftMouseClick())
                {
                    CurrentTex--;
                    if (CurrentTex < 0)
                        CurrentTex = GameSession.GS.CharacterTextureList.Count - 1;
                    TempPlayer.Texture = GameSession.GS.CharacterTextureList[CurrentTex];
                }
            }
            else if (MouseRect.Intersects(ChangeUnitRectRight))
            {
                if (mousecontroller.LeftMouseClick())
                {
                    CurrentTex++;
                    if (CurrentTex > GameSession.GS.CharacterTextureList.Count - 1)
                        CurrentTex = 0;
                    TempPlayer.Texture = GameSession.GS.CharacterTextureList[CurrentTex];
                }
            }
            /*else if (MouseRect.Intersects(new Rectangle((ScreenManager.GraphicsDevice.Viewport.Bounds.Right - CancelTex.Width) - 5, 7, CancelTex.Width, CancelTex.Height)))
            {
                if (mousecontroller.LeftMouseClick())
                {
                    ScreenManager.RemoveScreen(this);
                }
            }*/

            if (isNameSelected)
            {
                if (Keyboard.GetState().IsKeyDown(Keys.A))
                {
                    if (lastKeyboardState.IsKeyUp(Keys.A))
                    {
                        if (Keyboard.GetState().IsKeyDown(Keys.LeftShift) || Keyboard.GetState().IsKeyDown(Keys.RightShift))
                            name += "A";
                        else
                            name += "a";
                    }
                }
                else 
                if (Keyboard.GetState().IsKeyDown(Keys.B))
                {
                    if (lastKeyboardState.IsKeyUp(Keys.B))
                    {
                        if (Keyboard.GetState().IsKeyDown(Keys.LeftShift) || Keyboard.GetState().IsKeyDown(Keys.RightShift))
                            name += "B";
                        else
                            name += "b";
                    }
                }
                else 
                if (Keyboard.GetState().IsKeyDown(Keys.C))
                {
                    if (lastKeyboardState.IsKeyUp(Keys.C))
                    {
                        if (Keyboard.GetState().IsKeyDown(Keys.LeftShift) || Keyboard.GetState().IsKeyDown(Keys.RightShift))
                            name += "C";
                        else
                            name += "c";
                    }
                }
                else 
                if (Keyboard.GetState().IsKeyDown(Keys.D))
                {
                    if (lastKeyboardState.IsKeyUp(Keys.D))
                    {
                        if (Keyboard.GetState().IsKeyDown(Keys.LeftShift) || Keyboard.GetState().IsKeyDown(Keys.RightShift))
                            name += "D";
                        else
                            name += "d";
                    }
                }
                else 
                if (Keyboard.GetState().IsKeyDown(Keys.E))
                {
                    if (lastKeyboardState.IsKeyUp(Keys.E))
                    {
                        if (Keyboard.GetState().IsKeyDown(Keys.LeftShift) || Keyboard.GetState().IsKeyDown(Keys.RightShift))
                            name += "E";
                        else
                            name += "e";
                    }
                }
                else 
                if (Keyboard.GetState().IsKeyDown(Keys.F))
                {
                    if (lastKeyboardState.IsKeyUp(Keys.F))
                    {
                        if (Keyboard.GetState().IsKeyDown(Keys.LeftShift) || Keyboard.GetState().IsKeyDown(Keys.RightShift))
                            name += "F";
                        else
                            name += "f";
                    }
                }
                else 
                if (Keyboard.GetState().IsKeyDown(Keys.G))
                {
                    if (lastKeyboardState.IsKeyUp(Keys.G))
                    {
                        if (Keyboard.GetState().IsKeyDown(Keys.LeftShift) || Keyboard.GetState().IsKeyDown(Keys.RightShift))
                            name += "G";
                        else
                            name += "g";
                    }
                }
                else 
                if (Keyboard.GetState().IsKeyDown(Keys.H))
                {
                    if (lastKeyboardState.IsKeyUp(Keys.H))
                    {
                        if (Keyboard.GetState().IsKeyDown(Keys.LeftShift) || Keyboard.GetState().IsKeyDown(Keys.RightShift))
                            name += "H";
                        else
                            name += "h";
                    }
                }
                else 
                if (Keyboard.GetState().IsKeyDown(Keys.I))
                {
                    if (lastKeyboardState.IsKeyUp(Keys.I))
                    {
                        if (Keyboard.GetState().IsKeyDown(Keys.LeftShift) || Keyboard.GetState().IsKeyDown(Keys.RightShift))
                            name += "I";
                        else
                            name += "i";
                    }
                }
                else 
                if (Keyboard.GetState().IsKeyDown(Keys.J))
                {
                    if (lastKeyboardState.IsKeyUp(Keys.J))
                    {
                        if (Keyboard.GetState().IsKeyDown(Keys.LeftShift) || Keyboard.GetState().IsKeyDown(Keys.RightShift))
                            name += "J";
                        else
                            name += "j";
                    }
                }
                else 
                if (Keyboard.GetState().IsKeyDown(Keys.K))
                {
                    if (lastKeyboardState.IsKeyUp(Keys.K))
                    {
                        if (Keyboard.GetState().IsKeyDown(Keys.LeftShift) || Keyboard.GetState().IsKeyDown(Keys.RightShift))
                            name += "K";
                        else
                            name += "k";
                    }
                }
                else 
                if (Keyboard.GetState().IsKeyDown(Keys.L))
                {
                    if (lastKeyboardState.IsKeyUp(Keys.L))
                    {
                        if (Keyboard.GetState().IsKeyDown(Keys.LeftShift) || Keyboard.GetState().IsKeyDown(Keys.RightShift))
                            name += "L";
                        else
                            name += "l";
                    }
                }
                else 
                if (Keyboard.GetState().IsKeyDown(Keys.M))
                {
                    if (lastKeyboardState.IsKeyUp(Keys.M))
                    {
                        if (Keyboard.GetState().IsKeyDown(Keys.LeftShift) || Keyboard.GetState().IsKeyDown(Keys.RightShift))
                            name += "M";
                        else
                            name += "m";
                    }
                }
                else 
                if (Keyboard.GetState().IsKeyDown(Keys.N))
                {
                    if (lastKeyboardState.IsKeyUp(Keys.N))
                    {
                        if (Keyboard.GetState().IsKeyDown(Keys.LeftShift) || Keyboard.GetState().IsKeyDown(Keys.RightShift))
                            name += "N";
                        else
                            name += "n";
                    }
                }
                else 
                if (Keyboard.GetState().IsKeyDown(Keys.O))
                {
                    if (lastKeyboardState.IsKeyUp(Keys.O))
                    {
                        if (Keyboard.GetState().IsKeyDown(Keys.LeftShift) || Keyboard.GetState().IsKeyDown(Keys.RightShift))
                            name += "O";
                        else
                            name += "o";
                    }
                }
                else 
                if (Keyboard.GetState().IsKeyDown(Keys.P))
                {
                    if (lastKeyboardState.IsKeyUp(Keys.P))
                    {
                        if (Keyboard.GetState().IsKeyDown(Keys.LeftShift) || Keyboard.GetState().IsKeyDown(Keys.RightShift))
                            name += "P";
                        else
                            name += "p";
                    }
                }
                else 
                if (Keyboard.GetState().IsKeyDown(Keys.Q))
                {
                    if (lastKeyboardState.IsKeyUp(Keys.Q))
                    {
                        if (Keyboard.GetState().IsKeyDown(Keys.LeftShift) || Keyboard.GetState().IsKeyDown(Keys.RightShift))
                            name += "Q";
                        else
                            name += "q";
                    }
                }
                else 
                if (Keyboard.GetState().IsKeyDown(Keys.R))
                {
                    if (lastKeyboardState.IsKeyUp(Keys.R))
                    {
                        if (Keyboard.GetState().IsKeyDown(Keys.LeftShift) || Keyboard.GetState().IsKeyDown(Keys.RightShift))
                            name += "R";
                        else
                            name += "r";
                    }
                }
                else 
                if (Keyboard.GetState().IsKeyDown(Keys.S))
                {
                    if (lastKeyboardState.IsKeyUp(Keys.S))
                    {
                        if (Keyboard.GetState().IsKeyDown(Keys.LeftShift) || Keyboard.GetState().IsKeyDown(Keys.RightShift))
                            name += "S";
                        else
                            name += "s";
                    }
                }
                else 
                if (Keyboard.GetState().IsKeyDown(Keys.T))
                {
                    if (lastKeyboardState.IsKeyUp(Keys.T))
                    {
                        if (Keyboard.GetState().IsKeyDown(Keys.LeftShift) || Keyboard.GetState().IsKeyDown(Keys.RightShift))
                            name += "T";
                        else
                            name += "t";
                    }
                }
                else 
                if (Keyboard.GetState().IsKeyDown(Keys.U))
                {
                    if (lastKeyboardState.IsKeyUp(Keys.U))
                    {
                        if (Keyboard.GetState().IsKeyDown(Keys.LeftShift) || Keyboard.GetState().IsKeyDown(Keys.RightShift))
                            name += "U";
                        else
                            name += "u";
                    }
                }
                else 
                if (Keyboard.GetState().IsKeyDown(Keys.V))
                {
                    if (lastKeyboardState.IsKeyUp(Keys.V))
                    {
                        if (Keyboard.GetState().IsKeyDown(Keys.LeftShift) || Keyboard.GetState().IsKeyDown(Keys.RightShift))
                            name += "V";
                        else
                            name += "v";
                    }
                }
                else 
                if (Keyboard.GetState().IsKeyDown(Keys.W))
                {
                    if (lastKeyboardState.IsKeyUp(Keys.W))
                    {
                        if (Keyboard.GetState().IsKeyDown(Keys.LeftShift) || Keyboard.GetState().IsKeyDown(Keys.RightShift))
                            name += "W";
                        else
                            name += "w";
                    }
                }
                else 
                if (Keyboard.GetState().IsKeyDown(Keys.X))
                {
                    if (lastKeyboardState.IsKeyUp(Keys.X))
                    {
                        if (Keyboard.GetState().IsKeyDown(Keys.LeftShift) || Keyboard.GetState().IsKeyDown(Keys.RightShift))
                            name += "X";
                        else
                            name += "x";
                    }
                }
                else 
                if (Keyboard.GetState().IsKeyDown(Keys.Y))
                {
                    if (lastKeyboardState.IsKeyUp(Keys.Y))
                    {
                        if (Keyboard.GetState().IsKeyDown(Keys.LeftShift) || Keyboard.GetState().IsKeyDown(Keys.RightShift))
                            name += "Y";
                        else
                            name += "y";
                    }
                }
                else 
                if (Keyboard.GetState().IsKeyDown(Keys.Z))
                {
                    if (lastKeyboardState.IsKeyUp(Keys.Z))
                    {
                        if (Keyboard.GetState().IsKeyDown(Keys.LeftShift) || Keyboard.GetState().IsKeyDown(Keys.RightShift))
                            name += "Z";
                        else
                            name += "z";
                    }
                }
                else
                if (Keyboard.GetState().IsKeyDown(Keys.Space))
                {
                    if (lastKeyboardState.IsKeyUp(Keys.Space))
                    {
                        name += " ";
                    }
                }
                else 
                if (Keyboard.GetState().IsKeyDown(Keys.Back))
                {
                    if (lastKeyboardState.IsKeyUp(Keys.Back))
                    {
                        if (name.Length > 0)
                            name = name.Remove(name.Length - 1);
                    }
                }
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Enter))
            {
                if (lastKeyboardState.IsKeyUp(Keys.Enter))
                {
                    isNameSelected = !isNameSelected;
                    if (TempPlayer._NAME == "Insert Name")
                    {
                        TempPlayer._NAME = "";
                    }
                }
            }
            else if (MouseRect.Intersects(new Rectangle((int)(ScreenManager.GraphicsDevice.Viewport.Width * 0.21640625f), (int)(ScreenManager.GraphicsDevice.Viewport.Height * 0.030555556f), (int)(ScreenManager.GraphicsDevice.Viewport.Width * 0.21015625f), (int)(ScreenManager.GraphicsDevice.Viewport.Height * 0.045833333f))))
            {
                if (mousecontroller.LeftMouseClick())
                {
                    isNameSelected = !isNameSelected; 
                    if (TempPlayer._NAME == "Insert Name")
                    {
                        TempPlayer._NAME = "";
                    }
                }
            }

            if (ScreenManager.fader.checkFadeOutOver())
            {
                ScreenManager.AddScreen(new WorldMapScreen(), null);
            }

            GameSession.gameAudioEngine.Update(gameTime);
            // Reset prevMouseState
            prevMouseState = Mouse.GetState();
            lastKeyboardState = Keyboard.GetState();

        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Draw(GameTime gameTime)
        {

            ScreenManager.GraphicsDevice.Clear(Color.Black);

            ScreenManager.GraphicsDevice.BlendState = BlendState.Opaque;
            ScreenManager.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            //NewParticle.Draw(gameTime, TestCamera);
            frameRate = (float)(1 / gameTime.ElapsedGameTime.TotalSeconds);

            float width = ScreenManager.GraphicsDevice.Viewport.Width;
            float scale = (width / 1920);
            Matrix scalingMatrix = Matrix.CreateScale(scale);
            spriteBatch.Begin(SpriteSortMode.Immediate, null, null, null, null, null, scalingMatrix);

            spriteBatch.Draw(WorldMapTex, new Vector2(ScreenManager.GraphicsDevice.Viewport.TitleSafeArea.X, ScreenManager.GraphicsDevice.Viewport.TitleSafeArea.Y), null, Color.White, 0f, new Vector2(0, 0), 1, SpriteEffects.None, 0f);

            spriteBatch.End();

            //UI DRAWING
            spriteBatch.Begin();
            #region Draw Stats
            DrawBorderedText(name,
                (int)(ScreenManager.GraphicsDevice.Viewport.Width / 3.047) - (int)(spritefont.MeasureString(name).X / 2),
                (int)(ScreenManager.GraphicsDevice.Viewport.Height / 42.6667) + (int)(spritefont.MeasureString(name).Y / 2),
                Color.White,
                spritefont);
            
            spriteBatch.Draw(TempPlayer.Texture, new Vector2((int)(ScreenManager.GraphicsDevice.Viewport.Width * 0.12890625), (int)(ScreenManager.GraphicsDevice.Viewport.Height * 0.02116667)), new Rectangle(0, 0, TempPlayer.animator.frameWidth, TempPlayer.animator.frameHeight), Color.White, 0.0f, new Vector2(0), 2.5f, SpriteEffects.None, 0f);
            
            #endregion

            #region Draw Dialogue Box
            if (MouseRect.Intersects(new Rectangle((int)(0.68359375f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.068055556f * ScreenManager.GraphicsDevice.Viewport.Height), (int)(0.2109375f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.0819444444f * ScreenManager.GraphicsDevice.Viewport.Height))) || MouseRect.Intersects(StatPlusRects[0]) || MouseRect.Intersects(StatMinusRects[0]))
            {
                spriteBatch.DrawString(InconsolataFont, "Base Strength : " + AddedPts[0],new Vector2((int)(0.0390625f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.695f * ScreenManager.GraphicsDevice.Viewport.Height)), Color.White);
                spriteBatch.DrawString(InconsolataFont, "Strength is the stat that affects physical activity \nThis includes melee combat and forcing open doors\nRelated Skills: Melee/Scavenge", new Vector2((int)(0.0390625f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.7638888889f * ScreenManager.GraphicsDevice.Viewport.Height)), Color.White);
                //DrawBorderedText("Base Strength : " + AddedPts[0], (int)(0.0390625f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.7538888889f * ScreenManager.GraphicsDevice.Viewport.Height), Color.White, InconsolataFont);
                //DrawBorderedText("Strength is the stat that affects physical activity \nThis includes melee combat and forcing open doors\nRelated Skills: Melee/Scavenge", (int)(0.0390625f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.7938888889f * ScreenManager.GraphicsDevice.Viewport.Height), Color.White, InconsolataFont);
            }
            else if (MouseRect.Intersects(new Rectangle((int)(0.68359375f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.1888888889f * ScreenManager.GraphicsDevice.Viewport.Height), (int)(0.2109375f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.081944444f * ScreenManager.GraphicsDevice.Viewport.Height))) || MouseRect.Intersects(StatPlusRects[1]) || MouseRect.Intersects(StatMinusRects[1]))
            {
                spriteBatch.DrawString(InconsolataFont, "Base Dexterity : " + AddedPts[1], new Vector2((int)(0.0390625f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.695f * ScreenManager.GraphicsDevice.Viewport.Height)), Color.White);
                spriteBatch.DrawString(InconsolataFont, "Dexterity is the stat that affects deftness\nThis includes accuracy and speed of attacks\nRelated Skills: Scavenge/Lockpick", new Vector2((int)(0.0390625f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.7638888889f * ScreenManager.GraphicsDevice.Viewport.Height)), Color.White);
                //DrawBorderedText("Base Dexterity : " + AddedPts[1], (int)(0.0390625f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.7538888889f * ScreenManager.GraphicsDevice.Viewport.Height), Color.White, InconsolataFont);
                //DrawBorderedText("Dexterity is the stat that affects deftness\nThis includes accuracy and speed of attacks\nRelated Skills: Scavenge/Lockpick", (int)(0.0390625f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.7938888889f * ScreenManager.GraphicsDevice.Viewport.Height), Color.White, InconsolataFont);
            }
            else if (MouseRect.Intersects(new Rectangle((int)(0.68359375f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.309722222f * ScreenManager.GraphicsDevice.Viewport.Height), (int)(0.2109375f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.0819444444f * ScreenManager.GraphicsDevice.Viewport.Height))) || MouseRect.Intersects(StatPlusRects[2]) || MouseRect.Intersects(StatMinusRects[2]))
            {
                spriteBatch.DrawString(InconsolataFont, "Base Intelligence : " + AddedPts[2], new Vector2((int)(0.0390625f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.695f * ScreenManager.GraphicsDevice.Viewport.Height)), Color.White);
                spriteBatch.DrawString(InconsolataFont, "Intelligence is the stat that affects brainpower\nThis includes negotiation and knowledge\nRelated Skills: Lockpick/Healing/Barter", new Vector2((int)(0.0390625f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.7638888889f * ScreenManager.GraphicsDevice.Viewport.Height)), Color.White);
                //DrawBorderedText("Base Intelligence : " + AddedPts[2], (int)(0.0390625f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.7538888889f * ScreenManager.GraphicsDevice.Viewport.Height), Color.White, InconsolataFont);
                //DrawBorderedText("Intelligence is the stat that affects brainpower\nThis includes negotiation and knowledge\nRelated Skills: Lockpick/Healing/Barter", (int)(0.0390625f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.7938888889f * ScreenManager.GraphicsDevice.Viewport.Height), Color.White, InconsolataFont);
            }
            else if (MouseRect.Intersects(new Rectangle((int)(0.68359375f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.430555556f * ScreenManager.GraphicsDevice.Viewport.Height), (int)(0.2109375f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.0819444444f * ScreenManager.GraphicsDevice.Viewport.Height))) || MouseRect.Intersects(StatPlusRects[3]) || MouseRect.Intersects(StatMinusRects[3]))
            {
                spriteBatch.DrawString(InconsolataFont, "Base Charisma : " + AddedPts[3], new Vector2((int)(0.0390625f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.695f * ScreenManager.GraphicsDevice.Viewport.Height)), Color.White);
                spriteBatch.DrawString(InconsolataFont, "Charisma is the stat that affects charm\nThis includes negotiation and social skills\nRelated Skills: Barter/Healing", new Vector2((int)(0.0390625f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.7638888889f * ScreenManager.GraphicsDevice.Viewport.Height)), Color.White);
                
                //DrawBorderedText("Base Charisma : " + AddedPts[3], (int)(0.0390625f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.7538888889f * ScreenManager.GraphicsDevice.Viewport.Height), Color.White, InconsolataFont);
                //DrawBorderedText("Charisma is the stat that affects charm\nThis includes negotiation and social skills\nRelated Skills: Barter/Healing", (int)(0.0390625f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.7938888889f * ScreenManager.GraphicsDevice.Viewport.Height), Color.White, InconsolataFont);
            }
            else if (MouseRect.Intersects(new Rectangle((int)(0.68359375f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.551388889f * ScreenManager.GraphicsDevice.Viewport.Height), (int)(0.2109375f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.0819444444f * ScreenManager.GraphicsDevice.Viewport.Height))) || MouseRect.Intersects(StatPlusRects[4]) || MouseRect.Intersects(StatMinusRects[4]))
            {
                spriteBatch.DrawString(InconsolataFont, "Base Endurance : " + AddedPts[4], new Vector2((int)(0.0390625f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.695f * ScreenManager.GraphicsDevice.Viewport.Height)), Color.White);
                spriteBatch.DrawString(InconsolataFont, "Endurance is the stat that affects toughness\nThis includes survivalbility and ability to take hits\nRelated Skills: Melee", new Vector2((int)(0.0390625f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.7638888889f * ScreenManager.GraphicsDevice.Viewport.Height)), Color.White);
                //DrawBorderedText("Base Endurance : " + AddedPts[4], (int)(0.0390625f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.7538888889f * ScreenManager.GraphicsDevice.Viewport.Height), Color.White, InconsolataFont);
                //DrawBorderedText("Endurance is the stat that affects toughness\nThis includes survivalbility and ability to take hits\nRelated Skills: Melee", (int)(0.0390625f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.7938888889f * ScreenManager.GraphicsDevice.Viewport.Height), Color.White, InconsolataFont);
            }
            else if (MouseRect.Intersects(new Rectangle((int)(0.68359375f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.67222222f * ScreenManager.GraphicsDevice.Viewport.Height), (int)(0.2109375f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.0819444444f * ScreenManager.GraphicsDevice.Viewport.Height))) || MouseRect.Intersects(StatPlusRects[5]) || MouseRect.Intersects(StatMinusRects[5]))
            {
                spriteBatch.DrawString(InconsolataFont, "Base Agility : " + AddedPts[5], new Vector2((int)(0.0390625f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.695f * ScreenManager.GraphicsDevice.Viewport.Height)), Color.White);
                spriteBatch.DrawString(InconsolataFont, "Agility is the stat that affects speed\nThis includes avoidability and speed\nRelated Skills: Action Points", new Vector2((int)(0.0390625f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.7638888889f * ScreenManager.GraphicsDevice.Viewport.Height)), Color.White);
                
                //DrawBorderedText("Base Agility : " + AddedPts[5], (int)(0.0390625f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.7538888889f * ScreenManager.GraphicsDevice.Viewport.Height), Color.White, InconsolataFont);
                //DrawBorderedText("Agility is the stat that affects speed\nThis includes avoidability and speed\nRelated Skills: Action Points", (int)(0.0390625f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.7938888889f * ScreenManager.GraphicsDevice.Viewport.Height), Color.White, InconsolataFont);
            }
            else
            {
                spriteBatch.DrawString(InconsolataFont, "Note : Once confirmed, Base Stats cannot be changed once confirmed.\nType Name by clicking on Name box or pressing Enter.\nMelee: " + ((Math.Pow(AddedPts[0], 2) + Math.Pow(AddedPts[4] - 5, 2)) / 20) + "    Scavenge: " + ((Math.Pow(AddedPts[1], 2) + Math.Pow(AddedPts[0] - 5, 2)) / 20) + "\nLockpick: " + ((Math.Pow(AddedPts[1], 2) + Math.Pow(AddedPts[2] - 5, 2)) / 20) + "    Barter: " + ((Math.Pow(AddedPts[3], 2) + Math.Pow(AddedPts[2] - 5, 2)) / 20) + "    Heal: " + ((Math.Pow(AddedPts[2], 2) + Math.Pow(AddedPts[3] - 5, 2)) / 20), new Vector2((int)(0.0390625f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.7638888889f * ScreenManager.GraphicsDevice.Viewport.Height)), Color.White);
                //DrawBorderedText("Note : Once confirmed, Base Stats cannot be changed once confirmed.\nType Name by clicking on Name box or pressing Enter.\nMelee: " + ((Math.Pow(AddedPts[0], 2) + Math.Pow(AddedPts[4] - 5, 2)) / 20) + "    Scavenge: " + ((Math.Pow(AddedPts[1], 2) + Math.Pow(AddedPts[0] - 5, 2)) / 20) + "\nLockpick: " + ((Math.Pow(AddedPts[1], 2) + Math.Pow(AddedPts[2] - 5, 2)) / 20) + "    Barter: " + ((Math.Pow(AddedPts[3], 2) + Math.Pow(AddedPts[2] - 5, 2)) / 20) + "    Heal: " + ((Math.Pow(AddedPts[2], 2) + Math.Pow(AddedPts[3] - 5, 2)) / 20), (int)(0.0390625f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.7638888889f * ScreenManager.GraphicsDevice.Viewport.Height), Color.White, InconsolataFont);
            }
            #endregion

            #region Draw Stats & Added values
            DrawBorderedText("Pts : " + (90 - TotalPts), (int)(0.5244375f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.097222222f * ScreenManager.GraphicsDevice.Viewport.Height), Color.White, InconsolataFont);
            DrawBorderedText(" Strength      : " + AddedPts[0], (int)(0.70703125f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.1f * ScreenManager.GraphicsDevice.Viewport.Height), Color.White, InconsolataFont);
            DrawBorderedText(" Dexterity     : " + AddedPts[1], (int)(0.70703125f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.22022222f * ScreenManager.GraphicsDevice.Viewport.Height), Color.White, InconsolataFont);
            DrawBorderedText(" Intelligence  : " + AddedPts[2], (int)(0.70703125f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.34044444f * ScreenManager.GraphicsDevice.Viewport.Height), Color.White, InconsolataFont);
            DrawBorderedText(" Charisma      : " + AddedPts[3], (int)(0.70703125f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.46066666f * ScreenManager.GraphicsDevice.Viewport.Height), Color.White, InconsolataFont);
            DrawBorderedText(" Endurance     : " + AddedPts[4], (int)(0.70703125f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.58088888f * ScreenManager.GraphicsDevice.Viewport.Height), Color.White, InconsolataFont);
            DrawBorderedText(" Agility       : " + AddedPts[5], (int)(0.70703125f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.70111111f * ScreenManager.GraphicsDevice.Viewport.Height), Color.White, InconsolataFont);
            DrawBorderedText("Confirm", (int)(0.7659375f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.82255556f * ScreenManager.GraphicsDevice.Viewport.Height), Color.White, InconsolataFont);
            #endregion

            //  spriteBatch.Draw(players[0].Texture, new Vector2(70, 60) - new Vector2(0, 806) * 0.2f, null, Color.White, 0f, Vector2.Zero, 0.6f, SpriteEffects.None, 0f);
            //spriteBatch.Draw(CancelTex, new Vector2((ScreenManager.GraphicsDevice.Viewport.Bounds.Right - CancelTex.Width / 2) - 3, 7 + CancelTex.Height / 2), null, Color.White, 0f, new Vector2(CancelTex.Width / 2, CancelTex.Height / 2), 0.7f, SpriteEffects.None, 0f);
#if DEBUG
            spriteBatch.DrawString(spritefont, "Mouse X : " + MouseRect.X + " Mouse Y : " + MouseRect.Y, new Vector2(50, 100), Color.White);
#endif
            spriteBatch.End();


            base.Draw(gameTime);
        }


        private void DrawModel(Model model, Matrix world, CameraObj camera)
        {
            Matrix[] transforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(transforms);

            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    //effect.SpecularColor = Color.WhiteSmoke.ToVector3();
                    ///effect.SpecularPower = 100.0f;
                    //effect.FogEnabled = true;
                    //effect.FogColor = Color.White.ToVector3();
                    //effect.FogStart = 999999.0f;
                    //effect.FogEnd = 1000000.0f;


                    effect.EnableDefaultLighting();
                    effect.DiffuseColor = new Vector3(0.7f, 0.7f, 0.7f);
                    effect.World = transforms[mesh.ParentBone.Index] * world;
                    // Use the matrices provided by the chase camera
                    effect.View = camera.view;
                    effect.Projection = camera.proj;
                }
                mesh.Draw();
            }
        }

        public void DrawBorderedText(string text, int PosX, int PosY, Color color, SpriteFont font)
        {
            spriteBatch.DrawString(font, text, new Vector2(PosX + 1, PosY + 1), Color.Black * 0.5f);
            spriteBatch.DrawString(font, text, new Vector2(PosX + 1, PosY - 1), Color.Black * 0.5f);
            spriteBatch.DrawString(font, text, new Vector2(PosX - 1, PosY + 1), Color.Black * 0.5f);
            spriteBatch.DrawString(font, text, new Vector2(PosX - 1, PosY - 1), Color.Black * 0.5f);
            spriteBatch.DrawString(font, text, new Vector2(PosX, PosY), color);

        }

        public void ResetPoints()
        {
            for (int i = 0; i < AddedPts.Length; i++)
            {
                AddedPts[i] = 10;
            }
            TotalPts = 60;
        }
    }
}

