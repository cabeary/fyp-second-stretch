﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using Supernova.Particles2D;
using Supernova.Particles2D.Modifiers.Alpha;
using Supernova.Samples.Windows.Utilities;
using GameStateManagement;
using System.Threading;
using Microsoft.Xna.Framework.Content;
using BrokenLands;
using BrokenLands;


namespace BrokenLands
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class PartyScreen : GameScreen
    {

        ContentManager content;
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SpriteFont spritefont, InconsolataFont;

        MouseState prevMouseState, currentMouseState;

        float FrameCounter, frameRate;
        Texture2D WorldMapTex, WorldMapUIOverlay;
        Rectangle WorldMapTexPos;

        Texture2D CancelTex;

        //List<InventoryItem> Items = new List<InventoryItem>();

        float[] ButtonAlpha;

        Texture2D SettingsIcon;

        Rectangle MouseRect;
        Rectangle[] CharRects;

        List<Player> players = new List<Player>();
        int CurrentActivePlayer = 5;

        Random random;

        MouseController mousecontroller = new MouseController();

        public PartyScreen()
            : base()
        {
            this.ScreenState = ScreenState.Active;

        }



        public override void Activate(bool instancePreserved)
        {
            if (!instancePreserved)
            {
                if (content == null)
                    content = new ContentManager(ScreenManager.Game.Services, "Content");

                //TestCamera = new CameraObj(this, new Vector3(0, 0, 0), new Vector3(0, 0, 10), Vector3.Up, 1f, 1000f, 1f);
                FrameCounter = 0;
                prevMouseState = Mouse.GetState();
                currentMouseState = Mouse.GetState();

                //Loading
                spriteBatch = new SpriteBatch(ScreenManager.GraphicsDevice);

                //Textures
                WorldMapTex = content.Load<Texture2D>("UI/PartyScreen-UI");
                
                //Icons
                SettingsIcon = content.Load<Texture2D>("UI/Icons/Settings-Icon");
                CancelTex = content.Load<Texture2D>("UI/Icons/Cancel-Icon");

                //Fonts
                spritefont = content.Load<SpriteFont>("Fonts/Times12");
                InconsolataFont = content.Load<SpriteFont>("Fonts/Inconstella");

                //Models

                //Variables
                WorldMapTexPos = new Rectangle(0, 0, UIElement.viewport.Width, UIElement.viewport.Height);
                random = new Random();
                MouseRect = new Rectangle(Mouse.GetState().X, Mouse.GetState().Y, 10, 10);//Used for mouse Collision
                ButtonAlpha = new float[5];

                CharRects = new Rectangle[5];
                for (int i = 0; i < 5; i++)
                {
                    Rectangle newRect = new Rectangle((int)(ScreenManager.GraphicsDevice.Viewport.Width * 0.0640625), (int)(ScreenManager.GraphicsDevice.Viewport.Height * (0.07916667 + i * 0.15833333)), (int)(ScreenManager.GraphicsDevice.Viewport.Width * 0.2109375), (int)(ScreenManager.GraphicsDevice.Viewport.Height * 0.11805556));
                    CharRects[i] = newRect;
                }

                //Items = new List<InventoryItem>(40);

                //Temp to get in Units in
                Model Tile = content.Load<Model>("Tile/Tile");
                Texture2D tex = content.Load<Texture2D>("Units/Gunbowman");

                players = GameSession.players;
                //players[0].Texture = tex;
                CurrentActivePlayer = 0;

                ScreenManager.Game.ResetElapsedTime();
            }
        }

        //Every activate must have a deactivate
        public override void Deactivate()
        {
            base.Deactivate();
        }

        /// <summary>
        /// Unload graphics content used by the game.
        /// </summary>
        public override void Unload()
        {
            // content.Unload();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);

            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                ScreenManager.RemoveScreen(this);
            
            mousecontroller.MouseUpdate();

            //Rectangle[] prevLocations = Locations;
            currentMouseState = Mouse.GetState();

            //Update Mouse Position
            MouseRect.X = currentMouseState.X;
            MouseRect.Y = currentMouseState.Y;

            for(int r = 0; r < CharRects.Length; r++)
            {
                if (MouseRect.Intersects(CharRects[r]))
                {
                    if (mousecontroller.LeftMouseClick())
                    {
                        CurrentActivePlayer = r;
                    }
                }
            }

            if (MouseRect.Intersects(new Rectangle((ScreenManager.GraphicsDevice.Viewport.Bounds.Right - CancelTex.Width) - 5, 7, CancelTex.Width, CancelTex.Height)))
            {
                if (mousecontroller.LeftMouseClick())
                {
                    ScreenManager.RemoveScreen(this);
                }
            }

            // Reset prevMouseState
            GameSession.gameAudioEngine.Update(gameTime);
            prevMouseState = Mouse.GetState();
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Draw(GameTime gameTime)
        {
            ScreenManager.FadeBackBufferToBlack(TransitionAlpha * 2 / 3);

            ScreenManager.GraphicsDevice.BlendState = BlendState.Opaque;
            ScreenManager.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            //NewParticle.Draw(gameTime, TestCamera);
            frameRate = (float)(1 / gameTime.ElapsedGameTime.TotalSeconds);

            float width = ScreenManager.GraphicsDevice.Viewport.Width;
            float scale = (width / 1920);
            Matrix scalingMatrix = Matrix.CreateScale(scale);
            spriteBatch.Begin(SpriteSortMode.Immediate, null, null, null, null, null, scalingMatrix);

            spriteBatch.Draw(WorldMapTex, new Vector2(ScreenManager.GraphicsDevice.Viewport.TitleSafeArea.X, ScreenManager.GraphicsDevice.Viewport.TitleSafeArea.Y), null, Color.White, 0f, new Vector2(0, 0), 1, SpriteEffects.None, 0f);

            spriteBatch.End();

            //UI DRAWING
            spriteBatch.Begin();

            for (int i = 0; i < CharRects.Length; i++)
            {
                if (players.Count > i)
                {
                    //spriteBatch.Draw(players[i].animator., new Rectangle(CharRects[i].X + (int)(ScreenManager.GraphicsDevice.Viewport.Width * 0.01f), CharRects[i].Y + (int)(ScreenManager.GraphicsDevice.Viewport.Width * 0.01f), CharRects[i].Height - (int)(ScreenManager.GraphicsDevice.Viewport.Width * 0.02f), CharRects[i].Height - (int)(ScreenManager.GraphicsDevice.Viewport.Width * 0.02f)), Color.White);
                    spriteBatch.Draw(players[i].Texture, new Vector2(CharRects[i].X - (int)(ScreenManager.GraphicsDevice.Viewport.Width * 0.025f), CharRects[i].Y - (int)(ScreenManager.GraphicsDevice.Viewport.Width * 0.025f)), new Rectangle(0,0, players[i].animator.frameWidth, players[i].animator.frameHeight), Color.White, 0.0f, new Vector2(0), 0.7f, SpriteEffects.None, 0f);
                    DrawBorderedText(players[i]._NAME, CharRects[i].X + CharRects[i].Height + (int)(0.05 * ScreenManager.GraphicsDevice.Viewport.Width) - (int)InconsolataFont.MeasureString(players[i]._NAME).X / 2, CharRects[i].Y + CharRects[i].Height / 2 - (int)(InconsolataFont.MeasureString(players[i]._NAME).Y) / 2 + (int)(ScreenManager.GraphicsDevice.Viewport.Height * 0.01f), Color.White, InconsolataFont);
                }
                else
                {
                    DrawBorderedText("EMPTY", CharRects[i].X + CharRects[i].Width / 2 - (int)InconsolataFont.MeasureString("EMPTY").X / 2, CharRects[i].Y + CharRects[i].Height / 2 - (int)InconsolataFont.MeasureString("EMPTY").Y / 2 + (int)(ScreenManager.GraphicsDevice.Viewport.Height * 0.01f), Color.White, InconsolataFont);
                }
            }

            if (CurrentActivePlayer < players.Count)
            {
                spriteBatch.Draw(players[CurrentActivePlayer].Texture, new Vector2((int)(ScreenManager.GraphicsDevice.Viewport.Width * 0.44890625), (int)(ScreenManager.GraphicsDevice.Viewport.Height * 0.02116667)) , new Rectangle(0, 0, players[CurrentActivePlayer].animator.frameWidth, players[CurrentActivePlayer].animator.frameHeight), Color.White, 0.0f, new Vector2(0), 2.5f, SpriteEffects.None, 0f);
                #region Draw Stats
                DrawBorderedText(players[CurrentActivePlayer]._NAME,
                    (int)(ScreenManager.GraphicsDevice.Viewport.Width * 0.634375) - (int)(spritefont.MeasureString(players[CurrentActivePlayer]._NAME).X / 2),
                    (int)(ScreenManager.GraphicsDevice.Viewport.Height * 0.02116667) + (int)(spritefont.MeasureString(players[CurrentActivePlayer]._NAME).Y / 2),
                    Color.White,
                    spritefont);

                DrawBorderedText(players[CurrentActivePlayer].MyStatus.BaseStr.ToString(),
                    (int)(ScreenManager.GraphicsDevice.Viewport.Width * 0.42546875) - (int)(spritefont.MeasureString(players[CurrentActivePlayer].MyStatus.BaseStr.ToString()).X / 2),
                    (int)(ScreenManager.GraphicsDevice.Viewport.Height * 0.25222),
                    Color.White,
                    spritefont);

                DrawBorderedText(players[CurrentActivePlayer].MyStatus.BaseDex.ToString(),
                    (int)(ScreenManager.GraphicsDevice.Viewport.Width * 0.42546875) - (int)(spritefont.MeasureString(players[CurrentActivePlayer].MyStatus.BaseStr.ToString()).X / 2),
                    (int)(ScreenManager.GraphicsDevice.Viewport.Height * 0.25222) + (int)(ScreenManager.GraphicsDevice.Viewport.Height / 23.5687) * 1,
                    Color.White,
                    spritefont);

                DrawBorderedText(players[CurrentActivePlayer].MyStatus.BaseInt.ToString(),
                    (int)(ScreenManager.GraphicsDevice.Viewport.Width * 0.42546875) - (int)(spritefont.MeasureString(players[CurrentActivePlayer].MyStatus.BaseStr.ToString()).X / 2),
                    (int)(ScreenManager.GraphicsDevice.Viewport.Height * 0.25222) + (int)(ScreenManager.GraphicsDevice.Viewport.Height / 23.5687) * 2,
                    Color.White,
                    spritefont);

                DrawBorderedText(players[CurrentActivePlayer].MyStatus.BaseCha.ToString(),
                    (int)(ScreenManager.GraphicsDevice.Viewport.Width * 0.42546875) - (int)(spritefont.MeasureString(players[CurrentActivePlayer].MyStatus.BaseStr.ToString()).X / 2),
                    (int)(ScreenManager.GraphicsDevice.Viewport.Height * 0.25222) + (int)(ScreenManager.GraphicsDevice.Viewport.Height / 23.5687) * 3,
                    Color.White,
                    spritefont);
                
                DrawBorderedText(players[CurrentActivePlayer].MyStatus.BaseEnd.ToString(),
                    (int)(ScreenManager.GraphicsDevice.Viewport.Width * 0.42546875) - (int)(spritefont.MeasureString(players[CurrentActivePlayer].MyStatus.BaseStr.ToString()).X / 2),
                    (int)(ScreenManager.GraphicsDevice.Viewport.Height * 0.25222) + (int)(ScreenManager.GraphicsDevice.Viewport.Height / 23.5687) * 4,
                    Color.White,
                    spritefont);

                DrawBorderedText(players[CurrentActivePlayer].MyStatus.BaseAgi.ToString(),
                    (int)(ScreenManager.GraphicsDevice.Viewport.Width * 0.42546875) - (int)(spritefont.MeasureString(players[CurrentActivePlayer].MyStatus.BaseStr.ToString()).X / 2),
                    (int)(ScreenManager.GraphicsDevice.Viewport.Height * 0.25222) + (int)(ScreenManager.GraphicsDevice.Viewport.Height / 23.5687) * 5,
                    Color.White,
                    spritefont);
                #endregion
                #region Draw Skills

                DrawBorderedText(players[CurrentActivePlayer].MyStatus.Skills.melee.ToString(),
                    (int)(ScreenManager.GraphicsDevice.Viewport.Width * 0.875375) - (int)(spritefont.MeasureString(players[CurrentActivePlayer].MyStatus.BaseStr.ToString()).X / 2),
                    (int)(ScreenManager.GraphicsDevice.Viewport.Height * 0.26922),
                    Color.White,
                    spritefont);

                DrawBorderedText(players[CurrentActivePlayer].MyStatus.Skills.scavenge.ToString(),
                    (int)(ScreenManager.GraphicsDevice.Viewport.Width * 0.875375) - (int)(spritefont.MeasureString(players[CurrentActivePlayer].MyStatus.BaseStr.ToString()).X / 2),
                    (int)(ScreenManager.GraphicsDevice.Viewport.Height * 0.26922) + (int)(ScreenManager.GraphicsDevice.Viewport.Height / 24.0987) * 1,
                    Color.White,
                    spritefont);

                DrawBorderedText(players[CurrentActivePlayer].MyStatus.Skills.lockpick.ToString(),
                    (int)(ScreenManager.GraphicsDevice.Viewport.Width * 0.875375) - (int)(spritefont.MeasureString(players[CurrentActivePlayer].MyStatus.BaseStr.ToString()).X / 2),
                    (int)(ScreenManager.GraphicsDevice.Viewport.Height * 0.26922) + (int)(ScreenManager.GraphicsDevice.Viewport.Height / 24.0987) * 2,
                    Color.White,
                    spritefont);

                DrawBorderedText(players[CurrentActivePlayer].MyStatus.Skills.barter.ToString(),
                    (int)(ScreenManager.GraphicsDevice.Viewport.Width * 0.875375) - (int)(spritefont.MeasureString(players[CurrentActivePlayer].MyStatus.BaseStr.ToString()).X / 2),
                    (int)(ScreenManager.GraphicsDevice.Viewport.Height * 0.26922) + (int)(ScreenManager.GraphicsDevice.Viewport.Height / 24.0987) * 3,
                    Color.White,
                    spritefont);

                DrawBorderedText(players[CurrentActivePlayer].MyStatus.Skills.heal.ToString(),
                    (int)(ScreenManager.GraphicsDevice.Viewport.Width * 0.875375) - (int)(spritefont.MeasureString(players[CurrentActivePlayer].MyStatus.BaseStr.ToString()).X / 2),
                    (int)(ScreenManager.GraphicsDevice.Viewport.Height * 0.26922) + (int)(ScreenManager.GraphicsDevice.Viewport.Height / 24.0987) * 4,
                    Color.White,
                    spritefont);
                #endregion
                #region Draw Desc
                String Desc = "";
                Desc += "EXP: " + players[CurrentActivePlayer].MyStatus.Exp + "\n";
                Desc += "Max HP: " + players[CurrentActivePlayer].MyStatus.calcHP() + " | Max AP: " + players[CurrentActivePlayer].MyStatus.calcAP() + "\n";
                Desc += "Ability: " + GameSession.AbilityList.AbilityList[players[CurrentActivePlayer].MyStatus.AbilityID].Title + "\n";
                Desc += GameSession.AbilityList.AbilityList[players[CurrentActivePlayer].MyStatus.AbilityID].Desc;
                DrawBorderedText(Desc, (int)(ScreenManager.GraphicsDevice.Viewport.Width * 0.34375), (int)(ScreenManager.GraphicsDevice.Viewport.Height * 0.7638889), Color.White, InconsolataFont);

                #endregion
            }
          //  spriteBatch.Draw(players[CurrentActivePlayer].Texture, new Vector2(70, 60) - new Vector2(0, 806) * 0.2f, null, Color.White, 0f, Vector2.Zero, 0.6f, SpriteEffects.None, 0f);
            spriteBatch.Draw(CancelTex, new Vector2((ScreenManager.GraphicsDevice.Viewport.Bounds.Right - CancelTex.Width / 2) - 3, 7 + CancelTex.Height / 2), null, Color.White, 0f, new Vector2(CancelTex.Width / 2, CancelTex.Height / 2), 0.7f, SpriteEffects.None, 0f);
#if DEBUG
            //spriteBatch.DrawString(spritefont, "Mouse X : " + MouseRect.X + " Mouse Y : " + MouseRect.Y, new Vector2(50, 100), Color.White);
#endif
            spriteBatch.End();


            base.Draw(gameTime);
        }


        private void DrawModel(Model model, Matrix world, CameraObj camera)
        {
            Matrix[] transforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(transforms);

            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    //effect.SpecularColor = Color.WhiteSmoke.ToVector3();
                    ///effect.SpecularPower = 100.0f;
                    //effect.FogEnabled = true;
                    //effect.FogColor = Color.White.ToVector3();
                    //effect.FogStart = 999999.0f;
                    //effect.FogEnd = 1000000.0f;


                    effect.EnableDefaultLighting();
                    effect.DiffuseColor = new Vector3(0.7f, 0.7f, 0.7f);
                    effect.World = transforms[mesh.ParentBone.Index] * world;
                    // Use the matrices provided by the chase camera
                    effect.View = camera.view;
                    effect.Projection = camera.proj;
                }
                mesh.Draw();
            }
        }

        public void DrawBorderedText(string text, int PosX, int PosY, Color color, SpriteFont font)
        {
            spriteBatch.DrawString(font, text, new Vector2(PosX + 1, PosY + 1), Color.Black * 0.5f);
            spriteBatch.DrawString(font, text, new Vector2(PosX + 1, PosY - 1), Color.Black * 0.5f);
            spriteBatch.DrawString(font, text, new Vector2(PosX - 1, PosY + 1), Color.Black * 0.5f);
            spriteBatch.DrawString(font, text, new Vector2(PosX - 1, PosY - 1), Color.Black * 0.5f);
            spriteBatch.DrawString(font, text, new Vector2(PosX, PosY), color);

        }
    }
}

