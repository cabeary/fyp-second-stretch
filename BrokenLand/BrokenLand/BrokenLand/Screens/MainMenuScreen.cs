﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using Supernova.Particles2D;
using Supernova.Particles2D.Modifiers.Alpha;
using Supernova.Samples.Windows.Utilities;
using GameStateManagement;
using System.Threading;
using Microsoft.Xna.Framework.Content;
using BrokenLands;



namespace BrokenLands
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class MainMenuScreen : GameScreen
    {

        ContentManager content;
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SpriteFont spritefont, InconsolataFont;

        MouseState prevMouseState, currentMouseState;
        Texture2D WorldMapTex;
        Rectangle WorldMapTexPos;

        Rectangle MouseRect;

        float FrameCounter, frameRate;

        Texture2D MainMenuAnyBack, MainMenuAnyText, MainMenuNormBack;
        Texture2D MainMenuNormStart, MainMenuNormContinue, MainMenuNormCredits, MainMenuNormQuit;
        Rectangle StartRect, ContinueRect,OptionsRect,QuitRect;
        float StartOpacity, ContinueOpacity, OptionsOpacity, QuitOpacity;
        
        MouseController mousecontroller = new MouseController();
        
        bool PressKey;
        String NextScreen;

        public MainMenuScreen()
            : base()
        {
            
            this.ScreenState = ScreenState.Active;

        }

        public override void Activate(bool instancePreserved)
        {
            if (!instancePreserved)
            {
                if (content == null)
                    content = new ContentManager(ScreenManager.Game.Services, "Content");

                //TestCamera = new CameraObj(this, new Vector3(0, 0, 0), new Vector3(0, 0, 10), Vector3.Up, 1f, 1000f, 1f);
                FrameCounter = 0;
                prevMouseState = Mouse.GetState();
                currentMouseState = Mouse.GetState();

                //Loading
                spriteBatch = new SpriteBatch(ScreenManager.GraphicsDevice);
                
                //Textures
                WorldMapTex = content.Load<Texture2D>("UI/MainMenuScreen");
                MainMenuAnyBack = content.Load<Texture2D>("UI/MainMenuAnyBack");
                MainMenuAnyText = content.Load<Texture2D>("UI/MainMenuAnyText");
                MainMenuNormBack = content.Load<Texture2D>("UI/MainMenuNormBack");
                MainMenuNormContinue = content.Load<Texture2D>("UI/MainMenuNormContinue");
                MainMenuNormStart = content.Load<Texture2D>("UI/MainMenuNormStart");
                MainMenuNormCredits = content.Load<Texture2D>("UI/MainMenuCredits");
                MainMenuNormQuit = content.Load<Texture2D>("UI/MainMenuNormQuit");

                //Icons
                

                //Fonts
                spritefont = content.Load<SpriteFont>("Fonts/Times12");
                InconsolataFont = content.Load<SpriteFont>("Fonts/Inconstella");

                //Variables
                WorldMapTexPos = new Rectangle(0, 0, UIElement.viewport.Width, UIElement.viewport.Height);
                MouseRect = new Rectangle(Mouse.GetState().X, Mouse.GetState().Y, 10, 10);//Used for mouse Collision
                PressKey = false;
                StartRect = new Rectangle((int)(ScreenManager.GraphicsDevice.Viewport.Width*0.805f),(int)(ScreenManager.GraphicsDevice.Viewport.Height*0.6f),MainMenuNormStart.Width,MainMenuNormStart.Height);
                ContinueRect = new Rectangle((int)(ScreenManager.GraphicsDevice.Viewport.Width * 0.821f), (int)(ScreenManager.GraphicsDevice.Viewport.Height * 0.68f), MainMenuNormContinue.Width, MainMenuNormContinue.Height);
                OptionsRect = new Rectangle((int)(ScreenManager.GraphicsDevice.Viewport.Width * 0.83f), (int)(ScreenManager.GraphicsDevice.Viewport.Height * 0.76f), MainMenuNormCredits.Width, MainMenuNormCredits.Height);
                QuitRect = new Rectangle((int)(ScreenManager.GraphicsDevice.Viewport.Width * 0.855f), (int)(ScreenManager.GraphicsDevice.Viewport.Height * 0.84f), MainMenuNormQuit.Width, MainMenuNormQuit.Height);
                
                //Temp to get in Units in
                Model Tile = content.Load<Model>("Tile/Tile");
                Texture2D tex = content.Load<Texture2D>("Units/Gunbowman");

                QuestManager.loadQuests();
                GameSession.GS.UpdateMusic();
                ScreenManager.Game.ResetElapsedTime();
                ScreenManager.fader.FadeInNOutFrom0fImage(2000, 0.5f);
            }
        }

        //Every activate must have a deactivate
        public override void Deactivate()
        {
            base.Deactivate();
        }

        /// <summary>
        /// Unload graphics content used by the game.
        /// </summary>
        public override void Unload()
        {
            content.Unload();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
            bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);
            
            mousecontroller.MouseUpdate();

            //Rectangle[] prevLocations = Locations;
            currentMouseState = Mouse.GetState();

            //Update Mouse Position
            MouseRect.X = currentMouseState.X;
            MouseRect.Y = currentMouseState.Y;

            // Reset prevMouseState

            
            if (PressKey == false)
            {
                if (mousecontroller.LeftMouseClick() || Keyboard.GetState().GetPressedKeys().GetLength(0) > 0)
                {
                    PressKey = true;
                    ScreenManager.fader.resetFader();
                    ScreenManager.fader.FadeInImage(2000);
                }
            }
            else
            {
                if (MouseRect.Intersects(StartRect))
                {
                    StartOpacity = 1.0f;
                    if (mousecontroller.LeftMouseClick())
                    {
                        NextScreen = "CharacterCreation";
                        ScreenManager.fader.FadeOutScreen(1000);
                    }
                }
                else
                {
                    StartOpacity = 0.7f;
                }

                if (MouseRect.Intersects(ContinueRect))
                {
                    ContinueOpacity = 1.0f;
                    if (mousecontroller.LeftMouseClick())
                    {
                        GameSession.map = new Map(content.Load<Texture2D>("MapObjects/defaultfloor"), content.Load<Texture2D>("MapObjects/BattleArenaSide"));
                        GameSession.map.mapGen = new MapGenerator(ScreenManager.Game, GameSession.GS.defaultModel, GameSession.map);
                        GameSession.map.mapGen.loader.loadObject(0, 12, new Vector2(0));
                        GameSession.map = null;
                        if (GameSession.players.Count > 0)
                        {
                            NextScreen = "Continue";
                            ScreenManager.fader.FadeOutScreen(1000);
                        }
                        else
                        {
                            ScreenManager.notifier.setNewText("File not found", "Characters missing, please start new game.");
                            ScreenManager.notifier.StartNotify();
                        }
                    }
                }
                else
                {
                    ContinueOpacity = 0.7f;
                }

                if (MouseRect.Intersects(OptionsRect))
                {
                    OptionsOpacity = 1.0f;
                    if (mousecontroller.LeftMouseClick())
                    {
                        NextScreen = "Credit";
                        ScreenManager.AddScreen(new CreditsScreen(), null);
                    }
                }
                else
                {
                    OptionsOpacity = 0.7f;
                }

                if (MouseRect.Intersects(QuitRect))
                {
                    QuitOpacity = 1.0f;
                    if (mousecontroller.LeftMouseClick())
                    {
                        ScreenManager.Game.Exit();
                    }
                }
                else
                {
                    QuitOpacity = 0.7f;
                }
            }
            if (ScreenManager.fader.checkFadeOutOver())
            {
#if DEBUG
                if (NextScreen == "CharacterCreation")
                {
                    ScreenManager.AddScreen(new CharacterCreationScreen(), null);//CharacterCreationScreen(), null);
                }
                else if (NextScreen == "Continue")
                {
                    GameSession.map = new Map(content.Load<Texture2D>("MapObjects/defaultfloor"), content.Load<Texture2D>("MapObjects/BattleArenaSide"));
                    GameSession.map.mapGen = new MapGenerator(ScreenManager.Game, GameSession.GS.defaultModel, GameSession.map);
                    GameSession.map.mapGen.loader.loadObject(0, 12, new Vector2(0));
                    GameSession.map.mapGen.loader.loadObject(0, 15, new Vector2(0));
                    GameSession.map = null;
                    List<Entity> PartyUnits = new List<Entity>();
                    for (int unitNo = 0; unitNo < 3; unitNo++)
                    {
                        if (GameSession.players.Count > unitNo)
                        {
                            PartyUnits.Add(GameSession.players[unitNo]);
                        }
                    }
                    GameSession.party = new DungeonParty(PartyUnits);
                    ScreenManager.AddScreen(new WorldMapScreen(), null);
                    
                }
#else
                if (NextScreen == "CharacterCreation")
                {
                    ScreenManager.AddScreen(new StoryScreen(), null);//CharacterCreationScreen(), null);
                }
                else if (NextScreen == "Continue")
                {
                    GameSession.map = new Map(content.Load<Texture2D>("MapObjects/defaultfloor"), content.Load<Texture2D>("MapObjects/BattleArenaSide"));
                    GameSession.map.mapGen = new MapGenerator(ScreenManager.Game, GameSession.GS.defaultModel, GameSession.map);
                    GameSession.map.mapGen.loader.loadObject(0, 12, new Vector2(0));
                    GameSession.map.mapGen.loader.loadObject(0, 15, new Vector2(0));
                    GameSession.map = null;
                    List<Entity> PartyUnits = new List<Entity>();
                    for (int unitNo = 0; unitNo < 3; unitNo++)
                    {
                        if (GameSession.players.Count > unitNo)
                        {
                            PartyUnits.Add(GameSession.players[unitNo]);
                        }
                    }
                    GameSession.party = new DungeonParty(PartyUnits);
                    ScreenManager.AddScreen(new WorldMapScreen(), null);
                    
                }
#endif
                ExitScreen();
            }

            GameSession.gameAudioEngine.Update(gameTime);
            prevMouseState = Mouse.GetState();

        }
        
        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Draw(GameTime gameTime)
        {


            ScreenManager.FadeBackBufferToBlack(TransitionAlpha * 2 / 3);

            ScreenManager.GraphicsDevice.BlendState = BlendState.Opaque;
            ScreenManager.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            //NewParticle.Draw(gameTime, TestCamera);
            frameRate = (float)(1 / gameTime.ElapsedGameTime.TotalSeconds);



            float width = ScreenManager.GraphicsDevice.Viewport.Width;
            float scale = (width / 1920);
            Matrix scalingMatrix = Matrix.CreateScale(scale);
            spriteBatch.Begin(SpriteSortMode.Immediate, null, null, null, null, null, scalingMatrix);
            spriteBatch.Draw(WorldMapTex, new Vector2(ScreenManager.GraphicsDevice.Viewport.TitleSafeArea.X, ScreenManager.GraphicsDevice.Viewport.TitleSafeArea.Y), null, Color.White, 0f, new Vector2(0, 0), 1, SpriteEffects.None, 0f);

            if (PressKey == false)
            {
                spriteBatch.Draw(MainMenuAnyBack, Vector2.Zero, Color.White * ScreenManager.fader.Opacity);
                spriteBatch.Draw(MainMenuAnyText, Vector2.Zero, Color.White * ScreenManager.fader.Opacity);
            }
            else 
            {
                spriteBatch.Draw(MainMenuNormBack, Vector2.Zero, Color.White * ScreenManager.fader.Opacity);
            }

            
            spriteBatch.End();
            spriteBatch.Begin();
            if (PressKey)
            {
                spriteBatch.Draw(MainMenuNormStart,new Vector2(StartRect.X,StartRect.Y), null, Color.White * StartOpacity, 0f, Vector2.Zero,scale, SpriteEffects.None, 0f);
                spriteBatch.Draw(MainMenuNormContinue, new Vector2(ContinueRect.X, ContinueRect.Y), null, Color.White * ContinueOpacity, 0f, Vector2.Zero, scale, SpriteEffects.None, 0f);
                spriteBatch.Draw(MainMenuNormCredits, new Vector2(OptionsRect.X,OptionsRect.Y), null, Color.White * OptionsOpacity, 0f, Vector2.Zero,scale, SpriteEffects.None, 0f);
                spriteBatch.Draw(MainMenuNormQuit, new Vector2(QuitRect.X,QuitRect.Y), null, Color.White * QuitOpacity, 0f, Vector2.Zero,scale, SpriteEffects.None, 0f);
            }

            //mousecontroller.MouseDraw(spriteBatch);
            spriteBatch.End();


            base.Draw(gameTime);
        }

    }
}

