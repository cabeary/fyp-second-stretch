﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using Supernova.Particles2D;
using Supernova.Particles2D.Modifiers.Alpha;
using Supernova.Samples.Windows.Utilities;
using GameStateManagement;
using System.Threading;
using Microsoft.Xna.Framework.Content;
using BrokenLands;


namespace BrokenLands
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    internal class LootScreen : GameScreen
    {

        ContentManager content;
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SpriteFont spritefont, InconsolataFont;

        MouseState prevMouseState, currentMouseState;

        string hoverName, hoverDesc;

        float FrameCounter, frameRate;
        Texture2D WorldMapTex, WorldMapUIOverlay;
        Rectangle WorldMapTexPos;

        Texture2D CancelTex;
        Texture2D CancelNorm, CancelHover;

        Item currentActiveItem;

        float[] ButtonAlpha;

        Texture2D SettingsIcon;

        Rectangle[] itemHolderRects;
        Rectangle[] lootHolderRects;
        Inventory LootInventory;

        //ToolTip Box 
        UIBack ToolTipBack;
        Texture2D ToolTipTex;
        Vector2 ToolTipBackPos;

        Rectangle MouseRect;

        List<Player> players = new List<Player>();

        Random random;

        MouseController mousecontroller = new MouseController();

        public LootScreen(Inventory Loot)
            : base()
        {
            LootInventory = Loot;
            this.ScreenState = ScreenState.Active;

        }



        public override void Activate(bool instancePreserved)
        {
            if (!instancePreserved)
            {
                if (content == null)
                    content = new ContentManager(ScreenManager.Game.Services, "Content");

                //TestCamera = new CameraObj(this, new Vector3(0, 0, 0), new Vector3(0, 0, 10), Vector3.Up, 1f, 1000f, 1f);
                FrameCounter = 0;
                prevMouseState = Mouse.GetState();
                currentMouseState = Mouse.GetState();

                //Loading
                spriteBatch = new SpriteBatch(ScreenManager.GraphicsDevice);
                random = new Random();

                //Textures
                WorldMapTex = content.Load<Texture2D>("UI/Loot-UI");
                ToolTipTex = content.Load<Texture2D>("UI/UI-TooltipBack");

                //Icons
                SettingsIcon = content.Load<Texture2D>("UI/Icons/Settings-Icon");
                CancelNorm = content.Load<Texture2D>("UI/Continue-Btn");
                CancelHover = content.Load<Texture2D>("UI/Continue-Btn-Hover");
                CancelTex = CancelNorm;

                //Fonts
                spritefont = content.Load<SpriteFont>("Fonts/Times12");
                InconsolataFont = content.Load<SpriteFont>("Fonts/Inconstella");

                //Variables
                WorldMapTexPos = new Rectangle(0, 0, UIElement.viewport.Width, UIElement.viewport.Height);
                MouseRect = new Rectangle(Mouse.GetState().X, Mouse.GetState().Y, 10, 10);//Used for mouse Collision
                ButtonAlpha = new float[5];
                ToolTipBackPos = new Vector2(0,0);
                ToolTipBack = new UIBack(ToolTipTex, ToolTipBackPos, 1.0f);
                ToolTipBack.isHidden = true;

                //Items = new List<InventoryItem>(40);

                //Temp to get in Units in
                Model Tile = content.Load<Model>("Tile/Tile");

                //Texture2D tex = content.Load<Texture2D>("Units/Gunbowman");

                players = GameSession.players;
                
                hoverDesc = null;
                hoverName = null;



                itemHolderRects = new Rectangle[42];
                lootHolderRects = new Rectangle[27];
                for (int i = 0; i < 40; i++)
                {
                    Item item = new Item(i, "Poop", "Fecal Material", 1, 1f, new Vector2(ScreenManager.GraphicsDevice.Viewport.Width * 0.28725f + (i % 5) * ScreenManager.GraphicsDevice.Viewport.Width * 0.041625f,
                        ScreenManager.GraphicsDevice.Viewport.Height * 0.1989f + (i / 5) * ScreenManager.GraphicsDevice.Viewport.Height * 0.081167f), SettingsIcon);
                    itemHolderRects[i] = item.rectangle;
                }
                for (int i = 0; i < 25; i++)
                {
                    Item item = new Item(i, "Poop", "Fecal Material", 1, 1f, new Vector2(ScreenManager.GraphicsDevice.Viewport.Width * 0.555f + (i % 5) * ScreenManager.GraphicsDevice.Viewport.Width * 0.041625f,
                        ScreenManager.GraphicsDevice.Viewport.Height * 0.1989f + (i / 5) * ScreenManager.GraphicsDevice.Viewport.Height * 0.081167f), SettingsIcon);
                    lootHolderRects[i] = item.rectangle;
                }
                for (int i = 0; i < GameSession.GS.inventory.activeList.Count; i++)
                {
                    GameSession.GS.inventory.activeList[i].position = new Vector2(itemHolderRects[i].X, itemHolderRects[i].Y);
                    GameSession.GS.inventory.activeList[i].rectangle = new Rectangle((int)GameSession.GS.inventory.activeList[i].position.X, (int)GameSession.GS.inventory.activeList[i].position.Y,
                        GameSession.GS.inventory.activeList[i].texture.Width, GameSession.GS.inventory.activeList[i].texture.Height);
                }

                for (int i = 0; i < LootInventory.activeList.Count; i++)
                {
                    LootInventory.activeList[i].position = new Vector2(lootHolderRects[i].X, lootHolderRects[i].Y);
                    LootInventory.activeList[i].rectangle = new Rectangle(lootHolderRects[i].X, lootHolderRects[i].Y,
                        LootInventory.activeList[i].texture.Width, LootInventory.activeList[i].texture.Height);
                }
                
                ScreenManager.Game.ResetElapsedTime();
            }
        }

        //Every activate must have a deactivate
        public override void Deactivate()
        {
            base.Deactivate();
        }

        /// <summary>
        /// Unload graphics content used by the game.
        /// </summary>
        public override void Unload()
        {
            // content.Unload();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);

            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                ScreenManager.RemoveScreen(this);



            mousecontroller.MouseUpdate();

            //Rectangle[] prevLocations = Locations;
            currentMouseState = Mouse.GetState();

            //Update Mouse Position
            MouseRect.X = currentMouseState.X;
            MouseRect.Y = currentMouseState.Y;

            ToolTipBack.position.X = MouseRect.X+20;
            ToolTipBack.position.Y = MouseRect.Y+20;

            

            for (int i = 0; i < GameSession.GS.inventory.activeList.Count; i++)
            {
                bool check = false;
                if (MouseRect.Intersects(GameSession.GS.inventory.activeList[i].rectangle))
                {
                    hoverDesc = GameSession.GS.inventory.activeList[i].desc;
                    hoverName = GameSession.GS.inventory.activeList[i].name;
                    if (currentActiveItem != GameSession.GS.inventory.activeList[i])
                    {
                        ToolTipBack.isHidden = false;
                        check = true;
                    }
                }
                if (check)
                {
                    break;
                }
            } 
            for (int i = 0; i < LootInventory.activeList.Count; i++)
            {
                bool check = false;
                if (MouseRect.Intersects(LootInventory.activeList[i].rectangle))
                {
                    hoverDesc = LootInventory.activeList[i].desc;
                    hoverName = LootInventory.activeList[i].name;
                    if (currentActiveItem != LootInventory.activeList[i])
                    {
                        ToolTipBack.isHidden = false;
                        check = true;
                    }
                }
                if (check)
                {
                    break;
                }
            }
            bool hovercheck = false;
            for (int i = 0; i < 2; i++)
            {
                switch (i) 
                {
                    case 0:
                        for (int e = 0; e < GameSession.GS.inventory.activeList.Count;e++ )
                        {
                            if (MouseRect.Intersects(GameSession.GS.inventory.activeList[e].rectangle))
                            {
                                hovercheck = true;
                            }
                        }
                        break;
                    case 1:
                        for (int e = 0; e < LootInventory.activeList.Count; e++)
                        {
                            if (MouseRect.Intersects(LootInventory.activeList[e].rectangle))
                            {
                                hovercheck = true;
                            }
                        }
                        break;
                }

            }

            if (hovercheck == false)
            {
                hoverDesc = null;
                hoverName = null;
                ToolTipBack.isHidden = true;
            }

            foreach (Item item in GameSession.GS.inventory.activeList)
            {
                item.Update(MouseRect);
                if (currentActiveItem == null)
                {
                    if (mousecontroller.LeftMouseClick())
                    {
                        if (item.rectangle.Intersects(MouseRect))
                        {
                            currentActiveItem = item;
                            currentActiveItem.selected = true;
                            return;
                        }
                    }
                }
                if (currentActiveItem == item)
                {
                    item.opacity = 1.0f;
                }
            } 
            foreach (Item item in LootInventory.activeList)
            {
                item.Update(MouseRect);
                if (currentActiveItem == null)
                {
                    if (mousecontroller.LeftMouseClick())
                    {
                        if (item.rectangle.Intersects(MouseRect))
                        {
                            currentActiveItem = item;
                            currentActiveItem.selected = true;
                            return;
                        }
                    }
                }
                if (currentActiveItem == item)
                {
                    item.opacity = 1.0f;
                }
            }
            if (currentActiveItem != null)
            {
                currentActiveItem.Update(MouseRect);
            }
            //Region
            #region Item Clicking Stuff
            if (currentActiveItem != null)
            {
                if (mousecontroller.LeftMouseClick())
                {
                    for (int i = 0; i < itemHolderRects.Length; i++)
                    {
                        if (currentActiveItem != null)
                        {
                            if (currentActiveItem.rectangle.Intersects(itemHolderRects[i]))
                            {
                                bool occupied = false;
                                Item switchingItem = null;

                                for (int e = 0; e < GameSession.GS.inventory.activeList.Count; e++)
                                {
                                    if (GameSession.GS.inventory.activeList[e].position == new Vector2(itemHolderRects[i].X, itemHolderRects[i].Y))
                                    {
                                        switchingItem = GameSession.GS.inventory.activeList[e];
                                        occupied = true;
                                    }
                                }
                                if (!occupied)
                                {
                                    Vector2 temp = new Vector2(itemHolderRects[i].X, itemHolderRects[i].Y);
                                    currentActiveItem.position = temp;
                                    currentActiveItem.rectangle = itemHolderRects[i];
                                    currentActiveItem.selected = false;
                                    currentActiveItem = null;
                                }
                                else
                                {
                                    Vector2 temp = new Vector2(itemHolderRects[i].X, itemHolderRects[i].Y);
                                    currentActiveItem.position = temp;
                                    currentActiveItem.rectangle = itemHolderRects[i];
                                    currentActiveItem.selected = false;
                                    currentActiveItem = switchingItem;
                                    switchingItem.selected = true;
                                    break;
                                }
                            }
                        }
                    }


                    for (int i = 0; i < lootHolderRects.Length; i++)
                    {
                        if (currentActiveItem != null)
                        {
                            if (currentActiveItem.rectangle.Intersects(lootHolderRects[i]))
                            {
                                bool occupied = false;
                                Item switchingItem = null;

                                for (int e = 0; e < GameSession.GS.inventory.activeList.Count; e++)
                                {
                                    if (GameSession.GS.inventory.activeList[e].position == new Vector2(lootHolderRects[i].X, lootHolderRects[i].Y))
                                    {
                                        switchingItem = GameSession.GS.inventory.activeList[e];
                                        occupied = true;
                                    }
                                }
                                if (!occupied)
                                {
                                    Vector2 temp = new Vector2(lootHolderRects[i].X, lootHolderRects[i].Y);
                                    currentActiveItem.position = temp;
                                    currentActiveItem.rectangle = lootHolderRects[i];
                                    currentActiveItem.selected = false;
                                    currentActiveItem = null;
                                }
                                else
                                {
                                    Vector2 temp = new Vector2(lootHolderRects[i].X, lootHolderRects[i].Y);
                                    currentActiveItem.position = temp;
                                    currentActiveItem.rectangle = lootHolderRects[i];
                                    currentActiveItem.selected = false;
                                    currentActiveItem = switchingItem;
                                    switchingItem.selected = true;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            #endregion
            float width = ScreenManager.GraphicsDevice.Viewport.Width;
            float scale = (width / 1920);
            if (MouseRect.Intersects(new Rectangle((int)(ScreenManager.GraphicsDevice.Viewport.Width * 0.695f) - CancelTex.Width / 3, (int)(ScreenManager.GraphicsDevice.Viewport.Height * 0.868f) - CancelTex.Height / 4, (int)(CancelTex.Width * scale), (int)(CancelTex.Height * scale))))
            {
                CancelTex = CancelHover;
                if (mousecontroller.LeftMouseClick())
                {
                    if (currentActiveItem != null)
                    {
                        //for (int i = 0; i < lootHolderRects.Length; i++)
                        //{ 
                        //    int count=0;
                        //    for(int e = 0;e<LootInventory.activeList.Count;e++)
                        //    {
                        //        if (new Vector2(lootHolderRects[i].X,lootHolderRects[i].Y) == LootInventory.activeList[e].position)
                        //        {
                        //            count++;
                        //        }
                        //    }
                        //    if (count == 0)
                        //    { 
                        //        currentActiveItem.position = new Vector2(lootHolderRects[i].X,lootHolderRects[i].Y);
                        //    }
                        //}

                        LootInventory.activeList.Add(currentActiveItem);
                        currentActiveItem = null;
                    }
                    CalculateInventories();
                    ScreenManager.RemoveScreen(this);
                }
            }
            else 
            {
                CancelTex = CancelNorm;
            }



            GameSession.gameAudioEngine.Update(gameTime);
            // Reset prevMouseState
            prevMouseState = Mouse.GetState();

            //CalculateInventories();

        }
        /// <summary>
        /// Calculates what inventories contain
        /// </summary>
        void CalculateInventories()
        {
            Inventory TempMainInventory = new Inventory();

            for (int i = 0; i < itemHolderRects.Length; i++)
            {
                for (int e = 0; e < GameSession.GS.inventory.activeList.Count; e++)
                {
                    if (GameSession.ToVector2(itemHolderRects[i].Location).Equals(GameSession.ToVector2(GameSession.GS.inventory.activeList[e].rectangle.Location)))
                    {
                        Item tempItem = new Item();
                        tempItem = GameSession.GS.inventory.ReplicateItem(GameSession.GS.inventory.activeList[e]);
                        TempMainInventory.activeList.Add(tempItem);
                    }
                }
                //for(int e = 0;e<;)
            }
            for (int i = 0; i < itemHolderRects.Length; i++)
            {
                for (int e = 0; e < LootInventory.activeList.Count; e++)
                {
                    if (GameSession.ToVector2(itemHolderRects[i].Location).Equals(GameSession.ToVector2(LootInventory.activeList[e].rectangle.Location)))
                    {
                        Item tempItem = new Item();
                        tempItem = LootInventory.ReplicateItem(LootInventory.activeList[e]);
                        TempMainInventory.activeList.Add(tempItem);
                    }
                }
            }
            GameSession.GS.inventory.activeList = TempMainInventory.activeList;

        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Draw(GameTime gameTime)
        {


            ScreenManager.FadeBackBufferToBlack(TransitionAlpha * 2 / 3);

            ScreenManager.GraphicsDevice.BlendState = BlendState.Opaque;
            ScreenManager.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            //NewParticle.Draw(gameTime, TestCamera);
            frameRate = (float)(1 / gameTime.ElapsedGameTime.TotalSeconds);

            float width = ScreenManager.GraphicsDevice.Viewport.Width;
            float scale = (width / 1920);
            Matrix scalingMatrix = Matrix.CreateScale(scale);
            spriteBatch.Begin(SpriteSortMode.Immediate, null, null, null, null, null, scalingMatrix);

            spriteBatch.Draw(WorldMapTex, new Vector2(ScreenManager.GraphicsDevice.Viewport.TitleSafeArea.X, ScreenManager.GraphicsDevice.Viewport.TitleSafeArea.Y), null, Color.White, 0f, new Vector2(0, 0), 1, SpriteEffects.None, 0f);

            spriteBatch.End();

            //UI DRAWING
            spriteBatch.Begin();

            foreach (Item item in GameSession.GS.inventory.activeList)
            {
                item.Draw(spriteBatch,scale*1.2f,InconsolataFont);
            }
            foreach (Item item in LootInventory.activeList)
            {
                item.Draw(spriteBatch, scale * 1.2f, InconsolataFont);
            }
            
            //Once players are properly placed replace with currentUnit
            /*
            #region Draw Stats
            DrawBorderedText(players[0]._NAME,
                (int)(ScreenManager.GraphicsDevice.Viewport.Width / 3.047) - (int)(spritefont.MeasureString(players[0]._NAME).X / 2),
                (int)(ScreenManager.GraphicsDevice.Viewport.Height / 42.6667) + (int)(spritefont.MeasureString(players[0]._NAME).Y / 2),
                Color.White,
                spritefont);

            DrawBorderedText(players[0].MyStatus.BaseStr.ToString(),
                (int)(ScreenManager.GraphicsDevice.Viewport.Width / 1.76778) - (int)(spritefont.MeasureString(players[0].MyStatus.BaseStr.ToString()).X / 2),
                (int)(ScreenManager.GraphicsDevice.Viewport.Height / 2.9687),
                Color.White,
                spritefont);

            DrawBorderedText(players[0].MyStatus.BaseEnd.ToString(),
                (int)(ScreenManager.GraphicsDevice.Viewport.Width / 1.76778) - (int)(spritefont.MeasureString(players[0].MyStatus.BaseStr.ToString()).X / 2),
                (int)(ScreenManager.GraphicsDevice.Viewport.Height / 2.9687) + (int)(ScreenManager.GraphicsDevice.Viewport.Height / 21.5687) * 1,
                Color.White,
                spritefont);

            DrawBorderedText(players[0].MyStatus.BaseAgi.ToString(),
                (int)(ScreenManager.GraphicsDevice.Viewport.Width / 1.76778) - (int)(spritefont.MeasureString(players[0].MyStatus.BaseStr.ToString()).X / 2),
                (int)(ScreenManager.GraphicsDevice.Viewport.Height / 2.9687) + (int)(ScreenManager.GraphicsDevice.Viewport.Height / 21.5687) * 2,
                Color.White,
                spritefont);

            DrawBorderedText(players[0].MyStatus.BaseCha.ToString(),
                (int)(ScreenManager.GraphicsDevice.Viewport.Width / 1.76778) - (int)(spritefont.MeasureString(players[0].MyStatus.BaseStr.ToString()).X / 2),
                (int)(ScreenManager.GraphicsDevice.Viewport.Height / 2.9687) + (int)(ScreenManager.GraphicsDevice.Viewport.Height / 21.5687) * 3,
                Color.White,
                spritefont);

            DrawBorderedText(players[0].MyStatus.BaseDex.ToString(),
                (int)(ScreenManager.GraphicsDevice.Viewport.Width / 1.76778) - (int)(spritefont.MeasureString(players[0].MyStatus.BaseStr.ToString()).X / 2),
                (int)(ScreenManager.GraphicsDevice.Viewport.Height / 2.9687) + (int)(ScreenManager.GraphicsDevice.Viewport.Height / 21.5687) * 4,
                Color.White,
                spritefont);

            DrawBorderedText(players[0].MyStatus.BaseInt.ToString(),
                (int)(ScreenManager.GraphicsDevice.Viewport.Width / 1.76778) - (int)(spritefont.MeasureString(players[0].MyStatus.BaseStr.ToString()).X / 2),
                (int)(ScreenManager.GraphicsDevice.Viewport.Height / 2.9687) + (int)(ScreenManager.GraphicsDevice.Viewport.Height / 21.5687) * 5,
                Color.White,
                spritefont);
            #endregion
             * */

            //spriteBatch.Draw(players[0].Texture, new Vector2(70, 60) - new Vector2(0, 806) * 0.2f, null, Color.White, 0f, Vector2.Zero, 0.6f, SpriteEffects.None, 0f);
            spriteBatch.Draw(CancelTex, new Vector2(ScreenManager.GraphicsDevice.Viewport.Width*0.695f, ScreenManager.GraphicsDevice.Viewport.Height*0.868f), null, Color.White, 0f, new Vector2(CancelTex.Width / 2, CancelTex.Height / 2), 1f*scale, SpriteEffects.None, 0f);

            if (currentActiveItem != null)
            {
                DrawBorderedText(currentActiveItem.name, (int)(ScreenManager.GraphicsDevice.Viewport.Width * 0.55859375), (int)(ScreenManager.GraphicsDevice.Viewport.Height * 0.612056f), Color.White, spritefont);
                DrawBorderedText(currentActiveItem.desc, (int)(ScreenManager.GraphicsDevice.Viewport.Width * 0.55859375), (int)(ScreenManager.GraphicsDevice.Viewport.Height * 0.67089f), Color.White, spritefont);
            }
#if DEBUG
            spriteBatch.DrawString(spritefont, "Mouse X : " + MouseRect.X + " Mouse Y : " + MouseRect.Y, new Vector2(50, 100), Color.White);
#endif
            ToolTipBack.Draw(spriteBatch);

            if (ToolTipBack.isHidden == false)
            {
                DrawBorderedText(hoverName, (int)ToolTipBack.position.X + 10, (int)ToolTipBack.position.Y + 10, Color.WhiteSmoke, spritefont);
                //spriteBatch.DrawString(spritefont, hoverName, ToolTipBack.position+new Vector2(10,10), Color.White);
                spriteBatch.DrawString(spritefont, hoverDesc, ToolTipBack.position + new Vector2(10, 40), Color.White);
            }
            
            
            spriteBatch.End();


            base.Draw(gameTime);
        }


        private void DrawModel(Model model, Matrix world, CameraObj camera)
        {
            Matrix[] transforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(transforms);

            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    //effect.SpecularColor = Color.WhiteSmoke.ToVector3();
                    ///effect.SpecularPower = 100.0f;
                    //effect.FogEnabled = true;
                    //effect.FogColor = Color.White.ToVector3();
                    //effect.FogStart = 999999.0f;
                    //effect.FogEnd = 1000000.0f;


                    effect.EnableDefaultLighting();
                    effect.DiffuseColor = new Vector3(0.7f, 0.7f, 0.7f);
                    effect.World = transforms[mesh.ParentBone.Index] * world;
                    // Use the matrices provided by the chase camera
                    effect.View = camera.view;
                    effect.Projection = camera.proj;
                }
                mesh.Draw();
            }
        }

        public void DrawBorderedText(string text, int PosX, int PosY, Color color, SpriteFont font)
        {
            spriteBatch.DrawString(font, text, new Vector2(PosX + 1, PosY + 1), Color.Black * 0.5f);
            spriteBatch.DrawString(font, text, new Vector2(PosX + 1, PosY - 1), Color.Black * 0.5f);
            spriteBatch.DrawString(font, text, new Vector2(PosX - 1, PosY + 1), Color.Black * 0.5f);
            spriteBatch.DrawString(font, text, new Vector2(PosX - 1, PosY - 1), Color.Black * 0.5f);
            spriteBatch.DrawString(font, text, new Vector2(PosX, PosY), color);

        }
    }
}

