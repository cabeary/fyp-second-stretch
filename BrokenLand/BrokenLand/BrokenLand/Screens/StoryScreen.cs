﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using Supernova.Particles2D;
using Supernova.Particles2D.Modifiers.Alpha;
using Supernova.Samples.Windows.Utilities;
using GameStateManagement;
using System.Threading;
using Microsoft.Xna.Framework.Content;
using BrokenLands;
using System.Text.RegularExpressions;


namespace BrokenLands
{
    public class StoryScreen : GameScreen
    {
        ContentManager content;
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SpriteFont spritefont, InconsolataFont;

        MouseState prevMouseState, currentMouseState;

        List<string> storyStrings = new List<string>();

        Rectangle MouseRect;
        int currentString = 0;

        float FrameCounter, frameRate;

        bool Exiting = false;
        
        MouseController mousecontroller = new MouseController();
        string test;
        public StoryScreen()
            : base()
        {
            this.ScreenState = ScreenState.Active;

        }



        public override void Activate(bool instancePreserved)
        {
            if (!instancePreserved)
            {
                if (content == null)
                    content = new ContentManager(ScreenManager.Game.Services, "Content");

                //TestCamera = new CameraObj(this, new Vector3(0, 0, 0), new Vector3(0, 0, 10), Vector3.Up, 1f, 1000f, 1f);
                FrameCounter = 0;
                prevMouseState = Mouse.GetState();
                currentMouseState = Mouse.GetState();

                //Loading
                spriteBatch = new SpriteBatch(ScreenManager.GraphicsDevice);

                //Textures
                

                //Icons


                //Fonts
                spritefont = content.Load<SpriteFont>("Fonts/Times12");
                InconsolataFont = content.Load<SpriteFont>("Fonts/BigNoodle");

                //Variables
                storyStrings.Add("The year is 2104");
                storyStrings.Add("Nations have unleashed nuclear destruction upon each other");
                storyStrings.Add("Most of the human race were wiped out");
                storyStrings.Add("The remaining few humans went underground and stayed alive");
                storyStrings.Add("Decades after the nuclear fallout");
                storyStrings.Add("Animals mutated into more intelligent species");
                storyStrings.Add("Mutated animals take over the surface and the ruins of the human race");
                storyStrings.Add("Human survivors now roam the earth in the shadows");
                storyStrings.Add("Rumours say there is safety still");
                storyStrings.Add("In a place called");
                storyStrings.Add("Haven");

                MouseRect = new Rectangle(Mouse.GetState().X, Mouse.GetState().Y, 10, 10);//Used for mouse Collision

                
                ScreenManager.Game.ResetElapsedTime();
                ScreenManager.fader.FadeInScreen(2000);
                ScreenManager.fader.Opacity = 0.001f;
            }
        }

        //Every activate must have a deactivate
        public override void Deactivate()
        {
            
            base.Deactivate();
            
        }

        public static string SpliceText(string text, int lineLength)
        {
            return Regex.Replace(text, "(.{" + lineLength + "})", "$1" + Environment.NewLine);
        }
        /// <summary>
        /// Unload graphics content used by the game.
        /// </summary>
        public override void Unload()
        {
            content.Unload();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
            bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);
            
            mousecontroller.MouseUpdate();

            //Rectangle[] prevLocations = Locations;
            currentMouseState = Mouse.GetState();

            //Update Mouse Position
            MouseRect.X = currentMouseState.X;
            MouseRect.Y = currentMouseState.Y;
            if (ScreenManager.fader.checkFadeInOver())
            {
                ScreenManager.fader.FadeInNOutTo0f(1000, 0f, 4000);
            }

            if (ScreenManager.fader.Opacity <= 0)
            {
                if (ScreenManager.fader.fadeinNOut == false)
                {
                    currentString++;
                    ScreenManager.fader.FadeInNOutTo0f(1000, 0f, 2500);
                }
            }
            if (currentMouseState.LeftButton == ButtonState.Pressed)
            {
                if (prevMouseState != currentMouseState)
                {
                    if (ScreenManager.fader.fadeinNOut == true)
                    {
                        ScreenManager.fader.delayTimer = ScreenManager.fader.delayTime;
                    }
                }
            }


            if (ScreenManager.fader.checkFadeOutOver())
            {
                ScreenManager.AddScreen(new CharacterCreationScreen(), null);
                //ScreenManager.AddScreen(new WorldMapScreen(),null);
                Exiting = true;
                ExitScreen();
            }
            if (currentString > storyStrings.Count)
            {
                if (ScreenManager.fader.fadeScreenOut == false)
                {
                    if (!Exiting)
                    {
                        ScreenManager.fader.FadeOutScreen(1000);
                    }
                }
            }
            
            prevMouseState = Mouse.GetState();
            GameSession.gameAudioEngine.Update(gameTime);
        }
        
        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Draw(GameTime gameTime)
        {


            ScreenManager.FadeBackBufferToBlack(TransitionAlpha * 2 / 3);

            ScreenManager.GraphicsDevice.BlendState = BlendState.Opaque;
            ScreenManager.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            //NewParticle.Draw(gameTime, TestCamera);
            frameRate = (float)(1 / gameTime.ElapsedGameTime.TotalSeconds);
            ScreenManager.GraphicsDevice.Clear(Color.Black);

            spriteBatch.Begin();
            for(int i = 0;i<storyStrings.Count;i++)
            { 
                if(i==currentString)
                {
                    spriteBatch.DrawString(InconsolataFont,
                        storyStrings[i],
                        new Vector2(
                            (int)(ScreenManager.GraphicsDevice.Viewport.Width / 2 - InconsolataFont.MeasureString(storyStrings[i]).X / 2),
                            (int)(ScreenManager.GraphicsDevice.Viewport.Height / 2 - InconsolataFont.MeasureString(storyStrings[i]).Y / 2)), 
                            Color.White * ScreenManager.fader.Opacity);
                }
            }
            spriteBatch.End();


            base.Draw(gameTime);
        }

    }
}

