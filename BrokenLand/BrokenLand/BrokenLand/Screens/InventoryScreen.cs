﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using Supernova.Particles2D;
using Supernova.Particles2D.Modifiers.Alpha;
using Supernova.Samples.Windows.Utilities;
using GameStateManagement;
using System.Threading;
using Microsoft.Xna.Framework.Content;
using BrokenLands;
using BrokenLands;


namespace BrokenLands
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class InventoryScreen : GameScreen
    {

        ContentManager content;
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SpriteFont spritefont, InconsolataFont;

        MouseState prevMouseState, currentMouseState;

        float FrameCounter, frameRate;
        Texture2D WorldMapTex, WorldMapUIOverlay;
        Rectangle WorldMapTexPos;
        Rectangle ArrowLeftRect, ArrowRightRect;
        float opacityLeft, opacityRight;

        Texture2D CancelTex;

        //List<InventoryItem> Items = new List<InventoryItem>();
        
        Item currentActiveItem;

        float[] ButtonAlpha;

        Texture2D SettingsIcon;
        Texture2D ArrowUI;
        Rectangle[] itemHolderRects;

        Rectangle MouseRect;

        List<Player> players = new List<Player>();
        List<Item> tempInventory = new List<Item>();

        Random random;

        MouseController mousecontroller = new MouseController();

        public InventoryScreen()
            : base()
        {
            this.ScreenState = ScreenState.Active;

        }



        public override void Activate(bool instancePreserved)
        {
            if (!instancePreserved)
            {
                if (content == null)
                    content = new ContentManager(ScreenManager.Game.Services, "Content");

                //TestCamera = new CameraObj(this, new Vector3(0, 0, 0), new Vector3(0, 0, 10), Vector3.Up, 1f, 1000f, 1f);
                FrameCounter = 0;
                prevMouseState = Mouse.GetState();
                currentMouseState = Mouse.GetState();

                //Loading
                spriteBatch = new SpriteBatch(ScreenManager.GraphicsDevice);

                //Textures
                WorldMapTex = content.Load<Texture2D>("UI/Inventory-UI");
                ArrowUI = content.Load<Texture2D>("UI/ArrowUI");

                //Icons
                SettingsIcon = content.Load<Texture2D>("UI/Icons/Settings-Icon");
                CancelTex = content.Load<Texture2D>("UI/Icons/Cancel-Icon");

                //Fonts
                spritefont = content.Load<SpriteFont>("Fonts/Times12");
                InconsolataFont = content.Load<SpriteFont>("Fonts/Inconstella");

                //Models

                //Variables
                WorldMapTexPos = new Rectangle(0, 0, UIElement.viewport.Width, UIElement.viewport.Height);
                random = new Random();
                MouseRect = new Rectangle(Mouse.GetState().X, Mouse.GetState().Y, 10, 10);//Used for mouse Collision
                ButtonAlpha = new float[5];

                //Items = new List<InventoryItem>(40);

                //Temp to get in Units in
                Model Tile = content.Load<Model>("Tile/Tile");
                Texture2D tex = content.Load<Texture2D>("Units/Gunbowman");

                players = GameSession.players;
                //players[0].Texture = tex;

                ArrowLeftRect = new Rectangle(210, 145, ArrowUI.Width, ArrowUI.Height);
                ArrowRightRect = new Rectangle(620, 145, ArrowUI.Width, ArrowUI.Height);
                opacityLeft = 0.5f;
                opacityRight = 0.5f;
                itemHolderRects = new Rectangle[42];
                for (int i = 0; i < 40; i++)
                {
                    Item item = new Item(i, "Poop", "Fecal Material", 1, 1f, new Vector2(ScreenManager.GraphicsDevice.Viewport.Width * 0.6484375f + (i % 5) * ScreenManager.GraphicsDevice.Viewport.Height * 0.106944f,
                        ScreenManager.GraphicsDevice.Viewport.Height * 0.051389f + (i / 5) * ScreenManager.GraphicsDevice.Viewport.Height * 0.106944f), SettingsIcon);
                    itemHolderRects[i] = item.rectangle;

                }
                //Inventory Positioning
                for(int i = 0;i<GameSession.GS.inventory.activeList.Count;i++)
                {
                    //NEED TO FIX POSITIONING
                    GameSession.GS.inventory.activeList[i].position = 
                        new Vector2((ScreenManager.GraphicsDevice.Viewport.Width * 0.6484375f) + 
                            (i % 5)
                    * (ScreenManager.GraphicsDevice.Viewport.Width * 0.06015625f),
                    (ScreenManager.GraphicsDevice.Viewport.Height * 0.051388888889f) 
                    + (i / 5) * (ScreenManager.GraphicsDevice.Viewport.Height * 0.10694444f));
                    //(i / ScreenManager.GraphicsDevice.Viewport.Height * 0.006944444f) 
                    //* ScreenManager.GraphicsDevice.Viewport.Height * 0.10694444f);
                    GameSession.GS.inventory.activeList[i].rectangle = new Rectangle((int)GameSession.GS.inventory.activeList[i].position.X, (int)GameSession.GS.inventory.activeList[i].position.Y, 
                        GameSession.GS.inventory.activeList[i].texture.Width, GameSession.GS.inventory.activeList[i].texture.Height);
                }

               
                itemHolderRects[40] = new Rectangle((int)(ScreenManager.GraphicsDevice.Viewport.Width / 1.86861), (int)(ScreenManager.GraphicsDevice.Viewport.Height / 5.901639), SettingsIcon.Width, SettingsIcon.Height);
                itemHolderRects[41] = new Rectangle((int)(ScreenManager.GraphicsDevice.Viewport.Width / 12.8), (int)(ScreenManager.GraphicsDevice.Viewport.Height / 5.901639), SettingsIcon.Width, SettingsIcon.Height);

                for (int i = 0; i < 1; i++)
                {
                    if (players[0].equipList["Armor"] != null)
                    {
                        Item item = new Item(players[0].equipList["Armor"], new Vector2(itemHolderRects[41].X, itemHolderRects[41].Y));
                        //Item item = new Item(i + 50, players[0].equipList["Armor"].name, players[0].equipList["Armor"].desc, (int)players[0].equipList["Armor"].type, players[0].equipList["Armor"].shopValue, new Vector2(itemHolderRects[41].X, itemHolderRects[41].Y), players[0].equipList["Armor"].tex);
                        
                        tempInventory.Add(item);
                    }
                    if (players[0].equipList["LeftWeapon"] != null)
                    {
                        Item item = new Item(players[0].equipList["LeftWeapon"], new Vector2(itemHolderRects[40].X, itemHolderRects[40].Y));
                        //Item item = new Item(i + 50, players[0].equipList["LeftWeapon"].name, players[0].equipList["LeftWeapon"].desc, (int)players[0].equipList["LeftWeapon"].type, players[0].equipList["LeftWeapon"].shopValue, new Vector2(itemHolderRects[40].X, itemHolderRects[40].Y), players[0].equipList["LeftWeapon"].tex);
                        tempInventory.Add(item);
                    }
                }

                //players.Add(new Player(13, 10, 15, 6, 11, 11, map.gridNodes[0, 0], tex, 1, "Chanboy"));
                //players[0].MyStatus.Range = 4;
                // once the load has finished, we use ResetElapsedTime to tell the game's
                // timing mechanism that we have just finished a very long frame, and that
                // it should not try to catch up.
                ScreenManager.Game.ResetElapsedTime();
            }
        }

        //Every activate must have a deactivate
        public override void Deactivate()
        {
            base.Deactivate();
        }

        /// <summary>
        /// Unload graphics content used by the game.
        /// </summary>
        public override void Unload()
        {
            // content.Unload();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);

            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                ScreenManager.RemoveScreen(this);



            mousecontroller.MouseUpdate();

            //Rectangle[] prevLocations = Locations;
            currentMouseState = Mouse.GetState();

            //Update Mouse Position
            MouseRect.X = currentMouseState.X;
            MouseRect.Y = currentMouseState.Y;

            foreach (Item item in tempInventory)
            {
                item.Update(MouseRect);
                if (currentActiveItem == null)
                {
                    if (mousecontroller.LeftMouseClick())
                    {
                        if (item.rectangle.Intersects(MouseRect))
                        {
                            if ((int)item.type == 1)
                            {
                                players[0].Unequip("Armor");
                            }
                            if ((int)item.type == 2)
                            {
                                players[0].Unequip("LeftWeapon");
                            }
                            currentActiveItem = item;
                            currentActiveItem.selected = true;
                            return;
                        }
                    }

                }
                if (currentActiveItem == item)
                {
                    item.opacity = 1.0f;
                }
            }
            foreach (Item item in GameSession.GS.inventory.activeList)
            {
                item.Update(MouseRect);
                if (currentActiveItem == null)
                {
                    if (mousecontroller.LeftMouseClick())
                    {
                        if (item.rectangle.Intersects(MouseRect))
                        {
                            
                            tempInventory.Add(GameSession.GS.inventory.ReplicateItem(item));
                            currentActiveItem = tempInventory[tempInventory.Count-1];
                            GameSession.GS.inventory.activeList.Remove(item);
                            currentActiveItem.selected = true;
                            return;
                        }
                    }
                }
                if (currentActiveItem == item)
                {
                    item.opacity = 1.0f;
                }
            }

            if (currentActiveItem != null)
            {
                if (mousecontroller.LeftMouseClick())
                {
                    for (int i = 0; i < itemHolderRects.Length; i++)
                    {
                        if (currentActiveItem != null)
                        {
                            if (currentActiveItem.rectangle.Intersects(itemHolderRects[i]))
                            {
                                bool occupied = false;
                                Item switchingItem = null;

                                for (int e = 0; e < GameSession.GS.inventory.activeList.Count; e++)
                                {
                                    if (GameSession.GS.inventory.activeList[e].position == new Vector2(itemHolderRects[i].X+1, itemHolderRects[i].Y))
                                    {
                                        switchingItem = GameSession.GS.inventory.activeList[e];
                                        occupied = true;
                                    }
                                }
                                for (int e = 0; e < tempInventory.Count; e++)
                                {
                                    if (tempInventory[e].position == new Vector2(itemHolderRects[i].X, itemHolderRects[i].Y))
                                    {
                                        switchingItem = tempInventory[e];
                                        occupied = true;
                                    }
                                }
                                    if (i == 40)
                                    {
                                        if ((int)currentActiveItem.type == 2)
                                        {
                                            if (!occupied)
                                            {
                                                foreach (Equipment equip in EquipmentLoader.equipList)
                                                {
                                                    if (equip.desc == currentActiveItem.desc)
                                                        players[0].Equip(equip);
                                                }
                                                Vector2 temp = new Vector2(itemHolderRects[i].X, itemHolderRects[i].Y);
                                                currentActiveItem.position = temp;
                                                currentActiveItem.rectangle = itemHolderRects[i];
                                                currentActiveItem.selected = false;

                                                currentActiveItem = null;
                                                break;
                                            }
                                            else
                                            {
                                                players[0].Unequip("LeftWeapon");
                                                foreach (Equipment equip in EquipmentLoader.equipList)
                                                {
                                                    if (equip.desc == currentActiveItem.desc)
                                                        players[0].Equip(equip);
                                                }
                                                Vector2 temp = new Vector2(itemHolderRects[i].X, itemHolderRects[i].Y);
                                                currentActiveItem.position = temp;
                                                currentActiveItem.rectangle = itemHolderRects[i];
                                                currentActiveItem.selected = false;
                                                currentActiveItem = switchingItem;
                                                switchingItem.selected = true;
                                                break;
                                            }
                                        }
                                        else
                                        {
                                            break;
                                        }
                                    }
                                    else if (i == 41)
                                    {
                                        if ((int)currentActiveItem.type == 1)
                                        {
                                            if (!occupied)
                                            {
                                                foreach (Equipment equip in EquipmentLoader.equipList)
                                                {
                                                    if (equip.desc == currentActiveItem.desc)
                                                        players[0].Equip(equip);
                                                }

                                                Vector2 temp = new Vector2(itemHolderRects[i].X, itemHolderRects[i].Y);
                                                currentActiveItem.position = temp;
                                                currentActiveItem.rectangle = itemHolderRects[i];
                                                currentActiveItem.selected = false;
                                                currentActiveItem = null;
                                                break;
                                            }
                                            else
                                            {
                                                players[0].Unequip("Armor");
                                                foreach (Equipment equip in EquipmentLoader.equipList)
                                                {
                                                    if (equip.desc == currentActiveItem.desc)
                                                        players[0].Equip(equip);
                                                }
                                                Vector2 temp = new Vector2(itemHolderRects[i].X, itemHolderRects[i].Y);
                                                currentActiveItem.position = temp;
                                                currentActiveItem.rectangle = itemHolderRects[i];
                                                currentActiveItem.selected = false;
                                                currentActiveItem = switchingItem;

                                                switchingItem.selected = true;
                                                break;
                                            }
                                        }
                                        else
                                        {
                                            break;
                                        }
                                    }
                                if (!occupied)
                                {
                                    Vector2 temp = new Vector2(itemHolderRects[i].X, itemHolderRects[i].Y);
                                    currentActiveItem.position = temp;
                                    currentActiveItem.rectangle = itemHolderRects[i];
                                    currentActiveItem.selected = false;
                                    GameSession.GS.inventory.activeList.Add(GameSession.GS.inventory.ReplicateItem(currentActiveItem));
                                    tempInventory.Remove(currentActiveItem);
                                    currentActiveItem = null;
                                }
                                else
                                {
                                    Vector2 temp = new Vector2(itemHolderRects[i].X, itemHolderRects[i].Y);
                                    currentActiveItem.position = temp;
                                    currentActiveItem.rectangle = itemHolderRects[i];
                                    currentActiveItem.selected = false;
                                    //currentActiveItem = switchingItem;
                                    GameSession.GS.inventory.activeList.Add(GameSession.GS.inventory.ReplicateItem(currentActiveItem));
                                    tempInventory.Add(GameSession.GS.inventory.ReplicateItem(switchingItem));

                                    tempInventory.Remove(currentActiveItem);
                                    currentActiveItem = tempInventory[tempInventory.Count - 1];
                                    GameSession.GS.inventory.activeList.Remove(switchingItem);
                                    currentActiveItem.selected = true;
                                    break;
                                }
                            }
                        }
                    }

                }
      
            }

            if (MouseRect.Intersects(ArrowLeftRect))
            {
                opacityLeft = 1.5f;
                if (mousecontroller.LeftMouseClick())
                { 
                    
                }
            }
            else 
            {
                opacityLeft = 0.5f;
            }
            if (MouseRect.Intersects(ArrowRightRect))
            {
                opacityRight = 1.5f;
                if (mousecontroller.LeftMouseClick())
                {

                }
            }
            else
            {
                opacityRight = 0.5f;
            }

            if (MouseRect.Intersects(new Rectangle((ScreenManager.GraphicsDevice.Viewport.Bounds.Right - CancelTex.Width) - 5, 7, CancelTex.Width, CancelTex.Height)))
            {
                if (mousecontroller.LeftMouseClick())
                {
                    if(currentActiveItem==null)
                    ScreenManager.RemoveScreen(this);
                }
            }



            // Reset prevMouseState
            GameSession.gameAudioEngine.Update(gameTime);
            prevMouseState = Mouse.GetState();




        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Draw(GameTime gameTime)
        {


            ScreenManager.FadeBackBufferToBlack(TransitionAlpha * 2 / 3);

            ScreenManager.GraphicsDevice.BlendState = BlendState.Opaque;
            ScreenManager.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            //NewParticle.Draw(gameTime, TestCamera);
            frameRate = (float)(1 / gameTime.ElapsedGameTime.TotalSeconds);

            float width = ScreenManager.GraphicsDevice.Viewport.Width;
            float scale = (width / 1920);
            Matrix scalingMatrix = Matrix.CreateScale(scale);
            spriteBatch.Begin(SpriteSortMode.Immediate, null, null, null, null, null, scalingMatrix);

            spriteBatch.Draw(WorldMapTex, new Vector2(ScreenManager.GraphicsDevice.Viewport.TitleSafeArea.X, ScreenManager.GraphicsDevice.Viewport.TitleSafeArea.Y), null, Color.White, 0f, new Vector2(0, 0), 1, SpriteEffects.None, 0f);

            spriteBatch.End();

            //UI DRAWING
            spriteBatch.Begin(); 
            
            spriteBatch.Draw(players[0].Texture, new Vector2((int)(ScreenManager.GraphicsDevice.Viewport.Width * 0.12890625), (int)(ScreenManager.GraphicsDevice.Viewport.Height * 0.02116667)), new Rectangle(0, 0, players[0].animator.frameWidth, players[0].animator.frameHeight), Color.White, 0.0f, new Vector2(0), 2.5f, SpriteEffects.None, 0f);
            

            foreach (Item item in GameSession.GS.inventory.activeList)
            {
                item.Draw(spriteBatch,InconsolataFont);
            }
            foreach (Item item in GameSession.GS.inventory.activeList)
            {
                item.DrawToolTip(spriteBatch, InconsolataFont);
            }
            if (currentActiveItem != null)
            {
                DrawBorderedText(currentActiveItem.name, (int)(ScreenManager.GraphicsDevice.Viewport.Width * 0.0390625f), (int)(ScreenManager.GraphicsDevice.Viewport.Height * 0.694f), Color.White, spritefont);
                DrawBorderedText(currentActiveItem.desc, (int)(ScreenManager.GraphicsDevice.Viewport.Width * 0.0390625f), (int)(ScreenManager.GraphicsDevice.Viewport.Height * 0.76389f), Color.White, spritefont);
            }
            foreach (Item item in tempInventory)
            {
                item.Draw(spriteBatch, InconsolataFont);
                item.DrawToolTip(spriteBatch, InconsolataFont);
            }
            //Once players are properly placed replace with currentUnit
            #region Draw Stats
            DrawBorderedText(players[0]._NAME,
                (int)(ScreenManager.GraphicsDevice.Viewport.Width / 3.047) - (int)(spritefont.MeasureString(players[0]._NAME).X / 2),
                (int)(ScreenManager.GraphicsDevice.Viewport.Height / 42.6667) + (int)(spritefont.MeasureString(players[0]._NAME).Y / 2),
                Color.White,
                spritefont);

            DrawBorderedText(players[0].MyStatus.BaseStr.ToString(),
                (int)(ScreenManager.GraphicsDevice.Viewport.Width / 1.76778) - (int)(spritefont.MeasureString(players[0].MyStatus.BaseStr.ToString()).X / 2),
                (int)(ScreenManager.GraphicsDevice.Viewport.Height / 2.9687),
                Color.White,
                spritefont);

            DrawBorderedText(players[0].MyStatus.BaseEnd.ToString(),
                (int)(ScreenManager.GraphicsDevice.Viewport.Width / 1.76778) - (int)(spritefont.MeasureString(players[0].MyStatus.BaseStr.ToString()).X / 2),
                (int)(ScreenManager.GraphicsDevice.Viewport.Height / 2.9687) + (int)(ScreenManager.GraphicsDevice.Viewport.Height / 21.5687) * 1,
                Color.White,
                spritefont);

            DrawBorderedText(players[0].MyStatus.BaseAgi.ToString(),
                (int)(ScreenManager.GraphicsDevice.Viewport.Width / 1.76778) - (int)(spritefont.MeasureString(players[0].MyStatus.BaseStr.ToString()).X / 2),
                (int)(ScreenManager.GraphicsDevice.Viewport.Height / 2.9687) + (int)(ScreenManager.GraphicsDevice.Viewport.Height / 21.5687) * 2,
                Color.White,
                spritefont);

            DrawBorderedText(players[0].MyStatus.BaseCha.ToString(),
                (int)(ScreenManager.GraphicsDevice.Viewport.Width / 1.76778) - (int)(spritefont.MeasureString(players[0].MyStatus.BaseStr.ToString()).X / 2),
                (int)(ScreenManager.GraphicsDevice.Viewport.Height / 2.9687) + (int)(ScreenManager.GraphicsDevice.Viewport.Height / 21.5687) * 3,
                Color.White,
                spritefont);

            DrawBorderedText(players[0].MyStatus.BaseDex.ToString(),
                (int)(ScreenManager.GraphicsDevice.Viewport.Width / 1.76778) - (int)(spritefont.MeasureString(players[0].MyStatus.BaseStr.ToString()).X / 2),
                (int)(ScreenManager.GraphicsDevice.Viewport.Height / 2.9687) + (int)(ScreenManager.GraphicsDevice.Viewport.Height / 21.5687) * 4,
                Color.White,
                spritefont);

            DrawBorderedText(players[0].MyStatus.BaseInt.ToString(),
                (int)(ScreenManager.GraphicsDevice.Viewport.Width / 1.76778) - (int)(spritefont.MeasureString(players[0].MyStatus.BaseStr.ToString()).X / 2),
                (int)(ScreenManager.GraphicsDevice.Viewport.Height / 2.9687) + (int)(ScreenManager.GraphicsDevice.Viewport.Height / 21.5687) * 5,
                Color.White,
                spritefont);
            #endregion
            spriteBatch.Draw(ArrowUI, ArrowLeftRect,null, Color.White*opacityLeft,MathHelper.Pi,new Vector2(ArrowUI.Width/2,ArrowUI.Height/2),SpriteEffects.None,0f);
            spriteBatch.Draw(ArrowUI, ArrowRightRect, null, Color.White*opacityRight, 0f, new Vector2(ArrowUI.Width / 2, ArrowUI.Height / 2), SpriteEffects.None, 0f);
          //  spriteBatch.Draw(players[0].Texture, new Vector2(70, 60) - new Vector2(0, 806) * 0.2f, null, Color.White, 0f, Vector2.Zero, 0.6f, SpriteEffects.None, 0f);
            spriteBatch.Draw(CancelTex, new Vector2((ScreenManager.GraphicsDevice.Viewport.Bounds.Right - CancelTex.Width / 2) - 3, 7 + CancelTex.Height / 2), null, Color.White, 0f, new Vector2(CancelTex.Width / 2, CancelTex.Height / 2), 0.7f, SpriteEffects.None, 0f);
#if DEBUG
            spriteBatch.DrawString(spritefont, "Mouse X : " + MouseRect.X + " Mouse Y : " + MouseRect.Y, new Vector2(50, 100), Color.White);
#endif
            spriteBatch.End();


            base.Draw(gameTime);
        }


        private void DrawModel(Model model, Matrix world, CameraObj camera)
        {
            Matrix[] transforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(transforms);

            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    //effect.SpecularColor = Color.WhiteSmoke.ToVector3();
                    ///effect.SpecularPower = 100.0f;
                    //effect.FogEnabled = true;
                    //effect.FogColor = Color.White.ToVector3();
                    //effect.FogStart = 999999.0f;
                    //effect.FogEnd = 1000000.0f;


                    effect.EnableDefaultLighting();
                    effect.DiffuseColor = new Vector3(0.7f, 0.7f, 0.7f);
                    effect.World = transforms[mesh.ParentBone.Index] * world;
                    // Use the matrices provided by the chase camera
                    effect.View = camera.view;
                    effect.Projection = camera.proj;
                }
                mesh.Draw();
            }
        }

        public void DrawBorderedText(string text, int PosX, int PosY, Color color, SpriteFont font)
        {
            spriteBatch.DrawString(font, text, new Vector2(PosX + 1, PosY + 1), Color.Black * 0.5f);
            spriteBatch.DrawString(font, text, new Vector2(PosX + 1, PosY - 1), Color.Black * 0.5f);
            spriteBatch.DrawString(font, text, new Vector2(PosX - 1, PosY + 1), Color.Black * 0.5f);
            spriteBatch.DrawString(font, text, new Vector2(PosX - 1, PosY - 1), Color.Black * 0.5f);
            spriteBatch.DrawString(font, text, new Vector2(PosX, PosY), color);

        }
    }
}

