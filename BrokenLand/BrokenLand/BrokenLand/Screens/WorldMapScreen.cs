﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using Supernova.Particles2D;
using Supernova.Particles2D.Modifiers.Alpha;
using Supernova.Samples.Windows.Utilities;
using GameStateManagement;
using System.Threading;
using Microsoft.Xna.Framework.Content;
using BrokenLands;


namespace BrokenLands
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class WorldMapScreen : GameScreen
    {

        ContentManager content;
        SpriteBatch spriteBatch;
        SpriteFont spritefont, InconsolataFont, BigInconsolataFont;

        MouseState prevMouseState, currentMouseState;
        KeyboardState prevKeyboardState;
        //Particle NewParticle;

        float FrameCounter, frameRate;
        Texture2D WorldMapTex, WorldMapUIOverlay;
        Rectangle WorldMapTexPos, prevWorldMapTexPos;

        Rectangle MapBounds;

        

        Texture2D CharacterIcon, ExitIcon, Inventoryicon, PartyIcon, SettingsIcon;
        Texture2D ButtonIcon;

        Rectangle[] ButtonsRect;
        float[] ButtonAlpha;

        Texture2D crtEffectTex;
        Vector2[] crtOverlayPos;

        float pauseAlpha;

        Rectangle MouseRect;

        float ZoomF, scalingFactor;

        Random random;

        MouseController mousecontroller = new MouseController();

        Inventory LootTemp = new Inventory();

        int intendedAction = -1;
        int mapInt = -1;

        public WorldMapScreen()
            : base()
        {


        }



        public override void Activate(bool instancePreserved)
        {
            if (!instancePreserved)
            {
                if (content == null)
                    content = new ContentManager(ScreenManager.Game.Services, "Content");

                //TestCamera = new CameraObj(this, new Vector3(0, 0, 0), new Vector3(0, 0, 10), Vector3.Up, 1f, 1000f, 1f);
                
                FrameCounter = 0;
                prevMouseState = Mouse.GetState();
                currentMouseState = Mouse.GetState();
                //prevKeyboardState = Keyboard.GetState();

                //Placeholder for the Load
                //Add All Loading stuff in here
                //Including Content Loading
                Thread.Sleep(1000);

                //Loading
                spriteBatch = new SpriteBatch(ScreenManager.GraphicsDevice);

                //Textures
                WorldMapTex = content.Load<Texture2D>("Maps/Matrya-MAP");
                
                WorldMapUIOverlay = content.Load<Texture2D>("UI/skyrim-UI");
                crtEffectTex = content.Load<Texture2D>("Screen Effects/CRT-Overlay");

                //Icons
                ButtonIcon = content.Load<Texture2D>("UI/Button");
                PartyIcon = content.Load<Texture2D>("UI/Icons/Party-Icon");
                CharacterIcon = content.Load<Texture2D>("UI/Icons/Character-Icon");
                ExitIcon = content.Load<Texture2D>("UI/Icons/Exit-Icon");
                Inventoryicon = content.Load<Texture2D>("UI/Icons/Inventory-Icon");
                SettingsIcon = content.Load<Texture2D>("UI/Icons/Settings-Icon");

                //Particles

                //Fonts
                spritefont = content.Load<SpriteFont>("Fonts/Times12");
                InconsolataFont = content.Load<SpriteFont>("Fonts/Inconstella");
                BigInconsolataFont = content.Load<SpriteFont>("Fonts/BigInconsolataFont");

                //Models

                //Variables
                WorldMapTexPos = new Rectangle(0, 0, WorldMapTex.Width, WorldMapTex.Height);
                prevWorldMapTexPos = WorldMapTexPos;
                ZoomF = 0.3f;

                random = new Random();
                MapBounds = ScreenManager.GraphicsDevice.Viewport.Bounds;
                //Used for mouse Collision
                MouseRect = new Rectangle(Mouse.GetState().X, Mouse.GetState().Y, 10, 10);
                crtOverlayPos = new Vector2[2];
                crtOverlayPos[0] = new Vector2(0, 0);
                crtOverlayPos[1] = new Vector2(crtEffectTex.Bounds.Right, crtEffectTex.Bounds.Bottom);

                for (int i = 0; i < EquipmentLoader.equipList.Count; i++)
                {
                    //Item item = new Item(i, EquipmentLoader.equipList[i].name, EquipmentLoader.equipList[i].desc, (int)EquipmentLoader.equipList[i].type,
                    //EquipmentLoader.equipList[i].shopValue, EquipmentLoader.equipTexture[i]);
                    Item item = new Item(EquipmentLoader.equipList[i], EquipmentLoader.equipTexture[i]);
                    //("Poop", "Fecal Material", 1, "Junk", new Vector2(830 + (i % 5) * 77, 37 + (i / 5) * 77), SettingsIcon);
                    LootTemp.AddItem(item);
                }


                GameSession.GS.mapNodes[0].setInitLocation(new Rectangle(1500, 1023, GameSession.GS.CirlceBtnTex.Width / 2, GameSession.GS.CirlceBtnTex.Height / 2));//First
                GameSession.GS.mapNodes[1].setInitLocation(new Rectangle(2060, 864, GameSession.GS.CirlceBtnTex.Width / 2, GameSession.GS.CirlceBtnTex.Height / 2));//Second
                GameSession.GS.mapNodes[2].setInitLocation(new Rectangle(2566, 400, GameSession.GS.CirlceBtnTex.Width / 2, GameSession.GS.CirlceBtnTex.Height / 2));//Third
                GameSession.GS.mapNodes[3].setInitLocation(new Rectangle(2966, 646, GameSession.GS.CirlceBtnTex.Width / 2, GameSession.GS.CirlceBtnTex.Height / 2));//Fourth
                GameSession.GS.mapNodes[4].setInitLocation(new Rectangle(3700, 440, GameSession.GS.CirlceBtnTex.Width / 2, GameSession.GS.CirlceBtnTex.Height / 2));//End

                ButtonsRect = new Rectangle[5];
                ButtonsRect[0] = new Rectangle((int)((ScreenManager.GraphicsDevice.Viewport.Width / 10) * 1f), (int)((int)(ScreenManager.GraphicsDevice.Viewport.Height / 10) * 9.35f), ButtonIcon.Width, ButtonIcon.Height);
                ButtonsRect[1] = new Rectangle((int)((ScreenManager.GraphicsDevice.Viewport.Width / 10) * 2.8f), (int)((int)(ScreenManager.GraphicsDevice.Viewport.Height / 10) * 9.35f), ButtonIcon.Width, ButtonIcon.Height);
                ButtonsRect[2] = new Rectangle((int)((ScreenManager.GraphicsDevice.Viewport.Width / 10) * 4.6f), (int)((int)(ScreenManager.GraphicsDevice.Viewport.Height / 10) * 9.35f), ButtonIcon.Width, ButtonIcon.Height);
                ButtonsRect[3] = new Rectangle((int)((ScreenManager.GraphicsDevice.Viewport.Width / 10) * 7.3f), (int)((int)(ScreenManager.GraphicsDevice.Viewport.Height / 10) * 9.35f), ButtonIcon.Width, ButtonIcon.Height);
                ButtonsRect[4] = new Rectangle((int)((ScreenManager.GraphicsDevice.Viewport.Width / 10) * 9f), (int)((int)(ScreenManager.GraphicsDevice.Viewport.Height / 10) * 9.35f), ButtonIcon.Width, ButtonIcon.Height);

                ButtonAlpha = new float[5];
                for (int i = 0; i < ButtonsRect.Length; i++)
                {
                    ButtonAlpha[i] = 0.8f;
                }

                //Initial Zoom
                // Values are ONLY 2,1,0.5f
                scalingFactor = 1f;
                //To get 0.5f
                MapBounds.Width *= 2;
                MapBounds.Height *= 2;
                scalingFactor /= 2;
                
                // once the load has finished, we use ResetElapsedTime to tell the game's
                // timing mechanism that we have just finished a very long frame, and that
                // it should not try to catch up.
                GameSession.GS.UpdateMusic();
                ScreenManager.Game.ResetElapsedTime();
                ScreenManager.fader.FadeInScreen(2000);
            }
        }

        //Every activate must have a deactivate
        public override void Deactivate()
        {
            base.Deactivate();
        }

        /// <summary>
        /// Unload graphics content used by the game.
        /// </summary>
        public override void Unload()
        {
            content.Unload();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);

            if (coveredByOtherScreen)
                pauseAlpha = Math.Min(pauseAlpha + 1f / 32, 1);
            else
                pauseAlpha = Math.Max(pauseAlpha - 1f / 32, 0);



            mousecontroller.MouseUpdate();

            //Rectangle[] prevLocations = Locations;
            currentMouseState = Mouse.GetState();

            //Update Mouse Position
            MouseRect.X = currentMouseState.X;
            MouseRect.Y = currentMouseState.Y;
            Rectangle temp = new Rectangle((int)(MouseRect.X / scalingFactor), (int)(MouseRect.Y / scalingFactor), (int)(GameSession.GS.mapNodes[0].currentTex.Width * scalingFactor), (int)(GameSession.GS.mapNodes[0].currentTex.Height * scalingFactor));
            MouseRect = temp;

            //crtJitterPos = Vector2.Lerp(crtJitterPos, new Vector2(crtJitterPos.X+100, crtJitterPos.Y+100), 0.5f*(float)gameTime.ElapsedGameTime.TotalSeconds);

            //if the world map hits the left border
            if (WorldMapTexPos.X > 0)
            {
                WorldMapTexPos.X = 0;
                for (int i = 0; i < GameSession.GS.mapNodes.Length; i++)
                {
                    GameSession.GS.mapNodes[i].Location.X = GameSession.GS.mapNodes[i].InitialLocation.X;
                }
            }
            //if the world map hits the top border
            if (WorldMapTexPos.Y > 0)
            {
                WorldMapTexPos.Y = 0;
                for (int i = 0; i < GameSession.GS.mapNodes.Length; i++)
                {
                    GameSession.GS.mapNodes[i].Location.Y = GameSession.GS.mapNodes[i].InitialLocation.Y;
                }
            }
            /*
            for (int i = 0; i < crtOverlayPos.Length; i++)
            {
                //crtOverlayPos[i].X += 10;
                crtOverlayPos[i].Y += 10;
                if (crtOverlayPos[i].Y > ScreenManager.GraphicsDevice.Viewport.Height)
                {
                    crtOverlayPos[i].Y = -crtEffectTex.Height;
                }
            }
            */
            //If the player scrolls the mouse wheel
            //Will zoom in and out
            //To achieve this, must multiply or divide the mapbounds source rectangle and change the scaling factor for the spritebatch
            //if (currentMouseState.ScrollWheelValue != prevMouseState.ScrollWheelValue)
            //{
            //    if ((currentMouseState.ScrollWheelValue - prevMouseState.ScrollWheelValue) > 0)
            //    {
            //        if (scalingFactor <= 1)
            //        {
            //            MapBounds.Width /= 2;
            //            MapBounds.Height /= 2;
            //            scalingFactor *= 2;
            //        }
            //    }
            //    if ((currentMouseState.ScrollWheelValue - prevMouseState.ScrollWheelValue) < 0)
            //    {
            //        if (scalingFactor > 0.5f)
            //        {
            //            MapBounds.Width *= 2;
            //            MapBounds.Height *= 2;
            //            scalingFactor /= 2;
            //        }
            //    }
            //}

            #region Input Checking
            //Used to do checking with the location nodes
            for (int i = 0; i < GameSession.GS.mapNodes.Length; i++)
            {
                if (GameSession.GS.mapNodes[i].discoveredBool == true)
                {
                    if (GameSession.GS.mapNodes[GameSession.GS.mapNodes[i].linkedNodes[0]].discoveredBool == true)
                    {
                        if (MouseRect.Intersects(GameSession.GS.mapNodes[i].Location))
                        {
                            GameSession.GS.mapNodes[i].currentTex = GameSession.GS.CircleBtnSelTex;
                            if (prevMouseState.LeftButton != ButtonState.Pressed)
                            {
                                if (mousecontroller.LeftMouseClick())
                                {
                                    intendedAction = 0;
                                    mapInt = GameSession.GS.mapNodes[i].mapInt;
                                    GameSession.GS.mapNodes = GameSession.GS.mapNodes[i + 1].discoverNode(GameSession.GS.mapNodes, i + 1);
                                    ScreenManager.currentMapNodeDesc = GameSession.GS.mapNodes[i].nodeDesc;
                                    ScreenManager.currentMapNodeName = GameSession.GS.mapNodes[i].nodeName;
                                    ScreenManager.fader.FadeOutScreen(1000);
                                }
                            }
                        }
                        else
                        {
                            GameSession.GS.mapNodes[i].currentTex = GameSession.GS.CirlceBtnTex;
                        }
                    }
                }
            }

            //To check for Bottom bar mouse interaction
            for (int i = 0; i < ButtonsRect.Length; i++)
            {
                if (new Rectangle(currentMouseState.X, currentMouseState.Y, 20, 20).Intersects(new Rectangle((int)(ButtonsRect[i].X - ((int)(ButtonsRect[i].Width * 0.75)) / 2),
                    ((int)(ButtonsRect[i].Y) - ((int)(ButtonsRect[i].Width * 0.75)) / 8), (int)(ButtonsRect[i].Width * 0.75), (int)(ButtonsRect[i].Height * 0.75))))
                {
                    ButtonAlpha[i] = 1f;
                    if (currentMouseState.LeftButton == ButtonState.Pressed)
                    {
                        if (prevMouseState.LeftButton != ButtonState.Pressed)
                        {
                            //ADD THE CHECKING CODE TO ENTER OTHER SCREENS FROM MAIN MENU
                            switch (i)
                            {
                                case 0: //this.ScreenState = ScreenState.Hidden;
                                    ScreenManager.AddScreen(new InventoryScreen(), null);
                                    break;
                                case 1:
                                    ScreenManager.AddScreen(new PartyScreen(), null);
                                    break;
                                case 2:
                                    ScreenManager.AddScreen(new SkillScreen(), null);
                                    break;
                                case 3:
                                    break;
                                case 4:
                                    ScreenManager.AddScreen(new ExitMenuScreen(), null);
                                    //ScreenManager.Game.Exit();
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
                else
                {
                    ButtonAlpha[i] = 0.8f;
                }
            }
            //if the user clicks the map and drags to move the map around
            if (currentMouseState.LeftButton == ButtonState.Pressed)
            {
                if (CheckBorderOnly())
                {
                    if (currentMouseState.X != prevMouseState.X)
                    {
                        MapBounds.X -= (currentMouseState.X - prevMouseState.X);
                        for (int i = 0; i < GameSession.GS.mapNodes.Length; i++)
                        {
                            GameSession.GS.mapNodes[i].Location.X += (currentMouseState.X - prevMouseState.X);
                        }
                    }
                    if (currentMouseState.Y != prevMouseState.Y)
                    {
                        MapBounds.Y -= (currentMouseState.Y - prevMouseState.Y);
                        for (int i = 0; i < GameSession.GS.mapNodes.Length; i++)
                        {
                            GameSession.GS.mapNodes[i].Location.Y += (currentMouseState.Y - prevMouseState.Y);
                        }
                    }
                }
            }
            #endregion
            if (ScreenManager.fader.checkFadeOutOver())
            {
                switch (intendedAction)
                {
                    case 0:
                        DungeonScreen game = new DungeonScreen();
                        game.mapName = QuestReactor.Locations[mapInt];
                        ScreenManager.AddScreen(game, null);
                        ScreenManager.RemoveScreen(this);
                        break;
                }
            }

            if (Keyboard.GetState().IsKeyDown(Keys.Tab))
            {
                if (prevKeyboardState.IsKeyUp(Keys.Tab))
                {
                    ScreenManager.AddScreen(new QuestScreen(), null);
                }
            }
            if (prevKeyboardState.IsKeyUp(Keys.Escape))
            {
                if (Keyboard.GetState().IsKeyDown(Keys.Escape))
                {
                    ScreenManager.AddScreen(new ExitMenuScreen(), null);
                }
            }

            GameSession.gameAudioEngine.Update(gameTime);
            // Reset prevMouseState
            prevMouseState = Mouse.GetState();
            prevKeyboardState = Keyboard.GetState();




            // NewParticle.Update(gameTime);

        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Draw(GameTime gameTime)
        {
            ScreenManager.GraphicsDevice.Clear(Color.Black);



            ScreenManager.GraphicsDevice.BlendState = BlendState.Opaque;
            ScreenManager.GraphicsDevice.DepthStencilState = DepthStencilState.Default;

            //NewParticle.Draw(gameTime, TestCamera);
            frameRate = (float)(1 / gameTime.ElapsedGameTime.TotalSeconds);
            Matrix scalingMatrix = Matrix.CreateScale(0.5f);
            spriteBatch.Begin(SpriteSortMode.Immediate, null, null, null, null, null, scalingMatrix);

            CheckBorders();
            spriteBatch.Draw(WorldMapTex, new Vector2(WorldMapTexPos.X, WorldMapTexPos.Y), MapBounds, Color.White, 0f, new Vector2(0, 0), 1, SpriteEffects.None, 0f);

            for (int i = 0; i < GameSession.GS.mapNodes.Length; i++)
            {
                spriteBatch.Draw(GameSession.GS.mapNodes[i].currentTex, new Vector2(GameSession.GS.mapNodes[i].Location.X, GameSession.GS.mapNodes[i].Location.Y), null, Color.White, 0f, new Vector2(0, 0), 0.4f, SpriteEffects.None, 0f);
                GameSession.GS.mapNodes[i].DrawName(spriteBatch, BigInconsolataFont);
                    /*
                    if (mapNodes[mapNodes[i].linkedNodes[0]].discoveredBool)
                    {
                        if (mapNodes[i].linkedNodes[0] < mapNodes.Length)
                        {
                            Texture2D SimpleTexture = new Texture2D(ScreenManager.GraphicsDevice, 1, 1, false,
                            SurfaceFormat.Color);

                            Int32[] pixel = { 0xFFFFFF }; // White. 0xFF is Red, 0xFF0000 is Blue
                            SimpleTexture.SetData<Int32>(pixel, 0, SimpleTexture.Width * SimpleTexture.Height);

                            double angle = Math.Atan2(mapNodes[mapNodes[i].linkedNodes[0]].Location.Y - mapNodes[i].Location.Y, mapNodes[mapNodes[i].linkedNodes[0]].Location.X - mapNodes[i].Location.X);
                            // Paint a 100x1 line starting at 20, 50
                            //this.spriteBatch.Draw(SimpleTexture, new Rectangle(mapNodes[i].Location.X, mapNodes[i].Location.Y, 100, 3), Color.Blue);
                            this.spriteBatch.Draw(SimpleTexture, new Rectangle(mapNodes[i].Location.X + (int)(mapNodes[i].currentTex.Width * scalingFactor) / 2, mapNodes[i].Location.Y + (int)(mapNodes[i].currentTex.Height * scalingFactor) / 2, (int)Vector2.Distance(new Vector2(mapNodes[i].Location.X, mapNodes[i].Location.Y), new Vector2(mapNodes[mapNodes[i].linkedNodes[0]].Location.X, mapNodes[mapNodes[i].linkedNodes[0]].Location.Y)), 3), null,
            Color.WhiteSmoke, (float)angle, new Vector2(0f, 0f), SpriteEffects.None, 1f);
                            //DrawLineTo(spriteBatch, CirlceBtnTex, new Vector2(mapNodes[i].Location.X, mapNodes[i].Location.Y),
                            //   new Vector2(mapNodes[mapNodes[i].linkedNodes[0]].Location.X, mapNodes[mapNodes[i].linkedNodes[0]].Location.Y),
                            //   Color.White);
                        }
                    }
                    */
                
            }

            spriteBatch.End();

            //UI DRAWING
            spriteBatch.Begin();

            //spriteBatch.DrawString(spritefont, "Zoom :" + scalingFactor + " " + Mouse.GetState().ScrollWheelValue + " " + MapBounds.X + " " + MapBounds.Y, new Vector2(50, 50), Color.White);
            //spriteBatch.DrawString(spritefont, "Mouse X : " + MouseRect.X + " Mouse Y : " + MouseRect.Y, new Vector2(50, 100), Color.White);
            //spriteBatch.DrawString(spritefont, "Button3 X : " + ButtonsRect[2].Location.X + " Button3 Y : " + ButtonsRect[2].Location.Y, new Vector2(50, 150), Color.White);
#if DEBUG
            spriteBatch.DrawString(spritefont, "Zoom :" + scalingFactor + " " + Mouse.GetState().ScrollWheelValue + " " + MapBounds.X + " " + MapBounds.Y, new Vector2(50, 50), Color.White);
            spriteBatch.DrawString(spritefont, "Mouse X : " + MouseRect.X + " Mouse Y : " + MouseRect.Y, new Vector2(50, 100), Color.White);
            spriteBatch.DrawString(spritefont, "Button3 X : " + ButtonsRect[2].Location.X + " Button3 Y : " + ButtonsRect[2].Location.Y, new Vector2(50, 150), Color.White);
            //spriteBatch.DrawString(spritefont, "X: " + Mouse.GetState().X + " Y : " + Mouse.GetState().Y, new Vector2(50, 100), Color.White);
#endif
            //</Debug
            spriteBatch.Draw(WorldMapUIOverlay, new Rectangle(0, 0, ScreenManager.GraphicsDevice.Viewport.Width, ScreenManager.GraphicsDevice.Viewport.Height), Color.White * 0.8f);

            //Buttons
            for (int i = 0; i < ButtonsRect.Length; i++)
            {
                spriteBatch.Draw(ButtonIcon, new Vector2(ButtonsRect[i].X, ButtonsRect[i].Y), null, Color.White * ButtonAlpha[i], 0f, new Vector2(ButtonIcon.Width / 2, ButtonIcon.Height / 2), 0.75f, SpriteEffects.None, 0f);
            }

            spriteBatch.Draw(Inventoryicon, new Vector2(ButtonsRect[0].Location.X - 60, (int)(ScreenManager.GraphicsDevice.Viewport.Height / 10) * 9.35f), null, Color.White, 0f, new Vector2(Inventoryicon.Width / 2, Inventoryicon.Height / 2), 0.75f, SpriteEffects.None, 0f);
            spriteBatch.Draw(PartyIcon, new Vector2(ButtonsRect[1].Location.X - 60, (int)(ScreenManager.GraphicsDevice.Viewport.Height / 10) * 9.35f), null, Color.White, 0f, new Vector2(PartyIcon.Width / 2, PartyIcon.Height / 2), 0.75f, SpriteEffects.None, 0f);
            spriteBatch.Draw(CharacterIcon, new Vector2(ButtonsRect[2].Location.X - 60, (int)(ScreenManager.GraphicsDevice.Viewport.Height / 10) * 9.35f), null, Color.White, 0f, new Vector2(CharacterIcon.Width / 2, CharacterIcon.Height / 2), 0.75f, SpriteEffects.None, 0f);
            spriteBatch.Draw(SettingsIcon, new Vector2(ButtonsRect[3].Location.X - 60, (int)(ScreenManager.GraphicsDevice.Viewport.Height / 10) * 9.35f), null, Color.White, 0f, new Vector2(SettingsIcon.Width / 2, SettingsIcon.Height / 2), 0.75f, SpriteEffects.None, 0f);
            spriteBatch.Draw(ExitIcon, new Vector2(ButtonsRect[4].Location.X - 60, (int)(ScreenManager.GraphicsDevice.Viewport.Height / 10) * 9.35f), null, Color.White, 0f, new Vector2(ExitIcon.Width / 2, ExitIcon.Height / 2), 0.75f, SpriteEffects.None, 0f);

            spriteBatch.DrawString(InconsolataFont, "INVENTORY", new Vector2((int)(ScreenManager.GraphicsDevice.Viewport.Width / 10) * 1f,
                (int)(ScreenManager.GraphicsDevice.Viewport.Height / 10) * 9.35f), Color.White, 0f, new Vector2(InconsolataFont.MeasureString("INVENTORY").X / 2,
                    InconsolataFont.MeasureString("INVENTORY").Y / 2), 0.75f, SpriteEffects.None, 0f);
            spriteBatch.DrawString(InconsolataFont, "PARTY", new Vector2((int)(ScreenManager.GraphicsDevice.Viewport.Width / 10) * 2.8f,
                (int)(ScreenManager.GraphicsDevice.Viewport.Height / 10) * 9.35f), Color.White, 0f, new Vector2(InconsolataFont.MeasureString("PARTY").X / 2,
                    InconsolataFont.MeasureString("PARTY").Y / 2), 0.75f, SpriteEffects.None, 0f);
            spriteBatch.DrawString(InconsolataFont, "CHARACTER", new Vector2((int)(ScreenManager.GraphicsDevice.Viewport.Width / 10) * 4.7f,
                (int)(ScreenManager.GraphicsDevice.Viewport.Height / 10) * 9.35f), Color.White, 0f, new Vector2(InconsolataFont.MeasureString("CHARACTER").X / 2,
                    InconsolataFont.MeasureString("CHARACTER").Y / 2), 0.75f, SpriteEffects.None, 0f);
            spriteBatch.DrawString(InconsolataFont, "SETTINGS", new Vector2((int)(ScreenManager.GraphicsDevice.Viewport.Width / 10) * 7.4f,
                (int)(ScreenManager.GraphicsDevice.Viewport.Height / 10) * 9.35f), Color.White, 0f, new Vector2(InconsolataFont.MeasureString("SETTINGS").X / 2,
                    InconsolataFont.MeasureString("SETTINGS").Y / 2), 0.75f, SpriteEffects.None, 0f);
            spriteBatch.DrawString(InconsolataFont, "EXIT", new Vector2((int)(ScreenManager.GraphicsDevice.Viewport.Width / 10) * 9f,
                (int)(ScreenManager.GraphicsDevice.Viewport.Height / 10) * 9.35f), Color.White, 0f, new Vector2(InconsolataFont.MeasureString("EXIT").X / 2,
                    InconsolataFont.MeasureString("EXIT").Y / 2), 0.75f, SpriteEffects.None, 0f);
            for (int i = 0; i < crtOverlayPos.Length; i++)
            {
                spriteBatch.Draw(crtEffectTex, crtOverlayPos[i], null, Color.White * 0.05f, 0f, new Vector2(0, 0), 0.7f, SpriteEffects.None, 0f);
            }


            mousecontroller.MouseDraw(spriteBatch);
            spriteBatch.End();


            base.Draw(gameTime);
        }

        #region BorderChecking
        private bool CheckBorders()
        {

            if (scalingFactor == 1)
            {
                if (MapBounds.X > 2724)
                {
                    MapBounds.X = 2724;
                    return false;
                }
                if (MapBounds.Y > 1634)
                {
                    MapBounds.Y = 1634;
                    return false;
                }
            }
            else if (scalingFactor == 2)
            {
                if (MapBounds.X > 3364)
                {
                    MapBounds.X = 3364;
                    return false;
                }
                if (MapBounds.Y > 1945)
                {
                    MapBounds.Y = 1945;
                    return false;
                }
            }
            else if (scalingFactor == 0.5)
            {
                if (MapBounds.X > 1450)
                {
                    MapBounds.X = 1450;
                    for (int i = 0; i < GameSession.GS.mapNodes.Length; i++)
                    {
                        GameSession.GS.mapNodes[i].Location.X = GameSession.GS.mapNodes[i].InitialLocation.X - MapBounds.X;// +1450;
                    }
                    return false;
                }
                if (MapBounds.Y > 1016)
                {
                    MapBounds.Y = 1016;
                    for (int i = 0; i < GameSession.GS.mapNodes.Length; i++)
                    {
                        GameSession.GS.mapNodes[i].Location.Y = GameSession.GS.mapNodes[i].InitialLocation.Y - MapBounds.Y;// +1016;
                    }
                    return false;
                }
            }
            if (MapBounds.X < 0)
            {
                MapBounds.X = 0;
                for (int i = 0; i < GameSession.GS.mapNodes.Length; i++)
                {
                    GameSession.GS.mapNodes[i].Location.X = GameSession.GS.mapNodes[i].InitialLocation.X;
                }
                return false;
            }
            if (MapBounds.Y < 0)
            {
                MapBounds.Y = 0;
                for (int i = 0; i < GameSession.GS.mapNodes.Length; i++)
                {
                    GameSession.GS.mapNodes[i].Location.Y = GameSession.GS.mapNodes[i].InitialLocation.Y;
                }
                return false;
            }
            return true;
        }
        public void DrawLineTo(SpriteBatch sb, Texture2D texture, Vector2 src, Vector2 dst, Color color)
        {
            //direction is destination - source vectors
            Vector2 direction = dst - src;
            //get the angle from 2 specified numbers (our point)
            var angle = (float)Math.Atan2(direction.Y, direction.X);
            //calculate the distance between our two vectors
            float distance;
            Vector2.Distance(ref src, ref dst, out distance);

            //draw the sprite with rotation
            sb.Draw(texture, src, new Rectangle((int)src.X, (int)src.Y, (int)distance, 1), Color.White, angle, Vector2.Zero, 1.0f, SpriteEffects.None, 0);
        }
        private bool CheckBorderOnly()
        {

            if (scalingFactor == 1)
            {
                if (MapBounds.X > 2724)
                {
                    return false;
                }
                if (MapBounds.Y > 1834)
                {
                    return false;
                }
            }
            else if (scalingFactor == 2)
            {
                if (MapBounds.X > 3364)
                {
                    return false;
                }
                if (MapBounds.Y > 2095)
                {
                    return false;
                }
            }
            else if (scalingFactor == 0.5)
            {
                if (MapBounds.X > 1450)
                {
                    return false;
                }
                if (MapBounds.Y > 1016)
                {
                    return false;
                }
            }
            if (MapBounds.X < 0)
            {
                return false;
            }
            if (MapBounds.Y < 0)
            {
                return false;
            }
            return true;
        }

    }

}
        #endregion