﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using Supernova.Particles2D;
using Supernova.Particles2D.Modifiers.Alpha;
using Supernova.Samples.Windows.Utilities;
using GameStateManagement;
using System.Threading;
using Microsoft.Xna.Framework.Content;


namespace BrokenLands
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class ExitMenuScreen : GameScreen
    {

        ContentManager content;
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SpriteFont spritefont, InconsolataFont;

        MouseState prevMouseState, currentMouseState;
        KeyboardState prevKeyboardState;

        float FrameCounter, frameRate;
        Texture2D WorldMapTex, WorldMapUIOverlay;
        Rectangle WorldMapTexPos;

        Texture2D CancelTex;
        Model Tile;
        bool isExiting = false;
        int timer;

        //List<InventoryItem> Items = new List<InventoryItem>();

        float[] ButtonAlpha;

        Texture2D SettingsIcon;

        Rectangle MouseRect;
        Rectangle[] OptionRects;

        List<Player> players = new List<Player>();
        
        Random random;

        MouseController mousecontroller = new MouseController();

        public ExitMenuScreen()
            : base()
        {
            this.ScreenState = ScreenState.Active;

        }



        public override void Activate(bool instancePreserved)
        {
            if (!instancePreserved)
            {
                if (content == null)
                    content = new ContentManager(ScreenManager.Game.Services, "Content");

                //TestCamera = new CameraObj(this, new Vector3(0, 0, 0), new Vector3(0, 0, 10), Vector3.Up, 1f, 1000f, 1f);
                FrameCounter = 0;
                prevMouseState = Mouse.GetState();
                currentMouseState = Mouse.GetState();
                prevKeyboardState = Keyboard.GetState();

                //Loading
                spriteBatch = new SpriteBatch(ScreenManager.GraphicsDevice);

                //Textures
                WorldMapTex = content.Load<Texture2D>("UI/ExitMenu");
                Tile = content.Load<Model>("Tile/Tile");

                //Icons
                SettingsIcon = content.Load<Texture2D>("UI/Icons/Settings-Icon");
                CancelTex = content.Load<Texture2D>("UI/Icons/Cancel-Icon");

                //Fonts
                spritefont = content.Load<SpriteFont>("Fonts/Times12");
                InconsolataFont = content.Load<SpriteFont>("Fonts/Inconstella");

                //Models

                //Variables
                WorldMapTexPos = new Rectangle(0, 0, UIElement.viewport.Width, UIElement.viewport.Height);
                random = new Random();
                MouseRect = new Rectangle(Mouse.GetState().X, Mouse.GetState().Y, 10, 10);//Used for mouse Collision
                ButtonAlpha = new float[5];

                OptionRects = new Rectangle[6];

                for (int i = 0; i < OptionRects.Length; i++)
                {
                    OptionRects[i] = new Rectangle((int)(0.42265625f * ScreenManager.GraphicsDevice.Viewport.Width), (int)((0.269444444f + i * 0.0875f) * ScreenManager.GraphicsDevice.Viewport.Height), (int)(0.153125f * ScreenManager.GraphicsDevice.Viewport.Width), (int)(0.06944444f * ScreenManager.GraphicsDevice.Viewport.Height));
                }

                //Temp to get in Units in
                Texture2D tex = content.Load<Texture2D>("Units/Gunbowman");

                players = GameSession.players;
                //players.Add(new Player(5, 10, 15, 20, 25, 30, 0, "XDROFLMAO"));


                // once the load has finished, we use ResetElapsedTime to tell the game's
                // timing mechanism that we have just finished a very long frame, and that
                // it should not try to catch up.
                ScreenManager.Game.ResetElapsedTime();
            }
        }

        //Every activate must have a deactivate
        public override void Deactivate()
        {
            base.Deactivate();
        }

        /// <summary>
        /// Unload graphics content used by the game.
        /// </summary>
        public override void Unload()
        {
            // content.Unload();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);

            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || (Keyboard.GetState().IsKeyDown(Keys.Escape) && prevKeyboardState.IsKeyUp(Keys.Escape)))
                ScreenManager.RemoveScreen(this);

            mousecontroller.MouseUpdate();

            //Rectangle[] prevLocations = Locations;
            currentMouseState = Mouse.GetState();

            //Update Mouse Position
            MouseRect.X = currentMouseState.X;
            MouseRect.Y = currentMouseState.Y;

            for (int i = 0; i < OptionRects.Length; i++)
            {
                if (MouseRect.Intersects(OptionRects[i]))
                {
                    if (mousecontroller.LeftMouseClick())
                    {
                        if (i == 0)
                        {
                            //Inventory
                            ScreenManager.AddScreen(new InventoryScreen(), null);
                        }
                        else if (i == 1)
                        {
                            //Quests
                            ScreenManager.AddScreen(new QuestScreen(), null);
                        }
                        else if (i == 2)
                        {
                            //Party Screen
                            ScreenManager.AddScreen(new PartyScreen(), null);
                        }
                        else if (i == 3)
                        {
                            //Skill Screen (Character Screen)
                            ScreenManager.AddScreen(new SkillScreen(), null);
                        }
                        else if (i == 4)
                        {
                            //Save
                            if (GameSession.map != null)
                            {
                                ScreenManager.notifier.setNewText("Saved", "Game & Map");
                                ScreenManager.notifier.StartNotify();
                                GameSession.map.mapGen.SaveActors();
                                GameSession.map.mapGen.SaveCharacters();
                                GameSession.map.mapGen.SaveInventory();
                                GameSession.map.mapGen.SaveLootContainers();
                                GameSession.map.mapGen.SaveQuestCondition();
                                GameSession.map.mapGen.WriteMap("../../../../BrokenLandContent/ObjectData/Map/SavedMaps/" + GameSession.map.mapName + ".map");
                                
                            }
                            else
                            {
                                ScreenManager.notifier.setNewText("Saved", "Game");
                                ScreenManager.notifier.StartNotify();
                                GameSession.map = new Map(content.Load<Texture2D>("MapObjects/defaultfloor"), content.Load<Texture2D>("MapObjects/BattleArenaSide"));
                                GameSession.map.mapGen = new MapGenerator(ScreenManager.Game, GameSession.GS.defaultModel, GameSession.map);
                                GameSession.map.mapGen.SaveCharacters();
                                GameSession.map.mapGen.SaveInventory();
                                GameSession.map = null;
                            }
                        }
                        else if (i == 5)
                        {
                            //Exit
                            foreach (GameScreen Screen in ScreenManager.GetScreens())
                            {
                                ScreenManager.RemoveScreen(Screen);
                            }
                            ScreenManager.AddScreen(new MainMenuScreen(), null);
                            /*if (GameSession.map != null)
                            {
                                ScreenManager.notifier.setNewText("Saved", "Exiting Game");
                                ScreenManager.notifier.StartNotify();
                                GameSession.map.mapGen.SaveActors();
                                GameSession.map.mapGen.SaveCharacters();
                                GameSession.map.mapGen.SaveLootContainers();
                                GameSession.map.mapGen.SaveQuestCondition();
                                GameSession.map.mapGen.WriteMap("../../../../BrokenLandContent/ObjectData/Map/SavedMaps/" + GameSession.map.mapName + ".map");
                                isExiting = true;
                                //Change audio music
                            }
                            else
                            {
                                GameSession.map = new Map(content.Load<Texture2D>("MapObjects/defaultfloor"), content.Load<Texture2D>("MapObjects/BattleArenaSide"));
                                GameSession.map.mapGen = new MapGenerator(ScreenManager.Game, GameSession.GS.defaultModel, GameSession.map);
                                GameSession.map.mapGen.SaveCharacters();
                                GameSession.map = null;
                                if (!isExiting)
                                {
                                    ScreenManager.notifier.setNewText("Saved", "Exiting Game");
                                    ScreenManager.notifier.StartNotify();
                                    isExiting = true;
                                    timer = gameTime.TotalGameTime.Seconds;
                                }
                            }*/
                        }
                    }
                }
            }

            if (MouseRect.Intersects(new Rectangle((ScreenManager.GraphicsDevice.Viewport.Bounds.Right - CancelTex.Width) - 5, 7, CancelTex.Width, CancelTex.Height)))
            {
                if (mousecontroller.LeftMouseClick())
                {
                    ScreenManager.RemoveScreen(this);
                }
            }

            // Reset prevMouseState
            GameSession.gameAudioEngine.Update(gameTime);
            prevMouseState = Mouse.GetState();
            prevKeyboardState = Keyboard.GetState();
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Draw(GameTime gameTime)
        {


            ScreenManager.FadeBackBufferToBlack(TransitionAlpha * 2 / 3);

            ScreenManager.GraphicsDevice.BlendState = BlendState.Opaque;
            ScreenManager.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            //NewParticle.Draw(gameTime, TestCamera);
            frameRate = (float)(1 / gameTime.ElapsedGameTime.TotalSeconds);

            float width = ScreenManager.GraphicsDevice.Viewport.Width;
            float scale = (width / 1920);
            Matrix scalingMatrix = Matrix.CreateScale(scale);
            spriteBatch.Begin(SpriteSortMode.Immediate, null, null, null, null, null, scalingMatrix);

            spriteBatch.Draw(WorldMapTex, new Vector2(ScreenManager.GraphicsDevice.Viewport.TitleSafeArea.X, ScreenManager.GraphicsDevice.Viewport.TitleSafeArea.Y), null, Color.White, 0f, new Vector2(0, 0), 1, SpriteEffects.None, 0f);

            spriteBatch.End();

            //UI DRAWING
            spriteBatch.Begin();
            //  spriteBatch.Draw(players[0].Texture, new Vector2(70, 60) - new Vector2(0, 806) * 0.2f, null, Color.White, 0f, Vector2.Zero, 0.6f, SpriteEffects.None, 0f);
            spriteBatch.Draw(CancelTex, new Vector2((ScreenManager.GraphicsDevice.Viewport.Bounds.Right - CancelTex.Width / 2) - 3, 7 + CancelTex.Height / 2), null, Color.White, 0f, new Vector2(CancelTex.Width / 2, CancelTex.Height / 2), 0.7f, SpriteEffects.None, 0f);
#if DEBUG
            spriteBatch.DrawString(spritefont, "Mouse X : " + MouseRect.X + " Mouse Y : " + MouseRect.Y, new Vector2(50, 100), Color.White);
#endif
            spriteBatch.End();


            base.Draw(gameTime);
        }


        private void DrawModel(Model model, Matrix world, CameraObj camera)
        {
            Matrix[] transforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(transforms);

            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    //effect.SpecularColor = Color.WhiteSmoke.ToVector3();
                    ///effect.SpecularPower = 100.0f;
                    //effect.FogEnabled = true;
                    //effect.FogColor = Color.White.ToVector3();
                    //effect.FogStart = 999999.0f;
                    //effect.FogEnd = 1000000.0f;


                    effect.EnableDefaultLighting();
                    effect.DiffuseColor = new Vector3(0.7f, 0.7f, 0.7f);
                    effect.World = transforms[mesh.ParentBone.Index] * world;
                    // Use the matrices provided by the chase camera
                    effect.View = camera.view;
                    effect.Projection = camera.proj;
                }
                mesh.Draw();
            }
        }

        public void DrawBorderedText(string text, int PosX, int PosY, Color color, SpriteFont font)
        {
            spriteBatch.DrawString(font, text, new Vector2(PosX + 1, PosY + 1), Color.Black * 0.5f);
            spriteBatch.DrawString(font, text, new Vector2(PosX + 1, PosY - 1), Color.Black * 0.5f);
            spriteBatch.DrawString(font, text, new Vector2(PosX - 1, PosY + 1), Color.Black * 0.5f);
            spriteBatch.DrawString(font, text, new Vector2(PosX - 1, PosY - 1), Color.Black * 0.5f);
            spriteBatch.DrawString(font, text, new Vector2(PosX, PosY), color);

        }
    }
}

