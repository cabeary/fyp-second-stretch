﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using Supernova.Particles2D;
using Supernova.Particles2D.Modifiers.Alpha;
using Supernova.Samples.Windows.Utilities;
using GameStateManagement;
using System.Threading;
using Microsoft.Xna.Framework.Content;
using BrokenLands;
using XNAGameConsole;


namespace BrokenLands
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class DungeonScreen : GameScreen
    {
        bool prerendered = false;
        Random random = new Random();
        ContentManager content;
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SpriteFont spritefont;
        CameraObj TestCamera;
        bool flowStart = true;
        Texture2D firesprite;

        public string mapName = "Norkansa_Station";//Barrens_Town
        MapGenerator mapGen;

        Model Tile, Floor;

        MouseState prevMouseState;
        KeyboardState lastKeyboardState;
        Map map;

        ParticleBase FireParticle;
        float queueTimer = 0;
        const float TIMER = 2000;

        List<Player> players = new List<Player>();
        List<Enemy> enemies = new List<Enemy>();
        public Vector3 centre;
        Texture2D tex;

        AI_Mother ai = new AI_Mother();

        float FrameCounter, frameRate;

        MouseController mousecontroller = new MouseController();
        GameSession gameSession;

        LootPopUp lootPopUp;

        Texture2D gradient;
        Effect effect;

        //string mapName;
        Texture2D CardTex;

        Texture2D AbilityTxt, AbilityButton, AbilityButtonPressed;


        //debugging
        float least = 10000000000;

        public DungeonScreen()
            : base()
        {
            

        }



        public override void Activate(bool instancePreserved)
        {
            if (!instancePreserved)
            {

                if (content == null)
                    content = new ContentManager(ScreenManager.Game.Services, "Content");

                
                gameSession = GameSession.GS;

                gameSession.SetMouse(mousecontroller);
                gameSession.SetGame(this);
                //if (GameSession.firstInitialise)
                   //gameSession.LoadContent(content);

                gameSession.GameViewport = ScreenManager.GraphicsDevice.Viewport;

                //TestCamera = new CameraObj(this, new Vector3(0, 59.6f, 84.288f), new Vector3(0, 0, 0), Vector3.Up, 0, 1000f, 1f, true);
                TestCamera = new CameraObj(this, new Vector3(0, 59.6f, 84.288f), new Vector3(0, 0, 0), Vector3.Up, 0, 1000f, 1f, true);
                
                GameSession.gameCamera = TestCamera;

                TestCamera.VCameraPosition = new Vector3(-13, 70.1f, 112);

                FrameCounter = 0;
                prevMouseState = Mouse.GetState();


                //Loading
                spriteBatch = new SpriteBatch(ScreenManager.GraphicsDevice);

                //Textures
                firesprite = content.Load<Texture2D>("Particles/Fire");
                //Particles
                // FireParticle = new ParticleBase(TestCamera.proj, TestCamera.view, new Vector4(0, 0, 9f, 1.0f), ScreenManager.GraphicsDevice, firesprite, 0.1f, new Vector3(1f, 1f, 1f));
                // FireParticle.SetVelocity(new Vector3(0, 0, 1));

                //Fonts
                spritefont = content.Load<SpriteFont>("Fonts/Times12");

                UIElement.cam = TestCamera;
                UIElement.viewport = ScreenManager.GraphicsDevice.Viewport;
                //Models
                Tile = content.Load<Model>("Tile/Tile");
                Floor = content.Load<Model>("Floor");

                Inventory LootTemp = new Inventory();
                for (int i = 0; i < EquipmentLoader.equipList.Count; i++)
                {
                    //Item item = new Item(i, EquipmentLoader.equipList[i].name, EquipmentLoader.equipList[i].desc, (int)EquipmentLoader.equipList[i].type,
                    //EquipmentLoader.equipList[i].shopValue, EquipmentLoader.equipTexture[i]);
                    Item item = new Item(EquipmentLoader.equipList[i], EquipmentLoader.equipTexture[i]);
                    //("Poop", "Fecal Material", 1, "Junk", new Vector2(830 + (i % 5) * 77, 37 + (i / 5) * 77), SettingsIcon);
                    if (LootTemp.activeList.Count < 9)
                        LootTemp.AddItem(item);
                }
                for (int i = 0; i < EquipmentLoader.equipList.Count; i++)
                {
                    Item item = new Item(i, EquipmentLoader.equipList[i].name, EquipmentLoader.equipList[i].desc, (int)EquipmentLoader.equipList[i].type,
                    EquipmentLoader.equipList[i].shopValue, EquipmentLoader.equipTexture[i]);

                    //("Poop", "Fecal Material", 1, "Junk", new Vector2(830 + (i % 5) * 77, 37 + (i / 5) * 77), SettingsIcon);
                    if (LootTemp.activeList.Count < 9)
                        LootTemp.AddItem(item);
                } 


                //Variables
                Texture2D DialougeBack = content.Load<Texture2D>("UI/DialougeBack");
                Texture2D DialougeFrame = content.Load<Texture2D>("UI/DialougeFrame");
                Texture2D DialougeOptionTex = content.Load<Texture2D>("UI/DialougeOption");
                SpriteFont InconsolataFont = content.Load<SpriteFont>("Fonts/Inconstella");
                SpriteFont ComfortaaFont = content.Load<SpriteFont>("Fonts/Comfortaa");

                DialougeOverlay.Init(ComfortaaFont, DialougeBack, DialougeFrame, DialougeOptionTex, mousecontroller, ScreenManager);

                lootPopUp = new LootPopUp(content, LootTemp, ScreenManager, Vector2.Zero, mousecontroller);

                map = new Map(content.Load<Texture2D>("MapObjects/defaultfloor"), content.Load<Texture2D>("MapObjects/BattleArenaSide"));
                mapGen = new MapGenerator(ScreenManager.Game, Tile, map);

                Effect layer = content.Load<Effect>("Shaders/Layer");
                Effect scenelayer = content.Load<Effect>("Shaders/SceneLayer");
                Effect shadow = content.Load<Effect>("Shaders/Shadows");
                map.setShaders(layer,scenelayer,shadow);

                //generally working. Underlay still not being saved tho
                //mapGen.newmap = true;
                //mapGen.ReadMap("../../../../BrokenLandContent/ObjectData/Map/" + mapName + ".map", mapName);
                //mapGen.ReadMap("../../../../BrokenLandContent/ObjectData/Map/SavedMaps/" + mapName + ".map", mapName);//Barrens_Town

                try
                {
                    mapGen.ReadMap("../../../../BrokenLandContent/ObjectData/Map/SavedMaps/" + mapName + ".map", mapName);//Barrens_Town

                }
                catch (Exception ex)
                {
                    mapGen.newmap = true;
                    mapGen.ReadMap("../../../../BrokenLandContent/ObjectData/Map/" + mapName + ".map", mapName);
                }
                GameSession.dialogueActive = false;
                map.mapGen = mapGen;

                centre = map.gridNodes[(int)map.gridNodes.GetLength(0) / 2, (int)map.gridNodes.GetLength(0) / 2].getPositon();
                centre -= new Vector3(1.1f, 0.1f, 0.8f);

                tex = content.Load<Texture2D>("Units/Gunbowman");

                ai = new AI_Mother();
                enemies = new List<Enemy>();

                QuestReactor.setSession(gameSession);
                gameSession.NewMap(map, map.enemList);
                gameSession.Initiate();



                mapGen.AddPlayers(GameSession.players);
                mapGen.addtoDraw();
                foreach (Enemy enem in map.enemList)
                {
                    enem.MyStatus.BaseStr = random.Next(5, 9);
                    enem.MyStatus.BaseDex = random.Next(5, 13);
                    enem.MyStatus.BaseAgi = random.Next(5, 9);
                    enem.MyStatus.BaseEnd = random.Next(5, 9);
                    enem.MyStatus.BaseCha = random.Next(5, 9);
                    enem.MyStatus.BaseInt = random.Next(5, 9);
                    enem.reSpec();
                    enem.SetLocation(enem.MyLocation);

                }
                foreach (Player p in GameSession.players) {
                    p.MyStatus.UpdateEquips();
                }
                gradient = content.Load<Texture2D>("UI/gradient");

                effect = content.Load<Effect>("Shaders/Circlebar");
                //System.Diagnostics.Debug.WriteLine(players[0].Describe_Self());
                //System.Diagnostics.Debug.WriteLine(players[1].Describe_Self());
                // System.Diagnostics.Debug.WriteLine(enemies[0].Describe_Self());
                // System.Diagnostics.Debug.WriteLine(enemies[1].Describe_Self());




                //ai.Think(enemies[1], _path.arrayToList(map.gridNodes));
                //ai.Think(enemies[0], _path.arrayToList(map.gridNodes));

                //ai.Think(enemies[0], map);
                //ai.Think(enemies[1], _path.arrayToList(map.gridNodes));

                // once the load has finished, we use ResetElapsedTime to tell the game's
                // timing mechanism that we have just finished a very long frame, and that
                // it should not try to catch up.
                GameSession.GS.UpdateMusic();
                ScreenManager.Game.ResetElapsedTime();
                ScreenManager.fader.FadeInScreen(1000);
                ScreenManager.notifier.setNewText(ScreenManager.currentMapNodeName,ScreenManager.currentMapNodeDesc);
                ScreenManager.notifier.StartNotify();
            }
        }

        //Every activate must have a deactivate
        public override void Deactivate()
        {
            base.Deactivate();
        }

        /// <summary>
        /// Unload graphics content used by the game.
        /// </summary>
        public override void Unload()
        {   /*
            //content.Unload();
            content = null;
            gameSession.ExitMap();
            //gameSession.SetGame(null);
            //gameSession.SetMouse(null);
            //TestCamera = null;
            //GameSession.gameCamera = null;
            //spriteBatch.Dispose();
            firesprite = null;
            Tile = null;
            map = null;
            mapGen = null;
            centre = Vector3.Zero;
            tex = null;
            ai = null;
            enemies = null;
            players = null;
            // mapGen.AddPlayers(players);
            // mapGen.addtoDraw();

            */
            //gameSession.NewMap(map, map.playerList, map.enemList);
            //gameSession.Initiate();
        }
        
        private void LightUp(GameSession gameSession)
        {

            if (!GameSession.tileTexDict.TryGetValue("bluetile", out a[i].tex))
            {
                throw new NullReferenceException("MOVE TILE NOT IN DICTIONARY!");
            }
            a[i].useTex = true;

            if (i < a.Count - 1)
            {
                i++;
            }
        }
        bool lightup = false;
        List<MapCell> a;
        int i = 0;

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen)
        {

            if(gameSession.lockcamera){

            //if tutorial actor within range: start dialog
            least = 10000000;
            foreach(Actor a in map.actorList){

                float distance = Vector2.Distance(a.getPosition(), map.playerList[0].getPosition());

                if (least > distance)
                    least = distance;


                if ((a.name == "Tutorial") && (distance < 150)) {
                    a.chat = true;
                    GameSession.dialogueActive = true;

                    foreach (Player p in GameSession.players){
                        p.StopMove();
                    }
                    
                }
            }
            }

            /*
             * 
             * if(camera.target != target)
             * move camera to that position
 */
            if (GameSession.currentGameState == GameSession.GameState.Combat)
            {
                if (GameSession.GS.currentUnit.MyType == Entity.uType.Player)
                {
                    if (GameSession.GS.MouseRect.Intersects(new Rectangle(UIElement.viewport.TitleSafeArea.Right - GameSession.GS.endTurnButton.Width - 70, UIElement.viewport.TitleSafeArea.Bottom - GameSession.GS.endTurnButton.Height * 2 - 50, GameSession.GS.AbilityButton.Width, GameSession.GS.AbilityButton.Height)))
                    {
                        if (mousecontroller.LeftMouseClick())
                        {
                            if (!GameSession.GS.abilityClicked)
                            {
                                GameSession.AbilityList.UseAbility(GameSession.players[GameSession.players.IndexOf((Player)GameSession.GS.currentUnit)], null);
                                GameSession.GS.abilityClicked = true;
                            }
                        }
                    }
                }

            }

            if (flowStart)
            {
                gameSession.Update(gameTime,ScreenManager.notifier);
            }





            //pi.Update(gameTime, gameSession);

            //Vector2 vector2 = players[0].MyLocation.Location;
            queueTimer += (int)gameTime.ElapsedGameTime.TotalMilliseconds;
            /*
            if (queueTimer >= TIMER) {
                queueTimer = 0;
                CombatActions action;
                enemies[0].PrintQueue();
                enemies[0].PerformNext(_combat,out action);
                if (action.Unit == enemies[0])
                {
                    ai.Think(enemies[0], _path.arrayToList(map.gridNodes));
                }
                else
                {
                    ai.Think(enemies[1], _path.arrayToList(map.gridNodes));
                }
            }*/

            //if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
              //  ScreenManager.Game.Exit();

            if (lastKeyboardState.IsKeyUp(Keys.Escape))
            {
                if (Keyboard.GetState().IsKeyDown(Keys.Escape))
                {
                    ScreenManager.AddScreen(new ExitMenuScreen(), null);
                }
            }

            /*
            if (Keyboard.GetState().IsKeyDown(Keys.Z))
            {
                if (lastKeyboardState.IsKeyUp(Keys.Z) == true)
                {
                    TestCamera.scale += 5f;
                }
            } if (Keyboard.GetState().IsKeyDown(Keys.C))
            {
                if (lastKeyboardState.IsKeyUp(Keys.C) == true)
                {
                    TestCamera.posdiff /= 1.5f;
                }
            }*/
            int speed = 3;
            if (!ScreenManager.console.Opened)
            {
                if (Keyboard.GetState().IsKeyDown(Keys.W))
                    TestCamera.PanCamera(new Vector3(0, 0, -speed));
                if (Keyboard.GetState().IsKeyDown(Keys.S))
                    TestCamera.PanCamera(new Vector3(0, 0, speed));
                if (Keyboard.GetState().IsKeyDown(Keys.A))
                    TestCamera.PanCamera(new Vector3(-speed, 0, 0));
                if (Keyboard.GetState().IsKeyDown(Keys.D))
                    TestCamera.PanCamera(new Vector3(speed, 0, 0));
               
                if (lightup)
                {
                    LightUp(gameSession);
                }
                if (Keyboard.GetState().IsKeyDown(Keys.D6))
                {
                    if (lastKeyboardState.IsKeyUp(Keys.D6) == true)
                    {
                        GameSession.players[0].baseOffset.X += Math.Abs((GameSession.map.gridNodes[0, 0].getPositon().X - GameSession.map.gridNodes[1, 1].getPositon().X) / 2);
                        GameSession.players[0].baseOffset.Z += Math.Abs((GameSession.map.gridNodes[0, 0].getPositon().Z - GameSession.map.gridNodes[1, 1].getPositon().Z) / 2);
                    }
                }
                if (Keyboard.GetState().IsKeyDown(Keys.D7))
                {
                    if (lastKeyboardState.IsKeyUp(Keys.D7) == true)
                    {
                        GameSession.players[0].baseOffset.X -= 1;
                        GameSession.players[0].baseOffset.Z -= 1;
                    }
                }
                if (Keyboard.GetState().IsKeyDown(Keys.D3))
                {
                    if (lastKeyboardState.IsKeyUp(Keys.D3) == true)
                    { }
                }
                if (Keyboard.GetState().IsKeyDown(Keys.D4))
                {
                    if (lastKeyboardState.IsKeyUp(Keys.D4) == true)
                    { }
                }
                if (Keyboard.GetState().IsKeyDown(Keys.G))
                {
                    if (lastKeyboardState.IsKeyUp(Keys.G) == true)
                    {
                        TestCamera.ChangeTarget(GameSession.currentlySelectedTile.getPositon());

                    }
                }
                if (Keyboard.GetState().IsKeyDown(Keys.H))
                {
                    if (lastKeyboardState.IsKeyUp(Keys.H) == true)
                    {
                        gameSession.currentUnit.moving = true;
                        //GameSession.gameAudioEngine.playAudio("WinMusic");
                    }
                }
                if (Keyboard.GetState().IsKeyDown(Keys.J))
                {
                    if (lastKeyboardState.IsKeyUp(Keys.J) == true)
                    {
                        gameSession.leaveMap = true;
                        //gameSession.uiNumbers.Add(new UINumbers(2
                        //    , "HI MOM", Color.Red, players[0].GetDrawLocation(ScreenManager.GraphicsDevice.Viewport, TestCamera)));
                        //players[0].Move();
                        // players[0].posOffset += new Vector3(0, 0, 1) * 0.5f;
                        //flowStart = true;
                    }
                }
                if (Keyboard.GetState().IsKeyDown(Keys.K))
                {
                    GameSession.map.playerList[0].PrintEquips();

                    System.Diagnostics.Debug.WriteLine("Equipped");
                    if (lastKeyboardState.IsKeyUp(Keys.K) == true)
                    {
                        GameSession._input.EndTurn(gameSession);
                    }
                }
                if (Keyboard.GetState().IsKeyDown(Keys.P))
                {
                    if (lastKeyboardState.IsKeyUp(Keys.P) == true)
                    {
                        //GameSession.map.playerList[0].PrintEquips();
                        ScreenManager.notifier.StartNotify();
                    }
                }

                if (map != null)
                    map.updateActor(mousecontroller);
                mousecontroller.Update(ScreenManager.GraphicsDevice.Viewport, TestCamera);


                if (Keyboard.GetState().IsKeyDown(Keys.Z))
                {
                    if (lastKeyboardState.IsKeyUp(Keys.Z) == true)
                    {
                        GameSession.AbilityList.UseAbility(GameSession.players[0], GameSession.map.enemList[0]);
                    }
                }
                if (Keyboard.GetState().IsKeyDown(Keys.X))
                {
                    if (lastKeyboardState.IsKeyUp(Keys.X) == true)
                    {
                        GameSession.GS.ActivateEvent();
                    }
                }

                if (Keyboard.GetState().IsKeyDown(Keys.C))
                {
                    if (lastKeyboardState.IsKeyUp(Keys.C) == true)
                    {
                        GameSession.GS.CardCondition--;
                        if (GameSession.GS.CardCondition == 0)
                        {
                            GameSession.GS.CurrentCard.SpawnEnem(GameSession.GS.CurrentCard.Severity / 2.0f, GameSession.GS.CurrentCard.SpawnZone);
                        }
                    }
                }

                if (Keyboard.GetState().IsKeyDown(Keys.F1))
                {
                    if (lastKeyboardState.IsKeyUp(Keys.F1) == true)
                    {
                       //List<MapCell> combatZone;
                       //if (GameSession.map.isInZone(gameSession.currentUnit.MyLocation, out combatZone))
                       // {
                       //     GameSession.currentGameState = GameSession.GameState.Combat;
                       //     GameSession.combatZone = combatZone;
                       // }

                        // System.Diagnostics.Debug.WriteLine(GameSession.currentlySelectedTile.Ent + "");
                    }
                }

                if (Keyboard.GetState().IsKeyDown(Keys.F2))
                {
                    if (lastKeyboardState.IsKeyUp(Keys.F2) == true)
                    {
                        GameSession.currentGameState = GameSession.GameState.Dungeon;
                    }
                }
                if (Keyboard.GetState().IsKeyDown(Keys.F4))
                {

                    if (lastKeyboardState.IsKeyUp(Keys.F4) == true)
                    {
                        lootPopUp.isActive = !lootPopUp.isActive;

                    }
                }
                if (Keyboard.GetState().IsKeyDown(Keys.Tab))
                {
                    if (lastKeyboardState.IsKeyUp(Keys.Tab) == true)
                    {
                        ScreenManager.AddScreen(new QuestScreen(), null);
                    }
                }


                GameSession.gameAudioEngine.Update(gameTime);
                lootPopUp.Update();
                // Reset prevMouseState
                prevMouseState = Mouse.GetState();
                lastKeyboardState = Keyboard.GetState();
                TestCamera.CreateLookAt();
            }
            //FireParticle.Update(ScreenManager.GraphicsDevice,gameTime,1);
            if (!prerendered)
            {
                map.prerender(spriteBatch, TestCamera, ScreenManager.GraphicsDevice);
                prerendered = true;
            }
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Draw(GameTime gameTime)
        {
            ScreenManager.GraphicsDevice.Clear(Color.Black);
            ScreenManager.GraphicsDevice.BlendState = BlendState.AlphaBlend;
            ScreenManager.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            frameRate = (float)(1 / gameTime.ElapsedGameTime.TotalSeconds);
            //DrawModel(Tile, Matrix.Identity, TestCamera);
            if(prerendered)
            map.Draw(Tile,spriteBatch, TestCamera, ScreenManager.GraphicsDevice, spritefont);
            spriteBatch.Begin();

            lootPopUp.Draw(spriteBatch, gameSession.InconsolataFont);

            // foreach (MapCell mc in map.gridNodes)
            
            //spriteBatch.DrawString(spritefont,mc.Location.X + "," +  mc.Location.Y, mc.getUnprojectedPositon(ScreenManager.GraphicsDevice.Viewport,TestCamera), Color.White);
            
            //  DrawModel(MapStreets, TestCamera.world, TestCamera);

            //foreach (Player player in players) {
            //    if (player.IsAlive)
            //    {
            //        Matrix world = Matrix.CreateScale(0.05f) * Matrix.CreateTranslation(player.MyLocation.getPositon() + player.posOffset);
            //        // DrawModel(Cube, world, TestCamera);
            //        player.Draw(spriteBatch, ScreenManager.GraphicsDevice.Viewport, TestCamera);
            //        player.UpdateLocation(ScreenManager.GraphicsDevice.Viewport, TestCamera);
            //    }

            //}
            //foreach (Enemy enemy in enemies) {
            //    Matrix world = Matrix.CreateScale(0.05f) * Matrix.CreateTranslation(enemy.MyLocation.getPositon()+enemy.posOffset);
            // //   DrawModel(Cylinder, world, TestCamera);

            //}

            gameSession.currentUnit.DrawPointer(spriteBatch);
            gameSession.Draw(spriteBatch);
            base.Draw(gameTime);
#if DEBUG

            //spriteBatch.DrawString(spritefont, "CamTarget: " + TestCamera.target + "PosD: " + TestCamera.RotatedPosDiff + Math.Abs((GameSession.map.gridNodes[0, 0].getPositon().X - GameSession.map.gridNodes[1, 1].getPositon().X) / 2), new Vector2(70, 50), Color.White);
            if (GameSession.currentlySelectedTile != null)
               spriteBatch.DrawString(spritefont, "gridnodePos " + GameSession.currentlySelectedTile.Location, new Vector2(70, 70), Color.White);

            spriteBatch.DrawString(spritefont, "CamPos " + TestCamera.VCameraPosition+ ", Dir" + TestCamera.VCameraDirection + "Tar:" + TestCamera.target, new Vector2(70, 50), Color.White);
            //spriteBatch.DrawString(spritefont, "Unit Currently Acting : " + gameSession.currentUnit.MyType + ", " + gameSession.currentUnit._NAME, new Vector2(70, 50), Color.White);
            //spriteBatch.DrawString(spritefont, "AP Remaining: : " + gameSession.currentUnit.MyStatus.CurrentAP + ", " + gameSession.currentUnit._NAME, new Vector2(70, 70), Color.White);
            ////spriteBatch.DrawString(spritefont, "PRESS J TO START GAMEFLOW(START TURNS SYSTEM)", new Vector2(70, 90), Color.White);

            //spriteBatch.DrawString(spritefont, "PRESS K TO END TURN, " + frameRate, new Vector2(70, 110), Color.White);
             spriteBatch.DrawString(spritefont, "Framerate:" + frameRate, new Vector2(70, 110), Color.White);
            //spriteBatch.DrawString(spritefont, "Framerate:" + frameRate, new Vector2(70, 110), Color.White);

             if (GameSession.currentlySelectedTile != null && GameSession.currentlySelectedTile.Ent != null)
                spriteBatch.DrawString(spritefont, "\nPOINTING TO: " + GameSession.currentlySelectedTile.Ent.MyType, new Vector2(70, 110), Color.White);

             spriteBatch.DrawString(spritefont, "\n\n" + GameSession.players[0].MyStatus.calcMeleeAttack() + " atk, range: " + GameSession.players[0].MyStatus.calcRangedAttack(), new Vector2(70, 110), Color.White);
             if (GameSession.currentlySelectedTile != null)
                 spriteBatch.DrawString(spritefont, "\nLocation: " + GameSession.currentlySelectedTile.Location, new Vector2(70, 140), Color.White);


             spriteBatch.DrawString(spritefont, "\nLeast: " + least, new Vector2(70, 160), Color.White);

            //spriteBatch.Draw(swordPointer, new Vector2(gameSession.currentUnit.getPosition().X -( swordPointer.Width / 2), gameSession.currentUnit.getPosition().Y - (swordPointer.Height / 2)), Color.White);
            //FireParticle.Draw(spriteBatch,TestCamera,graphics,ScreenManager.GraphicsDevice);
            //spriteBatch.DrawString(spritefont, "Translation : " + TestCamera.world.Translation, new Vector2(70, 70), Color.White);
            //spriteBatch.DrawString(spritefont, "Position : " + TestCamera.VCameraPosition, new Vector2(70, 90), Color.White);
            //spriteBatch.DrawString(spritefont, "Direction : " + TestCamera.VCameraDirection, new Vector2(70, 110), Color.White);

            QuestManager.debugQuest(spriteBatch, spritefont);
            spriteBatch.DrawString(spritefont, GameSession.GS.counterr.ToString(), new Vector2(200, 200), Color.White);

            // TODO: Add your drawing code here

            

            spriteBatch.DrawString(spritefont, "FPS " + frameRate, new Vector2(70, 110), Color.White);
           // spriteBatch.DrawString(spritefont, "Items" + gameSession.inventory.activeList.Count, new Vector2(100, 310), Color.White);
           
#endif
            spriteBatch.End();

            effect.Parameters["HealthRatio"].SetValue(1-(gameSession.currentUnit.health/(gameSession.currentUnit.MyStatus.Endurance*3)));

            spriteBatch.Begin(0, BlendState.AlphaBlend, null, null, null, effect); //everything between this and End uses the shader. Do not draw anything here
            //spriteBatch.Draw(gradient,new Vector2(300,50),Color.White);
            if (GameSession.currentGameState == BrokenLands.GameSession.GameState.Combat)
            {
                foreach (UIBackText back in gameSession.uibackTexts)
                {
                    back.Draw(spriteBatch, spritefont);
                }
                

            }
            spriteBatch.End();


            spriteBatch.Begin();
            if (GameSession.GS.CurrentCard != null)
            {
                GameSession.GS.CurrentCard.Draw(spriteBatch, GameSession.GS.InconsolataFont, GameSession.GS.MouseRect);
            }
            spriteBatch.End();

        }

        private void DrawModel(Model model, Matrix world, CameraObj camera)
        {//16.0f
            world = Matrix.Identity * Matrix.CreateTranslation(centre) * Matrix.CreateScale(16);
            Matrix[] transforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(transforms);

            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.EnableDefaultLighting();
                    effect.Texture = map.floorTexture;
                    effect.DiffuseColor = new Vector3(0.7f, 0.7f, 0.7f);
                    effect.World = transforms[mesh.ParentBone.Index] * world;
                    effect.View = camera.view;
                    effect.Projection = camera.proj;
                }
                mesh.Draw();
            }
        }

    }
}
