﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
using BrokenLands;

namespace BrokenLands
{
    class Actor : Entity
    {
        int cycle = 0;
        public DialogTree dialogtree = new DialogTree();
        public bool metBefore = false;
        public int questID;
        public Entity loadObject;
        public string name;
        public bool chat = false;
        public string fileName = null;

        public Actor(string name, int questID, Entity loadObject, Texture2D texture, MapCell location, int lineNo)
            : base(texture, location.Location, lineNo)
        {
            this.name = name;
            this.questID = questID;
            this.loadObject = loadObject;
            this.MyLocation = location;
            this.position = location.Location;
            myType = uType.Actor;
            System.Diagnostics.Debug.WriteLine("Actor spawned");
        }

        public bool getChat() {
            return chat;
        }

        public void printDialog()
        {
            foreach (Dialog d in dialogtree.dialogList)
            {
                System.Diagnostics.Debug.WriteLine(d.message);
            }
        }

        public void toggleChat()
        {
            chat = !chat;
            if (chat)
            {
                GameSession.dialogueActive = true;
            }
            else { GameSession.dialogueActive = false; } 
            dialogtree.getDefaultGroup(); // set the default group
        }


        //replace with UI input
        public void UpdateChat(MouseController controller)
        {
            if(chat){

            dialogtree.fetchConversattion(); // loads active list and display

            DialougeOverlay.setDialog(name,dialogtree.displayMessage,dialogtree.activeList);

            //detect input for each action
            int nextgroup = dialogtree.currentGroup;
            Action action = new Action(0, 0);

            foreach (Dialog d in dialogtree.activeList)
            {
                //if within bounds and click, set the action and set the next group
                if (controller.Selected(d.box))
                {
                    d.opacity = 1.0f;
                    if (controller.LeftMouseClick() && cycle > 0)
                    {
                        action = d.getAction();
                        nextgroup = d.getNextGroup();
                    }
                }
                else {
                    d.opacity = 0.5f;
                }
            }

            cycle++;

            if (action.action >= 0)
            {
                dialogtree.currentGroup = nextgroup;
                QuestReactor.reaction(action, questID);
            }
            else
            {
                GameSession.dialogueActive = false;
                chat = false;
                metBefore = true;
                cycle = 0;

            }
        }

        }

        public override void Update(GameTime gameTime)
        {
            if (chat)
            {
                DialougeOverlay.Update();
            }
            base.Update(gameTime);
        }


        //replace with UI version
        public void DrawChat(SpriteBatch batch, SpriteFont font)
        {
            if (chat)
            {

                DialougeOverlay.Draw(batch);

                //--------------------------------------------
                //Vector2 start = new Vector2(50, 400);
                //float offsety = 20;
                //batch.DrawString(font, name + ":" + dialogtree.displayMessage, start, Color.White);
                //start.Y += offsety;

                //foreach (Dialog d in dialogtree.activeList)
                //{
                //    Vector2 heightWidth = font.MeasureString(d.message);
                //    start.Y += offsety + heightWidth.Y;
                //    //d.setBounds(heightWidth, start);
                //    //batch.DrawString(font, d.message, start, Color.White);
                //}
            }
        }

        public void UpdateLocation(Viewport viewport, CameraObj camera)
        {
            //test the world
            Vector3 projected = viewport.Project(MyLocation.getPositon(), camera.proj, camera.view, Matrix.Identity);
            position.X = projected.X;
            position.Y = projected.Y;
        }

    }
}
