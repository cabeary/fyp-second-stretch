﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BrokenLands;
using Microsoft.Xna.Framework;

namespace BrokenLands
{
    class ConditionObject
    {
        //condition is int for branching conditions
        //interobjects will act as trigger. can be programmed to do diff stuff depending on condition no


        //out1 out2 loads new conditions  with their own triggers etc

        public int condition = 0;
        public string description = "";
        Vector2 outcome1=Vector2.Zero, outcome2=Vector2.Zero;  //x: outcome type y:out come No

        //0 :none
        //1 : load next condition
        //2 : unlock dialogue
        //3 : -> reward. Terminate quests

        public ConditionObject(string des, Vector2 outcome1, Vector2 outcome2)
        {
            description = des;
        }

        public Vector2 nextStep()
        {
            if(condition ==1){
                return outcome1;
            }
            else if(condition >1){
                return outcome2;
            }

            return Vector2.Zero;
        }

        

       
    }
}
