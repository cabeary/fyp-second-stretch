﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace BrokenLands
{
    class FloorObject
    {
        public int lineNo = 1; //default lineNo
        Texture2D texture; //default texture

        public FloorObject(Texture2D texture, int lineNo) {
            this.texture = texture;
            this.lineNo = lineNo;
        }
        
    }
}
