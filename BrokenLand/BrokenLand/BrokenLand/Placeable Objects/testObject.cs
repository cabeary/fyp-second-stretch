﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace BrokenLands
{
    class testObject
    {
        Model model;
        Texture2D texture;
        Vector3 position;
        int rotation;
        public int lineNo=0;

        public int strength, dexterity, intelligence, charisma, endurance,agility;

        public testObject(Model model,Vector3 position,int lineNo) {
            this.model = model;
            this.position = position;
            this.lineNo = lineNo;

        }

        public Vector3 getPosition() {
            return position;
        }

        public void setStats(int strength,int dexterity,int intelligence,int charisma,int endurance,int agility) {
            this.strength = strength;
            this.dexterity = dexterity;
            this.intelligence = intelligence;
            this.charisma = charisma;
            this.endurance = endurance;
            this.agility = agility;

            //System.Diagnostics.Debug.WriteLine(strength.ToString());
            //System.Diagnostics.Debug.WriteLine(endurance.ToString());

        }

        public void Draw(CameraObj camera) {
            Matrix world = Matrix.CreateScale(0.05f) * Matrix.CreateTranslation(position);

            DrawModel(model,world,camera);
        }

        private void DrawModel(Model model, Matrix world, CameraObj camera)
        {
            Matrix[] transforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(transforms);

            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.EnableDefaultLighting();
                    effect.DiffuseColor = new Vector3(0.7f, 0.7f, 0.7f);
                    effect.World = transforms[mesh.ParentBone.Index] * world;
                    // Use the matrices provided by the chase camera
                    effect.View = camera.view;
                    effect.Projection = camera.proj;
                }
                mesh.Draw();
            }
        }




    }
}
