﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace BrokenLands
{
    class Camp
    {
        CampType thisType = CampType.Basic;
        public int capacity = 4;
        Dictionary<string, int> resourcesCost = new Dictionary<string,int>();
        public int defence = 0;
        public int grade = 0;
        public WorldMapNode location;
        public float exhaustionRecovery = 5;
        public List<Player> playersInCamp;
        public static void BuildCamp(CampType type, WorldMapNode loc, List<Resource> resourceList) {
            foreach (Resource r in resourceList) { 
                
            }
            Camp a = new Camp(CampType.Basic, new WorldMapNode());
        }
        private Camp(CampType type, WorldMapNode loc) {
            thisType = type;
            switch (type) { 
                case CampType.Basic:
                    capacity = 2;
                    resourcesCost.Add("Wood", 5);
                    break;
                case CampType.Attack:
                    capacity = 3;
                    resourcesCost.Add("Wood", 5);
                    resourcesCost.Add("Metal", 5);
                    break;
                case CampType.Defence:
                    capacity = 4;
                    resourcesCost.Add("Wood", 5);
                    resourcesCost.Add("Stone", 5);
                    resourcesCost.Add("Metal", 5);
                    break;
                case CampType.Resource:
                    capacity = 4;
                    resourcesCost.Add("Wood", 25);
                    resourcesCost.Add("Metal", 5);
                    break;
                case CampType.Resting:
                    capacity = 4;
                    resourcesCost.Add("Wood", 25);
                    resourcesCost.Add("Fabric", 5);
                    resourcesCost.Add("Stone", 5);
                    exhaustionRecovery = 10;
                    break;
                default: capacity = 2;
                    break;
            }
            playersInCamp = new List<Player>(capacity);

        }

        public void AddPlayer(Player p) {
            playersInCamp.Add(p);
        }

        public enum CampType { 
            Basic,
            Attack,
            Resource,
            Defence,
            Resting
        }
    }
}
