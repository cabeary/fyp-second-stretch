﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System.IO;
using GameStateManagement;

namespace BrokenLands
{
    static class QuestManager
    {
        public static List<QuestObject> questList = new List<QuestObject>();
        public static List<QuestObject> activeList = new List<QuestObject>();


        public static void debugQuest(SpriteBatch batch, SpriteFont font)
        {
            //List<string> queststringList = new List<string>();
            //queststringList.Add("Quests:");
            //foreach (QuestObject q in activeList)
            //{

            //    if (!q.complete)
            //        queststringList.Add(q.title);
            //    else
            //        queststringList.Add(q.title + " Completed");

            //    foreach (Condition c in q.conditionList)
            //    {
            //        if (c.condition > 0)
            //        {
            //            queststringList.Add(c.name + " Done(" + c.condition.ToString() + ")");
            //        }
            //        else
            //            queststringList.Add(c.name);
            //    }

            //}

            //Vector2 startpos = new Vector2(50, 180);
            //float spacing = 20.0f;

            //foreach (string s in queststringList)
            //{
            //    batch.DrawString(font, s, startpos, Color.White);
            //    startpos.Y += spacing;
            //}
        }

        public static void loadQuests()
        {
            //load all inactive quests first. Assume there are no active quests

            //open the file with the inactive quest log
            using (StreamReader reader = new StreamReader(ObjectLoader.path + "/objectdata/QuestLog/quest.list"))
            {
                int questCount = int.Parse(reader.ReadLine());

                for (int i = 0; i < questCount; i++)
                {
                    bool isActive = true;
                    string questname = reader.ReadLine();
                    //Checks if active or not
                    //string isActive = reader.ReadLine();
                    int questID = int.Parse(reader.ReadLine());

                    QuestObject quest = new QuestObject(questname, questID);
                    
                    using (StreamReader questreader = new StreamReader(ObjectLoader.path + "/objectdata/Quest/" + questname + ".quest"))
                    {
                        //load quest desc
                        string desc = questreader.ReadLine();
                        quest.title = desc;
                        quest.location = questname;

                        //fetch condition count
                        int conditionCount = int.Parse(questreader.ReadLine());

                        //loop condition
                        for (int j = 0; j < conditionCount; j++)
                        {
                            string conditionName = questreader.ReadLine();
                            //conditionID will be assigned by list index
                            Condition condition = new Condition(conditionName);
                            //add condition to quest                            
                            quest.inactiveConditionList.Add(condition);
                        }

                        int rewardCount = int.Parse(questreader.ReadLine());

                        for (int z = 0; z < rewardCount; z++)
                        {
                            //read the reward
                            int type = int.Parse(questreader.ReadLine());
                            int value = int.Parse(questreader.ReadLine());
                            quest.rewardactionList.Add(new RewardAction(type, value));

                        }

                        using (StreamReader conditionreader = new StreamReader(ObjectLoader.path + "/objectdata/Quest/" + questname + ".condition"))
                        {
                            bool ReadConditions = false;

                            while (!ReadConditions)
                            {
                                if (reader.EndOfStream)
                                {
                                    System.Diagnostics.Debug.WriteLine("Unable to find quest");
                                    break;
                                }

                                String QuestTitle = conditionreader.ReadLine();
                                int cCount = int.Parse(conditionreader.ReadLine());
                                int activeCCount = int.Parse(conditionreader.ReadLine());

                                if (activeCCount == 0)
                                {
                                    isActive = false;
                                }

                                if (QuestTitle == quest.title)
                                {
                                    for (int conditions = 0; conditions < cCount; conditions++)
                                    {
                                        string ConditionName = conditionreader.ReadLine();
                                        int ConditionValue = int.Parse(conditionreader.ReadLine());
                                        foreach (Condition C in quest.inactiveConditionList)
                                        {
                                            if (C.name == ConditionName)
                                            {
                                                C.condition = ConditionValue;
                                                //If current condition is active
                                                if (activeCCount > conditions)
                                                {
                                                    quest.conditionList.Add(C);
                                                    quest.inactiveConditionList.Remove(C);
                                                }
                                                break;
                                            }
                                        }
                                    }
                                    ReadConditions = true;
                                }
                                else
                                {
                                    //Skip activeCCount
                                    reader.ReadLine();
                                    //Skips conditions until next quest
                                    for (int skipCount = 0; skipCount < cCount; skipCount++)
                                    {
                                        reader.ReadLine();
                                        reader.ReadLine();
                                    }
                                }
                            }
                        }
                    }

                    //If conditionList are all fill -> complete, inactiveConditionList are all filled -> not started
                    if (quest.inactiveConditionList.Count == 0 || quest.conditionList.Count == 0)
                    {
                        isActive = false;
                    }
                    
                    quest.matchIndexID();
                    quest.addCondition(0);

                    if (isActive)
                    {
                        activeList.Add(quest);
                    }
                    else
                    {
                        questList.Add(quest);
                    }
                    
                }
            }
        }

        public static void addQuest(int questNo)
        {
            int no = -1;
            for (int i = 0; i < questList.Count; i++)
            {
                if (questList[i].lineNo == questNo)
                {
                    no = i;
                    break;
                }
            }

            if (no >= 0)
            {
                //adds are quest to the active list
                activeList.Add(questList[no]);
                GameSession.GS.game1.ScreenManager.notifier.setNewText("New Quest: " + questList[no].title, questList[no].desc);
                GameSession.GS.game1.ScreenManager.notifier.StartNotify();
                questList.RemoveAt(no);
            }

        }

        public static void setCondition(int questNo, int ConditionNo, int value)
        {

            if (questNo >= 0)
            {

                foreach (QuestObject q in activeList)
                {
                    if (q.lineNo == questNo)
                    {
                        q.setCondition(ConditionNo, value);
                        break;
                    }
                }
            }
        }

        public static void incrementCondition(int questNo, int ConditionNo)
        {
            if (questNo >= 0)
            {

                foreach (QuestObject q in activeList)
                {
                    if (q.lineNo == questNo)
                    {
                        q.incrementCondition(ConditionNo);
                        break;
                    }
                }
            }
        }

        public static void decrementCondition(int questNo, int ConditionNo)
        {
            if (questNo >= 0)
            {

                foreach (QuestObject q in activeList)
                {
                    if (q.lineNo == questNo)
                    {
                        q.decrementCondition(ConditionNo);
                        break;
                    }
                }
            }
        }

        public static void isComplete()
        {
            foreach (QuestObject q in activeList)
            {
                int count = 0;
                foreach (Condition c in q.conditionList)
                {
                    if (c.condition > 0)
                        count++;

                }

                if (count == q.conditionList.Count)
                    q.questComplete();
            }
        }

        public static void addCondition(int questNo, int conditionNo)
        {

            if (questNo >= 0)
            {

                foreach (QuestObject q in activeList)
                {
                    if (q.lineNo == questNo)
                    {
                        q.addCondition(conditionNo);

                        String ConditionListStr = "";
                        for (int i = 0; i < q.conditionList.Count; i++)
                        {
                            ConditionListStr += (q.conditionList[i].name + "\n");  
                        }
                        GameSession.GS.game1.ScreenManager.notifier.setNewText(q.title, ConditionListStr);
                        GameSession.GS.game1.ScreenManager.notifier.StartNotify();
                        break;
                    }
                }
            }
        }


    }
}
