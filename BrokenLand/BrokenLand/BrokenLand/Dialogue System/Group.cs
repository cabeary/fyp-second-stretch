﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrokenLands
{
    public class Group
    {
        public Lock locK;
        public int group;

        public Group(Lock locK, int group)
        {
            this.locK = locK;
            this.group = group;
        }

        public bool getLock()
        {
            return locK.getMet();
        }

        public int getGroup()
        {
            return group;
        }


        public void requirementCheck()
        {
            Player player = GameSession.players[0];
            switch (locK.type)
            {
                case 0: //cases for stat check
                    switch (locK.value)
                    {
                        case 0:// str
                            if (player.MyStatus.Strength >= locK.requirement)
                                locK.toggleMet();
                            break;
                        case 1://dex
                            if (player.MyStatus.Dexterity >= locK.requirement)
                                locK.toggleMet();
                            break;
                        case 2://int
                            if (player.MyStatus.Intelligence >= locK.requirement)
                                locK.toggleMet();
                            break;
                        case 3://cha
                            if (player.MyStatus.Charisma >= locK.requirement)
                                locK.toggleMet();
                            break;
                        case 4://end
                            if (player.MyStatus.Endurance >= locK.requirement)
                                locK.toggleMet();
                            break;
                        case 5://agi
                            if (player.MyStatus.Agility >= locK.requirement)
                                locK.toggleMet();
                            break;
                    }
                    break;
                case 1://cases for skill check
                    switch (locK.value)
                    {

                        case 0://melee
                            if (player.MyStatus.Skills.melee > locK.requirement)
                                locK.toggleMet();
                            break;
                        case 1://scavenge
                            if (player.MyStatus.Skills.scavenge > locK.requirement)
                                locK.toggleMet();
                            break;
                        case 2://lockpick
                            if (player.MyStatus.Skills.lockpick > locK.requirement)
                                locK.toggleMet();
                            break;
                        case 3://barter
                            if (player.MyStatus.Skills.barter > locK.requirement)
                                locK.toggleMet();
                            break;
                        case 4://heal
                            if (player.MyStatus.Skills.heal > locK.requirement)
                                locK.toggleMet();
                            break;
                    }
                    break;
                case 2://cases for condition check
                    bool found = false;
                    foreach (QuestObject q in QuestManager.activeList)
                    {

                        foreach (Condition c in q.conditionList)
                        {
                            if (locK.value == c.conditionID) //need to fetch a conditionid
                            {
                                found = true;
                                if (locK.requirement == c.condition)
                                {
                                    locK.toggleMet();
                                    break;
                                }
                            }

                            if (found)
                                break;
                        }

                    }
                    break;
                case 3://case check for quest
                    foreach (QuestObject q in QuestManager.activeList)
                    {
                        if (locK.value == q.lineNo)
                        { //checks for q id

                            if (locK.requirement == (Convert.ToInt32(q.complete)))
                            {
                                locK.toggleMet();
                            }


                            break;
                        }
                    }
                    break;
                case 4: //check item
                    Inventory inventory = GameSession.GS.inventory;
                    int count = 0;
                    foreach (Item i in inventory.activeList)
                    {
                        if (i.id == locK.value)
                        {
                            count++;
                        }

                        if (count > 0)
                            locK.toggleMet();
                    }
                    break;

                case 5: // check met before
                    List<Actor> actorList = GameSession.map.actorList;

                    foreach (Actor a in actorList)
                    {
                        if (a.getChat())
                        {
                            if (a.metBefore)
                            {
                                locK.toggleMet();
                                break;
                            }
                        }
                    }

                    break;

                case 7:
                     found = false;
                    foreach (QuestObject q in QuestManager.activeList)
                    {

                        foreach (Condition c in q.inactiveConditionList)
                        {
                            if (locK.value == c.conditionID) //need to fetch a conditionid
                            {
                                found = true;
                                //if (locK.requirement == c.condition)
                                //{
                                    locK.toggleMet();
                                    break;
                                //}
                            }

                            if (found)
                                break;
                        }

                    }
                    break;

                default: //do nothing
                    break;
            }
        }
    }
}

