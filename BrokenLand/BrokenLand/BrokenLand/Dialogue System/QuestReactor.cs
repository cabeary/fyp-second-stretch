﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace BrokenLands
{
    static class QuestReactor
    {
        static GameSession mysession;
        public static Dictionary<int, string> Locations = new Dictionary<int,string>();
        public static CameraObj camera;
        public static List<Vector2> camerapositons = new List<Vector2>();
        //takes in an action and creates the appropriate response
        //action, outcome, value,condition
        //quest complete should be an action


        //call prerender again whenever an action that changes the scene occurs
        //aka lockpickingm fight etc

        public static void Init() {
            Locations.Add(0,"Norkansa_Station");
            Locations.Add(1,"Barrens_Town");
            Locations.Add(2, "Barrens_Town2");
            Locations.Add(3,"Warehouse");
            Locations.Add(4, "Warehouse2");
            Locations.Add(5, "Barrens_Town3");


            //camera positons
            camerapositons.Add(new Vector2(36,19));
            camerapositons.Add(new Vector2(30, 41));
            camerapositons.Add(new Vector2(57, 7));


            //add a list of vec2's for camera position. Rehauling actions will take more time than I am willing to put in
        }

        public static void setSession(GameSession session) {
            mysession = session;
        }

        private static void parseAction(string actionValue,string former, string latter) {
            int length = actionValue.Length;
            bool readFormer = false;
            for (int i = 0; i < length;i++ ) {

                if (readFormer)
                {
                    if (actionValue[i].CompareTo('#') == 0)
                    {
                        readFormer = false;
                    }
                    else
                    {
                        former += actionValue[i];
                    }
                }

                else {
                    latter += actionValue[i];
                }
            }
        }
 
        public static void reaction(Action action, int questID)
        {

            switch (action.action)
            {
                case 1: //fight -> load enemy at the same place
                    QuestManager.setCondition(questID, action.value, 1);
                    List<Actor> actorList = GameSession.map.actorList;

                    System.Diagnostics.Debug.WriteLine("Before Reactor actorList:"+actorList.Count);
                    System.Diagnostics.Debug.WriteLine("Before Map actorList:" + GameSession.map.actorList.Count);

                    for (int i = 0; i < actorList.Count;i++ ) {
                        if(actorList[i].getChat()){

                            //add an enemy of similar ai type and similar equipment
                            Enemy enemy = (Enemy)actorList[i].loadObject;
                            actorList[i].Die();
                            mysession.addEnemy(enemy,actorList[i].MyLocation);
                            actorList[i].draw = false;
                            actorList.RemoveAt(i);
                            GameSession.dialogueActive = false;
                            break;
                        }
                    }
                    System.Diagnostics.Debug.WriteLine("Reactor actorList:"+actorList.Count);
                    System.Diagnostics.Debug.WriteLine("Map actorList:" + GameSession.map.actorList.Count);
                    //remove actor and shit

                    actorList = null;
                    QuestManager.isComplete();

                    GameSession.map.rerender = true;
                    break;
                case 2: //bribe
                    //deduct funds
                    mysession.inventory.money -= action.value;
                    QuestManager.setCondition(questID, action.value, 2);
                    QuestManager.isComplete();
                    break;
                case 3: //melee
                    QuestManager.setCondition(questID, action.value, 3);
                    QuestManager.isComplete();
                    break;
                case 4: // scavenge
                    QuestManager.setCondition(questID, action.value, 4);
                    QuestManager.isComplete();
                    break;

                case 5: //lockpick
                    QuestManager.setCondition(questID, action.value, 5);

                     List<Actor> actorList2 = GameSession.map.actorList;

                    for (int i = 0; i < actorList2.Count;i++ ) {
                        if(actorList2[i].getChat()){
                            //door doesn't exist in the overlay list
                            //add an enemy of similar ai type and similar equipment
                            SceneObject overlay = (SceneObject)actorList2[i].loadObject;
                            MapCell cell = actorList2[i].MyLocation;
                            overlay.MyLocation = cell;
                            overlay.MyType = Entity.uType.Catch;
                            GameSession.map.overlayList.Add(overlay);
                            actorList2[i].draw = false;
                            actorList2[i].Die();
                            actorList2.RemoveAt(i);
                            cell.Ent = null;
                            actorList2 = null;
                            GameSession.dialogueActive = false;
                            break;
                        }
                    }

                    QuestManager.isComplete();
                    GameSession.map.rerender = true;
                    break;

                case 6://barter
                    QuestManager.setCondition(questID, action.value, 6);
                    QuestManager.isComplete();
                    break;
                case 7://heal
                    QuestManager.setCondition(questID, action.value, 7);
                    QuestManager.isComplete();
                    break;

                case 8: //help 
                    //add next condition
                    QuestManager.addCondition(questID, action.value);
                    break;
                case 9:// add quest
                    QuestManager.addQuest(questID);
                    //System.Diagnostics.Debug.WriteLine("A quest has been added");
                    break;

                case 10://clear dungeon
                    QuestManager.setCondition(questID, action.value,10);
                    QuestManager.isComplete();
                    break;
                case 11:// leave the dungeon

                    mysession.leaveMap = true;
                    break;
                case 12: // add item
                    mysession.inventory.AddItem(action.value);
                    break;
                case 13: // kill actor
                    List<Actor> actorList1 = new List<Actor>();
                    actorList1 = GameSession.map.actorList;

                    for (int i = 0; i < actorList1.Count;i++ ) {
                        if(actorList1[i].getChat()){
                            actorList1[i].toggleChat();
                            actorList1[i].draw = false;
                            actorList1[i].Die();
                            GameSession.dialogueActive = false;
                            MapCell cell = actorList1[i].MyLocation;
                            actorList1.RemoveAt(i);
                            cell.Ent = null;
                        }
                    }
                    GameSession.map.rerender = true;
                    break;

                case 14: // add loot
                    QuestManager.setCondition(questID, action.value, 5);

                     List<Actor> actorList3 = GameSession.map.actorList;

                     for (int i = 0; i < actorList3.Count; i++)
                     {
                         if (actorList3[i].getChat())
                         {

                            //add an enemy of similar ai type and similar equipment
                             LootContainer underlay = (LootContainer)actorList3[i].loadObject;
                             MapCell cell = actorList3[i].MyLocation;
                            GameSession.map.sceneList.Add(underlay);
                            actorList3[i].Die();
                            actorList3[i].draw = false;
                            actorList3.RemoveAt(i);
                            cell.Ent = underlay;
                            actorList3 = null;
                            GameSession.dialogueActive = false;
                            break;
                        }
                    }

                    QuestManager.isComplete();
                    GameSession.map.rerender = true;
                    break;

                case 15: // complete condition with no action
                    QuestManager.setCondition(questID,action.value,15);
                    break;
                case 16: //remove item
                    break; //remove all item
                case 17:
                    break;
                case 18: // wild card. Sets a condition to any type
                    break;
                case 19: //togglelock a dialog option of current talker
                   actorList = GameSession.map.actorList;
                   Actor handle = null;

                    for (int i = 0; i < actorList.Count;i++ ) {
                        if(actorList[i].getChat()){
                            handle = actorList[i];
                            break;
                        }
                    }
                    if (handle != null) {
                        foreach (Dialog d in handle.dialogtree.dialogList) {
                            if (d.locK.type == 6 && d.locK.requirement== action.value) {
                                d.locK.toggleMet();
                            }

                            foreach(Group g in handle.dialogtree.defaultgroupList){

                                if (g.locK.type == 6 && d.locK.requirement == action.value)
                                    d.locK.toggleMet();
                            }
                        }
                    }
                    break;

                case 20: //all actors become hostile
                    QuestManager.setCondition(questID, action.value, 20);  //FIXME
                    actorList = GameSession.map.actorList;
                    handle = null;


                    //add enemy first
                    foreach(Actor a in actorList){
                        if (a.chat)
                        {
                            a.chat = false;
                            handle = a;
                        }

                        if(a.loadObject!=null){
                            if (a.loadObject.MyType == Entity.uType.Enemy) {
                                Enemy enemy = (Enemy)a.loadObject;
                                mysession.addEnemy(enemy, a.MyLocation);
                            }
                        }
                    }


                    //unlock door (hardcoded cos this is the only instance where this happens)
                    if(handle != null){
                    SceneObject overlay = (SceneObject)handle.loadObject;
                    MapCell cell = handle.MyLocation;
                    overlay.MyLocation = cell;
                    overlay.MyType = Entity.uType.Catch;
                    GameSession.map.overlayList.Add(overlay);
                    handle.draw = false;
                    handle.Die();
                    actorList.Remove(handle);
                    cell.Ent = null;
                    }

                    //--------------------

                    //remove all actors

                    for (int i = 0; i < actorList.Count;i++ ) {

                            //add an enemy of similar ai type and similar equipment
                        if (actorList[i].loadObject!=null)
                        {
                        if (actorList[i].loadObject.MyType == Entity.uType.Enemy)
                        {
                            actorList[i].draw = false;
                            actorList[i].Die();
                            GameSession.dialogueActive = false;
                            actorList.RemoveAt(i);
                        }
                        }
                    }
                    GameSession.map.rerender = true;
                    GameSession.dialogueActive = false;
                    break;

                case 21: //add condition and quit
                    QuestManager.addCondition(questID, action.value);
                    actorList = GameSession.map.actorList;
                    handle = null;

                    for (int i = 0; i < actorList.Count;i++ ) {
                        if(actorList[i].getChat()){
                            handle = actorList[i];
                            break;
                        }
                    }

                    handle.chat=false;
                    GameSession.dialogueActive = false;
                    break;
                case 22://increment condition
                    QuestManager.incrementCondition(questID, action.value);
                    break;

                case 23: //add two conditions then quit
                    QuestManager.addCondition(questID, action.value);
                    QuestManager.addCondition(questID, action.value+1);

                     actorList = GameSession.map.actorList;
                    handle = null;

                    for (int i = 0; i < actorList.Count;i++ ) {
                        if(actorList[i].getChat()){
                            handle = actorList[i];
                            break;
                        }
                    }

                    handle.chat=false;
                    GameSession.dialogueActive = false;
                    break;

                case 24://inc and quit
                    QuestManager.incrementCondition(questID, action.value);

                    actorList = GameSession.map.actorList;
                    handle = null;

                    for (int i = 0; i < actorList.Count;i++ ) {
                        if(actorList[i].getChat()){
                            handle = actorList[i];
                            break;
                        }
                    }
                    GameSession.dialogueActive = false;
                    handle.chat=false;
                    break;

                case 25: //load new map
                    mysession.loadMap = true;
                    mysession.mapName = Locations[action.value];
                    mysession.leaveMap = true;
                    break;

                case 26: //Activate shop
                    if (GameSession.map == null)
                    {
                        GameSession.map = new Map(GameSession.GS.defaultTile, GameSession.GS.defaultBattleTile);
                        GameSession.map.mapGen = new MapGenerator(GameSession.GS.game1.ScreenManager.Game, GameSession.GS.defaultModel, GameSession.map);
                    }
                    GameSession.map.mapGen.loader.loadObject(0, 14, new Vector2());
                    GameSession.GS.game1.ScreenManager.AddScreen(new ShopScreen(10, 10, GameSession.GS.shopItems.activeList), null);
                    break;

                case 27: //pan camera
                    GameSession.locktoplayer = false;
                    mysession.cameraTarget = camerapositons[action.value];
                    break;

                case 28: //lock camera to player
                    GameSession.locktoplayer = true;
                    break;

                case 29: //dec condition
                    QuestManager.decrementCondition(questID, action.value);
                    break;
                case 30: //greater or equals zero
                    break;
                case 31://less than equals
                    break;

                case 32: //fail quest
                    QuestManager.setCondition(questID, action.value, 32);
                    break;

                case 33: //Loading Ally

                    //kill actor
                    actorList1 = new List<Actor>();
                    actorList1 = GameSession.map.actorList;
                    MapCell location = null;

                    for (int i = 0; i < actorList1.Count;i++ ) {
                        if(actorList1[i].getChat()){
                            actorList1[i].toggleChat();
                            actorList1[i].draw = false;
                            actorList1[i].Die();
                            GameSession.dialogueActive = false;
                            MapCell cell = actorList1[i].MyLocation;
                            actorList1.RemoveAt(i);
                            location = cell;
                            cell.Ent = null;
                        }
                    }
                    



                    //load ally
                    if (GameSession.map == null)
                    {
                        GameSession.map = new Map(GameSession.GS.defaultTile, GameSession.GS.defaultBattleTile);
                        GameSession.map.mapGen = new MapGenerator(GameSession.GS.game1.ScreenManager.Game, GameSession.GS.defaultModel, GameSession.map);
                    }
                    GameSession.map.mapGen.loader.loadObject(GameSession.GS.NextCharacterInt, 13, new Vector2(0));
                    GameSession.GS.NextCharacterInt++;
                    GameSession.GS.game1.ScreenManager.notifier.setNewText("New Party Member", GameSession.players[GameSession.players.Count - 1]._NAME + " has joined your party");
                    GameSession.GS.game1.ScreenManager.notifier.StartNotify();


                    GameSession.map.playerList[1].MyLocation = location;

                    GameSession.map.rerender = true;
                    break;
                    
                case 34: // addquest and quit
                    QuestManager.addQuest(questID);


                    actorList = GameSession.map.actorList;
                    handle = null;

                    for (int i = 0; i < actorList.Count;i++ ) {
                        if(actorList[i].getChat()){
                            handle = actorList[i];
                            break;
                        }
                    }

                    handle.chat=false;
                    GameSession.dialogueActive = false;
                    break;

                case 35: // dmage player
                    Random ran = new Random();
                    GameSession.gameAudioEngine.playSound("Explosion");
                    GameSession.players[0].health -= ran.Next(2,7) ;

                    //kill actor
                    break;
                case 36: //add item and quit

                    mysession.inventory.AddItem(action.value);

                    handle = null;

                    actorList = GameSession.map.actorList;

                    for (int i = 0; i < actorList.Count;i++ ) {
                        if(actorList[i].getChat()){
                            handle = actorList[i];
                            break;
                        }
                    }

                    
                    handle.chat=false;
                    GameSession.dialogueActive = false;
                    break;

                default: // no action
                    break;
            }

            

            System.Diagnostics.Debug.WriteLine("Action:"+action.action);

            
        }
    }
}
