﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using GameStateManagement;
using BrokenLands;

namespace BrokenLands
{
    static class DialougeOverlay
    {
        static SpriteFont ComfortaaFont;

        static Texture2D DialougeBack, DialougeFrame, DialougeOptionTex;

        static List<DialougeOption> optionList = new List<DialougeOption>();

        static MouseController MouseC;
        static string PersonSpeakingName;
        static string SpokenText;
        static ScreenManager screen;

        public static void Init(SpriteFont font, Texture2D dialougeBack, Texture2D dialougeFrame, Texture2D dialougeOptionTex, MouseController controller, ScreenManager s)
        {
            ComfortaaFont = font;
            DialougeBack = dialougeBack;
            DialougeFrame = dialougeFrame;
            DialougeOptionTex = dialougeOptionTex;
            screen = s;
            MouseC = controller;
        }

        public static void setDialog(string speaker, string text, List<Dialog> list)
        {
            optionList.Clear();
            PersonSpeakingName = speaker;

            

            SpokenText = parseString(text);

            for (int i = 0; i < list.Count; i++)
            {
                optionList.Add(new DialougeOption(i, screen, DialougeOptionTex, list[i]));
                GameSession.GS.counterr++;
            }
        }

        public static string parseString(string message) {

            //---Parse the message for key characters
            int length = 0;
            List<int> indexList = new List<int>();
            List<int> indexList2 = new List<int>();
            List<int> indexList3 = new List<int>();

            if (message.Contains("#") || message.Contains("’") || message.Contains("‘") || message.Contains("…")) //…
            {
                length = message.Length;
            }



            for (int x = 0; x < length; x++)
            {
                char c = (char)message[x];

                if (c.CompareTo('#') == 0)
                {
                    indexList.Add(x);
                }

                if (c.CompareTo('’') == 0 || c.CompareTo('‘') == 0)
                {
                    indexList2.Add(x);
                }

                if (c.CompareTo('…') == 0 || c.CompareTo('…') == 0)
                {
                    indexList3.Add(x);
                }
            }

            foreach (int x in indexList)
            {
                message = message.Remove(x, 1);
                message = message.Insert(x, "\n");
            }

            foreach (int x in indexList2)
            {
                message = message.Remove(x, 1);
                message = message.Insert(x, "'");
            }


            foreach (int x in indexList3)
            {
                message = message.Remove(x, 1);
                message = message.Insert(x, "...");
            }
            //---End parse

            return message;
        }

        public static void Update()
        {
            //foreach (DialougeOption option in optionList)
            //{
            //    option.Update(MouseC);
            //}
        }

        public static void Draw(SpriteBatch spriteBatch)
        {
            float width = screen.GraphicsDevice.Viewport.Width;
            float scale = (width / 1920);

            spriteBatch.Draw(DialougeBack, new Rectangle(screen.GraphicsDevice.Viewport.TitleSafeArea.X, screen.GraphicsDevice.Viewport.TitleSafeArea.Y, screen.GraphicsDevice.Viewport.Width, screen.GraphicsDevice.Viewport.Height), null, Color.White * 0.9f, 0f, new Vector2(0, 0), SpriteEffects.None, 0f);
            spriteBatch.Draw(DialougeFrame, new Rectangle(screen.GraphicsDevice.Viewport.TitleSafeArea.X, screen.GraphicsDevice.Viewport.TitleSafeArea.Y, screen.GraphicsDevice.Viewport.Width, screen.GraphicsDevice.Viewport.Height), null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0f);

            foreach (DialougeOption option in optionList)
            {
                option.OptionText = parseString(option.OptionText);
                option.Draw(spriteBatch, ComfortaaFont, scale);
            }

            SpokenText = parseString(SpokenText);

            spriteBatch.DrawString(ComfortaaFont, PersonSpeakingName, new Vector2(70 * scale, (screen.GraphicsDevice.Viewport.TitleSafeArea.Height * 0.57f)), Color.White);
            spriteBatch.DrawString(ComfortaaFont,GameSession.SpliceText(SpokenText,110) , new Vector2(70 * scale, (screen.GraphicsDevice.Viewport.TitleSafeArea.Height * 0.61f)), Color.White);
        }



    }
}
