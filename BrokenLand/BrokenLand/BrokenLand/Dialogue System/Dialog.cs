﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace BrokenLands
{
    public class Dialog
    {
        //add metbefore check and item check

        //jump group should exist
        public string message;
        Action action;
        public int group; //group the message belongs to
        public Lock locK;
        public bool teller;
        int nextGroup; //the group to go to next
        public Rectangle box;

        public float opacity = 0.5f;

        public Dialog(string message, Lock lk, int group, int nextgroup, Action action, bool teller)
        {
            this.message = message;
            locK = lk;
            this.group = group;
            this.nextGroup = nextgroup;
            this.action = action;
            this.teller = teller;
        }

        public void setBounds(Vector2 heightWidth,Vector2 pos) {
            box = new Rectangle((int)pos.X, (int)pos.Y, (int)heightWidth.X, (int)heightWidth.Y);
        }

        public int getNextGroup()
        {
            return nextGroup;
        }

        public Action getAction()
        {
            return action;
        }



        public void requirementCheck()
        {
            Player player = GameSession.players[0];
            switch (locK.type)
            {
                case 0: //cases for stat check
                    switch (locK.value)
                    {
                        case 0:// str
                            if (player.MyStatus.Strength >= locK.requirement)
                                locK.toggleMet();
                            break;
                        case 1://dex
                            if (player.MyStatus.Dexterity >= locK.requirement)
                                locK.toggleMet();
                            break;
                        case 2://int
                            if (player.MyStatus.Intelligence >= locK.requirement)
                                locK.toggleMet();
                            break;
                        case 3://cha
                            if (player.MyStatus.Charisma >= locK.requirement)
                                locK.toggleMet();
                            break;
                        case 4://end
                            if (player.MyStatus.Endurance >= locK.requirement)
                                locK.toggleMet();
                            break;
                        case 5://agi
                            if (player.MyStatus.Agility >= locK.requirement)
                                locK.toggleMet();
                            break;
                    }
                    break;
                case 1://cases for skill check
                    switch (locK.value)
                    {

                        case 0://melee
                            if (player.MyStatus.Skills.melee >= locK.requirement)
                                locK.toggleMet();
                            break;
                        case 1://scavenge
                            if (player.MyStatus.Skills.scavenge >= locK.requirement)
                                locK.toggleMet();
                            break;
                        case 2://lockpick
                            if (player.MyStatus.Skills.lockpick >= locK.requirement)
                                locK.toggleMet();
                            break;
                        case 3://barter
                            if (player.MyStatus.Skills.barter >= locK.requirement)
                                locK.toggleMet();
                            break;
                        case 4://heal
                            if (player.MyStatus.Skills.heal > locK.requirement)
                                locK.toggleMet();
                            break;
                    }
                    break;
                case 2://cases for condition check
                    bool found = false;
                    foreach (QuestObject q in QuestManager.activeList)
                    {
                        //int test = 0;
                        foreach (Condition c in q.conditionList)
                        {
                            if (locK.value == c.conditionID) //need to fetch a conditionid
                            {
                                found = true;
                                if (locK.requirement == c.condition)
                                {
                                    locK.toggleMet();
                                    break;
                                }
                            }

                            if (found)
                                break;
                        }

                    }
                    break;
                case 3://case check for quest
                    foreach (QuestObject q in QuestManager.activeList)
                    {
                        if (locK.value == q.lineNo)
                        { //checks for q id

                            if (locK.requirement == (Convert.ToInt32(q.complete)))
                            {
                                locK.toggleMet();
                            }


                            break;
                        }
                    }
                    break;
                
                case 4: //check item
                    Inventory inventory = GameSession.GS.inventory;
                    int count = 0;
                    foreach(Item i in inventory.activeList){
                        if(i.id == locK.value){
                            count++;
                        }

                        if (count >= locK.requirement)
                            locK.toggleMet();
                    }
                    break;

                case 5: // check met before
                    List<Actor> actorList = GameSession.map.actorList;

                    foreach(Actor a in actorList){
                        if (a.getChat()) {
                            if (a.metBefore)
                            {
                                locK.toggleMet();
                                break;
                            }
                        }
                    }

                    break;
                case 6: //listen for unlock
                    break;

                case 7: //greater or equals to
                    found = false;
                    foreach (QuestObject q in QuestManager.activeList)
                    {
                        //int test = 0;
                        foreach (Condition c in q.conditionList)
                        {
                            if (locK.value == c.conditionID) //need to fetch a conditionid
                            {
                                found = true;
                                if (c.condition >= locK.requirement)
                                {
                                    locK.toggleMet();
                                    break;
                                }
                            }

                            if (found)
                                break;
                        }

                    }
                    break;
                case 8://lesser or equals to
                    found = false;
                    foreach (QuestObject q in QuestManager.activeList)
                    {
                        //int test = 0;
                        foreach (Condition c in q.conditionList)
                        {
                            if (locK.value == c.conditionID) //need to fetch a conditionid
                            {
                                found = true;
                                if (c.condition <= locK.requirement)
                                {
                                    locK.toggleMet();
                                    break;
                                }
                            }

                            if (found)
                                break;
                        }

                    }
                    break;

                default: //do nothing
                    break;
            }
        }
    }
}
