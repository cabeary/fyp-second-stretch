﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using GameStateManagement;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using BrokenLands;

namespace BrokenLands
{
    class DialougeOption
    {
        public string OptionText;
        Vector2 Position;
        float opacity;
        Rectangle optionRect;
        Texture2D texture;
        Dialog d = null;

        public DialougeOption(int count,ScreenManager screen,Texture2D tex,Dialog dialog)
        {
            string text = dialog.message;
            float width = screen.GraphicsDevice.Viewport.Width;
            float scale = (width / 1920);
            texture = tex;
            Position = new Vector2((int)(screen.GraphicsDevice.Viewport.TitleSafeArea.Width * 0.03f), (int)(screen.GraphicsDevice.Viewport.TitleSafeArea.Height * (0.72f+count*0.07f)));
            Position.Y -= 10;
            optionRect = new Rectangle((int)Position.X, (int)Position.Y, (int)(texture.Width * scale), (int)(texture.Height * scale));
            dialog.setBounds(new Vector2(texture.Width * scale,texture.Height * scale),Position);
            OptionText = text;

            d = dialog;
            
        }

        public void Draw(SpriteBatch spriteBatch,SpriteFont font,float scale)
        {
            opacity = d.opacity;
            spriteBatch.Draw(texture, optionRect, null, Color.White*opacity, 0f, new Vector2(0, 0), SpriteEffects.None, 0f);
            spriteBatch.DrawString(font, OptionText, Position + new Vector2(30 * scale, (texture.Height / 4) * scale), Color.White * opacity);
            //spriteBatch.DrawString(font, "’", Position + new Vector2(30 * scale, (texture.Height / 4) * scale), Color.White);
        }






    }

    
}
