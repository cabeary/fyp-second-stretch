﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BrokenLands;

namespace BrokenLands
{
    class QuestObject
    {
        public int lineNo = 0;
        public string title;
        public string desc="";
        public string location = "";
        public bool isActive = false;

        public bool complete = false;

        public List<Condition> conditionList = new List<Condition>();
        public List<Condition> inactiveConditionList = new List<Condition>();
        public List<RewardAction> rewardactionList = new List<RewardAction>();


        public QuestObject(string title,int lineNo)
        {
            this.title = title;
            this.lineNo = lineNo;
        }

        public void questComplete()
        {
            Console.WriteLine("Quest comepleted: " + title);
            complete = true;

            foreach(RewardAction r in rewardactionList){
                RewardSystem.Reward(r);
            }
        }

        public void printQuest() {
            System.Diagnostics.Debug.WriteLine(title);
            System.Diagnostics.Debug.WriteLine(desc);

            foreach(Condition c in conditionList){
                System.Diagnostics.Debug.WriteLine(c.name);
            }
        }


        public void setCondition(int conditionID, int value)
        {
            foreach (Condition c in inactiveConditionList)
            {
                if (conditionID == c.conditionID)
                    c.condition = value;
                Console.WriteLine("Method resolved:" + c.condition);
            }

            //foreach (Condition c in conditionList)
            //{
            //    if(conditionID == c.conditionID)
            //    c.condition = value;
            //    Console.WriteLine("Method resolved:" + c.condition);
            //}
        }


        public void incrementCondition(int conditionID)
        {


            foreach (Condition c in conditionList)
            {
                if (conditionID == c.conditionID)
                    c.condition ++;
            }
        }

        public void decrementCondition(int conditionID)
        {


            foreach (Condition c in conditionList)
            {
                if (conditionID == c.conditionID)
                    c.condition--;
            }
        }

        public void addCondition(int conditionNo)
        {
            for (int i = 0; i < inactiveConditionList.Count; i++)
            {
                if (inactiveConditionList[i].conditionID == conditionNo)
                {
                    conditionList.Add(inactiveConditionList[i]);
                    //inactiveConditionList.RemoveAt(i);
                    break;
                }
            }
        }

        public void matchIndexID() {
            for (int i = 0; i < inactiveConditionList.Count;i++ ) {
                inactiveConditionList[i].conditionID = i;
            }
        }

    }
}
