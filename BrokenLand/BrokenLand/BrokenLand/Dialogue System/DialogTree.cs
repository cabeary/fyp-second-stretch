﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrokenLands
{
    public class DialogTree
    {
        //default groups will be updated whenever a dialog session ends


        public List<Dialog> dialogList = new List<Dialog>(); //list of all dialog options
        public string displayMessage; //current display
        public List<Dialog> activeList = new List<Dialog>(); //dialog options
        public int currentGroup = 0;
        public int questID = -1, conditionID = -1;
        string name; //name of character

        public List<Group> defaultgroupList = new List<Group>();


        public DialogTree()
        {

        }

        public void getDefaultGroup()
        {
            foreach (Group g in defaultgroupList)
            {
                g.requirementCheck();
                if (g.getLock())
                {
                    currentGroup = g.getGroup();
                }
            }
        }

        public void setName(string name)
        {
            this.name = name;
        }


        public void fetchConversattion()
        {
            activeList.Clear();
            // sets the current group for display and dialog options
            foreach (Dialog d in dialogList)
            {
                if (d.group == currentGroup)
                {
                    d.requirementCheck();
                    if (d.locK.getMet() && !d.teller)
                    {
                        activeList.Add(d);
                    }

                    if (d.teller && d.locK.getMet())
                    {
                        // Console.WriteLine("Current:"+currentGroup+"group"+d.group);
                        displayMessage = d.message;
                    }

                }
            }
        }

        public void loadDialog()
        {
            //load all dialog from a file
        }

        //public Action Input()
        //{ //there should be a type that is separate from the reactor eg: ignore type -> trigger type
        //    fetchConversattion();
        //    showDialog();
        //    Console.WriteLine("CurrentGroup: " + currentGroup.ToString());

        //    Console.Write("Enter response index: ");
        //    int resp = int.Parse(Console.ReadLine());

        //    currentGroup = activeList[resp].getNextGroup();

        //    return activeList[resp].getAction();
        //}

        //public void showDialog()
        //{
        //    Dialog(name + ": " + displayMessage);

        //    for (int i = 0; i < activeList.Count; i++)
        //    {
        //        Console.WriteLine(i.ToString() + ": " + activeList[i].message);
        //    }
        //}

        //public static void Dialog(string dialog)
        //{
        //    Console.WriteLine(dialog);
        //    Console.ReadKey();
        //}

    }
}
