﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrokenLands
{
    public class Lock
    {
        //lock should be bidirectional
        //should be able to set to lock if condition met or unlock if condition met

        public int type; //type of check
        bool met = false;
        public bool prevmet;
        public int requirement; // the value that has to be met
        public int value;


        public Lock(int type, int requirement, bool met, int value)
        {
            this.type = type;
            this.requirement = requirement;
            this.met = met;
            prevmet = this.met;
            this.value = value;
        }


        //unlocks dialog if type check is met
        public bool getMet()
        {
            return met;
        }

        public void setMet(int unlock)
        {
            if (unlock > 0)
                met = true;
            else
                met = false;
        }

        public void toggleMet()
        {
            if (met == prevmet)
                met = !met;
        }

    }
}
