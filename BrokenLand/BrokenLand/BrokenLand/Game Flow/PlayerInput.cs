﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace BrokenLands
{
    class PlayerInput
    {
        Random _random;
        List<CombatActions> acts;
        Dictionary<Unit, List<MapCell>> currentlyLitTiles = new Dictionary<Unit, List<MapCell>>();
        public bool endTurn = false;
        MouseController mousecontroller;
        List<MapCell> clickableTiles;
        Player _PLAYER;


        public PlayerInput(MouseController mousecontroller)
        {
            _random = new Random();
            acts = new List<CombatActions>();
            this.mousecontroller = mousecontroller;

        }
        //run once
        public void PlayerTurn(Unit player, GameSession gs)
        {
            _PLAYER = (Player)player;
            if (GameSession.currentlySelectedTile == null)
            {
                GameSession.currentlySelectedTile = gs.currentUnit.MyLocation;
            }
        }

        public void WarmUp(GameSession gs)
        {
        }

        #region Tile Lighting

        private void LightTile(string textureName, MapCell toBeLit)
        {
            if (!GameSession.tileTexDict.TryGetValue(textureName, out toBeLit.tex))
            {
                throw new NullReferenceException(textureName + " TILE NOT IN DICTIONARY!");
            }
            toBeLit.useTex = true;
        }

        private void LightUp(GameSession gameSession)
        {
            if (toBeLit.Count <= 0)
            {
                lightup = false;
                return;
            }
            LightTile(toBeLit.Peek().Key, toBeLit.Dequeue().Value);

            //toBeLit.RemoveAt(0);
        }

        private void StartLightingTiles(List<MapCell> ToBeLit, Unit source, bool transition, string textureName)
        {

            if (transition)
            {
                //set to be lit tiles for the update function to light
                foreach (MapCell mc in ToBeLit)
                {
                    toBeLit.Enqueue(new KeyValuePair<string, MapCell>(textureName, mc));
                }

                this.source = source;
                //start the update loop
                lightup = true;
                //add them to the currently lit tiles dictionary

            }
            //no need transition
            else
            {
                List<MapCell> tempLitTiles = new List<MapCell>();
                //light all tiles in list
                foreach (MapCell mc in ToBeLit)
                {
                    tempLitTiles.Add(mc);
                    LightTile(textureName, mc);
                }
                /*
                List<MapCell> currentUnitTiles;
                //add the tiles to dictionary
                if (!currentlyLitTiles.TryGetValue(source, out currentUnitTiles))
                {currentlyLitTiles.Add(source, tempLitTiles);}
                else{
                    foreach (MapCell mc in tempLitTiles)
                    {currentUnitTiles.Add(mc);}
                }*/
            }
            // add them tiles to dictionary

            List<MapCell> currentUnitTiles;
            if (!currentlyLitTiles.TryGetValue(source, out currentUnitTiles))
                currentlyLitTiles.Add(source, ToBeLit);
            else
            {
                foreach (MapCell mc in ToBeLit)
                { currentUnitTiles.Add(mc); }
            }

        }

        private Queue<KeyValuePair<string, MapCell>> toBeLit = new Queue<KeyValuePair<string, MapCell>>();
        private Unit source;
        bool lightup = false;

        public void DrawMoveRange(Player p)
        {
            //StartLightingTiles(GameSession._pather.CellStraightLineCheck(p.MyLocation,2), p, true, "bluetile");
            StartLightingTiles(GameSession._pather.getPossibleMoveLocations(p), p, true, "bluetile");
            //GameSession._pather.CellSortCheck(p.MyLocation, 2);
        }

        public void DrawAttackTargets(Player p)
        {
            List<MapCell> attackTargets = GameSession._pather.getPossibleMoveAttackLocations(p);
            if (attackTargets.Count > 0)
                StartLightingTiles(attackTargets, p, true, "redtile");
        }

        public void DrawPlayerAbilityRange(GameSession gs, Player p) { }
        public void DrawSingleEnemyThreatRange(GameSession gs, Enemy e) { }
        public void DrawAllEnemyThreatRange(GameSession gs, List<Enemy> eList) { }

        public void DrawTileColours(MapCell mc, Texture2D tex)
        {
            mc.tex = tex;
            mc.useTex = true;//!ca.Target.useTex;

        }
        public void DisableTileColours(List<MapCell> mcs)
        {
            foreach (MapCell mc in mcs)
            {
                mc.useTex = false;
            }
        }
        public void UnlightTilesForPlayer(Unit u)
        {
            List<MapCell> toBeUnlit;
            if (currentlyLitTiles.TryGetValue(u, out toBeUnlit))
            {
                foreach (MapCell mc in toBeUnlit)
                {
                    mc.useTex = false;
                    mc.tex = null;
                }
                currentlyLitTiles.Remove(u);
            }
            else
            {
                //throw new Exception("NO TILES TO BE UNLIT BRO"); 
                System.Diagnostics.Debug.WriteLine("Nothing to unlight.");
            }
        }
        #endregion

        public void EndTurn(GameSession gs)
        {
            UnlightTilesForPlayer(gs.currentUnit);
            gs.unitTakingTurn = false;
        }
        MapCell prevSelected;
        List<MapCell> pathDrawing = new List<MapCell>();
        public void Update(GameTime gameTime, GameSession gs)
        {
            if (GameSession.currentGameState == GameSession.GameState.Combat)
            {
                //for lightup animation
                if (lightup)
                {
                    LightUp(gs);
                }

                //if next turn is called
                if (endTurn)
                {

                    if (GameSession.GS.CardCondition != -1 && GameSession.GS.CurrentCard != null)
                    {
                        if ((!GameSession.GS.CurrentCard.isEvent) && (GameSession.GS.CurrentCard.EffectType == 1))
                        {
                            GameSession.GS.CardCondition--;
                            if (GameSession.GS.CardCondition == 0)
                            {
                                GameSession.GS.CurrentCard.SpawnEnem(GameSession.GS.CurrentCard.Severity / 2.0f, GameSession.GS.CurrentCard.SpawnZone);
                            }
                        }
                    }
                    gs.unitTakingTurn = false;
                    endTurn = false;
                }
                switch (GameSession.currentCombatState)
                {
                    case GameSession.CombatState.overview:
                        break;
                    case GameSession.CombatState.unitSelected:
                       
                        break;
                    default:
                        break;

                }

                if (mousecontroller.RightMouseClick())
                {
                    UnlightTilesForPlayer(gs.currentUnit);
                }
                //if my turn!
                if (gs.currentUnit.MyType == Unit.uType.Player)
                {
                    if (gs.MouseRect.Intersects(new Rectangle((int)(GameSession.GS.game1.ScreenManager.GraphicsDevice.Viewport.Width * 0.79f), (int)(GameSession.GS.game1.ScreenManager.GraphicsDevice.Viewport.Height * 0.53f), (int)(GameSession.GS.game1.ScreenManager.GraphicsDevice.Viewport.Width * 0.075f), (int)(GameSession.GS.game1.ScreenManager.GraphicsDevice.Viewport.Width * 0.075f))))
                    {
                        if (mousecontroller.LeftMouseClick())
                        Console.WriteLine("Intersect Attack1");
                    }
                    else if (gs.MouseRect.Intersects(new Rectangle((int)(GameSession.GS.game1.ScreenManager.GraphicsDevice.Viewport.Width * 0.865f), (int)(GameSession.GS.game1.ScreenManager.GraphicsDevice.Viewport.Height * 0.53f), (int)(GameSession.GS.game1.ScreenManager.GraphicsDevice.Viewport.Width * 0.075f), (int)(GameSession.GS.game1.ScreenManager.GraphicsDevice.Viewport.Width * 0.075f))))
                    {
                        if (mousecontroller.LeftMouseClick())
                        Console.WriteLine("Intersect Attack2");
                    }
                    else if (gs.MouseRect.Intersects(new Rectangle((int)(GameSession.GS.game1.ScreenManager.GraphicsDevice.Viewport.Width * 0.865f), (int)(GameSession.GS.game1.ScreenManager.GraphicsDevice.Viewport.Height * 0.53f + GameSession.GS.game1.ScreenManager.GraphicsDevice.Viewport.Width * 0.075f), (int)(GameSession.GS.game1.ScreenManager.GraphicsDevice.Viewport.Width * 0.075f), (int)(GameSession.GS.game1.ScreenManager.GraphicsDevice.Viewport.Width * 0.075f))))
                    {
                        if (mousecontroller.LeftMouseClick())
                        Console.WriteLine("Intersect Ability");
                    }
                    else if (gs.MouseRect.Intersects(new Rectangle((int)(GameSession.GS.game1.ScreenManager.GraphicsDevice.Viewport.Width * 0.79f), (int)(GameSession.GS.game1.ScreenManager.GraphicsDevice.Viewport.Height * 0.53f + GameSession.GS.game1.ScreenManager.GraphicsDevice.Viewport.Width * 0.075f), (int)(GameSession.GS.game1.ScreenManager.GraphicsDevice.Viewport.Width * 0.075f), (int)(GameSession.GS.game1.ScreenManager.GraphicsDevice.Viewport.Width * 0.075f))))
                    {
                        if (mousecontroller.LeftMouseClick())
                        Console.WriteLine("Intersect Move");
                    }
                    
                    
                    for (int x = 0; x < GameSession.map.gridNodes.GetLength(0); x++)
                    {
                        for (int z = 0; z < GameSession.map.gridNodes.GetLength(0); z++)
                        {
                            //check if mouse is on tile
                            if (mousecontroller.Selected(GameSession.map.getTileBounds(new Vector2(x, z))))
                            {
                                GameSession.currentlySelectedTile = GameSession.map.gridNodes[x, z];
                            }
                        }
                    }
                    //check for lit tiles
                    if (currentlyLitTiles.Count > 0)
                    {
                        //check currently lit tiles for player
                        if (currentlyLitTiles.TryGetValue(gs.currentUnit, out clickableTiles))
                        {
                            //check each tile
                            foreach (MapCell mc in clickableTiles)
                            {
                                if (GameSession.currentlySelectedTile == mc)
                                {
                                    if (prevSelected != null)
                                    {
                                        //draw the path
                                        if (prevSelected.Location != GameSession.currentlySelectedTile.Location)
                                        {
                                            if (pathDrawing.Count > 0)
                                            {
                                                foreach (MapCell pathCell in pathDrawing)
                                                {
                                                    pathCell.tex = GameSession.tileTexDict["bluetile"];
                                                }
                                               // pathDrawing.RemoveAt(pathDrawing.Count - 1);
                                            }
                                            pathDrawing = GameSession._pather.getPath(gs.currentUnit.MyLocation, GameSession.currentlySelectedTile);
                                            foreach (MapCell pathCell in pathDrawing)
                                            {
                                                GameSession.tileTexDict.TryGetValue("yellowtile", out pathCell.tex);
                                                //System.Diagnostics.Debug.WriteLine(mc.Location);
                                            }
                                        }
                                    }

                                   // if (mousecontroller.LeftMouseClick())
                                   // {


                                        //if click on empty space, move there
                                        if (mc.Ent == null)
                                        {
                                            //CombatActions myAct = new CombatActions(gs.currentUnit, CombatActions.ActionType.MOVE, mc);

                                            CombatActions myAct = new CombatActions(gs.currentUnit, CombatActions.ActionType.MOVE, mc);

                                            if (mousecontroller.LeftMouseClick())
                                            {
                                                GameSession._combat.PerformAction(myAct);
                                                UnlightTilesForPlayer(gs.currentUnit);
                                            }
                                            //GameSession._combat.PerformAction(myAct);

                                        }
                                        //if click on unit, attack unit.
                                        else if (mc.Ent.MyType == Entity.uType.Enemy)
                                        {

                                            //check if i can attack 1 OR ATTACK 2
                                            CombatActions myAct = new CombatActions(gs.currentUnit, CombatActions.ActionType.ATTACK1, mc);

                                            //List<MapCell> attackables = GameSession._pather.getUnitsInRadius(gs.currentUnit.MyLocation, gs.currentUnit.MyStatus.Range);
                                            List<MapCell> attackables = GameSession._pather.getPossibleStationaryAttackLocations(gs.currentUnit);
                                            if (attackables.Contains(mc))
                                            {
                                                if (mousecontroller.LeftMouseClick())
                                                {
                                                    GameSession._combat.PerformAction(myAct);
                                                    UnlightTilesForPlayer(gs.currentUnit);
                                                }

                                            }
                                            else
                                            {
                                                //if you made it here, you clicked on enemy thats out of reach
                                                List<MapCell> moveLocations = GameSession._pather.getTargetPossibleAttackLocations(mc, gs.currentUnit);
                                                int lowCost = 10;
                                                MapCell goodMove = moveLocations[0];
                                                foreach (MapCell moveCells in moveLocations)
                                                {
                                                    
                                                    int cost;
                                                    List<MapCell> path;
                                                    path = GameSession._pather.getPath(gs.currentUnit.MyLocation, moveCells, out cost);
                                                    if (path.Count > 0)
                                                    {
                                                        if (cost < lowCost)
                                                        {
                                                            lowCost = cost;
                                                            goodMove = moveCells;
                                                        }
                                                    }
                                                }
                                                
                                                CombatActions moveAct = new CombatActions(gs.currentUnit, CombatActions.ActionType.MOVE, goodMove);
                                                pathDrawing = GameSession._pather.getPath(gs.currentUnit.MyLocation, moveAct.Target);
                                                foreach (MapCell pathCell in pathDrawing)
                                                {
                                                    GameSession.tileTexDict.TryGetValue("yellowtile", out pathCell.tex);
                                                    //System.Diagnostics.Debug.WriteLine(mc.Location);
                                                }
                                                GameSession.tileTexDict.TryGetValue("greentile", out goodMove.tex);
                                                if (pathDrawing.Count > 0)
                                                //GameSession.tileTexDict.TryGetValue("greentile",out pathDrawing[0].tex);

                                                if (mousecontroller.LeftMouseClick())
                                                {
                                                   
                                                    gs.currentUnit.QueueAction(moveAct);
                                                    gs.currentUnit.QueueAction(myAct);
                                                    UnlightTilesForPlayer(gs.currentUnit);
                                                }

                                            }


                                        }
                                        
                                    // end enemy check
                                        //if mouse click, perform action and unlight tile.
                                        //
                                    //}//end click
                                }//end check if mc = currenttile
                            }// end for loop each tile
                        }//end check all lit


                    }
                    else if (mousecontroller.LeftMouseClick())
                    {
                        //click on blank
                        if (GameSession.currentlySelectedTile.Ent != null)
                        {


                            //click on self
                            if (GameSession.currentlySelectedTile.Ent == (Entity)_PLAYER)
                            {

                                List<MapCell> liteTiles;
                                if (currentlyLitTiles.TryGetValue(_PLAYER, out liteTiles))
                                {
                                    UnlightTilesForPlayer(_PLAYER);
                                }
                                else
                                {
                                    GameSession.sw.Restart();
                                    //System.Diagnostics.Debug.WriteLine("DrawMove: " + GameSession.sw.Elapsed.TotalSeconds);
                                    DrawMoveRange(_PLAYER);
                                    System.Diagnostics.Debug.WriteLine("DrawMove: " + GameSession.sw.Elapsed.TotalSeconds);
                                    DrawAttackTargets(_PLAYER);
                                    System.Diagnostics.Debug.WriteLine("DrawAttack: " + GameSession.sw.Elapsed.TotalSeconds);
                                    //DrawPlayerMoveAttackRanges(gs, _PLAYER);
                                }
                            }
                            //click on ally
                            else if (GameSession.currentlySelectedTile.Ent.MyType == Entity.uType.Player)
                            {
                            }
                            //click on enemy
                            else if (GameSession.currentlySelectedTile.Ent.MyType == Entity.uType.Enemy)
                            {

                            }
                        }
                    }
                    



                    prevSelected = GameSession.currentlySelectedTile;
                }
                else
                {
                    GameSession.currentlySelectedTile = null;
                }
            }
            else if (GameSession.currentGameState==GameSession.GameState.Dungeon)
            {
                gs.currentUnit = (Unit)GameSession.party.members[0];
                gs.unitTakingTurn = true;

                //if no cutscene
                if (gs.currentUnit.MyType == Unit.uType.Player && !GameSession.dialogueActive)
                {
                    for (int x = 0; x < GameSession.map.gridNodes.GetLength(0); x++)
                    {
                        for (int z = 0; z < GameSession.map.gridNodes.GetLength(0); z++)
                        {
                            //check if mouse is on tile
                            if (mousecontroller.Selected(GameSession.map.getTileBounds(new Vector2(x, z))))
                            {
                                GameSession.currentlySelectedTile = GameSession.map.gridNodes[x, z];
                            }
                        }
                    }
                    if (mousecontroller.LeftMouseClick())
                    {
                        foreach (Unit un in GameSession.party.members) {
                            if (un.isPerformingAction)
                            {
                                un.StopMove();
                                un.actionQueue.Clear();
                            }  

                        }
                        /*
                        if (gs.currentUnit.isPerformingAction) {
                            gs.currentUnit.StopMove();
                            gs.currentUnit.actionQueue.Clear();
                        }  */
                        //if empty
                        if (GameSession.currentlySelectedTile.Ent == null) {
                            CombatActions myAct = new CombatActions(gs.currentUnit, CombatActions.ActionType.MOVE, GameSession.currentlySelectedTile);
                            gs.currentUnit.QueueAction(myAct);
                   
                            
                        }
                        //if player, set leader
                        else if (GameSession.currentlySelectedTile.Ent != null)
                        {
                            if (GameSession.currentlySelectedTile.Ent.MyType == Entity.uType.Player){
                            GameSession.party.SetLeader(GameSession.currentlySelectedTile.Ent);
                        }
                            
                          

                        }

                       
                        // GameSession._combat.PerformAction(myAct);
                    }
                    if (mousecontroller.LeftMouseClick())
                    {
                        GameSession.clickedObject = GameSession.currentlySelectedTile.Ent;

                            if (GameSession.clickedObject != null)
                            {

                                if (GameSession._pather.proximityCheck(_PLAYER, (Entity)GameSession.clickedObject, 5))
                                {
                                    //Actor
                                    if (GameSession.clickedObject.MyType == Entity.uType.Actor)
                                    {
                                        Actor handle = (Actor)GameSession.clickedObject;
                                        System.Diagnostics.Debug.WriteLine("Click Actor");
                                        if (!handle.getChat())
                                            handle.toggleChat();
                                    }
                                    //Loot
                                    else if (GameSession.clickedObject.MyType == Entity.uType.Loot)
                                    {
                                        LootContainer handle = (LootContainer)GameSession.clickedObject;
                                        GameSession.GS.game1.ScreenManager.AddScreen(new LootScreen(handle.inventory), null);
                                        System.Diagnostics.Debug.WriteLine("Click Loot");
                                        //temp for now : take all

                                        //handle.grabAll();
                                    }
                                }
                                else {
                                    //int cost = 0;
                                    //List<MapCell> chatPath = GameSession._pather.getPath(gs.currentUnit.MyLocation, GameSession.clickedObject.MyLocation, out cost);
                                    //chatPath.RemoveAt(0);
                                    //CombatActions moveAct = new CombatActions(gs.currentUnit,CombatActions.ActionType.MOVE,chatPath);

                                   // gs.currentUnit.QueueAction(moveAct);

                                }
                            }
                    }
                }
                else
                {
                    GameSession.currentlySelectedTile = null;
                }
            }
        }

    }
}
