﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
using GameStateManagement;
using BrokenLands;
using XNAGameConsole;
using System.Text.RegularExpressions;
namespace BrokenLands
{
    sealed class GameSession 
    {
        private static readonly GameSession gameSession = new GameSession();
        public Vector2 cameraTarget = Vector2.Zero; 
        public static GameSession GS
        {
            get
            {
                return gameSession;
            }
        }
        public static GameConsole console;
        public static CameraObj gameCamera;
        public static CombatEngine _combat;
        public static Pathfinder _pather;
        public static AI_Mother ai = new AI_Mother();
        public static PlayerInput _input;
        public Queue<Unit> initiativeQueue = new Queue<Unit>();
        public List<Unit> speedQueue = new List<Unit>();
        public static Map map;
        public static List<List<MapCell>> combatZone;
        public static List<Player> players;
        public static Abilities AbilityList = new Abilities(); 
        public List<Enemy> enemies;
        public Unit currentUnit;
        public Texture2D yellowtile, bluetile, redtile, swordPointer, blueframe, redframe, apeframe, gunmanframe, borderframe, selectedInteractibleTile, selectedTile, endTurnButton, endTurnText, endTurnButtonPressed;
        //public Texture2D gun1, gun2;
        public Texture2D bottomLeftUI, circleOverlay, circleUnderlay, circleGlow, triangleGlow;
        //public Texture2D BattleOptionBase, BattleOptionAtk1, BattleOptionAtk2, BattleOptionIdle, BattleOptionBlock, BattleOptionMove, BattleOptionAbility;
        public Texture2D AbilityButtonPressed, AbilityButton, AbilityButtonText;
        public static bool dialogueActive = false;
        public Texture2D pointer;
        public bool autoEnd = true;
        public List<UINumbers> uiNumbers = new List<UINumbers>();
        public UIFrames unitsTurnUI;
        readonly float turnDelay = 0.5f;
        float delayTimer = 0;
        public List<UIBack> UIBackground = new List<UIBack>();
        public List<UIButtons> UIButtonL = new List<UIButtons>();
        public List<UIHealthBar> hpBars = new List<UIHealthBar>();
        public Inventory inventory = new Inventory();
        public List<UIBackText> uibackTexts = new List<UIBackText>();
        public List<UIOpacity> uiOpacity = new List<UIOpacity>();
        public static MapCell currentlySelectedTile;
        public static Texture2D healthRed, healthGreen, nextTurnUI;
        public Texture2D playerTex, CardTex, obsTex;
        public static Dictionary<string, Texture2D> tileTexDict = new Dictionary<string, Texture2D>();
        public SpriteFont damageFont, InconsolataFont;
        public static GameAudioEngine gameAudioEngine = new GameAudioEngine();
        public float benchmarkTimer = 0, benchmark = 0;
        public bool leaveMap = false;
        public bool loadMap = false;
        public string mapName = "";
        public static System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
        public DungeonScreen game1;
        public static bool firstInitialise = true;
        public bool mapActive;
        public Rectangle MouseRect;
        public Texture2D ToolTipTex;
        public Viewport GameViewport;
        public Deck deck = new Deck();
        public Card CurrentCard;
        public int CardCondition;
        public Inventory shopItems = new Inventory();
        public static RasterizerState rs;
        public static DungeonParty party;
        public static bool tutorialCompleted = false;
        //Temp To check for memory usage in dialouges
        public int counterr = 0;
        public List<Texture2D> CharacterTextureList = new List<Texture2D>();
        public Model defaultModel;
        public Texture2D defaultTile, defaultBattleTile;
        public String lastMusic = "";
        public int NextCharacterInt = 0;
        public MapNode[] mapNodes;
        public Texture2D CirlceBtnTex, CircleBtnSelTex;
        public bool abilityClicked = false;

        public bool lockcamera = false;
        public static bool locktoplayer = true;

        #region Debug
        public static Entity clickedObject;
        #endregion

        #region Initialisation
        private GameSession()
        {
            rs = new RasterizerState { MultiSampleAntiAlias = true };
            rs.CullMode = CullMode.CullCounterClockwiseFace;
            rs.MultiSampleAntiAlias = true;  

            players = new List<Player>();
            //players.Add(new Player(18, 15, 14, 13, 16, 14, 1, "Chanboy"));
            //players[0].MyStatus.Range = 4;
            //players[0].MyStatus.AbilityID = 9;
            
            //party = new DungeonParty(new List<Entity>{players[0]});
            // sw.Start();
            // System.Diagnostics.Debug.WriteLine("Start: " + sw.Elapsed.TotalSeconds);

        }

        public void DrawBorderedText(SpriteBatch spriteBatch,string text, int PosX, int PosY, Color color, SpriteFont font)
        {
            spriteBatch.DrawString(font, text, new Vector2(PosX + 1, PosY + 1), Color.Black * 0.5f);
            spriteBatch.DrawString(font, text, new Vector2(PosX + 1, PosY - 1), Color.Black * 0.5f);
            spriteBatch.DrawString(font, text, new Vector2(PosX - 1, PosY + 1), Color.Black * 0.5f);
            spriteBatch.DrawString(font, text, new Vector2(PosX - 1, PosY - 1), Color.Black * 0.5f);
            spriteBatch.DrawString(font, text, new Vector2(PosX, PosY), color);

        }

        public void SetMouse(MouseController mousecontroller)
        {

            _input = new PlayerInput(mousecontroller);
        }
        public void SetGame(DungeonScreen game)
        {
            game1 = game;
            
        }
        public String StringTrim(String OriginalStr, int Length)
        {
            String TrimmedStr = OriginalStr;
            List<String> TempString = new List<String>();
            while (TrimmedStr.Length > 0)
            {
                if (TrimmedStr.Length < Length)
                    Length = TrimmedStr.Length;
                String StrToAdd = TrimmedStr.Substring(0, Length);
                if (StrToAdd[StrToAdd.Length - 1] == ' ' || StrToAdd.Length <= Length)
                {
                    TempString.Add(StrToAdd);
                    TrimmedStr = TrimmedStr.Substring(Length);
                }
                else
                {
                    int index = StrToAdd.LastIndexOf(' ');
                    if (index != 0)
                    {
                        StrToAdd = StrToAdd.Substring(0, index);
                        TempString.Add(StrToAdd);
                        TrimmedStr = TrimmedStr.Substring(index);
                    }
                    else
                    {
                        TempString.Add(StrToAdd);
                        TrimmedStr = TrimmedStr.Substring(Length);
                    }
                }
            }

            foreach (String s in TempString)
            {
                TrimmedStr += s + "\n";
            }

            if (TrimmedStr.Length >= 2)
            {
                String lastLetter = TrimmedStr.Substring(TrimmedStr.Length - 1, 1);
                while (lastLetter == "\n")
                {
                    TrimmedStr = TrimmedStr.Substring(0, TrimmedStr.Length - 1);
                    lastLetter = TrimmedStr.Substring(TrimmedStr.Length - 1, 1);
                }
            }

            return TrimmedStr;
        }

        public void WarmUp()
        {

            //Lul
            Player dummy = new Player(1, 1, 1, 1, 1, 1, map.gridNodes[0, 0], tileTexDict["redtile"], 1, "Dummy");
            //dummy.Die();
            CombatActions dummyAct = new CombatActions(dummy, CombatActions.ActionType.IDLE, dummy.MyLocation);
            List<MapCell> attackables = _pather.getUnitsInRadius(dummy.MyLocation, dummy.MyStatus.Range);
            int useless = _combat.getRange(dummyAct.Action);
            GameSession._combat.PerformAction(dummyAct);
            _input.DrawMoveRange(dummy);
            _input.DrawAttackTargets(dummy);
            _input.UnlightTilesForPlayer(dummy);
            List<MapCell> dummyPath = _pather.getPath(dummy.MyLocation, dummyAct.Target, out useless);
            dummy.Die();
        }
        public void ResetGameSession()
        {

        }
        public void LoadContent(ContentManager content)
        {


            ToolTipTex = content.Load<Texture2D>("UI/UI-TooltipBack");
            System.Diagnostics.Debug.WriteLine("LOADCONTENT: " + sw.Elapsed.TotalSeconds);
            EquipmentLoader.LoadEquipments(content);

            for (int i = 0; i < EquipmentLoader.equipList.Count; i++)
            {
                //Item item = new Item(i, "Poop", "Fecal Material", 1, 1f, new Vector2(830 + (i % 5) * 77, 37 + (i / 5) * 77), SettingsIcon);
                //Item item = new Item(i, EquipmentLoader.equipList[i].name, EquipmentLoader.equipList[i].desc, (int)EquipmentLoader.equipList[i].type,
                //EquipmentLoader.equipList[i].shopValue, EquipmentLoader.equipTexture[i]);
                Item item = new Item(EquipmentLoader.equipList[i], EquipmentLoader.equipTexture[i]);
                //("Poop", "Fecal Material", 1, "Junk", new Vector2(830 + (i % 5) * 77, 37 + (i / 5) * 77), SettingsIcon);
                inventory.AddItem(item);
            }

            MouseRect = new Rectangle(Mouse.GetState().X, Mouse.GetState().Y, 20, 20);

            blueframe = content.Load<Texture2D>("UI/blueframe");
            redframe = content.Load<Texture2D>("UI/redframe");
            InconsolataFont = content.Load<SpriteFont>("Fonts/Inconstella");
            UIFrames.allyFrame = blueframe;
            UIFrames.enemyFrame = redframe;
            swordPointer = content.Load<Texture2D>("UI/sword_white");
            Entity.pointer = swordPointer;
            pointer = swordPointer;
            yellowtile = content.Load<Texture2D>("Tile/Yellow-Tile");
            bluetile = content.Load<Texture2D>("Tile/Blue-Tile");
            redtile = content.Load<Texture2D>("Tile/Red-Tile");
            healthGreen = content.Load<Texture2D>("Tile/Green-Tile");
            healthRed = content.Load<Texture2D>("Tile/Red-Tile");
            addTexToDic("yellowtile", yellowtile);
            addTexToDic("bluetile", bluetile);
            addTexToDic("redtile", redtile);
            addTexToDic("greentile", healthGreen);
            apeframe = content.Load<Texture2D>("Units/ApeFrame");
            gunmanframe = content.Load<Texture2D>("Units/GunmanFrame");
            borderframe = content.Load<Texture2D>("UI/LargeBorder");
            selectedTile = content.Load<Texture2D>("Tile/SelectedGrey");
            selectedInteractibleTile = content.Load<Texture2D>("Tile/SelectedGreen");
            nextTurnUI = content.Load<Texture2D>("UI/Next-Turn-UI");
            endTurnButton = content.Load<Texture2D>("UI/End-Turn-Button");
            endTurnText = content.Load<Texture2D>("UI/End-Turn-Text");
            endTurnButtonPressed = content.Load<Texture2D>("UI/End-Turn-Button-Pressed");
            bottomLeftUI = content.Load<Texture2D>("UI/Bottom-Left-UI");
            circleOverlay = content.Load<Texture2D>("UI/Circle-UI");
            circleUnderlay = content.Load<Texture2D>("UI/Circle-UI-Underlay");
            circleGlow = content.Load<Texture2D>("UI/Circle-UI-Glow");
            triangleGlow = content.Load<Texture2D>("UI/Glow-Triangle");
            playerTex = content.Load<Texture2D>("Units/Player");
            obsTex = content.Load<Texture2D>("MapObjects/RockObstacle");
            MapCell.selectedInteractibleTile = selectedInteractibleTile;
            MapCell.selectedTile = selectedTile;
            damageFont = content.Load<SpriteFont>("Fonts/Algerian20");
            UIElement.font = damageFont;
            CardTex = content.Load<Texture2D>("UI/CardTexture");
            AbilityButton = endTurnButton;
            AbilityButtonPressed = endTurnButtonPressed;
            AbilityButtonText = content.Load<Texture2D>("UI/Ability-Text");
            
            defaultModel = content.Load<Model>("Tile/Tile");
            defaultTile = content.Load<Texture2D>("MapObjects/defaultfloor");
            defaultBattleTile = content.Load<Texture2D>("MapObjects/BattleArenaSide");

            CharacterTextureList.Add(playerTex);
            CharacterTextureList.Add(content.Load<Texture2D>("Units/Ally"));
            CharacterTextureList.Add(content.Load<Texture2D>("Units/Gunbowman"));
            CharacterTextureList.Add(content.Load<Texture2D>("Units/Ape"));
            CharacterTextureList[0].Name = "Player";
            CharacterTextureList[1].Name = "Ally";
            CharacterTextureList[1].Name = "Gunbowman";
            CharacterTextureList[2].Name = "Ape";
            
            //gun1 = (content.Load<Texture2D>("Units/Gun-Player"));
            //gun2 = (content.Load<Texture2D>("Units/Gun2-Player"));
            //weaponTextures.Add("Gun1", gun1);
            //weaponTextures.Add("Gun2", gun2);

            CirlceBtnTex = content.Load<Texture2D>("UI/Circle-Btn");
            CircleBtnSelTex = content.Load<Texture2D>("UI/Circle-Btn-Selected");

            mapNodes = new MapNode[5];

            for (int i = 0; i < mapNodes.Length; i++)
            {
                mapNodes[i] = new MapNode(i);
                //if (mapNodes[i].checkID(mapNodes) == false)
                //{ 
                //    throw new Exception("FUCK THAT");
                //}
                if ((i + 1) >= mapNodes.Length)
                {
                    mapNodes[i].linkedNodes.Add(i);
                }
                else
                {
                    mapNodes[i].linkedNodes.Add(i + 1);
                }
                mapNodes[i].currentTex = CirlceBtnTex;
                switch (i)
                {
                    case 0: mapNodes[i].nodeName = "Norkansa Station";
                        mapNodes[i].nodeDesc = "Underground Station";
                        mapNodes[i].mapInt = 0;
                        break;
                    case 1: mapNodes[i].nodeName = "Barrens Town";
                        mapNodes[i].nodeDesc = "Ruined Town";
                        mapNodes[i].mapInt = 1;
                        break;
                    default: mapNodes[i].nodeName = "Warehouse";
                        mapNodes[i].nodeDesc = "Hostile Warehouse";
                        mapNodes[i].mapInt = 3;
                        break;
                }
            }
            mapNodes = mapNodes[0].discoverNode(mapNodes, 0);
            mapNodes = mapNodes[1].discoverNode(mapNodes, 1);
            mapNodes = mapNodes[2].discoverNode(mapNodes, 2);



            System.Diagnostics.Debug.WriteLine("ENDLOADCONTENT: " + sw.Elapsed.TotalSeconds);
        }

        public static Vector2 ToVector2(Point point)
        {
            return new Vector2(point.X, point.Y);
        }

        public void ExitMap() {
            mapActive = false;
            ai = new AI_Mother();
        
            initiativeQueue = new Queue<Unit>();
            speedQueue = new List<Unit>();
            _input = null;
            _pather = null;
            _combat = null;
            GameSession.map = null;
            this.enemies = null;
            unitsTurnUI = null;
            leaveMap = false;
            UIButtonL.Clear();
            //map.sceneList = null;
            currentUnit = null;
            currentlySelectedTile = null;
            speedQueue.Clear();
            map = null;
            //gameAudioEngine.stopAllAudio();
            //GC.Collect();
            //GC.WaitForPendingFinalizers();

            currentGameState = GameState.WorldMap;
        }

        /// <summary>		
        /// Initialize dungeon, drawing event card etc		
        /// </summary>		
        public static string SpliceText(string text, int lineLength)
        {
            return Regex.Replace(text, "(.{" + lineLength + "})", "$1" + Environment.NewLine);
        }
        public void addEnemy(Enemy enemy, MapCell Location)
        {
            UIHealthBar hpBar = new UIHealthBar(this, enemy);
            hpBars.Add(hpBar);
            enemy.frame = apeframe;
            speedQueue.Clear();
            enemy.Size = new Vector2(1,1);
            enemy.SetLocation(Location);
            //MapCell cell = Location;
            //cell.Ent = enemy;
            //map.enemList.Add(enemy);
            map.enemList.Add(enemy);
            generateUnitOrderBySpeed();
            //this.enemies.Add(enemy);		
        }

        public void addPlayer(Player player, MapCell Location)
        {
            UIHealthBar hpBar = new UIHealthBar(this, player);
            hpBars.Add(hpBar);
            player.frame = gunmanframe;
            speedQueue.Clear();
            //MapCell cell = Location;
            //cell.Ent = player;
            map.playerList.Add(player);
            players.Add(player);
            generateUnitOrderBySpeed();
        }

        public void addObstacle(Obstacle obstacle, MapCell Location)
        {
            obstacle.Texture = GS.obsTex;
            MapCell cell = Location;
            cell.Ent = obstacle;
            map.obstacleList.Add(obstacle);
            map.drawList.Add(obstacle);  //MARK DO NOT USE THE DRAWLIST
        }

        public void ActivateEvent()
        {
            CurrentCard = GameSession.GS.deck.RemoveCard();
            CurrentCard.PrintValues();
            CurrentCard.Activate();
        }		


        public void NewMap(Map map, List<Enemy> enemies)
        {
            UpdateMusic();
            mapActive = true;

            _pather = new Pathfinder(map);
            _combat = new CombatEngine(this);
            GameSession.map = map;
            GameSession.players[0].MyLocation = map.gridNodes[1, 1];
            
            //GameSession.players[0].SetLocation(map.gridNodes[1, 1]);
            this.enemies = enemies;

            unitsTurnUI = new UIFrames(this);


            UIBack UIB = new UIBack(nextTurnUI, new Vector2(UIElement.viewport.TitleSafeArea.Left + UIElement.viewport.Width / 30, UIElement.viewport.TitleSafeArea.Top + UIElement.viewport.Height / 30), 1f);
            UIBackground.Add(UIB);
            UIB = new UIBack(bottomLeftUI, new Vector2(UIElement.viewport.TitleSafeArea.Left + UIElement.viewport.Width / 30, UIElement.viewport.TitleSafeArea.Bottom - bottomLeftUI.Height * 0.5f - 30), 0.5f);
            UIBackground.Add(UIB);

            UIButtons UIBTN = new UIButtons(endTurnButton, new Vector2(UIElement.viewport.TitleSafeArea.Right - endTurnButton.Width - 70, UIElement.viewport.TitleSafeArea.Bottom - endTurnButton.Height - 40), 1f, true);
            UIButtonL.Add(UIBTN);
            UIBTN = new UIButtons(endTurnText, new Vector2(UIElement.viewport.TitleSafeArea.Right - endTurnText.Width - 70, UIElement.viewport.TitleSafeArea.Bottom - endTurnText.Height - 40), 0.5f, false);
            UIButtonL.Add(UIBTN);

            UIBackText UIBck = new UIBackText(circleOverlay, new Vector2(58, UIElement.viewport.TitleSafeArea.Top + UIElement.viewport.Height / 15 - 7), 0.875f, "AP", Color.White);
            uibackTexts.Add(UIBck);
            //UIBck = new UIBackText(circleGlow, new Vector2(58, UIElement.viewport.TitleSafeArea.Top + UIElement.viewport.Height / 15 - 7), 0.875f, "", Color.White);
            //uibackTexts.Add(UIBck);

            //UIButtons UIAbi = new UIButtons(AbilityButton, new Vector2(UIElement.viewport.TitleSafeArea.Right - endTurnButton.Width - 70, UIElement.viewport.TitleSafeArea.Bottom - endTurnButton.Height * 2 - 50), 1f, true);
            //UIButtonL.Add(UIAbi);
            //UIBTN = new UIButtons(AbilityButtonText, new Vector2(UIElement.viewport.TitleSafeArea.Right - endTurnText.Width - 70, UIElement.viewport.TitleSafeArea.Bottom - endTurnText.Height * 2 - 50), 0.5f, false);
            //UIButtonL.Add(UIAbi);

            for (int i = 0; i < 10; i++)
            {
                float rot = 0f;
                if (i % 2 == 0)
                {
                    rot = 180f;
                }

                UIOpacity UIOpa = new UIOpacity(triangleGlow, new Vector2((int)(UIElement.viewport.TitleSafeArea.Width / 4.740f) + i * triangleGlow.Width / 2, (int)(UIElement.viewport.TitleSafeArea.Width / 10.24f)), 0.8f, 1f, rot);
                uiOpacity.Add(UIOpa);
            }


            foreach (SceneObject so in map.sceneList)
            {
                so.SetLocation(so.MyLocation);
            }
            foreach (Player p in players)
            {
                UIHealthBar hpBar = new UIHealthBar(this, p);
                hpBars.Add(hpBar);
                p.frame = gunmanframe;
            }
            foreach (Enemy e in enemies)
            {
                UIHealthBar hpBar = new UIHealthBar(this, e);
                hpBars.Add(hpBar);
                e.frame = apeframe;
            }

            foreach(Actor a in map.actorList){
                a.SetLocation(a.MyLocation);
            }

            generateUnitOrderBySpeed();
            currentUnit = speedQueue[0];

            currentGameState = GameState.Dungeon;
            currentCombatState = CombatState.overview;
        }

        public void Initiate()
        {
             
            System.Diagnostics.Debug.WriteLine("STARTINIT: " + sw.Elapsed.TotalSeconds);
            console = game1.ScreenManager.console;
            if (firstInitialise)
            {
                console.AddCommand("players", a =>
                {
                    string display = "";
                    foreach (Unit u in players) {
                        display += "------------------------------------Name: " + u._NAME
                + "-------------------------------\nTexture: " + u.Texture
                + "\nPosition: " + u.MyLocation.Location
                + "\nStrength: " + u.MyStatus.Strength
                + "\nDexterity: " + u.MyStatus.Dexterity
                + "\nIntelligence: " + u.MyStatus.Intelligence
                + "\nCharisma: " + u.MyStatus.Charisma
                + "\nEndurance: " + u.MyStatus.Endurance
                + "\nAgility: " + u.MyStatus.Agility
                + "\nCurrentAP: " + u.MyStatus.CurrentAP
                + "/MaxAP: " + u.MyStatus.calcAP()
                + "\nCurrentInitiative: " + u.MyStatus.CurrentInitiative
                + "\nMyInitiativeStats: " + u.MyStatus.calcInitiativeTimer()
                + "\nBase Melee Attack: " + u.MyStatus.calcMeleeAttack()
                + "\nBase Ranged Attack: " + u.MyStatus.calcRangedAttack()
                + "\nDefence: " + u.MyStatus.calcDefence()
                            // + "\nEvasion: " + myStatus.calcEvade()                
                + "\nCurrentHealth: " + u.health
                + "/MaxHealth: " + u.MyStatus.calcHP() + "\n---------------------------------------------------------------------------------------"; 
                    }

                    return display;
                }, "hi");
                
                console.AddCommand("enemies", a =>
                {
                    var angle = float.Parse(a[0]);
                    return String.Format("Rotated the player to {0} radians", angle);
                }, "hi");
                
                console.AddCommand("killall", a =>
                { 
                    foreach (Unit u in GameSession.map.enemList){
                        u.health -= 100;
                        u.Die();
                        u.IsAlive = false;
                        currentUnit = players[0];    
                    }
                    return "";
                }, "kill all enemies");
                console.AddCommand("maxskills", a =>
                {
                    return "";
                }, "Max out all skills");

                console.AddCommand("level2", a =>
                {
                    leaveMap = true;
                    loadMap = true;
                    mapName = "Barrens_Town";
                    return "";
                }, "enter into level 2");
                console.AddCommand("level1", a =>
                {
                    leaveMap = true;
                    loadMap = true;
                    mapName = "Norkansa_Station";
                    return "";
                }, "enter into level 2");
                console.AddCommand("maxall", a =>
                {
                    foreach (Player p in players)
                    {
                        p.MyStatus.BaseStr = 50;
                        p.MyStatus.BaseDex = 50;
                        p.MyStatus.BaseInt = 50;
                        p.MyStatus.BaseCha = 50;
                        p.MyStatus.BaseEnd = 50;
                        p.MyStatus.BaseAgi = 50;
                        p.MyStatus.Skills.AddAll(100, 100, 100, 100, 100);
                    }
                    return "";
                }, "Max out all skills");
                //(players[1], map.gridNodes[2, 1]);
                //addPlayer(new Player(12, 13, 13, 13, 22, 12, map.gridNodes[2, 1], playerTex, 1, "Danboy"), map.gridNodes[2, 1]);
                //players[1].MyStatus.Range = 4;
                //players[1].MyStatus.AbilityID = 1;
                //party.members.Add(players[1]);
                WarmUp();
                //QuestManager.loadQuests();  //I assume this is run once 
                inventory.money = 100;
            }
            firstInitialise = false;

            System.Diagnostics.Debug.WriteLine("ENDINIT: " + sw.Elapsed.TotalSeconds);
        }
        #endregion
        public void addTexToDic(string s, Texture2D t2d)
        {
            tileTexDict.Add(s, t2d);
        }

        #region Turn Functions
        //public bool isUnitPerformingAct;
        public void EndTurn(Unit u) { }
        public void DeathOfUnit(Unit u) { }
        public bool unitTakingTurn = true;

        public void generateUnitOrderBySpeed()
        {
            speedQueue.Clear();
            int highestSpeed = 0;
            foreach (Player p in party.members)
            {
                if (p.MyStatus.Agility > highestSpeed)
                {
                    highestSpeed = p.MyStatus.Agility;
                }

            }
            foreach (Enemy e in enemies)
            {
                if (e.MyStatus.Agility > highestSpeed)
                {
                    highestSpeed = e.MyStatus.Agility;
                }
            }
            for (; highestSpeed >= 0; highestSpeed--)
            {
                foreach (Player p in party.members)
                {
                    if (p.MyStatus.Agility == highestSpeed)
                    {
                        if (p.IsAlive)
                            speedQueue.Add(p);
                    }

                }
                foreach (Enemy e in enemies)
                {
                    if (e.MyStatus.Agility == highestSpeed)
                    {
                        if (e.IsAlive)
                            speedQueue.Add(e);
                    }
                }
            }
        }

        public void NextTurnBySpeed()
        {
            if (!currentUnit.Disabled)
                currentUnit.MyStatus.CurrentAP = currentUnit.MyStatus.calcAP();
            else
            {
                currentUnit.MyStatus.CurrentAP = 0;
                currentUnit.Disabled = false;
            }   
            do
            {
                speedQueue.Remove(currentUnit);
                speedQueue.Add(currentUnit);
                currentUnit = speedQueue[0];
            } while (!currentUnit.IsAlive || !map.isInZone(currentUnit.MyLocation,combatZone));
            

        }
        public List<Unit> GetAliveUnitsList()
        {
            List<Unit> aliveUnits = new List<Unit>();
            foreach (Unit u in speedQueue)
            {
                if (u.IsAlive && map.isInZone(u.MyLocation, combatZone))
                {
                    aliveUnits.Add(u);
                }
            }
            return aliveUnits;
        }
        #endregion
        public enum GameState
        {
            Combat,
            Dungeon,
            WorldMap,
            Menu
        }
        public enum CombatState { 
            overview,
            unitSelected,
            nocontrol
        }
        public static GameState currentGameState;
        public static CombatState currentCombatState;
        double timer = 0;
        public void Draw(SpriteBatch spriteBatch)
        {
            if (currentGameState == GameState.Combat)
            {
                foreach (UIBack UIB in UIBackground)
                {
                    UIB.Draw(spriteBatch);
                }
                foreach (UIButtons UI in UIButtonL)
                {
                    UI.Draw(spriteBatch);
                }
                foreach (UINumbers ui in uiNumbers)
                {
                    ui.Draw(spriteBatch);
                }
                foreach (UIHealthBar uiHP in hpBars)
                {
                    uiHP.Draw(spriteBatch);
                }
                foreach (UIOpacity UI in uiOpacity)
                {
                    UI.Draw(spriteBatch);
                }
                unitsTurnUI.Draw(spriteBatch);
                
                if (MouseRect.Intersects(new Rectangle(UIElement.viewport.TitleSafeArea.Right - endTurnButton.Width - 70, UIElement.viewport.TitleSafeArea.Bottom - endTurnButton.Height * 2 - 50, AbilityButton.Width, AbilityButton.Height)))
                {
                    if (abilityClicked)
                    {
                        spriteBatch.Draw(AbilityButtonPressed, new Vector2(UIElement.viewport.TitleSafeArea.Right - endTurnButton.Width - 70, UIElement.viewport.TitleSafeArea.Bottom - endTurnButton.Height * 2 - 50), Color.White * 0.8f);
                        spriteBatch.Draw(AbilityButtonText, new Vector2(UIElement.viewport.TitleSafeArea.Right - endTurnButton.Width - 70, UIElement.viewport.TitleSafeArea.Bottom - endTurnButton.Height * 2 - 50), Color.White * 0.8f);
                    }
                    else
                    {
                        spriteBatch.Draw(AbilityButton, new Vector2(UIElement.viewport.TitleSafeArea.Right - endTurnButton.Width - 70, UIElement.viewport.TitleSafeArea.Bottom - endTurnButton.Height * 2 - 50), Color.White);
                        spriteBatch.Draw(AbilityButtonText, new Vector2(UIElement.viewport.TitleSafeArea.Right - endTurnButton.Width - 70, UIElement.viewport.TitleSafeArea.Bottom - endTurnButton.Height * 2 - 50), Color.White);
                    }
                }
                else
                {
                    spriteBatch.Draw(AbilityButton, new Vector2(UIElement.viewport.TitleSafeArea.Right - endTurnButton.Width - 70, UIElement.viewport.TitleSafeArea.Bottom - endTurnButton.Height * 2 - 50), Color.White);
                    spriteBatch.Draw(AbilityButtonText, new Vector2(UIElement.viewport.TitleSafeArea.Right - endTurnButton.Width - 70, UIElement.viewport.TitleSafeArea.Bottom - endTurnButton.Height * 2 - 50), Color.White * 0.5f);
                }
                //UIButtons UIAbi = new UIButtons(AbilityButton, new Vector2(UIElement.viewport.TitleSafeArea.Right - endTurnButton.Width - 70, UIElement.viewport.TitleSafeArea.Bottom - endTurnButton.Height * 2 - 50), 1f, true);
                //UIButtonL.Add(UIAbi);
                //UIBTN = new UIButtons(AbilityButtonText, new Vector2(UIElement.viewport.TitleSafeArea.Right - endTurnText.Width - 70, UIElement.viewport.TitleSafeArea.Bottom - endTurnText.Height * 2 - 50), 0.5f, false);
                //UIButtonL.Add(UIAbi);

            }
        }
        public void EnterCombat() { 
            

        }
        public void Update(GameTime gameTime,Notifier notifier)
        {

            if (locktoplayer)
            {

#if DEBUG
                GameSession.gameCamera.ChangeTarget(currentUnit.getOffsetPosition());
#else
            GameSession.gameCamera.ChangeTarget(currentUnit.getOffsetPosition());
#endif

                lockcamera = true;
            }

            else {
                Vector3 target = map.gridNodes[(int)cameraTarget.X, (int)cameraTarget.Y].getPositon();
                GameSession.gameCamera.ChangeTarget(target);
            }

            gameAudioEngine.Update(gameTime);
            //UpdateMusic();

            MouseRect.X = Mouse.GetState().X-MouseRect.Width/2;
            MouseRect.Y = Mouse.GetState().Y-MouseRect.Height/2;
            MouseRect.Width = (int)(GameViewport.Width * 0.015625f);
            MouseRect.Height = (int)(GameViewport.Height * 0.02778f);
            if (GameSession.currentGameState == GameSession.GameState.Dungeon)
            {
                UpdateMusic();
                List<List<MapCell>> combatZone;
                if (GameSession.map.isInAnyZone(gameSession.currentUnit.MyLocation, out combatZone))
                {
                    bool allUnitsInZone = true;
                    foreach (Unit u in party.members)
                    {
                        if (!GameSession.map.isInZone(u.MyLocation,combatZone)){
                            allUnitsInZone = false;
                        }
                    }
                    if (allUnitsInZone)
                    {
                        if (GameSession.map.enemInZone(combatZone))
                        {
                            notifier.setNewText("Battle", "BEGIN");
                            notifier.StartNotify();
                            ActivateEvent();
                            abilityClicked = false;
                            generateUnitOrderBySpeed();
                            _pather.SetMap(combatZone);
                            GameSession.currentGameState = GameSession.GameState.Combat;
                            GameSession.combatZone = combatZone;

                            foreach (Unit u in party.members)
                            {
                                u.drawWeapon = true;
                                u.StopMove();
                                u.movingPath.Clear();
                            }
                        }
                        
                    }
                    
                }
            }
            else
            {
                if (!GameSession.map.enemInZone(GameSession.combatZone))
                {
                    foreach (Obstacle obs in GameSession.map.obstacleList)
                    {
                        map.drawList.Remove(obs);
                        obs.MyLocation.Ent = null;
                    }
                    GameSession.map.obstacleList = new List<Obstacle>();

                    CurrentCard = null;
                    CardCondition = -1;
                    GameSession.currentGameState = GameSession.GameState.Dungeon;
                    map.rerender = true;
                    foreach (Unit u in players) {
                        u.drawWeapon = false;
                    }
                    _pather.SetMap(map.gridNodes);
                    generateUnitOrderBySpeed();
                }
            }

            if (leaveMap)
            {
                //GameSession.gameAudioEngine.playSound("WinMusic");


                GameSession.map.mapGen.WriteMap("../../../../BrokenLandContent/ObjectData/Map/SavedMaps/" + GameSession.map.mapName + ".map");
                GameSession.map.mapGen.SaveActors();
                GameSession.map.mapGen.SaveCharacters();
                GameSession.map.mapGen.SaveLootContainers();
                GameSession.map.mapGen.SaveQuestCondition();
                                
                if (!loadMap)
                {
                    currentGameState = GameState.WorldMap;
                    leaveMap = false;
                    game1.ScreenManager.AddScreen(new WorldMapScreen(), null);
                    game1.ExitScreen();
                    //game1.ScreenManager.RemoveScreen(game1);
                }
                else {
                    leaveMap = false;
                    loadMap = false;
                    DungeonScreen oldgame = this.game1;
                    DungeonScreen handle = new DungeonScreen();
                    handle.mapName = mapName;
                    game1.ScreenManager.AddScreen(handle,null);
                    this.SetGame(handle);
                    oldgame.ExitScreen();
                    //game1.ScreenManager.RemoveScreen(oldgame);
                }
            }


            if (currentGameState == GameState.Combat)
            {
                UpdateMusic();
                map.Update(gameTime);
                if (currentUnit.MyType == Entity.uType.Player)
                {
                   
                    foreach (UIOpacity opa in uiOpacity)
                    {
                        opa.opacity = 0.6f;
                    }
                    for (int i = 0; i < currentUnit.MyStatus.CurrentAP; i++)
                    {
                        uiOpacity[i].opacity = 1.0f;
                    }
                }

                foreach (UINumbers ui in uiNumbers)
                {
                    ui.Update(gameTime, this);
                }
                foreach (UIHealthBar uiHP in hpBars)
                {
                    uiHP.Update(gameTime, this);
                }
                foreach (UIButtons UI in UIButtonL)
                {
                    UI.Update(gameTime, this);
                }
                unitsTurnUI.Update(gameTime, this);
                //if unit needs to perform action, perform first.
                
                
                {
                    if (currentUnit.isPerformingAction)
                    {
                       
                        currentUnit.Move(gameTime);
                    }
                    //if action is waiting, assign it
                    else if (currentUnit.QueueExists())
                    {
                        currentUnit.PerformNext();
                    }
                    //if no unit is taking its turn, assign the next unit
                    else if (unitTakingTurn == false)
                    {

                        delayTimer += (float)gameTime.ElapsedGameTime.TotalSeconds;
                        if (delayTimer >= turnDelay)
                        {
                            NextTurnBySpeed();
                            
                            unitTakingTurn = true;
                            delayTimer = 0;
                        }
                    }
                    else if (currentUnit.MyStatus.CurrentAP <= 0)
                    { 
                        unitTakingTurn = false; 
                    }
                    else
                    {
                        //check who is taking turn
                        if (currentUnit.MyType == Entity.uType.Player)
                        {
                            _input.PlayerTurn((Player)currentUnit, this);
                            _input.Update(gameTime, this);
                        }
                        //if enemy
                        else
                        {
                            currentlySelectedTile = null;
                            ai.Think((Enemy)currentUnit, this);
                        }
                    }
                    bool enemiesAlive = false;
                    foreach (Unit u in GetAliveUnitsList())
                    {
                        if (u.MyType == Entity.uType.Enemy)
                        {
                            enemiesAlive = true;
                        }
                    }
                    if (enemiesAlive == false)
                    {
                       // winGame = true; Map exiting will be handled manually by the player
                    }
                }

            }
            else if (currentGameState == GameState.Dungeon)
            {

                map.Update(gameTime);
                foreach (Unit u in party.members) {

                    if (u.isPerformingAction) {
                        u.Move(gameTime);
                    }
                    else
                        if (u.QueueExists())
                        {
                            if (currentUnit == u) {
                                u.PerformNext();
                                for (int i = 1; i < GameSession.party.members.Count; i++)
                                {
                                    List<MapCell> tempPath = new List<MapCell>(currentUnit.movingPath);
                                    
                                    if (tempPath.Count>0)
                                        tempPath.RemoveAt(0);
                                    //EXPERIMENTAL!

                                    if (tempPath.Count > 0)
                                    {
                                        if (tempPath[0].Ent != party.members[i - 1])
                                        {
                                            tempPath.Add(GameSession.party.members[i - 1].MyLocation);

                                            CombatActions membersAct = new CombatActions((Unit)GameSession.party.members[i], CombatActions.ActionType.MOVE, tempPath);
                                            ((Unit)GameSession.party.members[i]).QueueAction(membersAct);
                                        }
                                    }
                                    else {
                                       
                                            tempPath.Insert(0, currentUnit.MyLocation);
                                            CombatActions membersAct = new CombatActions((Unit)GameSession.party.members[i], CombatActions.ActionType.MOVE, tempPath);
                                            ((Unit)GameSession.party.members[i]).QueueAction(membersAct);
                                        
                                    }
                                }
                            }

                            else
                                u.PerformNext();
                        }

                }
                /*
                if (currentUnit.isPerformingAction)
                {
                    currentUnit.Move(gameTime);
                }
                //if action is waiting, assign it
                else if (currentUnit.QueueExists())
                {
                    currentUnit.PerformNext();
                }
                //if no unit is taking its turn, assign the next unit
                else if (unitTakingTurn == false)
                {
                    NextTurnBySpeed();
                    unitTakingTurn = true;
                }
                else
                {

                
                }*/
                //check who is taking turn
                if (currentUnit.MyType == Entity.uType.Player)
                {
                    _input.PlayerTurn((Player)currentUnit, this);
                    _input.Update(gameTime, this);
                }

            }
            else
            {


            }
            if (leaveMap)
            {
                //GameSession.gameAudioEngine.playSound("WinMusic");
                leaveMap = false;
                currentGameState = GameState.WorldMap;

                game1.ScreenManager.AddScreen(new WorldMapScreen(), null);
                game1.ExitScreen();
                //game1.ScreenManager.RemoveScreen(game1);
            }
        }

        public void UpdateMusic()
        {
            if (map != null)
            {
                if (currentGameState == GameState.Combat)
                {
                    if (lastMusic != "CinematicRumble")
                    {
                        //Combat Music
                        gameAudioEngine.playMusic("CinematicRumble");
                        lastMusic = "CinematicRumble";
                    }
                }
                else
                {
                    if (map.mapName == QuestReactor.Locations[0])
                    {
                        if (lastMusic != "SubwayAmbience")
                        {
                            //Norkansa_Station
                            gameAudioEngine.playMusic("SubwayAmbience");
                            lastMusic = "SubwayAmbience";
                        }
                    }
                    else if (map.mapName == QuestReactor.Locations[1])
                    {
                        if (lastMusic != "BarrenTownMusic")
                        {
                            //Barrens_Town
                            gameAudioEngine.playMusic("BarrenTownMusic");
                            lastMusic = "BarrenTownMusic";
                        }
                    }
                    else if (map.mapName == QuestReactor.Locations[2])
                    {
                        if (lastMusic != "BarrenTownMusic")
                        {
                            //Barrens_Town 2
                            gameAudioEngine.playMusic("BarrenTownMusic");
                            lastMusic = "BarrenTownMusic";
                        }
                    }
                    else if (map.mapName == QuestReactor.Locations[3])
                    {
                        if (lastMusic != "CinemaRumble")
                        {
                            //Warehouse
                            gameAudioEngine.playMusic("CinemaRumble");
                            lastMusic = "CinemaRumble";
                        }
                    }
                    else if (map.mapName == QuestReactor.Locations[4])
                    {
                        if (lastMusic != "CaveAmbience")
                        {
                            //Warehouse 2
                            gameAudioEngine.playMusic("CaveAmbience");
                            lastMusic = "CaveAmbience";
                        }
                    }
                    else
                    {
                        if (lastMusic != "CaveAmbience")
                        {
                            //Default
                            gameAudioEngine.playMusic("CaveAmbience");
                            lastMusic = "CaveAmbience";
                        }
                    }
                }
            }
            else
            {
                gameAudioEngine.playMusic("MenuMusic");
            }
         }
    }
}
