﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using Supernova.Particles2D;
using Supernova.Particles2D.Modifiers.Alpha;
using Supernova.Samples.Windows.Utilities;
using GameStateManagement;
using System.Threading;
using Microsoft.Xna.Framework.Content;
using BrokenLands;
using XNAGameConsole;

namespace BrokenLands
{
    class DungeonParty
    {
        public List<Entity> members;
        //public Entity partyLeader;

        public DungeonParty(Entity en) {
            members = new List<Entity>();
            members.Add(en);
            
        }
        public DungeonParty(List<Entity> enL) {
            this.members = enL;
        }

        public void SetLeader(Entity ent){
            if (members.Contains(ent))
            {
                members.Remove(ent);
                List<Entity> newList = new List<Entity>();
                newList.Add(ent);
                for (int j = 0; j < members.Count; j++)
                {
                    newList.Add(members[j]);
                }
                members = newList;
            }
            else {
                throw new NullReferenceException("Cannot set null as leader!")
                {

                };
            }
        }

        public void SwapOrder(int index1, int index2)
        {
            members.RemoveAt(index1);
            members.Insert(index1, members[index2]);
            members.RemoveAt(index2);
            members.Insert(index2, members[index1]);
        }

        public void Move(GameTime gT) {
            foreach (Entity en in members) {
                ((Player)en).Move(gT);
            }
        }

    }
}
