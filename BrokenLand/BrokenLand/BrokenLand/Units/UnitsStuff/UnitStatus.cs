﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrokenLands
{
    class UnitStatus : UnitStats
    {
        int _STR; // strength modifier
        int _DEX; // dex modifier
        int _INT; // int modifier
        int _CHA; // charisma modifier
        int _END; // endurance modifier
        int _AGI; // agi modifier
        public int Strength { get { return (int)((base.strength + _STR) * StatMultiplier); } set { _STR = value - base.strength; } }
        public int Dexterity { get { return (int)((base.dexterity + _DEX) * StatMultiplier); } set { _DEX = value - base.dexterity; } }
        public int Intelligence { get { return (int)((base.intelligence + _INT) * StatMultiplier); } set { _INT = value - base.intelligence; } }
        public int Charisma { get { return (int)((base.charisma + _CHA) * StatMultiplier); } set { _CHA = value - base.charisma; } }
        public int Endurance { get { return (int)((base.endurance + _END) * StatMultiplier); } set { _END = value - base.endurance; } }
        public int Agility { get { return (int)((base.agility + _AGI) * StatMultiplier); } set { _AGI = value - base.agility; } }

        //read only
        public int BaseStr { get { return base.strength; } set { base.strength = value; } }
        public int BaseDex { get { return base.dexterity; } set { base.dexterity = value; } }
        public int BaseInt { get { return base.intelligence; } set { base.intelligence = value; } }
        public int BaseCha { get { return base.charisma; } set { base.charisma = value; } }
        public int BaseEnd { get { return base.endurance; } set { base.endurance = value; } }
        public int BaseAgi { get { return base.agility; } set { base.agility = value; } }
        public int Exhaustion { get { return (int)exhaustionLevel; } set { exhaustionLevel = value; } }
        public int Level { get { return level; }  }
        public int Experience { get { return experience; } set {experience = value; } }
        public UnitSkills Skills { get { return unitSkills; } }

        int abilityID;
        UnitSkills unitSkills;
        float exhaustionLevel;
        float StatMultiplier;
        float currentHealth;
        int currentAP;
        int currentInitiative;
        int initiativeTimer;
        int range;
        UnitState state;
        UnitExhaustionState exhaustionState;
        public Weapon GetWeapon() { if (rightHanding) return rightWeapon; else return leftWeapon; }
        public UnitExhaustionState ExhaustionState { get { return exhaustionState; } set { exhaustionState = value; } }
        public UnitState State { get { return state; } set { state = value; } }
        //public float CurrentHealth { get { return currentHealth; } set { currentHealth = value; } }
        public int CurrentAP { get { return currentAP; } set { currentAP = value; } }
        public int CurrentInitiative { get { return currentInitiative; } set { currentInitiative = value; } }
        public int InitiativeTimer { get { return (int)calcInitiativeTimer(); } }
        public int Range { get { return leftWeapon.range; } }
        public int Exp { get { return experience; } }
        //public int attack; //attack modified
        //public int defence; // defence modified

        public int AbilityID
        {
            get { return abilityID; }
            set { abilityID = value; }
        }

        public Unit parent;
        public List<Equipment> equippedItems = new List<Equipment>();
        public List<float> equippedItemStats = new List<float>();
        bool rightHanding = false;
        public enum StatType { 
            Defence ,
            Attack ,
            Speed,
          //  APCost ,
          //  Accuracy ,
          //  Evade ,
          //  HitCount ,
            Weight
        }
        public void UpdateEquips() {
            try
            {
                equippedItems = new List<Equipment>(parent.equipList.Values.Count);
                foreach (Equipment E in parent.equipList.Values)
                {
                    if (E != null)
                    {
                        equippedItems.Add(E);
                        if (E.type == Equipment.EquipType.Weapon)
                        {
                            if (leftWeapon == null) {
                                leftWeapon = (Weapon)E;
                            }
                            else
                            {
                                rightWeapon = (Weapon)E;
                            }
                        }
                        
                    }

                }
                if (leftWeapon == null) {
                    leftWeapon = new Weapon { 
                        baseDamage = 2,
                        baseSpeed = 10,
                        hitCount = 1,
                        name = "Fists",
                        range = 1,
                        apCost = 1
                    };
                }
                for (int i = 0; i < equippedItems.Count; i++) {
                    Equipment eq = equippedItems[i];
                    Equipment.EquipType type = eq.type;
                    switch (type) { 

                        case Equipment.EquipType.Accessory:
                        case Equipment.EquipType.Armor:
                        case Equipment.EquipType.Helmet:
                            equippedItemStats[(int)StatType.Defence] += eq.baseDefence;
                            equippedItemStats[(int)StatType.Attack] += eq.baseAttack;
                            equippedItemStats[(int)StatType.Weight] += eq.baseWeight;
                            break;
                        case Equipment.EquipType.Weapon:
                            //break;
                            //equippedItemStats[(int)StatType.Attack] += eq.baseAttack;
                            //equippedItemStats[(int)StatType.Weight] += eq.baseWeight;
                            //equippedItemStats[(int)StatType.Defence] += eq.baseDefence;
                            equippedItemStats[(int)StatType.Speed] += ((Weapon)eq).baseSpeed;
                            goto case Equipment.EquipType.Helmet;
                            //equippedItemStats[(int)StatType.APCost] = ((Weapon)eq).apCost;
                            //equippedItemStats[(int)StatType.Accuracy] += ((Weapon)eq).baseAccuracy;

                            //equippedItemStats[(int)StatType.Accuracy] += ((Weapon)eq).baseAccuracy;

                           // break;
                        default: 
                            break;

                    }
                }
            
            
            }
            catch (Exception ex) {
                System.Diagnostics.Debug.WriteLine("\n\n\n\n\n\nMAJOR BUG\n\n\n\n\n\n\n" + ex);
            }
        }
        public Weapon leftWeapon = new Weapon { 
                        baseDamage = 2,
                        baseSpeed = 10,
                        hitCount = 1,
                        name = "Fists",
                        range = 1,
                        apCost = 1
                    };
        public Weapon rightWeapon = null;

        public UnitStatus(int STR, int DEX, int INT, int CHA, int END, int AGI, Unit parent)
        {
            var enumList = Enum.GetValues(typeof(StatType));
            equippedItemStats = new List<float>(enumList.GetLength(0));
            for (int i = 0; i < equippedItemStats.Capacity; i++) {
                equippedItemStats.Add(0);
            }

            this.parent = parent;
           // UpdateEquips();
            strength = STR;
            dexterity = DEX;
            intelligence = INT;
            charisma = CHA;
            endurance = END;
            agility = AGI;
            range = 1;
            currentInitiative = (int)calcInitiativeTimer() / 2;
            currentHealth = calcHP();
            currentAP = calcAP();
            exhaustionLevel = 20;
            exhaustionState = UnitExhaustionState.Rested;
            StatMultiplier = 1.0f;
            unitSkills = new UnitSkills(BaseStr, BaseDex, BaseInt, BaseCha, BaseEnd, BaseAgi);
            unitSkills.UpdateValues(Strength, Dexterity, Intelligence, Charisma, Endurance, Agility);
        }

        /// <summary>
        /// calculate AP!
        /// </summary>
        /// <returns></returns>
        public int calcAP()
        {
            int ap = 1;
            if (dexterity / 5 > 1)
            {
                ap = dexterity / 5;
            }
            return ap;
        }
        public float calcHP() { return (float)(endurance * 3); }
        public float calcAccuracy(bool leftWeapon = true) {
            float acc;
            if (leftWeapon) { acc = this.leftWeapon.baseAccuracy; }
            else { acc = this.rightWeapon.baseAccuracy; }

            float hitChance = (float)dexterity * 4 + acc + 60 * (1 - (equippedItemStats[(int)StatType.Weight] / (parent.Size.X * 50)));
            if (hitChance >= 100) {
                hitChance = 100;
            }
            return (int)hitChance;
        }
        public float calcInitiativeTimer() { return 2000 / (endurance * 1 + agility * 2); }

        public float calcRangedAttack(bool useLeftWeapon = true)
        {
            int temp;
            return calcRangedAttack(out temp, useLeftWeapon);
        }
        public float calcMeleeAttack(bool useLeftWeapon = true)
        {
            int temp;
            return calcMeleeAttack(out temp, useLeftWeapon);
        }

        public float calcRangedAttack(out int hitCount, bool useLeftWeapon = true) {
            Weapon w;
            if (useLeftWeapon)
            {
                w = leftWeapon;
            }
            else
            {
                w = rightWeapon;
            }
            hitCount = w.hitCount;
            return w.baseDamage * (1 + ((float)dexterity * 3 + equippedItemStats[(int)StatType.Attack])/100);
        }
        public float calcMeleeAttack(out int hitCount, bool useLeftWeapon = true)
        {
            Weapon w;
            if (useLeftWeapon)
            {
                w = leftWeapon;
            }
            else
            {
                w = rightWeapon;
            }
            hitCount = w.hitCount;
            return w.baseDamage * (1 + ((float)strength * 1.3f + equippedItemStats[(int)StatType.Attack])/100);
        }

        public float calcDefence() { return (float)endurance * 2 + equippedItemStats[(int)StatType.Defence]; }
        public float calcEvade() { return agility + equippedItemStats[(int)StatType.Speed]; }
        
        //public float calcEvade() { return (float)dexterity * 2 ; }
        //public float calcAccuracy() { return (float)agility;}

        /// <summary>
        /// Method to level up and increase growth points and ability leevl (If applicable)
        /// </summary>
        public void LevelUp()
        {
            level += 1;
            experience -= 100;
            unitSkills.LevelUp();
            if (level % 5 == 0)
                GameSession.AbilityList.LevelupAbility(AbilityID);
        }

        /// <summary>
        /// Adds experience
        /// </summary>
        /// <param name="EXPAmount"></param>
        public void EXPGain(int EXPAmount)
        {
            experience += EXPAmount;
            if (experience >= 100)
            {
                String name = "A character";
                foreach (Player p in GameSession.players)
                {
                    if (p.MyStatus == this)
                    {
                        name = p._NAME;
                        break;
                    }
                }
                LevelUp();
                GameSession.GS.game1.ScreenManager.notifier.setNewText("Level Up!", name + " has leveled up!");
                GameSession.GS.game1.ScreenManager.notifier.StartNotify();
            }
        }

        /// <summary>
        /// Run at the end of every day. Increases exhaustion of all units by a flat amount (30)
        /// </summary>
        public void DailyExhaustion()
        {
            exhaustionLevel += 30;
            if (exhaustionLevel > 100)
            {
                exhaustionLevel = 100;
            }
            UpdateExhaustionState();
        }

        /// <summary>
        /// Updates ExhaustionState based on actions taken. Does not change exhaustion state (Not until end of day/combat)
        /// </summary>
        /// <param name="actionCount"></param>
        /// <param name="inCombat"></param>
        public void ActionExhaustion(int actionCount, bool inCombat)
        {
            float increaseValue;
            if (inCombat)
                increaseValue = 1.0f;
            else
                increaseValue = 0.5f;
            exhaustionLevel += increaseValue * actionCount;

            if (exhaustionLevel > 100)
            {
                exhaustionLevel = 100;
            }
        }

        /// <summary>
        /// Rests player, reducing exhaustionLevel. Also updates exhaustionState
        /// </summary>
        public void Rest()
        {
            exhaustionLevel -= (80 - BaseEnd - (BaseStr / 2));
            if (exhaustionLevel < 0)
            {
                exhaustionLevel = 0;
            }
            UpdateExhaustionState();
        }

        /// <summary>
        /// Updates unit's current exhaustion state based on current exhaustion level
        /// </summary>
        public void UpdateExhaustionState()
        {
            if (exhaustionLevel < 30)
            {
                exhaustionState = UnitExhaustionState.Rested;
                StatMultiplier = 1.0f;
            }
            else if (exhaustionLevel < 50)
            {
                exhaustionState = UnitExhaustionState.SlightlyTired;
                StatMultiplier = 0.85f;
            }
            else if (exhaustionLevel < 70)
            {
                exhaustionState = UnitExhaustionState.ModeratelyTired;
                StatMultiplier = 0.70f;
            }
            else if (exhaustionLevel < 90)
            {
                exhaustionState = UnitExhaustionState.VeryTired;
                StatMultiplier = 0.55f;
            }
            else
            {
                exhaustionState = UnitExhaustionState.ExtremelyTired;
                StatMultiplier = 0.40f;
            }
        }

        //Is it my turn or am i dead ?
        public enum UnitState
        {
            Inactive,
            Active,
            Dead
        }

        public enum UnitExhaustionState
        {
            Rested,
            SlightlyTired,
            ModeratelyTired,
            VeryTired,
            ExtremelyTired
        }

        public void Update()
        {
            if (rightHanding) {
                if (rightWeapon == null) {
                    rightHanding = false;
                }
            }
            
            /*
            if (experience >= 100)
            {
                LevelUp();
            }*/
        }


    }
}
