﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrokenLands
{
    class UnitSkills
    {
        public int BaseStr;
        public int BaseDex;
        public int BaseInt;
        public int BaseCha;
        public int BaseEnd;
        public int BaseAgi;

        float Strength;
        float Dexterity;
        float Intelligence;
        float Charisma;
        float Endurance;
        float Agility;

        public float MeleePts;
        public float ScavengePts;
        float Accuracy;
        float Avoidability;
        public float LockpickPts;
        public float BarterPts;
        public float HealPts;

        float MaxHealth;
        public float GrowthPts;
        float GrowthPtsPerLevel;

        /// <summary>
        /// Constructor for UnitSkills
        /// </summary>
        /// <param name="Str"></param>
        /// <param name="Dex"></param>
        /// <param name="Int"></param>
        /// <param name="Cha"></param>
        /// <param name="End"></param>
        /// <param name="Agi"></param>
        public UnitSkills(int Str, int Dex, int Int, int Cha, int End, int Agi)
        {
            BaseStr = Str;
            BaseDex = Dex;
            BaseInt = Int;
            BaseCha = Cha;
            BaseEnd = End;
            BaseAgi = Agi;

            MeleePts = 0;//(float)Math.Pow(BaseStr, 2) / 50) + ((Strength / 20)
            ScavengePts = 0;//(Math.Pow(BaseDex, 2) + Math.Pow(BaseStr - 5, 2)) / 10) ;
            LockpickPts = 0;//(float)Math.Pow(Dex + Int - 10, 2) / 20;
            BarterPts = 0;//(float)Math.Pow(Cha, 2) / 10;
            HealPts = 0;// (float)Math.Pow(Int, 2) / 10;

            Accuracy = (float)(Math.Pow(Dex, 2) / 10) + 10;
            Avoidability = (float)(Math.Pow(Agi, 2) / 10 + (Dex / 2));
            MaxHealth = (float)Math.Pow(End, 2) / 20 + 2;
            GrowthPtsPerLevel = (((Int / 5) + (Cha / 5)) / 4) + 1;
            GrowthPts = 10;
        }

        /// <summary>
        /// Gains points + natural growth on level
        /// </summary>
        public void LevelUp()
        {
            GrowthPts += GrowthPtsPerLevel;
            //Avoidability += (agility / 10) + (dexterity / 20);
            //AccuracyPerPt = (dexterity / 10) + 1;
        }

        /// <summary>
        /// Different Methods for Adding Points to different skills
        /// </summary>
        public void AddAll(int MelAmt, int ScaAmt, int LocAmt, int BarAmt, int HeaAmt)
        {
            AddMelee(MelAmt);
            AddScavenge(ScaAmt);
            AddLockpick(LocAmt);
            AddBarter(BarAmt);
            AddHeal(HeaAmt);
        }
        public void AddMelee(int amount)
        {
            if (GrowthPts >= amount)
            {
                MeleePts += amount;//= (Strength / 20) + ((Endurance - 5) / 20))
                GrowthPts -= amount;
            }
        }
        /*public void AddHealth()
        {
            if (GrowthPts >= 1)
            {
                MaxHealth ++;//= (strength / 10) + (endurance / 5);
                GrowthPts--;
            }
        }*/
        public void AddScavenge(int amount)
        {
            if (GrowthPts >= amount)
            {
                ScavengePts += amount;//= (dexterity / 10) + (strength / 20);
                GrowthPts -= amount;
            }
        }
        public void AddLockpick(int amount)
        {
            if (GrowthPts >= amount)
            {
                LockpickPts += amount;//= ((dexterity + intelligence) / 10) + 1;
                GrowthPts -= amount;
            }
        }
        public void AddBarter(int amount)
        {
            if (GrowthPts >= amount)
            {
                BarterPts += amount;//= charisma / 10;
                GrowthPts -= amount;
            }
        }
        public void AddHeal(int amount)
        {
            if (GrowthPts >= amount)
            {
                HealPts += amount;//= intelligence / 10;
                GrowthPts -= amount;
            }
        }

        /// <summary>
        /// Get methods to return in skill values
        /// </summary>
        public int melee
        {
            get 
            {
                return (int)(((Math.Pow(BaseStr, 2) + Math.Pow(BaseEnd - 5, 2)) / 20) + ((Strength + Endurance - 5) / 20) * MeleePts);
            }
        }
        public int health
        {
            get { return (int)MaxHealth; }
        }
        public int scavenge
        {
            get
            {
                return (int)(((Math.Pow(BaseDex, 2) + Math.Pow(BaseStr - 5, 2)) / 20) + ((Dexterity + Strength - 5) / 20) * ScavengePts);
            }
        }
        /*public int accuracy
        {
            get { return (int)Accuracy; }
        }*/
        public int avoid
        {
            get
            {
                    return (int)Avoidability;
            }
        }
        public int lockpick
        {
            get
            {
                return (int)(((Math.Pow(BaseDex, 2) + Math.Pow(BaseInt - 5, 2)) / 20) + ((Dexterity + Intelligence - 5) / 20) * LockpickPts);
            }
        }
        public int barter
        {
            get
            {
                return (int)(((Math.Pow(BaseCha, 2) + Math.Pow(BaseInt - 5, 2)) / 20) + ((Charisma + Intelligence - 5) / 20) * BarterPts);
            }
        }
        public int heal
        {
            get
            {
                return (int)(((Math.Pow(BaseInt, 2) + Math.Pow(BaseCha - 5, 2)) / 20) + ((Intelligence + Charisma - 5) / 20) * HealPts);
            }
        }
        public int growthPts
        {
            get { return (int)GrowthPts; }
        }

        public float CalcMelee(int UnconfirmedPts)
        {
            return (float)(((Math.Pow(BaseStr, 2) + Math.Pow(BaseEnd - 5, 2)) / 20) + ((Strength + Endurance - 5) / 20) * (MeleePts + UnconfirmedPts));
        }
        public float CalcScavenge(int UnconfirmedPts)
        {
            return (float)(((Math.Pow(BaseDex, 2) + Math.Pow(BaseStr - 5, 2)) / 20) + ((Dexterity + Strength - 5) / 20) * (ScavengePts + UnconfirmedPts));
        }
        public float CalcLockpick(int UnconfirmedPts)
        {
            return (float)(((Math.Pow(BaseDex, 2) + Math.Pow(BaseInt - 5, 2)) / 20) + ((Dexterity + Intelligence - 5) / 20) * (LockpickPts + UnconfirmedPts));
        }
        public float CalcBarter(int UnconfirmedPts)
        {
            return (float)(((Math.Pow(BaseCha, 2) + Math.Pow(BaseInt - 5, 2)) / 20) + ((Charisma + Intelligence - 5) / 20) * (BarterPts + UnconfirmedPts));
        }
        public float CalcHeal(int UnconfirmedPts)
        {
            return (float)(((Math.Pow(BaseInt, 2) + Math.Pow(BaseCha - 5, 2)) / 20) + ((Intelligence + Charisma - 5) / 20) * (HealPts + UnconfirmedPts));
        }

        /// <summary>
        /// Updates non-base values
        /// </summary>
        /// <param name="Str"></param>
        /// <param name="Dex"></param>
        /// <param name="Int"></param>
        /// <param name="Cha"></param>
        /// <param name="End"></param>
        /// <param name="Agi"></param>
        public void UpdateValues(float Str, float Dex, float Int, float Cha, float End, float Agi)
        {
            Strength = Str;
            Dexterity = Dex;
            Intelligence = Int;
            Charisma = Cha;
            Endurance = End;
            Agility = Agi;
        }

        /// <summary>
        /// Hit/Dodge chances (returns 0-100). Currently not in use due to Weapon Formula not in use
        /// </summary>
        /// <param name="enemyUnit"></param>
        /// <returns></returns>
        public float HitChance(UnitSkills enemyUnit)
        {
            float hitChance = (int)((Accuracy - enemyUnit.Avoidability) * 2 + 80);
            if (hitChance > 100)
                hitChance = 100;
            return hitChance;
        }
        public float DodgeChance(UnitSkills enemyUnit)
        {
            float dodgeChance = 100 - (int)((enemyUnit.Accuracy - Avoidability) * 2 + 80);
            if (dodgeChance > 100)
                dodgeChance = 100;
            return dodgeChance;
        }

        //The following methods are comparison between user and target to return a % success (and on lockpick, % break) 
        //chance on action

        /// <summary>
        /// Determines % value of successful chance of melee-ing an object (Forcing open doors, smashing windows etc)
        /// </summary>
        /// <param name="targetUnit"></param>
        /// <returns></returns>
        public float MeleeValue(UnitSkills targetUnit)
        {
            float meleeComparison = (melee - targetUnit.melee) * 2 + 100;
            return meleeComparison;
        }

        /// <summary>
        /// Scavenge chance on extra items. Over 100 indicates possible 2nd item. 
        /// ItemValue indicates difficulty of scavenge from 1-10, 10 being most difficult
        /// </summary>
        /// <param name="itemValue"></param>
        /// <returns></returns>
        public float ScavengeValue(int itemValue)
        {
            float scavengeValue = scavenge * 2 - (float)Math.Pow(itemValue, 2);
            return itemValue;
        }

        /// <summary>
        /// Lockpick refers to specifically locked areas. Contain good stuff, but has chance to break on failure.
        /// </summary>
        /// <param name="itemValue"></param>
        /// <param name="breakChance"></param>
        /// <returns></returns>
        public float LockpickValue(int itemValue, out float breakChance)
        {
            float lockpickValue = lockpick * 2 - (float)Math.Pow(itemValue, 2);
            breakChance = 80 - lockpickValue;
            return lockpickValue;
        }

        /// <summary>
        /// Determines the value % of the item you get in trade against a trader. The higher, the more you get for the same trade
        /// </summary>
        /// <param name="targetUnit"></param>
        /// <returns></returns>
        public float BarterValue(int TargetBarter)
        {
            float barterValue = (barter - TargetBarter) * 2 + 100;
            return barterValue;
        }

        /// <summary>
        /// Heal formula. Returns % healed. Heal more if healing ally
        /// </summary>
        /// <param name="targetUnit"></param>
        /// <returns></returns>
        public float HealValue(UnitSkills targetUnit, bool healingSelf)
        {
            float charismaValue;
            if (healingSelf)
                charismaValue = 10;
            else
                charismaValue = (float)(Math.Pow(targetUnit.BaseCha, 2) / 10);
            float healValue = heal + charismaValue + 50;
            return healValue;
        }

    }
}
