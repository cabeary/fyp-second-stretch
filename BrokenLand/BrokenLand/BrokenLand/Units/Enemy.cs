﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;



namespace BrokenLands
{
    class Enemy : Unit
    {
        public AIStyle aiStyle;
        public Enemy(int STR, int DEX, int INT, int CHA, int END, int AGI, MapCell loc, Texture2D tex, int lineNo, string Name, int aiNo)
            : base(STR, DEX, INT, CHA, END, AGI, loc, tex, lineNo, Name)
        {
           aiStyle = (AIStyle)aiNo;
           base.myType = uType.Enemy;
           if (aiStyle == AIStyle.Tank) { 
               base.Size = new Vector2(2, 2);
               MyStatus.leftWeapon = new Weapon
               {
                   baseDamage = 5,
                   baseSpeed = 10,
                   hitCount = 1,
                   name = "Fists",
                   range = 1,
                   apCost = 1,
                   baseAccuracy = -20
               };              // MyLocation.getPositon();
           }
        }
        


       public enum AIStyle { 
           Tank=0,
           Normal
       }

       public override void Die()
       {
           if (GameSession.map.enemList.Contains(this))
           {
               GameSession.map.enemList.Remove(this);

           }
           if (Size != new Vector2(1, 1))
           {
               for (int i = 0; i < (int)Size.X; i++)
               {
                   for (int j = 0; j < (int)Size.Y; j++)
                   {
                       GameSession.map.gridNodes[(int)myLocation.Location.X + i, (int)myLocation.Location.Y + j].Ent = null;
                   }
               }
           }
           else
           {
               myLocation.Ent = null;
           }
       }

    }
}
