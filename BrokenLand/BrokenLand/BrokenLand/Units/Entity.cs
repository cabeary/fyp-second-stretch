﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
using BrokenLands;

namespace BrokenLands
{
    class Entity
    {

        //protected Vector2 size = new Vector2(1, 1);
        //public Vector2 Size { get { return size; } set { size = value;  baseOffset = new Vector3((size.X - 1) * 4, 0, (size.Y - 1) * 4); } 


        //Model model;
        Texture2D texture;
        Texture2D weaponTexture;
        public bool draw = true;
        public Texture2D Texture { get { return texture; } set { animator.texture = value;  texture = value; } }
        public bool drawWeapon = true;
        protected Vector2 position;
        public Vector3 posOffset = Vector3.Zero;
        public Vector3 baseOffset = Vector3.Zero;
        public int rotation;
        public int lineNo = 0;
        public float health;
        public string _NAME;
        public static Texture2D pointer;
        protected Vector2 pointerPos = Vector2.Zero;
        protected Vector2 size = new Vector2(1, 1);
        public Vector2 Size { get { return size; } set { size = value; baseOffset = new Vector3((size.X - 1) * 3.25f, 0, (size.Y - 1) * 3.25f); } }
        public bool faceLeft = false;
        public SpriteEffects flipped = SpriteEffects.None;
        public ConditionObject condition; // for handling simple kill x number of things quest or destroy quests

        public bool isActive = true;
        protected Vector2 textureOffset;
        protected uType myType = uType.Other;
        public uType MyType { get { return myType; } set { myType = value; } }
        public enum uType

        {
            Player, Enemy, Obstacle, Actor, Loot, Other,Catch
        }

        protected MapCell myLocation, tempLocation;
        public MapCell MyLocation
        {
            get { return myLocation; }
            set
            {


                myLocation = value; position = value.Location;
            }
        }
        public Vector3 getOffsetPosition(){
            return MyLocation.getPositon() + new Vector3(posOffset.X + baseOffset.X, 0, posOffset.Z + baseOffset.Z);
        //MyLocation.getPositon() + new Vector3(posOffset.X + baseOffset.X, Texture.Height * 0.015f, posOffset.Z + baseOffset.Z)
        }
        public Vector3 getOffsetPosition2()
        {
            return MyLocation.getPositon() + new Vector3(posOffset.X, 0, posOffset.Z);
            //MyLocation.getPositon() + new Vector3(posOffset.X + baseOffset.X, Texture.Height * 0.015f, posOffset.Z + baseOffset.Z)
        }
        public Entity() {
            textureOffset = new Vector2(540, 806);
            animator = new SpriteSheetReader(texture, 200, 200, 10, 3);
            animator.setAnimation(idle, 1, true);
        }
        public Entity(Texture2D tex, MapCell position, int lineNo)
        {
            //this.model = model;
            this.texture = tex;
            this.position = position.Location;
            this.MyLocation = position;
            this.lineNo = lineNo;

            //  textureOffset = new Vector2(tex.Width/2,tex.Height/2);
            textureOffset = new Vector2(540, 806);
            animator = new SpriteSheetReader(texture, 200, 200, 10, 3);
            animator.setAnimation(idle, 1, true);

        }
        public Entity(Texture2D tex, Vector2 position, int lineNo)
        {
            //this.model = model;
            this.texture = tex;
            this.position = position;
            this.lineNo = lineNo;


            textureOffset = new Vector2(540, 806);
            animator = new SpriteSheetReader(texture, 200, 200, 10, 3);
            animator.setAnimation(idle, 1, true);
            //textureOffset = new Vector2(0, 0);
        }

        public void UpdateLocation(Viewport viewport, CameraObj camera)
        {
            //test the world
            Vector3 projected = viewport.Project(MyLocation.getPositon(), camera.proj, camera.view, Matrix.Identity);
            position.X = (int)projected.X;
            position.Y = (int)projected.Y;
        }

        public virtual Vector2 getPosition()
        {
            return position;
        }

        public void conditionKilled() {
            //sets a condition if killed
            if(condition!=null)
            condition.condition = 0;
        }

        public void conditionKilled(int typeNo)
        {
            //sets a condition if killed
            if (condition != null)
                condition.condition = typeNo;
        }

        public void setPosition(Vector2 vec)
        {
            position = vec;
        }

        public void SetLocation(MapCell mc)
        {
            if (myLocation != null)
            {
                if (Size != new Vector2(1, 1))
                {
                    for (int i = 0; i < (int)Size.X; i++)
                    {
                        for (int j = 0; j < (int)Size.Y; j++)
                        {
                            GameSession.map.gridNodes[(int)myLocation.Location.X + i, (int)myLocation.Location.Y + j].Ent = null;
                        }
                    }
                }
                else
                {
                    myLocation.Ent = null;
                }
            }
            //myLocation = mc;

            if (Size != new Vector2(1, 1))
            {
                for (int i = 0; i < (int)Size.X; i++)
                {
                    for (int j = 0; j < (int)Size.Y; j++)
                    {
                        GameSession.map.gridNodes[(int)mc.Location.X + i, (int)mc.Location.Y + j].Ent = this;
                    }
                }
            }
            else {
                mc.Ent = this;
            }
            myLocation = mc; position = mc.Location;
        }

        public virtual void Die() { // DRAWLIST IS NOW ONLY USED FOR SORTING
           /* if (GameSession.map.drawList.Contains(this))
            {
                GameSession.map.drawList.Remove(this);
            }*/
            draw = false;
            if (Size != new Vector2(1, 1))
            {
                for (int i = 0; i < (int)Size.X; i++)
                {
                    for (int j = 0; j < (int)Size.Y; j++)
                    {
                        GameSession.map.gridNodes[(int)myLocation.Location.X + i, (int)myLocation.Location.Y + j].Ent = null;
                    }
                }
            }
            else
            {
                myLocation.Ent = null;
            }
            
        }
        public List<MapCell> GetOccupied() {
            List<MapCell> occupying = new List<MapCell>();

            if (Size != new Vector2(1, 1))
            {
                for (int i = 0; i < (int)Size.X; i++)
                {
                    for (int j = 0; j < (int)Size.Y; j++)
                    {
                        occupying.Add(GameSession.map.gridNodes[(int)MyLocation.Location.X + i, (int)MyLocation.Location.Y + j]);
                    }
                }
            }
            else
            {
                occupying.Add(MyLocation);
            }
            return occupying;
            
        }
        public List<MapCell> GetOccupied(MapCell mc)
        {
            List<MapCell> occupying = new List<MapCell>();

            if (Size != new Vector2(1, 1))
            {
                for (int i = 0; i < (int)Size.X; i++)
                {
                    for (int j = 0; j < (int)Size.Y; j++)
                    {
                        occupying.Add(GameSession.map.gridNodes[(int)mc.Location.X + i, (int)mc.Location.Y + j]);
                    }
                }
            }
            else
            {
                occupying.Add(MyLocation);
            }
            return occupying;
        }
        public virtual void Draw(SpriteBatch spriteBatch, Viewport viewport, CameraObj camera)
        {

            float scale = 0.2f;


            if (this.myType == uType.Enemy || this.myType == uType.Player)
            {
                scale = 1;
                textureOffset = new Vector2(95, 150);
                animator.Draw(spriteBatch, position - textureOffset * scale, Color.White, 0, Vector2.Zero, scale, flipped);
            }

            else
            {
                spriteBatch.Draw(texture, position - textureOffset * scale, null, Color.White, 0, Vector2.Zero, scale, SpriteEffects.None, 0.0f);
            }
           
        }

        public virtual void Draw(SpriteBatch spriteBatch, Viewport viewport, CameraObj camera, float opacity)
        {
            // spriteBatch.Draw(texture, position, Color.White);
            float scale = 0.2f;
            spriteBatch.Draw(texture, position - textureOffset * scale, null, new Color(1 * opacity, 1 * opacity, 1 * opacity, opacity), 0, Vector2.Zero, scale, SpriteEffects.None, 0.0f);
        }

        public void DrawPointer(SpriteBatch spriteBatch)
        {
                spriteBatch.Draw(pointer, pointerPos - new Vector2(pointer.Width / 2, pointer.Height / 2) * 0.5f, null, Color.White, 0, Vector2.Zero, 0.5f, SpriteEffects.None, 0.0f);
        }

        public enum AnimationState
        {
            Idle,
            Walking,
            Attacking,
            Flinch
        };

        public AnimationState state = AnimationState.Idle;

        public SpriteSheetReader animator;

        //frame animations
        public int[] idle = { 0 };
        public int[] walk = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
        public int[] attack = { 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 };
        public int[] flinch = { 21, 22, 23, 24, 25, 26, 27, 28, 29 };

        public virtual void Update(GameTime gameTime)
        {
            animator.Update(gameTime);

            if (state == AnimationState.Walking)
            {
                animator.setAnimation(walk, 50, true);

                //if (prevLocation.Location == myLocation.Location)
                //{
                //    state = AnimationState.Idle;
                //    animator.frameIndex = 0;
                //}
            }
            else if (state == AnimationState.Idle)
            {
                animator.frameIndex = 0;
                animator.setAnimation(idle, 1, false);
            }
            else if (state == AnimationState.Attacking)
            {
                animator.setAnimation(attack, 1, false);

                if (animator.frameIndex == 8)
                {
                    System.Diagnostics.Debug.WriteLine("Reset at attack");
                    state = AnimationState.Idle;
                    animator.frameIndex = 0;
                    animator.setAnimation(idle, 1, false);
                }
            }
            else if (state == AnimationState.Flinch)
            {
                animator.setAnimation(flinch, 1, false);
                if (animator.frameIndex == 8)
                {
                    System.Diagnostics.Debug.WriteLine("Reset at flinch");
                    state = AnimationState.Idle;
                    animator.frameIndex = 0;
                    animator.setAnimation(idle, 1, false);
                }
            }
        }

    }
}
