﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
namespace BrokenLands
{
    class Player : Unit
    {
        public Player(int STR, int DEX, int INT, int CHA, int END, int AGI, MapCell loc,Texture2D tex,int lineNo, string Name) : base(STR, DEX, INT, CHA, END, AGI, loc,tex,lineNo,Name) {
            base.myType = uType.Player;
        }
        public Player(int STR, int DEX, int INT, int CHA, int END, int AGI, int lineNo, string name)
            : base(STR, DEX, INT, CHA, END, AGI, lineNo, name)
        {
            base.myType = uType.Player;
        }
    }
}
