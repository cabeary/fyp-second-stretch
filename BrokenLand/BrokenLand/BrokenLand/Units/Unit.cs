﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
namespace BrokenLands
{
    class Unit : Entity
    {
        //UnitStats myStats;
        UnitStatus myStatus;
        public Texture2D frame;
        float actionPoints;
        bool isAlive = true;
        bool isActive = false;
        bool isSelected = false;
        public float speed = 0.03f;
        public Vector3 direction = Vector3.Zero;
        public bool moving = false;
        bool disabled = false;
        private int evade = 0;
        public int MaxWeight { get { return (int)size.X * 50; } }
        //public int Evade { get { if (this.myType == uType.Player) return myStatus.calc} set { } }
        public Dictionary<string , Equipment> equipList;//  = new Dictionary<Equipment.EquipType, Equipment>(8);
        //List<KeyValuePair<String,Equipment>> equipsList;

        public bool Disabled
        {
            get { return disabled; }
            set { disabled = value; }
        }

        public Unit(int STR, int DEX, int INT, int CHA, int END, int AGI, int lineNo, string name) : base() {
            myStatus = new UnitStatus(STR, DEX, INT, CHA, END, AGI, this);
            
            base._NAME = name;
            
            base.health = myStatus.calcHP();
            myStatus.CurrentAP = myStatus.calcAP();

            var enumList = Enum.GetValues(typeof(Equipment.EquipType));
            equipList = new Dictionary<string, Equipment>(enumList.GetLength(0) + 1);
            //equipsList = new List<KeyValuePair<Equipment.EquipType, Equipment>>(enumList.GetLength(0) + 1); 
            
            foreach (Equipment.EquipType e in enumList)
            {
                //yer got 2 hands!!
                if (e == Equipment.EquipType.Weapon)
                {
                    equipList.Add("Left" + e.ToString("g"), null);
                    equipList.Add("Right" + e.ToString("g"), null);
                }
                else
                    equipList.Add(e.ToString("g"), null);
            }
        }

        //List<Equipment> equips;
        public Unit(int STR, int DEX, int INT, int CHA, int END, int AGI, MapCell loc, Texture2D tex, int lineNo, string name)
            : base(tex, loc.Location, lineNo)
        {
            myStatus = new UnitStatus(STR, DEX, INT, CHA, END, AGI, this);
            //name = "Ara";
            base._NAME = name;
            //MyLocation = loc;
            SetLocation(loc);
            //loc.Ent = this;
            /*
            myStatus.Agility = 10;
            myStatus.Charisma = 10;
            myStatus.Dexterity = 10;
            myStatus.Endurance = 10;
            myStatus.Intelligence = 10;
            myStatus.Strength = 10;
            */
            base.health = myStatus.calcHP();
            myStatus.CurrentAP = myStatus.calcAP();

            var enumList = Enum.GetValues(typeof(Equipment.EquipType));
            equipList = new Dictionary<string, Equipment>(enumList.GetLength(0)+1);
            //equipsList = new List<KeyValuePair<Equipment.EquipType, Equipment>>(enumList.GetLength(0) + 1); 
            foreach (Equipment.EquipType e in enumList){
                //yer got 2 hands!!
                if (e == Equipment.EquipType.Weapon) {
                    equipList.Add("Left" + e.ToString("g"),null);
                    equipList.Add("Right" + e.ToString("g"), null);
                }else 
                    equipList.Add(e.ToString("g"), null);
            }
            

        }

        public void PrintEquips() { 
            foreach (KeyValuePair<string,Equipment> es in equipList){
                if (es.Value != null)
                {
                    System.Diagnostics.Debug.WriteLine(es.Value.prefix + " " + es.Value.name);
                }
            }
        }

        public void Equip(Equipment e)
        {
            //Equipment e = ObjectCopier.Clone<Equipment>(eq);
            if (e.type == Equipment.EquipType.Weapon)
            {
                
                equipList["Left" + e.type.ToString("g")] = e;
            }
            else
            {
                equipList[e.type.ToString("g")] = e;
            }
            myStatus.UpdateEquips();

        }
        public void Equip(Equipment e, out Equipment oldEquipment) {

            //Equipment e = ObjectCopier.Clone<Equipment>(eq);
            if (e.type == Equipment.EquipType.Weapon)
            {
                equipList.TryGetValue("Left" + e.type.ToString("g"), out oldEquipment);
                equipList["Left" + e.type.ToString("g")] = e;

            }
            else
            {
                equipList.TryGetValue(e.type.ToString("g"), out oldEquipment);

                equipList[e.type.ToString("g")] = e;
            }
            myStatus.UpdateEquips();

        }
        public void Equip(Equipment e, out Equipment oldEquipment, bool leftHand)
        {

           // Equipment e = ObjectCopier.Clone<Equipment>(eq);
            if (e.type == Equipment.EquipType.Weapon)
            {
                string hand;
                if (leftHand)
                    hand = "Left";
                else 
                    hand = "Right";
                equipList.TryGetValue(hand + e.type.ToString("g"), out oldEquipment);
                equipList[hand + e.type.ToString("g")] = e;

            }
            else
            {
                equipList.TryGetValue(e.type.ToString("g"), out oldEquipment);
                equipList[e.type.ToString("g")] = e;
            }
            myStatus.UpdateEquips();

        }
        
        public void Unequip(string slot) { 
            equipList[slot] = null;
            myStatus.UpdateEquips();
        }



        public void reSpec()
        {
            base.health = myStatus.calcHP();
            myStatus.CurrentAP = myStatus.calcAP();
        }
        //public UnitStats myStatus { get { return myStatus; } set { myStatus = value; } }
        public UnitStatus MyStatus { get { return myStatus; } set { myStatus = value; } }
        
        public float ActionPoints { get { return actionPoints; } set { actionPoints = value; } }
        public bool IsAlive { get { return isAlive; } set { isAlive = value; } }
        /// <summary>
        /// Unit can potentially make a turn
        /// </summary>
        public bool IsActive { get { return isActive; } set { isActive = value; } }
        /// <summary>
        /// Unit is currently in control by the game
        /// </summary>
        public bool IsSelected { get { return isSelected; } set { isSelected = value; } }

        public bool isPerformingAction = false;

        new public void UpdateLocation(Viewport viewport, CameraObj camera)
        {
            //test the world
            Vector3 projected = viewport.Project(MyLocation.getPositon() + new Vector3(posOffset.X + baseOffset.X, 0, posOffset.Z + baseOffset.Z), camera.proj, camera.view, Matrix.Identity);
            Vector3 pointProjected = viewport.Project(MyLocation.getPositon() + new Vector3(posOffset.X + baseOffset.X, Texture.Height * 0.025f, posOffset.Z + baseOffset.Z), camera.proj, camera.view, Matrix.Identity);
            base.pointerPos.X = pointProjected.X;
            base.pointerPos.Y = pointProjected.Y;
            position.X = projected.X;
            position.Y = projected.Y;
           
        }
        public Vector2 GetDrawLocation(Viewport viewport, CameraObj camera)
        {
            Vector2 drawLoc;

            Vector3 projected = viewport.Project(MyLocation.getPositon() + new Vector3(posOffset.X, 10, posOffset.Z), camera.proj, camera.view, Matrix.Identity);
            
            drawLoc.X = projected.X;
            drawLoc.Y = projected.Y;
            return drawLoc;

        }

        
        public override void Draw(SpriteBatch spriteBatch, Viewport viewport, CameraObj camera)
        {
            if (drawWeapon && myStatus.GetWeapon().name != "Fists")
            {
                {
                    animator.texture2 = myStatus.GetWeapon().spriteSheet;
                    animator.drawTexture2 = true;
                }
            }
            else {
                animator.drawTexture2 = false;
            }

            UpdateLocation(viewport, camera);
            base.Draw(spriteBatch, viewport, camera);
        }
       
        public String Describe_Self()
        {

            return "------------------------------------Name: " + base._NAME
                + "-------------------------------\nTexture: " + base.Texture
                + "\nPosition: " + base.position
                + "\nStrength: " + myStatus.Strength
                + "\nDexterity: " + myStatus.Dexterity
                + "\nIntelligence: " + myStatus.Intelligence
                + "\nCharisma: " + myStatus.Charisma
                + "\nEndurance: " + myStatus.Endurance
                + "\nAgility: " + myStatus.Agility
                + "\nCurrentAP: " + myStatus.CurrentAP
                + "/MaxAP: " + myStatus.calcAP()
                + "\nCurrentInitiative: " + myStatus.CurrentInitiative
                + "\nMyInitiativeStats: " + myStatus.calcInitiativeTimer()
                + "\nBase Melee Attack: " + myStatus.calcMeleeAttack()
                + "\nBase Ranged Attack: " + myStatus.calcRangedAttack()
                + "\nDefence: " + myStatus.calcDefence()
                // + "\nEvasion: " + myStatus.calcEvade()                
                + "\nCurrentHealth: " + base.health
                + "/MaxHealth: " + myStatus.calcHP() + "\n---------------------------------------------------------------------------------------";
        }
        /*public void Move() {
            original++;
            posOffset += new Vector3(0, 0, 1) * 0.5f;
            if (original == 5000) { }
        }*/
        public List<MapCell> movingPath = new List<MapCell>();

        public void SetPath(List<MapCell> path, bool startMove) {
            movingPath = path;
            moving = startMove;
        }
        public void SetLeaderPath() { 
        }
        //Vector3 direction;
        public bool stopMove = false;
        public void StopMove() {
            stopMove = true;
            //movingPath = new List<MapCell>();
           
        }
        public void Move(GameTime gT)
        {
            if (moving)
            {

                state = AnimationState.Walking;
                //MapCell destination = movingPath[0];
                int pathCount = movingPath.Count - 1;
                if (pathCount < 0)
                {
                    moving = false;
                    isPerformingAction = false;
                    state = AnimationState.Idle;
                    animator.frameIndex = 0;
                    //System.Diagnostics.Debug.WriteLine("FINISHED MOVE!");
                }
                else
                {
                  //  if (tempLocation==null)
                  //  {
                        direction = (movingPath[pathCount].getPositon() - base.getOffsetPosition2());//.getPositon());
                        direction.Normalize();
                  //  }
                  //  else {
                  //      direction = (movingPath[pathCount].getPositon() - tempLocation.getPositon());
                  //      direction.Normalize();
                  //  }
                    if (direction.X < 0 || direction.Z < 0)
                    {
                        faceLeft = true;
                        base.flipped = SpriteEffects.FlipHorizontally;
                    }
                    else
                    {
                        faceLeft = false;
                        base.flipped = SpriteEffects.None;
                    }
                    posOffset += new Vector3(direction.X, 0, direction.Z) * (float)(speed * gT.ElapsedGameTime.TotalMilliseconds);


                    //if at destination
                    if ((myLocation.getPositon() + posOffset - movingPath[pathCount].getPositon()).Length() <= speed * gT.ElapsedGameTime.TotalMilliseconds)
                    {

                        tempLocation = null;
                        // myLocation = movingPath[pathCount];
                        if (movingPath[pathCount].Ent == null)
                        {
                           
                            posOffset = Vector3.Zero;
                            SetLocation(movingPath[pathCount]);
                        }
                        else
                        {
                            if (movingPath[pathCount].Ent == this) {
                                posOffset = Vector3.Zero;
                                SetLocation(movingPath[pathCount]);
                            }
                           // tempLocation = movingPath[pathCount];
                            //myLocation.Ent = null;
                            //myLocation = movingPath[pathCount];
                        }

                        if (!stopMove)
                            movingPath.RemoveAt(pathCount);
                        else
                        {
                            movingPath.Clear();
                            stopMove = false;
                        }

                    }
                }
            }
            
            /*
            for (int i=path.Count-1; i>=0 ;i--)
            {
                MapCell mc = path[i];
                //Vector2 cPos = new Vector2(mc.getPositon().X, mc.getPositon().Z);
                Vector3 direction = (mc.getPositon() - myLocation.getPositon() );

                //Vector3 drawnPosition = new Vector3(position+posOffset,mc);

                direction.Normalize();
                
                while (myLocation.getPositon()+posOffset  != mc.getPositon())
                {

                    posOffset += new Vector3(direction.X,0,direction.Z) * 0.5f;
                }
                myLocation = mc;
                posOffset = Vector3.Zero;
            }*/

        }


        public Queue<CombatActions> actionQueue = new Queue<CombatActions>();


        public void PrintQueue()
        {
            foreach (CombatActions a in actionQueue)
            {
                a.PrintAction();
            }
        }
        public void QueueAction(CombatActions act)
        {
            actionQueue.Enqueue(act);
        }

        public void PerformNext()
        {
            if (QueueExists())
            {
                GameSession._combat.PerformAction(actionQueue.Dequeue());
            }
        }

        public void PerformNext(out CombatActions aq)
        {

            if (QueueExists())
            {
                aq = actionQueue.Peek();
                //System.Diagnostics.Debug.WriteLine("\nPERFORMING!!!!!!\n" + aq.Unit + "\n" + aq.Action.ToString() + "\n" + aq.Target.getPositon() + "\n");
                GameSession._combat.PerformAction(actionQueue.Dequeue());
            }
            else
            {
                aq = null;
            }


        }

        public bool QueueExists()
        {
            return !(actionQueue.Count == 0);

        }
        public void CheckHealth() {

            if (health <= 0)
            {
                float EXPTotal = 80;
                foreach (Player p in GameSession.players)
                {
                    p.MyStatus.EXPGain((int)(EXPTotal / GameSession.players.Count));
                }
                this.myLocation.Ent.Die();
                isAlive = false;
            }
        }
        public void Update(GameTime gameTime, GameSession gs) {
            if (health <= 0)
            {
                isAlive = false;
            }
            else {
                isAlive = true;
            }
            base.Update(gameTime);
        }


    }
}
