﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace BrokenLands
{
    class Obstacle : Entity
    {
        int Cost;
        bool isDamage;
        string obsName;
        public Texture2D frame;

        public int TravelCost
        {
            get { return Cost; }
            set { Cost = value; }
        }
        public bool isDamageTile
        {
            get { return isDamage; }
            set { isDamage = value; }
        }
        public string Name
        {
            get { return obsName; }
            set { obsName = value; }
        }

        public Obstacle(Texture2D tex, MapCell loc, int lineNo)
            : base(tex, loc, lineNo)
        {
            base.myType = uType.Obstacle;
            Cost = 1;
            isDamage = false;
            obsName = "Default";
        }
        public Obstacle(Texture2D tex, Vector2 loc, int lineNo)
            : base(tex, loc, lineNo)
        {
            base.myType = uType.Obstacle;
            Cost = 1;
            isDamage = false;
            obsName = "Default";
        }

        public Obstacle(Texture2D tex, MapCell loc, int lineNo, int cost, bool damage, string name)
            : base(tex, loc, lineNo) 
        {
            base.myType = uType.Obstacle;
            Cost = cost;
            isDamage = damage;
            obsName = name;
        }
        public Obstacle(Texture2D tex, Vector2 loc, int lineNo, int cost, bool damage, string name)
            : base(tex, loc, lineNo)
        {
            base.myType = uType.Obstacle;
            Cost = cost;
            isDamage = damage;
            obsName = name;
        }
    }
}
