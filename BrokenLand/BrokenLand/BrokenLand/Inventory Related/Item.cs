﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace BrokenLands
{
    class Item
    {
        //ID is against master list, Row is against local list
        int ID;
        int Row;
        int Quantity;
        int MaxQuantity;
        String Name;
        String Description;
        float ItemValue; //Temp for how much item is worth
        ItemType iType;
        //0 for Helmet, 1 for Armor, 2 for Weapon, 3 for Material, 4 for Consumable, 5 for Misc, 6 for Quest Item
        public enum ItemType { 
            Helmet,
            Armor,
            Weapon,
            Material,
            Consumable,
            Misc,
            Quest
        }
        //Draw stuff
        Vector2 ScreenPos;
        Rectangle Rect;
        Texture2D icon;
        float Opacity;
        bool Selected;

        //ToolTip
        UIBack ToolTipBack;
        Texture2D ToolTipTex;
        Vector2 ToolTipBackPos;
       
        public int id
        {
            get { return ID; }
            set { ID = value; }
        }
        public int row
        {
            get { return Row; }
            set { Row = value; }
        }
        public int quantity
        {
            get { return Quantity; }
            set { Quantity = value; }
        }
        public int maxQuantity
        {
            get { return MaxQuantity; }
            set { MaxQuantity = value; }
        }
        public bool selected
        {
            get { return Selected; }
            set { Selected = value; }
        }
        public ItemType type
        {
            get { return iType; }
            set { iType = value; }
        }
        public String name
        {
            get { return Name; }
            set { Name = value; }
        }
        public String desc
        {
            get { return Description; }
            set { Description = value; }
        }
        public float value
        {
            get { return ItemValue; }
            set { ItemValue = value; }
        }

        public Vector2 position
        {
            get { return ScreenPos; }
            set { ScreenPos = value; }
        }
        public Texture2D texture
        {
            get { return icon; }
            set { icon = value; }
        }
        public Rectangle rectangle
        {
            get { return Rect; }
            set { Rect = value; }
        }
        public float opacity
        {
            get { return Opacity; }
            set { Opacity = value; }
        }

        /// <summary>
        /// Creates empty Item
        /// </summary>
        public Item()
        {
            ID = -1;
            //Type = -1;
            iType = (ItemType)0;
            Row = -1;
            Name = "default";
            Description = "Default Item Description";
            ItemValue = 0;
            icon = null;
            ScreenPos = Vector2.Zero;
            Opacity = 1.0f;
            selected = false;

            ToolTipTex = GameSession.GS.ToolTipTex;
            ToolTipBackPos = new Vector2(0, 0);
            ToolTipBack = new UIBack(ToolTipTex, ToolTipBackPos, 1.0f);
            ToolTipBack.isHidden = true;
        }

        public Item(Equipment eq, Vector2 pos, Texture2D tex = null) {
            ID = eq.itemID;
            Name = eq.name;
            Description = eq.desc;
            switch (eq.type) { 
                case Equipment.EquipType.Accessory:
                    iType = ItemType.Armor;
                    break;
                case Equipment.EquipType.Armor:
                    iType = ItemType.Armor;
                    break;
                case Equipment.EquipType.Helmet:
                    iType = ItemType.Helmet;
                    break;
                case Equipment.EquipType.Weapon:
                    iType = ItemType.Weapon;
                    break;
            }
            ItemValue = eq.shopValue;
            ScreenPos = pos;
            if (tex == null)
            {
                icon = eq.tex;
            }
            else {
                icon = tex;
            }
            Row = -1;
            Quantity = 1;
            MaxQuantity = 1;
            Rect = new Rectangle((int)(pos.X), (int)(pos.Y), icon.Width, icon.Height);
            Opacity = 1.0f;
            selected = false;
            ToolTipTex = GameSession.GS.ToolTipTex;
            ToolTipBackPos = new Vector2(0, 0);
            ToolTipBack = new UIBack(ToolTipTex, ToolTipBackPos, 1.0f);
            ToolTipBack.isHidden = true;
            //Item item = new Item(i + 50, players[0].equipList["LeftWeapon"].name, players[0].equipList["LeftWeapon"].desc, (int)players[0].equipList["LeftWeapon"].type, players[0].equipList["LeftWeapon"].shopValue, new Vector2(itemHolderRects[40].X, itemHolderRects[40].Y), players[0].equipList["LeftWeapon"].tex);
                                
        }

        public Item(Equipment eq, Texture2D tex = null)
        {
            ID = eq.itemID;
            Name = eq.name;
            Description = eq.desc;
            switch (eq.type)
            {
                case Equipment.EquipType.Accessory:
                    iType = ItemType.Armor;
                    break;
                case Equipment.EquipType.Armor:
                    iType = ItemType.Armor;
                    break;
                case Equipment.EquipType.Helmet:
                    iType = ItemType.Helmet;
                    break;
                case Equipment.EquipType.Weapon:
                    iType = ItemType.Weapon;
                    break;
            }
            ItemValue = eq.shopValue;
            if (tex == null)
            {
                icon = eq.tex;
            }
            else
            {
                icon = tex;
            }
            Row = -1;
            Quantity = 1;
            MaxQuantity = 1;
            Rect = new Rectangle(0, 0, (int)(GameSession.GS.GameViewport.Width * 0.0260416667f), (int)(GameSession.GS.GameViewport.Height * 0.0462963f));
            Opacity = 1.0f;
            selected = false;
            ToolTipTex = GameSession.GS.ToolTipTex;
            ToolTipBackPos = new Vector2(0, 0);
            ToolTipBack = new UIBack(ToolTipTex, ToolTipBackPos, 1.0f);
            ToolTipBack.isHidden = true;
            //Item item = new Item(i + 50, players[0].equipList["LeftWeapon"].name, players[0].equipList["LeftWeapon"].desc, (int)players[0].equipList["LeftWeapon"].type, players[0].equipList["LeftWeapon"].shopValue, new Vector2(itemHolderRects[40].X, itemHolderRects[40].Y), players[0].equipList["LeftWeapon"].tex);

        }
        /// <summary>
        /// Constructor to take in all variables
        /// </summary>
        /// <param name="itemID"></param>
        /// <param name="itemType"></param>
        /// <param name="itemName"></param>
        /// <param name="itemTexture"></param>
        public Item(int itemID, String itemName, String desc, int itemType, float val, Vector2 pos, Texture2D itemTexture = null)
        {
            ID = itemID;
            iType = (ItemType)itemType;
            Row = -1;
            Quantity = 1;
            MaxQuantity = 1;
            Name = itemName;
            Description = desc;
            ItemValue = val;
            icon = itemTexture;
            ScreenPos = pos; 
            Rect = new Rectangle((int)(pos.X), (int)(pos.Y), icon.Width, icon.Height);
            Opacity = 1.0f;
            selected = false;
            ToolTipTex = GameSession.GS.ToolTipTex;
            ToolTipBackPos = new Vector2(0, 0);
            ToolTipBack = new UIBack(ToolTipTex, ToolTipBackPos, 1.0f);
            ToolTipBack.isHidden = true;
        }
        public Item(int itemID, String itemName, String desc, int itemType, float val,  Texture2D itemTexture = null)
        {
            ID = itemID;
            iType = (ItemType)itemType;
            Row = -1;
            Name = itemName;
            Description = desc;
            Quantity = 1;
            MaxQuantity = 1;
            ItemValue = val;
            icon = itemTexture;
            Rect = new Rectangle(0, 0, (int)(GameSession.GS.GameViewport.Width * 0.0260416667f), (int)(GameSession.GS.GameViewport.Height * 0.0462963f));
            Opacity = 1.0f;
            selected = false;
            ToolTipTex = GameSession.GS.ToolTipTex;
            ToolTipBackPos = new Vector2(0, 0);
            ToolTipBack = new UIBack(ToolTipTex, ToolTipBackPos, 1.0f);
            ToolTipBack.isHidden = true;
        }
        public Item(int itemID, String itemName, String desc, int itemType, float val, int quan, int maxQuan, Vector2 pos, Texture2D itemTexture = null)
        {
            ID = itemID;
            iType = (ItemType)0;
            Row = -1;
            Quantity = quan;
            MaxQuantity = maxQuan;
            Name = itemName;
            Description = desc;
            ItemValue = val;
            icon = itemTexture;
            ScreenPos = pos;
            Rect = new Rectangle((int)(pos.X), (int)(pos.Y), icon.Width, icon.Height);
            Opacity = 1.0f;
            selected = false;
            ToolTipTex = GameSession.GS.ToolTipTex;
            ToolTipBackPos = new Vector2(0, 0);
            ToolTipBack = new UIBack(ToolTipTex, ToolTipBackPos, 1.0f);
            ToolTipBack.isHidden = true;
        }

        public void Update(Rectangle mouseRect)
        {
            ToolTipBack.position.X = mouseRect.X + 20;
            ToolTipBack.position.Y = mouseRect.Y + 20;

            if (mouseRect.Intersects(Rect))
            {
                Opacity = 1.0f;
                ToolTipBack.isHidden = false;
                
            }
            else
            {
                Opacity = 0.7f;
                ToolTipBack.isHidden = true;
            }
            if (selected)
            {
                ScreenPos.X = mouseRect.X;
                ScreenPos.Y = mouseRect.Y;
                Rect.X = mouseRect.X;
                Rect.Y = mouseRect.Y;
            }
        }
        public void Draw(SpriteBatch spriteBatch,SpriteFont font)
        {
            spriteBatch.Draw(icon, ScreenPos, Color.White * Opacity);
        }
        public void Draw(SpriteBatch spriteBatch, float scale, SpriteFont font)
        {
            spriteBatch.Draw(icon, ScreenPos,null, Color.White * Opacity,0f,Vector2.Zero,scale,SpriteEffects.None,0f);
            
        }
        public void DrawToolTip(SpriteBatch spriteBatch, SpriteFont font)
        {
            ToolTipBack.Draw(spriteBatch);

            if (ToolTipBack.isHidden == false)
            {
                GameSession.GS.DrawBorderedText(spriteBatch, Name, (int)ToolTipBack.position.X + 10, (int)ToolTipBack.position.Y + 10, Color.WhiteSmoke, font);
                //spriteBatch.DrawString(spritefont, hoverName, ToolTipBack.position+new Vector2(10,10), Color.White);
                spriteBatch.DrawString(font, Description, ToolTipBack.position + new Vector2(10, 40), Color.White);
            }
        }
        public void DrawAtRect(SpriteBatch spriteBatch, float scale, SpriteFont font)
        {
            spriteBatch.Draw(icon, Rect, Color.White);
            ToolTipBack.Draw(spriteBatch);

            if (ToolTipBack.isHidden == false)
            {
                GameSession.GS.DrawBorderedText(spriteBatch, Name, (int)ToolTipBack.position.X + 10, (int)ToolTipBack.position.Y + 10, Color.WhiteSmoke, font);
                //spriteBatch.DrawString(spritefont, hoverName, ToolTipBack.position+new Vector2(10,10), Color.White);
                spriteBatch.DrawString(font, Description, ToolTipBack.position + new Vector2(10, 40), Color.White);
            }
        }
        public void UpdateRectToPos()
        {
            Rect = new Rectangle((int)position.X + this.texture.Width / 5, (int)position.Y + this.texture.Height / 5, Rect.Width, Rect.Height);
        }
    }
}
