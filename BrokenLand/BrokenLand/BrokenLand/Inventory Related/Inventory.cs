﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrokenLands
{
    class Inventory
    {
        public List<Item> activeList = new List<Item>();
        public int money = 0;

        /// <summary>
        /// Constructor for Inventory
        /// </summary>
        public Inventory()
        {
        }

        /// <summary>
        /// Returns activeList
        /// </summary>
        /// <returns></returns>
        public List<Item> GetActiveList()
        {
            return activeList;
        }

        /// <summary>
        /// Adds item from inactive list to active list. Does not remove item from inactive list
        /// </summary>
        /// <param name="newItem"></param>
        /*public void AddItem(int ItemRow)
        {
            Item newItem = inactiveList[ItemRow];
            //If is material
            if (newItem.type == 2)
            {
                //Add value to represent count
                bool found = false;
                for (int i = 0; i < activeList.Count; i++)
                {
                    if (activeList[i].name == newItem.name)
                    {
                        activeList[i].value += newItem.value;
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    newItem.row = activeList.Count;
                    activeList.Add(newItem);
                }
            }
            else
            {
                newItem.row = activeList.Count;
                activeList.Add(newItem);
            }
        }*/
        public void AddItem(Item newItem)
        {
            //If is material
            if (activeList.Count < 40)
            {
                if ((int)newItem.type == 2)
                {
                    //Add value to represent count

                    newItem.row = activeList.Count;
                    activeList.Add(newItem);
                }
                else
                {
                    newItem.row = activeList.Count;
                    activeList.Add(newItem);
                }
            }
        }

        /// <summary>
        /// Adds a list of multiple items into active List
        /// </summary>
        /// <param name="newItems"></param>
        public void AddItem(List<Item> newItems)
        {
            for (int j = 0; j < newItems.Count; j++)
            {
                Item newItem = newItems[j];
                //If is material

                
                newItem.row = activeList.Count;
                activeList.Add(newItem);
                
            }
        }

        public void AddItem(int itemID)
        {
            foreach(Item i in EquipmentLoader.itemList){

                if(i.id == itemID){
                    activeList.Add(i);
                    break;
                }

            }
        }

        /// <summary>
        /// Returns item based on RowPosition
        /// </summary>
        /// <param name="itemID"></param>
        /// <returns></returns>
        public Item GetItem(int itemID)
        {
            Item toBeReturned = null;
            for (int i = 0; i < activeList.Count; i++)
            {
                if (activeList[i].id == itemID)
                {
                    toBeReturned = activeList[i];
                    break;
                }
            }
            return toBeReturned;
        }
        /*public Item GetItem(int itemRow)
        {
            Item toBeReturned = null;
            for (int i = 0; i < activeList.Count; i++)
            {
                if (activeList[i].row == itemRow)
                {
                    toBeReturned = activeList[i];
                    break;
                }
            }
            return toBeReturned;
        }*/
        public Item GetItem(String Name)
        {
            Item toBeReturned = null;
            for (int i = 0; i < activeList.Count; i++)
            {
                if (activeList[i].name == Name)
                {
                    toBeReturned = activeList[i];
                    break;
                }
            }
            return toBeReturned;
        }

        /// <summary>
        /// Creates and returns a complete replica of the item
        /// </summary>
        /// <param name="Name"></param>
        /// <returns></returns>
        public Item ReplicateItem(String Name)
        {
            Item Replicate = new Item();
            Item Original = GetItem(Name);
            Replicate.name = Original.name;
            Replicate.desc = Original.desc;
            Replicate.id = Original.id;
            Replicate.row = Original.row;
            Replicate.value = Original.value;
            Replicate.position = Original.position;
            Replicate.rectangle = Original.rectangle;
            Replicate.texture = Original.texture;
            Replicate.opacity = Original.opacity;

            return Replicate;
        }
        public Item ReplicateItem(Item item)
        {
            Item Replicate = new Item();
            Item Original = item;
            Replicate.name = Original.name;
            Replicate.desc = Original.desc;
            Replicate.id = Original.id;
            Replicate.row = Original.row;
            Replicate.type = Original.type;
            Replicate.value = Original.value;
            Replicate.position = Original.position;
            Replicate.rectangle = Original.rectangle;
            Replicate.texture = Original.texture;
            Replicate.opacity = 0.7f;

            return Replicate;
        }

        /// <summary>
        /// Removes Item from ActiveList, returns that item
        /// </summary>
        /// <param name="itemID"></param>
        /// <returns></returns>
        public Item TakeItem(Item item)
        {
            Item toBeReturned = null;
            for (int i = 0; i < activeList.Count; i++)
            {
                if (activeList[i] == item)
                {
                    toBeReturned = activeList[i];
                    activeList.RemoveAt(i);
                    break;
                }
            }

            return toBeReturned;
        }
        public Item RemoveItem(Item item)
        {
            Item toBeReturned = null;
            for (int i = 0; i < activeList.Count; i++)
            {
                if (activeList[i] == item)
                {
                    toBeReturned = activeList[i];
                    activeList.RemoveAt(i);
                    break;
                }
            }

            return toBeReturned;
        }

    }
}
