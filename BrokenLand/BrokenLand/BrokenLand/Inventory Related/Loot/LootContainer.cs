﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace BrokenLands
{
    class LootContainer : SceneObject
    {
        public Inventory inventory = new Inventory();
        public List<Item> itemList = new List<Item>();
        public bool showGUI = false;
        public string fileName = null;

        //include container UI here

        public LootContainer(Texture2D text, int lineno, MapCell Location)
            : base(text, lineno, Location)
        {
            itemList = inventory.activeList;
            MyType = uType.Loot;

        }

        public void toggleLoot()
        {
            showGUI = !showGUI;
        }

        public void debugLoot()
        {
            foreach (Item i in itemList)
            {
                string name = i.name;
                System.Diagnostics.Debug.WriteLine("Item:" + i.name);
            }
        }


        public void grabAll() {
            foreach(Item i in itemList){
                GameSession.GS.inventory.activeList.Add(i);
            }

            itemList.Clear();
        }

        public void grabItem(int itemID) {
            GameSession.GS.inventory.AddItem(itemID);

            for (int i = 0; i < itemList.Count;i++ )
            {
                if (itemID == itemList[i].id)
                    itemList.RemoveAt(i);
            }
            
        }

    }
}
