﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using GameStateManagement;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace BrokenLands
{
    class LootPopUp
    {
        Texture2D LootScreenTex;
        Texture2D LootSquare;
        Texture2D TakeAll, TakeAllHoverOn, TakeAllHoverOff;
        Rectangle TakeAllRect;
        Inventory LootInv;
        ScreenManager screen;
        Vector2 position;
        public bool isActive;
        MouseController mouseC;
        int lootSize;

        public LootPopUp(ContentManager content,Inventory LootInventory,ScreenManager screenmanager,Vector2 location,MouseController mouse)
        {
            screen = screenmanager;
            LootInv = LootInventory;
            LootScreenTex = content.Load<Texture2D>("UI/Loot-Popup");
            LootSquare = content.Load<Texture2D>("UI/LootSquare");
            TakeAllHoverOff = content.Load<Texture2D>("UI/TakeAll-Btn");
            TakeAllHoverOn = content.Load<Texture2D>("UI/TakeAll-Btn-Hover");
            TakeAll = TakeAllHoverOff;
            position = location;
            TakeAllRect = new Rectangle((int)position.X, (int)position.Y, (int)(TakeAll.Width * 0.7f), (int)(TakeAll.Height * 0.8f));
            isActive = false;
            mouseC = mouse;
            lootSize = LootInventory.activeList.Count;
            foreach (Item item in LootInv.activeList)
            {
                //Update the location
                
            }
        }
        public void UpdateLocation(Vector2 pos, Viewport viewport, CameraObj camera, MapCell EntityPos)
        {
            Vector3 projected = viewport.Project(EntityPos.getPositon(), camera.proj, camera.view, Matrix.Identity);
            pos.X = projected.X;
            pos.Y = projected.Y;
        }
        public void Update()
        {
            foreach (Item item in LootInv.activeList)
            {
                item.Update(GameSession.GS.MouseRect);
                
                    if (mouseC.LeftMouseClick())
                    {
                        if (item.rectangle.Intersects(GameSession.GS.MouseRect))
                        {
                            Item itemm = new Item(item.id, item.name, item.desc, (int)item.type, item.value, item.texture);
                            GameSession.GS.inventory.AddItem(itemm);
                            LootInv.RemoveItem(item);
                            break;
                        }
                    }

            }
            if (GameSession.GS.MouseRect.Intersects(TakeAllRect))
            {
                TakeAll = TakeAllHoverOn;
                if (mouseC.LeftMouseClick())
                {
                    GameSession.GS.inventory.AddItem(LootInv.activeList);
                    LootInv.activeList.Clear();
                }
            }
            else 
            {
                TakeAll = TakeAllHoverOff;
            }
            if (LootInv.activeList.Count <= 0)
            { 
                lootSize = 0;
            }
        }
        public void Draw(SpriteBatch spriteBatch,SpriteFont font)
        {
            if (isActive)
            {
                Vector2 offset = new Vector2(0,0);
                float scale = 0.5f;
                
                if (lootSize > 0 && lootSize < 5)
                {

                    //Translate it to grid here
                    //
                    //
                    //
                    //UpdateLocation();
                    //
                    //
                    //
                    

                    spriteBatch.Draw(LootScreenTex, position, null, Color.White, 0f, Vector2.Zero, scale, SpriteEffects.None, 0f);
                    TakeAllRect.X = (int)position.X + 81;
                    TakeAllRect.Y = (int)position.Y + 170;
                    TakeAllRect.Width = (int)(TakeAll.Width * 0.5f);
                    TakeAllRect.Height = (int)(TakeAll.Height * 0.5f);
                    spriteBatch.Draw(TakeAll, TakeAllRect , Color.White);
                    for (int i = 0; i < 4; i++)
                    {
                        float framescale = 1.4f;
                        switch (i)
                        {
                            case 0: offset = new Vector2(0, 0);
                                offset *= framescale;
                                break;
                            case 1: offset = new Vector2(82, 0);
                                offset *= framescale;
                                break;
                            case 2: offset = new Vector2(0, 88);
                                offset *= framescale;
                                break;
                            case 3: offset = new Vector2(82, 88);
                                offset *= framescale;
                                break;
                            default: break;
                        }

                        position = (position + offset) * scale;
                        //Translate it to grid here
                        //
                        //
                        //
                        //UpdateLocation();
                        //
                        //
                        //
                        //
                        
                        spriteBatch.Draw(LootSquare, position + new Vector2(25, 70) * scale, null, Color.White * 0.8f, 0f, Vector2.Zero, 0.72f, SpriteEffects.None, 0f);

                        position = Vector2.Zero;
                        if (i <= LootInv.activeList.Count - 1)
                        {
                            LootInv.activeList[i].position = (position + new Vector2(40, 90) + offset) * scale;

                            LootInv.activeList[i].UpdateRectToPos();
                        }
                    }
                }
                if (lootSize > 4 && lootSize < 10)
                {
                    //position = new Vector2(GameSession.currentlySelectedTile.getPositon().X,GameSession.currentlySelectedTile.getPositon().Z);
                    //Translate it to grid here
                    //
                    //
                    //
                    //UpdateLocation();
                    //
                    //Probably will have problems with scaling for this one
                    //Just pass it back to adam
                    //
                   
                   // TEST
                      if (GameSession.currentlySelectedTile.Ent != null)
                        position = GameSession.currentlySelectedTile.Location;
                    spriteBatch.Draw(GameSession.healthGreen, position, null, Color.White, 0f, Vector2.Zero, 0.7f, SpriteEffects.None, 0.0f);
                   // TEST
                    
                   // spriteBatch.Draw(LootScreenTex, position, null, Color.White, 0f, Vector2.Zero, 0.7f, SpriteEffects.None, 0f);
                   // TakeAllRect.X = (int)position.X+113;
                   // TakeAllRect.Y = (int)position.Y+237;
          
                   // spriteBatch.Draw(LootScreenTex, position/*-(new Vector2(160,140)*scale)*/, null, Color.White, 0f, Vector2.Zero, 0.7f, SpriteEffects.None, 0f);
                    TakeAllRect.Width = (int)(TakeAll.Width * 0.7f);
                    TakeAllRect.Height = (int)(TakeAll.Height * 0.7f);
                     
                    spriteBatch.Draw(TakeAll, TakeAllRect , Color.White);
                    for (int i = 0; i < 9; i++)
                    {
                        float framescale = 1.4f;
                        switch (i)
                        {
                            case 0: offset = new Vector2(0, 0);
                                offset *= framescale;
                                break;
                            case 1: offset = new Vector2(82, 0);
                                offset *= framescale;
                                break;
                            case 2: offset = new Vector2(82*2, 0);
                                offset *= framescale;
                                break;
                            case 3: offset = new Vector2(0, 88);
                                offset *= framescale;
                                break;
                            case 4: offset = new Vector2(82, 88);
                                offset *= framescale;
                                break;
                            case 5: offset = new Vector2(82*2, 88);
                                offset *= framescale;
                                break;
                            case 6: offset = new Vector2(0, 88*2);
                                offset *= framescale;
                                break;
                            case 7: offset = new Vector2(82, 88*2);
                                offset *= framescale;
                                break;
                            case 8: offset = new Vector2(82*2, 88*2);
                                offset *= framescale;
                                break;
                            default: break;
                        }
                        position = (position + offset) * scale;
                        //Translate it to grid here
                        //
                        //
                        //
                        //
                        //UpdateLocation();
                        //
                        //
                        //
                        //
                        //
                        //
                        spriteBatch.Draw(LootSquare, position + new Vector2(30, 80) * scale, null, Color.White * 0.8f, 0f, Vector2.Zero, 0.72f, SpriteEffects.None, 0f);
                        position = Vector2.Zero;
                        if (i<=LootInv.activeList.Count-1)
                        {
                            LootInv.activeList[i].position = (position + new Vector2(50, 100) + offset) * scale;
                            LootInv.activeList[i].UpdateRectToPos();
                            
                        }
                    }
                }
                //spriteBatch.End();
                //spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.Opaque);
                //RasterizerState state = new RasterizerState();
                //state.FillMode = FillMode.WireFrame;
                //spriteBatch.GraphicsDevice.RasterizerState = state;

                foreach (Item item in LootInv.activeList)
                {
                    item.Draw(spriteBatch,scale*1.6f,font);
                }
                foreach (Item item in LootInv.activeList)
                {
                    item.DrawToolTip(spriteBatch, font);
                }
                if(LootInv.activeList.Count>0)
                spriteBatch.Draw(LootInv.activeList[0].texture, GameSession.GS.MouseRect, Color.White);
                //spriteBatch.End();
                //spriteBatch.Begin();
            }
        }










    }
}
