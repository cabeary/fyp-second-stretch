﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrokenLands
{
    class PlayerInventory : Inventory
    {
        List<Item> inactiveList = new List<Item>();

        /// <summary>
        /// Sets new Dungeon List of items
        /// </summary>
        /// <param name="newList"></param>
        public void SetInactiveList(List<Item> newList)
        {
            //FoodSupply = dungeonInventory.FoodSupply
            inactiveList = newList;
            for (int i = 0; i < activeList.Count; i++)
            {
                if (!newList.Contains(activeList[i]))
                    inactiveList.Add(activeList[i]);
            }
        }

        /// <summary>
        /// Returns inactive list. Used to set dungeon's new itemList when exiting a dungeon
        /// </summary>
        /// <returns></returns>
        public List<Item> GetInactiveList()
        {
            return inactiveList;
        }

        /// <summary>
        /// Returns item from dungeon. Usable with Name, ID or Row
        /// </summary>
        /// <param name="itemID"></param>
        /// <returns></returns>
        public Item GetDungeonItem(int itemID)
        {
            Item toBeReturned = null;
            for (int i = 0; i < inactiveList.Count; i++)
            {
                if (inactiveList[i].id == itemID)
                {
                    toBeReturned = inactiveList[i];
                    break;
                }
            }
            return toBeReturned;
        }
        /*public Item GetDungeonItem(int itemRow)
        {
            Item toBeReturned = null;
            for (int i = 0; i < inactiveList.Count; i++)
            {
                if (inactiveList[i].row == itemRow)
                {
                    toBeReturned = inactiveList[i];
                    break;
                }
            }
            return toBeReturned;
        }*/
        public Item GetDungeonItem(String Name)
        {
            Item toBeReturned = null;
            for (int i = 0; i < inactiveList.Count; i++)
            {
                if (inactiveList[i].name == Name)
                {
                    toBeReturned = inactiveList[i];
                    break;
                }
            }
            return toBeReturned;
        }

        /// <summary>
        /// Removes item from Inactive List, returns said item
        /// </summary>
        /// <param name="itemID"></param>
        /// <returns></returns>
        public Item RemoveDungeonItem(int itemID)
        {
            Item toBeReturned = null;
            for (int i = 0; i < inactiveList.Count; i++)
            {
                if (inactiveList[i].id == itemID)
                {
                    toBeReturned = inactiveList[i];
                    inactiveList.RemoveAt(i);
                    break;
                }
            }
            return toBeReturned;
        }
        /*public Item RemoveDungeonItem(int itemRow)
        {
            Item toBeReturned = null;
            for (int i = 0; i < inactiveList.Count; i++)
            {
                if (inactiveList[i].row == itemRow)
                {
                    toBeReturned = inactiveList[i];
                    inactiveList.RemoveAt(i);
                    break;
                }
            }
            return toBeReturned;
        }*/
        public Item RemoveDungeonItem(String Name)
        {
            Item toBeReturned = null;
            for (int i = 0; i < inactiveList.Count; i++)
            {
                if (inactiveList[i].name == Name)
                {
                    toBeReturned = inactiveList[i];
                    inactiveList.RemoveAt(i);
                    break;
                }
            }
            return toBeReturned;
        }
    }
}
