﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace BrokenLands
{
    class Weapon : Equipment
    {
        //public float hands;
        
        //public Texture2D texture;
        public Vector2 pos;
        public WeaponType wType = WeaponType.LightRanged;
        
        public Weapon(Weapon w) {
            apCost = w.apCost;
            baseAccuracy=w.baseAccuracy;
            baseAttack =  w.baseAttack;
            baseDamage=w.baseDamage;
            baseDefence=w.baseDefence;
            //baseEvade = w.baseEvade;
            baseSpeed = w.baseSpeed;
            baseWeight = w.baseWeight;
            //this.counterRate = w.counterRate;
            this.desc = w.desc;
            this.hitCount = w.hitCount;
            this.itemID = w.itemID;
            this.name = w.name;
            this.pos = w.pos;
            this.postfix = w.postfix;
            this.prefix = w.prefix;
            this.shopValue = w.shopValue;
            this.spritesheet_t = w.spritesheet_t;
            this.sprite_t = w.sprite_t;
            this.tex = w.tex;
            this.type = w.type;
            this.range = w.range;
        }
        public enum WeaponType { 
            LightMelee,
            HeavyMelee,
            LightRanged,
            HeavyRanged
        }
        public Weapon(float attack, float def, float magic, float weight, string name)
            : base(EquipType.Weapon,attack,def,magic, weight,name)
        {
            //this.hands = hands;
        }
        public Weapon() { 
        
        }
        public int range = 1;
        public int baseSpeed;
       // public int counterRate;
        public int baseDamage;
        public int hitCount;
        public int apCost;
        public int baseAccuracy;
       // public int baseEvade;

    }
}
