﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BrokenLands;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace BrokenLands
{
    //Used for gamescreen buttons only
    class UIButtons : UIElement
    {
        Vector2 scale = new Vector2(2, 0.1f);
        float opacity,initOpacity;
        bool clickableBool;
        MouseState prevMouseState;
        public UIButtons(Texture2D texture, Vector2 Pos,float opa,bool click)
            :base()
        {
            base.texture = texture;
            position = Pos;
            opacity = opa;
            initOpacity = opacity;
            clickableBool = click;
        }
        public override void Draw(SpriteBatch spritebatch)
        { 
            spritebatch.Draw(base.texture, position, new Rectangle(0, 0, base.texture.Width, base.texture.Height), Color.White *opacity);

        }
        public void Update(GameTime gameTime, GameSession gs)
        {
            Rectangle rect = new Rectangle(Mouse.GetState().X, Mouse.GetState().Y, 20, 20);

            if (gs.unitTakingTurn)
            {
                if (rect.Intersects(new Rectangle((int)position.X, (int)position.Y, base.texture.Width, base.texture.Height)))
                {
                    if (opacity != 1f)
                    {
                        opacity = 1f;
                    }
                    if (clickableBool)
                    {
                        if (Mouse.GetState().LeftButton == ButtonState.Pressed)
                        {
                            if (prevMouseState.LeftButton != ButtonState.Pressed)
                            {
                                base.texture = gs.endTurnButtonPressed;
                                gs.unitTakingTurn = false;
                            }
                            else
                                base.texture = gs.endTurnButton;
                        }
                    }

                }
                else
                {
                    opacity = initOpacity;
                    if (clickableBool)
                    {
                        if (prevMouseState.LeftButton != ButtonState.Pressed)
                            base.texture = gs.endTurnButton;
                    }
                }

            }
            prevMouseState = Mouse.GetState();
        }
    }
}
