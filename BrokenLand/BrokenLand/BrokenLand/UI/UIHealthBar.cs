﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
using GameStateManagement;
using BrokenLands;

namespace BrokenLands
{
    class UIHealthBar : UIElement
    {
        public static Texture2D tex;
        Vector2 scale = new Vector2 (2,0.1f);
        Vector2 offset;
        Unit unit;
        float hpMultiplier = 10;
        public UIHealthBar(GameSession gs, Unit u)
            : base()
        {
            unit = u;
            position = u.getPosition();
            active = unit.IsAlive;
        }


        public override void Draw(SpriteBatch spritebatch)
        {
            if (unit.MyType == Entity.uType.Enemy)
                spritebatch.Draw(GameSession.healthRed, position, new Rectangle(0, 0, (int)(GameSession.healthRed.Width / 10 * hpMultiplier), GameSession.healthRed.Height/10), Color.White, 0f, Vector2.Zero, 0.3f, SpriteEffects.None, 0.0f);
            else
                spritebatch.Draw(GameSession.healthGreen, position, new Rectangle(0, 0, (int)(GameSession.healthGreen.Width / 10 * hpMultiplier), GameSession.healthGreen.Height / 10), Color.White, 0f, Vector2.Zero, 0.3f, SpriteEffects.None, 0.0f);
          
        }
        public override void Update(GameTime gameTime, GameSession gs)
        {
            if (!unit.IsAlive||GameSession.map.isInZone(unit.MyLocation,GameSession.combatZone)) {
                active = false;
            }
            
                position = unit.getPosition();

                hpMultiplier = (unit.health / unit.MyStatus.calcHP()) * 10 * (1 + unit.MyStatus.calcHP()/50);
            
        }
    }
}
