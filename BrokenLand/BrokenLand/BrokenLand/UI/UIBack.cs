﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BrokenLands;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace BrokenLands
{
    //UI Elements that are only there for aesthetic and has no interactions
    class UIBack : UIElement
    {
        public Texture2D tex;
        public Rectangle rect;
        float scale;
        public bool isHidden;

        public UIBack(Texture2D texture, Vector2 Pos,float scale)
            :base()
        {
            tex = texture;
            position = Pos;
            this.scale = scale;
            isHidden = false;
            rect = new Rectangle(tex.Bounds.X, tex.Bounds.Y, tex.Bounds.Width, tex.Bounds.Height);
        }
        public override void Draw(SpriteBatch spritebatch)
        {
            if (!isHidden)
            {
                spritebatch.Draw(tex, position, new Rectangle(0, 0, tex.Width, tex.Height), Color.White, 0f, Vector2.Zero, scale, SpriteEffects.None, 1f);
            }
        }
    }
}
