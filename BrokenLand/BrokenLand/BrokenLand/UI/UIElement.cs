﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
using GameStateManagement;
using BrokenLands;

namespace BrokenLands
{
    class UIElement
    {
        Dictionary<string, Texture2D> textureDict = new Dictionary<string, Texture2D>();
        public Texture2D texture;
        public static SpriteFont font;
        public static CameraObj cam;
        public static Viewport viewport;
        public Vector2 position;
        protected float speed = 0.5f;
        protected float _TIMER = 3;
        protected float countup = 0;
        public bool active = false;
        public static GraphicsDevice graphics;
        protected string text;
        protected Color textColor;

        public UIElement(float lifetime, Vector2 position)
        {
            _TIMER = lifetime;
            active = true;
            this.position = position;
        }
        public UIElement(Vector2 position)
        {
            active = true;
            this.position = position;
        }
        public UIElement() {
            active = true;
        }
        public virtual void Update(GameTime gameTime, GameSession gs)
        {
            if (active)
            {
                countup += (float)gameTime.ElapsedGameTime.TotalSeconds;
                if (countup > _TIMER)
                {
                    active = false;
                }
            }


        }
        public virtual void Draw(SpriteBatch spritebatch)
        {
            if (active)
                spritebatch.DrawString(font, text, position, textColor);

        }
        public virtual void Draw(SpriteBatch spritebatch,SpriteFont spritefont)
        {
            if (active)
                spritebatch.DrawString(font, text, position, textColor);

        }

    }
}
