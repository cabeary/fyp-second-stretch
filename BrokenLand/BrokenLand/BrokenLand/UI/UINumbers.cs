﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
using GameStateManagement;
using BrokenLands;

namespace BrokenLands
{
    class UINumbers : UIElement
    {

        float alpha = 1.0f;

        public UINumbers(float lifetime, string text, Color color,Vector2 position):base(lifetime, position){
            this.text = text;
            textColor = color;
            speed = 0.5f;
        }

        public UINumbers(float lifetime, string text, Color color, Unit u):base (lifetime,u.getPosition()-new Vector2(0,100))
        {
            this.text = text;
            textColor = color;
            speed = 0.5f;
        }

        public override void Update(GameTime gameTime, GameSession gs){
            if (active){
                position.Y -= speed;
                alpha = 1 - (countup / _TIMER);
                countup+=(float)gameTime.ElapsedGameTime.TotalSeconds;
                if (countup > _TIMER){
                    active = false;
                    
                }
            }
        }
        public override void Draw(SpriteBatch spritebatch){
            if (active)
                spritebatch.DrawString(font,text,position,textColor*alpha);

        }


    }
}
