﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BrokenLands;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace BrokenLands
{
    //UI Elements that are only there for aesthetic and has no interactions
    class UIBackText : UIElement
    {
        public Texture2D tex;
        Vector2 textPos;
        float scale;
        Color colour;

        public UIBackText(Texture2D texture, Vector2 Pos, float scale,string text,Color color)
            :base()
        {
            tex = texture;
            base.text = text;
            position = Pos;
            this.scale = scale;
            this.colour = color;
        }
        public override void Draw(SpriteBatch spritebatch,SpriteFont spriteFont)
        {
            spritebatch.Draw(tex, position, new Rectangle(0, 0, tex.Width, tex.Height), Color.White,0f,Vector2.Zero,scale,SpriteEffects.None,0f);
            //spritebatch.DrawString(spriteFont, base.text, textPos, colour);
        }
    }
}
