﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
using GameStateManagement;
using BrokenLands;

namespace BrokenLands
{
    class UIDmgForm : UIElement
    {
        public static Texture2D allyFrame, enemyFrame;
        Texture2D border;
        Vector2 offset = Vector2.Zero;
        Vector2 frameOffset = Vector2.Zero;
        Color borderColor = Color.Blue;
        float spacing;
        float scale = 0.2f;
        public List<Unit> unitList = new List<Unit>();
        int numberOfUnits;

        public UIDmgForm(GameSession gs)
            : base()
        {
            base.position = new Vector2(75, UIElement.viewport.TitleSafeArea.Top + UIElement.viewport.Height / 15);
            spacing = (allyFrame.Width / 3 * 2) * scale;
            frameOffset.X = 5;
            frameOffset.Y = 5;
            border = gs.borderframe;
        }


        public override void Draw(SpriteBatch spritebatch)
        {
            if (active)
            {
                numberOfUnits = unitList.Count;
                int i = 0;
                foreach (Unit u in unitList)
                {
                    offset.X = i * spacing;
                    Texture2D frame;
                    if (u.MyType == Entity.uType.Player)
                    {
                        frame = allyFrame;

                    }
                    else
                    {
                        frame = enemyFrame;
                    }
                    if (i != 0)
                    {
                        offset.X = offset.X + 65;
                        if (i == 1)
                        {
                            offset.X = offset.X + 0;
                        }

                        if (i < 6)
                        {
                            spritebatch.Draw(frame, position + offset, new Rectangle(0, 0, frame.Width, frame.Height), Color.White, 0f, Vector2.Zero, scale * 0.55f, SpriteEffects.None, 0.0f);
                            spritebatch.Draw(u.frame, position + offset + frameOffset, new Rectangle(0, 0, u.frame.Width, u.frame.Height), Color.White, 0f, Vector2.Zero, 0.5f * 0.5f, SpriteEffects.None, 0.0f);
                        }

                        i++;
                        if (i > 8)
                        {
                            continue;
                        }
                    }
                    else
                    {
                        //spritebatch.Draw(frame, position + offset + new Vector2(0,5), new Rectangle(0, 0, frame.Width, frame.Height), Color.White, 0f, Vector2.Zero, scale*1.1f, SpriteEffects.None, 0.0f);
                        spritebatch.Draw(u.frame, position + offset + frameOffset + new Vector2(0, 5), new Rectangle(0, 0, u.frame.Width, u.frame.Height), Color.White, 0f, Vector2.Zero, 0.5f * 1.1f, SpriteEffects.None, 0.0f);

                        i++;
                        if (i > 8)
                        {
                            continue;
                        }

                        //spritebatch.Draw(border, position + frameOffset + new Vector2(0, 5), new Rectangle(0, 0, border.Width, border.Height), borderColor, 0f, Vector2.Zero, 0.5f * 1.1f, SpriteEffects.None, 0.0f);
                    }
                }
            }
        }

        public override void Update(GameTime gameTime, GameSession gs)
        {
            unitList = gs.GetAliveUnitsList();
            if (gs.currentUnit.MyType == Entity.uType.Player)
            {
                borderColor = Color.CadetBlue;
            }
            else
            {
                borderColor = Color.DarkRed;
            }
        }
    }
}
