﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BrokenLands;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace BrokenLands
{
    //UI Elements that are only there for aesthetic and has no interactions
    class UIOpacity : UIElement
    {
        public Texture2D tex;
        public float opacity, initOpacity;
        float scale,rotation;

        public UIOpacity(Texture2D texture, Vector2 Pos, float scale,float opacity,float rotation)
            :base()
        {
            tex = texture;
            position = Pos;
            this.opacity = opacity;
            this.scale = scale;
            this.rotation = rotation;
        }
        public override void Draw(SpriteBatch spritebatch)
        {
            spritebatch.Draw(tex, position, new Rectangle(0, 0, tex.Width, tex.Height), Color.White*opacity,MathHelper.ToRadians(rotation),new Vector2(tex.Width/2,tex.Height/2),scale,SpriteEffects.None,0f);
        }
    }
}
