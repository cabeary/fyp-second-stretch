﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using Supernova.Particles2D;
using Supernova.Particles2D.Modifiers.Alpha;
using Supernova.Samples.Windows.Utilities;
using GameStateManagement;
using System.Threading;
using Microsoft.Xna.Framework.Content;
using BrokenLands;
using System.Text.RegularExpressions;


namespace BrokenLands
{
    public class StoryScreen : GameScreen
    {
        ContentManager content;
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SpriteFont spritefont, InconsolataFont;

        MouseState prevMouseState, currentMouseState;

        Rectangle MouseRect;

        float FrameCounter, frameRate;

        string[] storyStringArray;
        
        MouseController mousecontroller = new MouseController();
        string test;
        public StoryScreen()
            : base()
        {
            
            this.ScreenState = ScreenState.Active;

        }



        public override void Activate(bool instancePreserved)
        {
            if (!instancePreserved)
            {
                if (content == null)
                    content = new ContentManager(ScreenManager.Game.Services, "Content");

                //TestCamera = new CameraObj(this, new Vector3(0, 0, 0), new Vector3(0, 0, 10), Vector3.Up, 1f, 1000f, 1f);
                FrameCounter = 0;
                prevMouseState = Mouse.GetState();
                currentMouseState = Mouse.GetState();

                //Loading
                spriteBatch = new SpriteBatch(ScreenManager.GraphicsDevice);
                Texture2D defaultPointer = content.Load<Texture2D>("pointer");
                mousecontroller.setTextures(defaultPointer);

                //Textures
                

                //Icons
                

                //Fonts
                spritefont = content.Load<SpriteFont>("Times12");
                InconsolataFont = content.Load<SpriteFont>("Inconstella");

                //Variables
                story


                MouseRect = new Rectangle(Mouse.GetState().X, Mouse.GetState().Y, 10, 10);//Used for mouse Collision
                
                
                ScreenManager.Game.ResetElapsedTime();
                ScreenManager.fader.FadeInNOutFrom0fImage(2000, 0.5f);
            }
        }

        //Every activate must have a deactivate
        public override void Deactivate()
        {
            
            base.Deactivate();
            
        }

        public static string SpliceText(string text, int lineLength)
        {
            return Regex.Replace(text, "(.{" + lineLength + "})", "$1" + Environment.NewLine);
        }
        /// <summary>
        /// Unload graphics content used by the game.
        /// </summary>
        public override void Unload()
        {
            content.Unload();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
            bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                ScreenManager.Game.Exit();
            
            mousecontroller.MouseUpdate();

            //Rectangle[] prevLocations = Locations;
            currentMouseState = Mouse.GetState();

            //Update Mouse Position
            MouseRect.X = currentMouseState.X;
            MouseRect.Y = currentMouseState.Y;

            // Reset prevMouseState

            
            if (ScreenManager.fader.checkFadeOver())
            {
                ScreenManager.AddScreen(new WorldMapScreen(),null);
                ExitScreen();
            }
            
            prevMouseState = Mouse.GetState();

        }
        
        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Draw(GameTime gameTime)
        {


            ScreenManager.FadeBackBufferToBlack(TransitionAlpha * 2 / 3);

            ScreenManager.GraphicsDevice.BlendState = BlendState.Opaque;
            ScreenManager.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            //NewParticle.Draw(gameTime, TestCamera);
            frameRate = (float)(1 / gameTime.ElapsedGameTime.TotalSeconds);



            float width = ScreenManager.GraphicsDevice.Viewport.Width;
            float scale = (width / 1920);
            Matrix scalingMatrix = Matrix.CreateScale(scale);
            spriteBatch.Begin(SpriteSortMode.Immediate, null, null, null, null, null, scalingMatrix);
            
            spriteBatch.End();
            spriteBatch.Begin();
            spriteBatch.DrawString(InconsolataFont, SpliceText(test, 10), new Vector2(50, 50), Color.White);
            //mousecontroller.MouseDraw(spriteBatch);
            spriteBatch.End();


            base.Draw(gameTime);
        }

    }
}

