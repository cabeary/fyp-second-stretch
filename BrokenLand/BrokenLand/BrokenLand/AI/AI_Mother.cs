﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
namespace BrokenLands
{
    class AI_Mother
    {
        Random _random;

        List<CombatActions> acts;
        public AI_Mother()
        {

            _random = new Random();
            acts = new List<CombatActions>();

        }
        int actScore = 0;
        public void Think(Enemy enem, GameSession gs)
        {
            List<List<CombatActions>> acts = GameSession._combat.GetCombatActionsByLocation(enem, enem.MyLocation);
            actScore = 0;
            //print out all possible actions
            /*
            for (int i = 0; i < acts.Count(); i++)
            {
                foreach (CombatActions a in acts[i])
                {
                    a.PrintAction();
                }
            }*/
            if (acts.Count > 0)
            {
                //attack without moving if possible
                if (acts[1].Count > 0)
                {
                    enem.QueueAction(acts[1][_random.Next(acts[1].Count - 1)]);
                }
                else
                {
                    //move to the closest attack target and attack it

                    List<MapCell> attackTargets = new List<MapCell>();//_pather.getPossibleAttackLocations(enem.MyLocation, enem);


                    // find all players
                    /*
                    foreach (MapCell mc in GameSession.map.gridNodes)
                    {
                        if (mc.Ent != null)
                        {
                            if (mc .Ent.MyType == Entity.uType.Player)
                            {
                                attackTargets.Add(mc);
                            }
                        }
                    }*/
                    foreach (Player p in GameSession.map.playerList) {
                        attackTargets.Add(p.MyLocation);
                    }

                    //attackTargets = _pather.getPossibleMoveAttackLocations(enem);
                    int lowMoveCost = 100;
                    List<MapCell> path = new List<MapCell>();

                    int i = -1;
                    int closestTarget = 0;
                    //look for the closest player
                    foreach (MapCell mc in attackTargets)
                    {
                        i++;
                        int moveCost;
                        List<MapCell> tempPath, tempAttackLocations;
                        // get nodes that i want to go to
                        tempAttackLocations = GameSession._pather.getTargetPossibleAttackLocations(mc, enem);

                        foreach (MapCell m in tempAttackLocations)
                        {
                            //tempPath = _pather.getPath(enem.MyLocation, m, out moveCost);

                            tempPath = GameSession._pather.getPath(enem.MyLocation, m);
                            if (tempPath.Count > 0)
                                moveCost = GameSession._pather.CalcPathCost(tempPath);
                            else moveCost = 100;
                            if (moveCost < lowMoveCost)
                            {
                                path = tempPath;
                                lowMoveCost = moveCost;
                                closestTarget = i;
                            }
                        }

                        //tempPath = _pather.getPath(mc,enem.MyLocation);
                        //moveCost = _pather.CalcPathCost(tempPath);



                    }

                    path = GameSession._pather.TrimPath(path, enem.MyStatus.CurrentAP, enem);
                    foreach (CombatActions a in acts[0])
                    {
                        if (path.Count > 0)
                        {
                            if (path[0] == a.Target)
                            {
                                if (GameSession.GS.CardCondition != -1 && GameSession.GS.CurrentCard != null)
                                {
                                    if ((!GameSession.GS.CurrentCard.isEvent) && (GameSession.GS.CurrentCard.EffectType == 2))
                                    {
                                        GameSession.GS.CardCondition--;
                                        if (GameSession.GS.CardCondition == 0)
                                        {
                                            GameSession.GS.CurrentCard.SpawnEnem(GameSession.GS.CurrentCard.Severity / 2.0f, GameSession.GS.CurrentCard.SpawnZone);
                                        }
                                    }
                                }
                                enem.QueueAction(a);
                            }
                        }
                        else
                        {
                            enem.QueueAction(acts[(int)CombatActions.ActionType.IDLE][0]);
                        }

                    }
                    /*
                    foreach (CombatActions a in acts[0])
                    {
                        for (int k = path.Count; k <= 0; k--) {
                            if (path[k] == a.Target) {

                                _combatEngine.PerformAction(a);
                            }
                    
                        }
                    
                 
                    }*/


                }
            }
            //gs.unitTakingTurn = false;
            


            // _combatEngine.PerformAction(acts[1][2]);
            //_combatEngine.PerformMeleeAttackAction(players[0],players[1]);
            //_pather.GetPossibleMoveLocations(players[2],map);
        }

    }
}
