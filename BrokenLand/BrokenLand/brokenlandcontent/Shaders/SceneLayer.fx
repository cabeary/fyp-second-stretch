sampler TextureSampler : register(s0);
float2 MousePosition;
int Width;
int Height;



float4 PixelShaderFunction(float2 texCoord : TEXCOORD0) : COLOR0
{
    float4 color = tex2D(TextureSampler, texCoord);

	float total = color.x+color.y+color.z;

	float2 mouse = float2(0,0);

	mouse.x = MousePosition.x/Width;
	mouse.y = MousePosition.y/Height;

	float dist = distance(texCoord,mouse); //closer lower opacity

	if(total==0){
	return float4(0,0,0,0);
	}

	float fade;
	fade = dist * 7;

	if(fade<1)
	return float4(color.x * fade,color.y * fade,color.z * fade,1 *fade);

	else
	return color;
}


//pointlight style transparency
	//if(dist>0.1)//controls the radius of the circle
	//return color;

	//float fade;
	//fade = dist * 0.1;

	//return float4(color.x * fade,color.y * fade,color.z * fade,1 *fade);

technique SceneLayer
{
    pass Pass1
    {
        PixelShader = compile ps_2_0 PixelShaderFunction();
    }
}