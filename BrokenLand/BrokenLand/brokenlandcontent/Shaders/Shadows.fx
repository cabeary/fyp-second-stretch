sampler TextureSampler : register(s0);
float2 PlayerPosition;
int Width;
int Height;



float4 PixelShaderFunction(float2 texCoord : TEXCOORD0) : COLOR0
{
    float4 color = tex2D(TextureSampler, texCoord);

	float2 playerpos = float2(0,0);

	playerpos.x = PlayerPosition.x/Width;
	playerpos.y = PlayerPosition.y/Height;

	float dist = distance(texCoord,playerpos); 


	float fade;
	fade = dist * 7;

	return float4(color.x/fade,color.y/fade,color.z/fade,1);
}


technique Shadow
{
    pass Pass1
    {
        PixelShader = compile ps_2_0 PixelShaderFunction();
    }
}