sampler TextureSampler : register(s0);
float HealthRatio;



float4 PixelShaderFunction(float2 texCoord : TEXCOORD0) : COLOR0
{
    float4 color = tex2D(TextureSampler, texCoord);

	//float total = color.x+color.y+color.z;


	if(color.x>HealthRatio){
	return float4(0,1,0,1);
	}
	else{
	return float4(0,0,0,0);
	}
}


technique CircleBar
{
    pass Pass1
    {
        PixelShader = compile ps_2_0 PixelShaderFunction();
    }
}
