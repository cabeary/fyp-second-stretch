sampler TextureSampler : register(s0);




float4 PixelShaderFunction(float2 texCoord : TEXCOORD0) : COLOR0
{
    float4 color = tex2D(TextureSampler, texCoord);

	float total = color.x+color.y+color.z;

	if(total>0){
	return color;
	}

	else{
	return float4(0,0,0,0);
	}
}


technique Layer
{
    pass Pass1
    {
        PixelShader = compile ps_2_0 PixelShaderFunction();
    }
}
