﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace GameStateManagement
{
    public class Fader
    {
        Texture2D image;
        //IS IN MILLISECONDS
        float maxSecIn;
        float screenFadeTime;
        public float Opacity = 1f;
        float minOpacity;
        float screenOpacity = 0f;
        bool Fading = false;
        bool prevFadingIN = false,prevFadingOUT;
        public bool fadeinNOut = false;
        bool fadeinonly = false;
        bool InOROut = false;
        bool fadeScreenIn = false;
        public bool fadeScreenOut = false;
        int currentFader = 0;
        public float delayTime, delayTimer;

        public Fader(Texture2D pic)
        {
            image = pic;
        }

        public void Update(GameTime gameTime, SpriteBatch spriteBatch, ScreenManager screen)
        {
            if (Fading)
            {
                switch(currentFader)
                {
                    case 1 : FadeIn(gameTime);
                        break;
                    case 2: FadeInNOut(gameTime);
                        break;
                    case 3: FadeInThenInNOut(gameTime);
                        break;
                    case 4: ScreenFadeIn(spriteBatch,screen,gameTime);
                        break;
                    case 5: ScreenFadeOut(spriteBatch, screen, gameTime);
                        break;
                    case 6: FadeInNOutTo0FWithDelay(gameTime,delayTime);
                        break;
                    default: break;
                }
            }
        }

        public void Draw(SpriteBatch spriteBatch,ScreenManager screen)
        {
            spriteBatch.Begin();
            spriteBatch.Draw(image, new Rectangle(0, 0, screen.GraphicsDevice.Viewport.Width, screen.GraphicsDevice.Viewport.Height), Color.Black * screenOpacity);
            spriteBatch.End();
        }


        public void FadeInScreen(float timeTofade)
        {
            Fading = true;
            prevFadingIN = true;
            fadeScreenIn = true;
            screenFadeTime = timeTofade;
            screenOpacity = 1f;
            currentFader = 4;
        }
        public void FadeOutScreen(float timeTofade)
        {
            Fading = true;
            prevFadingOUT = true;
            fadeScreenOut = true;
            screenFadeTime = timeTofade;
            screenOpacity = 0f;
            currentFader = 5;
        }
        public bool checkFadeOutOver()
        {
            if (prevFadingOUT == true && fadeScreenOut == false)
            {
                prevFadingOUT = false;
                return true;
            }
            return false;
        }
        public bool checkFadeInOver()
        {
            if (prevFadingIN == true && fadeScreenIn == false)
            {
                prevFadingIN = false;
                return true;
            }
            return false;
        }
        private void ScreenFadeIn(SpriteBatch spriteBatch, ScreenManager screen,GameTime gameTime)
        {
            if (fadeScreenIn)
            {
                if (screenOpacity >=0f)
                {
                    screenOpacity -= 1 * (gameTime.ElapsedGameTime.Milliseconds / screenFadeTime);
                    
                }
                else
                {
                    fadeScreenIn = false;
                    Fading = false;
                }
            }
            
        }
        private void ScreenFadeOut(SpriteBatch spriteBatch, ScreenManager screen, GameTime gameTime)
        {
            if (fadeScreenOut)
            {
                if (screenOpacity <=1f)
                {
                    screenOpacity += 1 * (gameTime.ElapsedGameTime.Milliseconds / screenFadeTime);

                }
                else
                {
                    fadeScreenOut = false;
                    Fading = false;
                }
            }

        }
        

        public void FadeInImage(float timeForFade)
        {
            Fading = true;
            fadeinonly = true;
            maxSecIn = timeForFade;
            Opacity = 0f;
            currentFader = 1;
        }
        public void FadeInNOutImage(float timeForFade, float minimumOpacity)
        {
            Fading = true;
            fadeinNOut = true;
            minOpacity = minimumOpacity;
            maxSecIn = timeForFade;
            Opacity = minimumOpacity;
            currentFader = 2;
        }
        public void FadeInNOutFrom0fImage(float timeForFade, float minimumOpacity)
        {
            Fading = true;
            fadeinonly = true;
            minOpacity = minimumOpacity;
            maxSecIn = timeForFade;
            Opacity = 0f;
            currentFader = 3;
        }
        public void FadeInNOutTo0f(float timeForFade, float minimumOpacity,float delay)
        {
            Fading = true;
            fadeinNOut = true;
            minOpacity = minimumOpacity;
            maxSecIn = timeForFade;
            Opacity = minimumOpacity;
            delayTime = delay;
            currentFader = 6;
        }
        public void resetFader()
        {
            Fading = false;
            fadeinonly = false;
            fadeinNOut = false;
        }
        private void FadeIn(GameTime gameTime)
        {
            if (fadeinonly)
            {
                if (Opacity < 1f)
                {
                    Opacity += 1 * (gameTime.ElapsedGameTime.Milliseconds / maxSecIn);
                }
                else
                {
                    Fading = false;
                    fadeinonly = false;
                }
            }
        }
        private void FadeInThenInNOut(GameTime gameTime)
        {
            if (fadeinonly)
            {
                if (Opacity < 1f)
                {
                    Opacity += 1 * (gameTime.ElapsedGameTime.Milliseconds / maxSecIn);
                }
                else 
                {
                    fadeinonly = false;
                    fadeinNOut = true;
                    currentFader = 2;
                }
            }
        }
        private void FadeInNOut(GameTime gameTime)
        {
            if (fadeinNOut == true)
            {
                if (InOROut == false)
                {
                    Opacity += 1 * (gameTime.ElapsedGameTime.Milliseconds / maxSecIn);
                    if (Opacity >= 1f)
                    {
                        InOROut = !InOROut;
                    }
                }
                else 
                {
                     Opacity -= 1 * (gameTime.ElapsedGameTime.Milliseconds / maxSecIn);
                     if (Opacity <= minOpacity)
                     {
                         InOROut = !InOROut;
                     }
                }
            }
        }
        private void FadeInNOutTo0FWithDelay(GameTime gameTime,float delay)
        {
            if (fadeinNOut == true)
            {
                if (InOROut == false)
                {
                    float prevOpacity = Opacity;
                    Opacity += 1 * (gameTime.ElapsedGameTime.Milliseconds / maxSecIn);
                    if (Opacity >= 1f)
                    {
                        Opacity = prevOpacity;
                        delayTimer += gameTime.ElapsedGameTime.Milliseconds;
                        if (delayTimer >= delayTime)
                        {
                            InOROut = true;
                            delayTimer = 0;
                        }
                    }
                }
                else
                {
                    Opacity -= 1 * (gameTime.ElapsedGameTime.Milliseconds / maxSecIn);
                    if (Opacity <= minOpacity)
                    {
                        resetFader();
                        InOROut = false;
                    }
                }
            }
        }














    }
}
