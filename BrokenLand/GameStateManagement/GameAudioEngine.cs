﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace GameStateManagement
{
    class GameAudioEngine
    {
        //Music
        AudioEngine audioEngine;
        SoundBank soundBank;
        WaveBank waveBank;
        WaveBank musicWaveBank;

        List<Cue> soundInPlay;
        List<bool> soundInPlayLoop;
        public Cue musicCue;
        List<String> soundListStr;
        List<String> musicListStr;
        Dictionary<String, Audio> soundDictionary;
        Dictionary<String, Audio> musicDictionary;

        float soundVolume;
        float musicVolume;
        float currentMusicVolume;
        AudioCategory soundCategory;
        AudioCategory musicCategory;

        string newMusicTitle;
        string oldMusicTitle;

        bool playingMusic;
        bool restartingMusic;
        bool fadingMusic;
        bool musicIsPlaying;
        bool fadeComplete;
        bool forceStop;

        float elapsedTime;
        float fadeTime;

        /// <summary>
        /// Creates a empty GameAudioEngine (Has no sounds or musics)
        /// </summary>
        public GameAudioEngine()
        {
            List<String> toAddSounds = new List<String>();
            List<String> toAddMusics = new List<String>();
            toAddSounds.Add("Explosion");
            toAddSounds.Add("GorillaRoar");
            toAddSounds.Add("FootstepsHeavy");
            toAddSounds.Add("FootstepsLight");
            toAddSounds.Add("GunFire");
            toAddSounds.Add("Miss");
            toAddMusics.Add("Ambience");
            toAddMusics.Add("TestMusic");
            toAddMusics.Add("WinMusic");
            toAddMusics.Add("BarrenTownMusic");
            toAddMusics.Add("MenuMusic");
            toAddMusics.Add("CinematicRumble");
            toAddMusics.Add("CaveAmbience");
            toAddMusics.Add("DefaultAmbience"); 
            toAddMusics.Add("SubwayAmbience");
            Initialize(toAddSounds, toAddMusics);
            audioEngine.Update();
        }

        /// <summary>
        /// Constructor to take in pre-defined lists
        /// </summary>
        /// <param name="soundNameList"></param>
        /// <param name="musicNameList"></param>
        public GameAudioEngine(List<String> soundNameList, List<String> musicNameList)
        {
            Initialize(soundNameList, musicNameList);
            audioEngine.Update();
        }

        void Initialize(List<String> soundList, List<String> musicList)
        {
            audioEngine = new AudioEngine("Content\\XACTAsgn.xgs");
            waveBank = new WaveBank(audioEngine, "Content\\Wave Bank.xwb");
            musicWaveBank = new WaveBank(audioEngine, "Content\\Music Wave Bank.xwb", 0, 4);
            soundBank = new SoundBank(audioEngine, "Content\\Sound Bank.xsb");
            soundInPlay = new List<Cue>();
            soundInPlayLoop = new List<bool>();

            soundVolume = 50;
            musicVolume = 50;
            currentMusicVolume = 0;
            //changedSoundVolume = false;
            //changedMusicVolume = false;

            playingMusic = false;
            restartingMusic = false;
            fadingMusic = false;
            fadeComplete = false;
            musicIsPlaying = false;
            forceStop = false;
            newMusicTitle = "";
            oldMusicTitle = "";
            fadeTime = 3;

            musicCategory = audioEngine.GetCategory("Music");
            soundCategory = audioEngine.GetCategory("Sound");
            musicCategory.SetVolume(MathHelper.Clamp(musicVolume / 50, 0, 2.0f));
            soundCategory.SetVolume(MathHelper.Clamp(soundVolume / 50, 0, 2.0f));

            audioEngine.Update();
            setAudioList(soundList, musicList);

            elapsedTime = 0;
        }

        public void Update(GameTime gameTime)
        {
            //elapsedTime += (int)gameTime.ElapsedGameTime.TotalMilliseconds;

            for (int i = 0; i < soundInPlay.Count; i++)
            {
                if (soundInPlay[i].IsStopped)
                {
                    //Console.WriteLine(soundInPlay[i].Name + "'s Loop: " + soundInPlayLoop[i]);
                    if (soundInPlayLoop[i] == false) //If sound not set to looping
                    {
                        soundInPlay.RemoveAt(i);
                        soundInPlayLoop.RemoveAt(i);
                        i--;
                    }
                    else
                    {
                        Cue replacementCue = soundBank.GetCue(soundInPlay[i].Name);
                        replacementCue.Play();
                        soundInPlay[i] = replacementCue;
                    }
                }
            }

            if (forceStop)
            {
                musicCue.Stop(AudioStopOptions.Immediate);
                musicCue = soundBank.GetCue(newMusicTitle);
                musicCue.Play();
                forceStop = false;
            }

            if (fadingMusic)
            {
                if (currentMusicVolume == 0)
                {
                    musicCue.Stop(AudioStopOptions.Immediate);
                    fadingMusic = false;
                    musicIsPlaying = false;
                }
                else
                {
                    fadeOut();
                }
            }
            else if (restartingMusic)
            {
                if (!fadeComplete)
                {
                    fadeOut();
                    if (currentMusicVolume == 0) //When fade out completely
                    {
                        musicCue.Stop(AudioStopOptions.Immediate);
                        musicCue = soundBank.GetCue(newMusicTitle);
                        musicCue.Play();
                        fadeComplete = true;
                    }
                }
                else//FadeOut Completed
                {
                    fadeIn();
                    if (currentMusicVolume == musicVolume) //When finish fading in
                    {
                        //Reset values
                        oldMusicTitle = newMusicTitle;
                        fadeComplete = false;
                        restartingMusic = false;
                    }
                }
            }
            else if (playingMusic)
            {
                if (!fadeComplete)
                {
                    if (currentMusicVolume == 0)
                    {
                        //Stops music if cue exists
                        if (musicCue != null)
                            musicCue.Stop(AudioStopOptions.Immediate);
                        musicCue = soundBank.GetCue(newMusicTitle);
                        musicCue.Play();
                        fadeComplete = true;
                    }
                    else
                        fadeOut();
                }
                else
                {
                    fadeIn();
                    if (currentMusicVolume == musicVolume) //When finish fading in
                    {
                        oldMusicTitle = newMusicTitle;
                        fadeComplete = false;
                        playingMusic = false;
                    }
                }
            }
            musicCategory.SetVolume(MathHelper.Clamp(currentMusicVolume / 50, 0, 2.0f));
            audioEngine.Update();
        }

        /// <summary>
        /// Fading in and out controlled by duration of fades
        /// </summary>
        private void fadeOut()
        {
            if (currentMusicVolume > 0)
            {
                if (elapsedTime % (fadeTime * 200 / musicVolume) == 0) //Spread fading into fadeTime
                {
                    currentMusicVolume--;
                }
            }
        }
        private void fadeIn()
        {
            if (currentMusicVolume < musicVolume)
            {
                if (elapsedTime % (fadeTime * 200 / musicVolume) == 0) //Spread fading into fadeTime
                {
                    currentMusicVolume++;
                }
            }
        }

        /// <summary>
        /// Function to play sound (parse in sound's name and looping if you want). Default is PlayOnce
        /// </summary>
        /// <param name="soundName"></param>
        public void playSound(string audioName)
        {
            Audio audio;
            if (soundDictionary.TryGetValue(audioName, out audio))
            {
                Cue cue = soundBank.GetCue(audioName);
                cue.Play();
                soundInPlay.Add(cue);
                soundInPlayLoop.Add(false);
            }
        }
        public void playSound(string audioName, bool looping)
        {
            Audio audio;
            if (soundDictionary.TryGetValue(audioName, out audio))
            {
                Cue cue = soundBank.GetCue(audioName);
                cue.Play();
                soundInPlay.Add(cue);
                soundInPlayLoop.Add(looping);
            }
        }

        /// <summary>
        /// Function to play Music (parse in music name)
        /// </summary>
        /// <param name="audioName"></param>
        public void playMusic(string audioName)
        {
            //System.Diagnostics.Debug.WriteLine("playMusic " + audioName);
            //System.Diagnostics.Debug.WriteLine("musIsP {0} playingMusic {1} forceStop {2}", musicIsPlaying, playingMusic, forceStop);

            Audio audio;
            if (musicDictionary.TryGetValue(audioName, out audio))
            {
                if (!musicIsPlaying)
                {
                    newMusicTitle = audioName;
                    playingMusic = true;
                    musicIsPlaying = true;
                    if (currentMusicVolume != 0)
                    {
                        forceStop = true;
                    }
                }
                else //If music already playing
                {
                    newMusicTitle = audioName;
                    restartingMusic = true;
                    if (currentMusicVolume != musicVolume)
                    {
                        forceStop = true;
                    }
                }
            }
        }

        /// <summary>
        /// Function to play Audio, with Override function to stop all other sounds/music
        /// </summary>
        /// <param name="audioName"></param>
        /// <param name="isOverride"></param>
        public void playAudioOverride(string audioName, bool isOverride)
        {
            if (isOverride)
                stopAllAudio();

            Audio audio;
            if (soundDictionary.TryGetValue(audioName, out audio))
            {
                playSound(audioName);
            }
            else if (musicDictionary.TryGetValue(audioName, out audio))
            {
                playMusic(audioName);
            }
        }

        /// <summary>
        /// Forces a stop on audio (using String name)
        /// </summary>
        /// <param name="audioName"></param>
        public void stopAudio(String audioName)
        {
            bool isFound = false;
            bool isMusic = false;
            Audio newAudio;

            //If audio is Music, check if it matches current Cue.
            //If audio is Sound, checks if it exists.
            //If audio is not found, ignore
            if (musicDictionary.TryGetValue(audioName, out newAudio))
            {
                isMusic = true;
                if (musicCue.Name != null)
                {
                    if (musicCue.Name == audioName)
                    {
                        isFound = true;
                    }
                }
            }
            else if (soundDictionary.TryGetValue(audioName, out newAudio))
            {
                isFound = true;
            }

            if (isFound && isMusic)
            {
                fadingMusic = true;
                musicIsPlaying = false;
            }
            else if (isFound)
            {
                for (int i = 0; i < soundInPlay.Count; i++)
                {
                    if (soundInPlay[i].Name == audioName)
                    {
                        soundInPlay[i].Stop(AudioStopOptions.Immediate);
                        soundInPlayLoop[i] = false;
                    }
                }
            }
        }

        /// <summary>
        /// Forces a stop on audio (using String name)
        /// </summary>
        /// <param name="audioName"></param>
        public void stopAudio(String audioName, bool forceStop)
        {
            bool isFound = false;
            bool isMusic = false;
            Audio newAudio;
            if (musicDictionary.TryGetValue(audioName, out newAudio))
            {
                isMusic = true;
                if (musicCue.Name != null)
                {
                    if (musicCue.Name == audioName)
                    {
                        isFound = true;
                    }
                }
            }
            else if (soundDictionary.TryGetValue(audioName, out newAudio))
            {
                isFound = true;
            }

            if (isFound && isMusic)
            {
                musicIsPlaying = false;
                if (forceStop)
                {
                    musicCue.Stop(AudioStopOptions.Immediate);
                }
                else
                    fadingMusic = true;
            }
            else if (isFound)
            {
                if (forceStop)
                {
                    if (musicIsPlaying)
                    {
                        musicIsPlaying = false;
                        musicCue.Stop(AudioStopOptions.Immediate);
                    }
                }
                for (int i = 0; i < soundInPlay.Count; i++)
                {
                    if (soundInPlay[i].Name == audioName)
                    {
                        soundInPlay[i].Stop(AudioStopOptions.Immediate);
                        soundInPlayLoop[i] = false;
                    }
                }
            }
        }

        /// <summary>
        /// Stops all audio currently in play
        /// </summary>
        public void stopAllAudio()
        {
            if (musicIsPlaying)
            {
                stopAudio(musicCue.Name, true);
            }
            for (int i = 0; i < soundInPlay.Count; i++)
            {
                stopAudio(soundInPlay[i].Name);
                soundInPlayLoop[i] = false;
            }
        }

        /// <summary>
        /// Returns the music or sound list based on parameter of name
        /// </summary>
        /// <param name="listName"></param>
        /// <returns></returns>
        public List<String> getAudioList(string listName)
        {
            if (listName == "sound")
                return soundListStr;
            else //if (listName == "music")
                return musicListStr;
        }

        /// <summary>
        /// Returns the music or sound Dictionary based on parameter of name
        /// </summary>
        /// <param name="listName"></param>
        /// <returns></returns>
        public Dictionary<String, Audio> getAudioDictionary(string dictionaryName)
        {
            if (dictionaryName == "sound")
                return soundDictionary;
            else //if (listName == "music")
                return musicDictionary;
        }

        /// <summary>
        /// Sets the sound and music lists using their names in the XACT file
        /// </summary>
        /// <param name="soundList"></param>
        /// <param name="musicList"></param>
        public void setAudioList(List<String> soundList, List<String> musicList)
        {
            //Resets Lists & Dictionaries
            musicDictionary = new Dictionary<string, Audio>();
            soundDictionary = new Dictionary<string, Audio>();
            musicListStr = new List<String>();
            soundListStr = new List<String>();

            for (int i = 0; i < soundList.Count; i++)
            {
                soundListStr.Add(soundList[i]);
                Audio newSound = new Audio(soundList[i]);
                soundDictionary.Add(soundList[i], newSound);
            }

            for (int i = 0; i < musicList.Count; i++)
            {
                musicListStr.Add(musicList[i]);
                Audio newMusic = new Audio(musicList[i]);
                musicDictionary.Add(musicList[i], newMusic);
            }
        }

        /// <summary>
        /// Gets and sets FadeTime
        /// </summary>
        public float FadeTime
        {
            get { return fadeTime; }
            set { fadeTime = value; }
        }

        /*
        /// <summary>
        /// Gets and sets volume for Sound
        /// </summary>
        public float SoundVolume
        {
            get { return soundVolume; }
            set { 
                    soundVolume += value;
                    changedSoundVolume = true;
                }
        }

        /// <summary>
        /// Gets and sets volume for Music
        /// </summary>
        public float MusicVolume
        {
            get { return musicVolume; }
            set { 
                    musicVolume += value;
                    changedMusicVolume = true;
                }
        }
        */
    }
}
