﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameStateManagement;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameStateManagement
{
    public class Notifier
    {
        string headerText;
        string subText;
        Fader fader;
        SpriteFont spriteFont1, spriteFont2;
        Vector2 HPosition,BPosition;
        bool Active, FixedPos;

        public Notifier(Texture2D tex, SpriteFont spritefont1, SpriteFont spritefont2)
        {
            Active = false;
            fader = new Fader(tex);
            spriteFont1 = spritefont1;
            spriteFont2 = spritefont2;
        }

        public void setNewText(string header,string sub)
        {
            //Resets FixedPos just in case. Call setPosition to reactivate it
            FixedPos = false;
            headerText = header;
            subText = sub;
        }
        
        public void setNewText(string header, string sub, Vector2 HeaderPos, Vector2 BodyPos)
        {
            FixedPos = true;
            headerText = header;
            subText = sub;
            setPosition(HeaderPos, BodyPos);
        }

        private void setPosition(Vector2 NewHeaderPosition, Vector2 NewBodyPosition)
        {
            HPosition = NewHeaderPosition;
            BPosition = NewBodyPosition;
        }

        public void StartNotify()
        {
            Active = true;
            fader.FadeInNOutTo0f(1000, 0f, 5000);
        }

        public void Update(GameTime gametime,SpriteBatch spriteBatch,ScreenManager screen)
        {
            if (Active)
            {
                fader.Update(gametime, spriteBatch, screen);
                if (fader.Opacity <= 0)
                {
                    if (fader.fadeinNOut == false)
                    {
                        Active = false;
                    }
                }
            }
            
        }

        public void Draw(SpriteBatch spriteBatch,Viewport view)
        {
            if (Active && !FixedPos)
            {
                spriteBatch.Begin();
                //spriteBatch.DrawString(spriteFont1, headerText, new Vector2((int)(view.Width / 2 - spriteFont1.MeasureString(headerText).X / 2)+1,
                //    (int)(view.Height / 7)+1), (Color.DarkSlateGray*0.5f)*fader.Opacity);
                //spriteBatch.DrawString(spriteFont1, headerText, new Vector2((int)(view.Width / 2 - spriteFont1.MeasureString(headerText).X / 2)+1,
                //    (int)(view.Height / 7) - 1), (Color.DarkSlateGray * 0.5f) * fader.Opacity);
                //spriteBatch.DrawString(spriteFont1, headerText, new Vector2((int)(view.Width / 2 - spriteFont1.MeasureString(headerText).X / 2)-1,
                //    (int)(view.Height / 7) + 1), (Color.DarkSlateGray * 0.5f) * fader.Opacity);
                //spriteBatch.DrawString(spriteFont1, headerText, new Vector2((int)(view.Width / 2 - spriteFont1.MeasureString(headerText).X / 2)-1,
                //    (int)(view.Height / 7) - 1), (Color.DarkSlateGray * 0.5f) * fader.Opacity);
                spriteBatch.DrawString(spriteFont1, headerText, new Vector2((int)(view.Width / 2 - spriteFont1.MeasureString(headerText).X / 2),
                    (int)(view.Height / 7)), Color.White * fader.Opacity);
                //spriteBatch.DrawString(spriteFont2, subText, new Vector2((int)(view.Width / 2 - spriteFont2.MeasureString(subText).X / 2) + 1,
                //    (int)(view.Height / 4.5f) + 1), (Color.DarkSlateGray * 0.5f) * fader.Opacity);
                //spriteBatch.DrawString(spriteFont2, subText, new Vector2((int)(view.Width / 2 - spriteFont2.MeasureString(subText).X / 2) + 1,
                //    (int)(view.Height / 4.5f) - 1), (Color.DarkSlateGray * 0.5f) * fader.Opacity);
                //spriteBatch.DrawString(spriteFont2, subText, new Vector2((int)(view.Width / 2 - spriteFont2.MeasureString(subText).X / 2) - 1,
                //    (int)(view.Height / 4.5f) + 1), (Color.DarkSlateGray * 0.5f) * fader.Opacity);
                //spriteBatch.DrawString(spriteFont2, subText, new Vector2((int)(view.Width / 2 - spriteFont2.MeasureString(subText).X / 2) - 1,
                //    (int)(view.Height / 4.5f) - 1), (Color.DarkSlateGray * 0.5f) * fader.Opacity);
                spriteBatch.DrawString(spriteFont2, subText, new Vector2((int)(view.Width / 2 - spriteFont2.MeasureString(subText).X / 2),
                    (int)(view.Height / 4.5f)), Color.White * fader.Opacity);
                spriteBatch.End();
            }
            else if (Active) //And Fixed Position
            {
                spriteBatch.Begin();
                //spriteBatch.DrawString(spriteFont1, headerText, new Vector2((int)(HPosition.X - spriteFont1.MeasureString(headerText).X / 2) + 1,
                //    (int)(HPosition.Y) + 1), (Color.DarkSlateGray * 0.5f) * fader.Opacity);
                //spriteBatch.DrawString(spriteFont1, headerText, new Vector2((int)(HPosition.X - spriteFont1.MeasureString(headerText).X / 2) + 1,
                //    (int)(HPosition.Y) - 1), (Color.DarkSlateGray * 0.5f) * fader.Opacity);
                //spriteBatch.DrawString(spriteFont1, headerText, new Vector2((int)(HPosition.X - spriteFont1.MeasureString(headerText).X / 2) - 1,
                //    (int)(HPosition.Y) + 1), (Color.DarkSlateGray * 0.5f) * fader.Opacity);
                //spriteBatch.DrawString(spriteFont1, headerText, new Vector2((int)(HPosition.X - spriteFont1.MeasureString(headerText).X / 2) - 1,
                //    (int)(HPosition.Y) - 1), (Color.DarkSlateGray * 0.5f) * fader.Opacity);
                spriteBatch.DrawString(spriteFont1, headerText, new Vector2((int)(HPosition.X - spriteFont1.MeasureString(headerText).X / 2),
                    (int)(HPosition.Y)), Color.White * fader.Opacity);
                //spriteBatch.DrawString(spriteFont2, subText, new Vector2((int)(BPosition.X - spriteFont2.MeasureString(subText).X / 2) + 1,
                //    (int)(BPosition.Y) + 1), (Color.DarkSlateGray * 0.5f) * fader.Opacity);
                //spriteBatch.DrawString(spriteFont2, subText, new Vector2((int)(BPosition.X - spriteFont2.MeasureString(subText).X / 2) + 1,
                //    (int)(BPosition.Y) - 1), (Color.DarkSlateGray * 0.5f) * fader.Opacity);
                //spriteBatch.DrawString(spriteFont2, subText, new Vector2((int)(BPosition.X - spriteFont2.MeasureString(subText).X / 2) - 1,
                //    (int)(BPosition.Y) + 1), (Color.DarkSlateGray * 0.5f) * fader.Opacity);
                //spriteBatch.DrawString(spriteFont2, subText, new Vector2((int)(BPosition.X - spriteFont2.MeasureString(subText).X / 2) - 1,
                //    (int)(BPosition.Y) - 1), (Color.DarkSlateGray * 0.5f) * fader.Opacity);
                spriteBatch.DrawString(spriteFont2, subText, new Vector2((int)(BPosition.X - spriteFont2.MeasureString(subText).X / 2),
                    (int)(BPosition.Y)), Color.White * fader.Opacity);
                spriteBatch.End();
            }
        }
    }
}
