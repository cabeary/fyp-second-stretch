﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DialogEditor
{
    //TODO: Sawp two indexes

    public partial class TreeEditor : Form
    {
        List<string> displayBufferList = new List<string>();
        List<string> groupdisplayBufferList = new List<string>();


        List<BrokenLands.Dialog> dialogList = new List<BrokenLands.Dialog>();
        List<BrokenLands.Group> groupList = new List<BrokenLands.Group>();

        Dictionary<string, int> lockDict = new Dictionary<string, int>();// divide into skill stats quests conditions
        Dictionary<int, string> LooklockDict = new Dictionary<int, string>();

        Dictionary<string, int> statDict = new Dictionary<string, int>();
        Dictionary<int, string> LookstatDict = new Dictionary<int, string>();

        Dictionary<string, int> skillDict = new Dictionary<string, int>();
        Dictionary<int, string> LookskillDict = new Dictionary<int, string>();

        Dictionary<string, int> actionDict = new Dictionary<string, int>();
        Dictionary<int, string> LookactionDict = new Dictionary<int, string>();

        string targetpath;

        List<int> groupsortList = new List<int>();

        int currentgroup = 0;

        public TreeEditor(BrokenLands.DialogTree handle,string targetpath)
        {
            InitializeComponent();

            this.targetpath = targetpath;

            //MessageBox.Show("The file has been loaded successfully!");

            groupList = handle.defaultgroupList;
            dialogList = handle.dialogList;

            //action dictionary
            actionDict.Add("fight",1);
            actionDict.Add("bribe", 2);
            actionDict.Add("melee", 3);
            actionDict.Add("scavenge", 4);
            actionDict.Add("lockpick", 5);
            actionDict.Add("barter", 6);
            actionDict.Add("heal", 7);
            actionDict.Add("addcondition", 8);
            actionDict.Add("addquest", 9);
            actionDict.Add("cleardungeon", 10);
            actionDict.Add("leavedungeon", 11);
            actionDict.Add("additem",12);
            actionDict.Add("killactor",13);
            actionDict.Add("addloot",14);
            actionDict.Add("setcondition",15);
            actionDict.Add("removeitem",16);
            actionDict.Add("removeallitem",17);
            actionDict.Add("reply",19);
            actionDict.Add("allhostile", 20);
            actionDict.Add("addconq",21);
            actionDict.Add("incond",22);
            actionDict.Add("add2con",23);
            actionDict.Add("incondq",24);
            actionDict.Add("loadmap",25);
            actionDict.Add("shopscreen",26);
            actionDict.Add("pancamera", 27);
            actionDict.Add("setcamera", 28);
            actionDict.Add("decCondition", 29);
            actionDict.Add(">=", 30);
            actionDict.Add("<=", 31);
            actionDict.Add("fail", 32);
            actionDict.Add("addally", 33);
            actionDict.Add("addquestq", 34);
            actionDict.Add("damageplayer",35);
            actionDict.Add("additemq",36);
            actionDict.Add("none", 0);
            actionDict.Add("quit", -1);

            LookactionDict.Add(-1, "quit");
            LookactionDict.Add(1,"fight");
            LookactionDict.Add(2,"bribe");
            LookactionDict.Add(3,"melee");
            LookactionDict.Add(4,"scavenge");
            LookactionDict.Add(5,"lockpick");
            LookactionDict.Add(6,"barter");
            LookactionDict.Add(7,"heal");
            LookactionDict.Add(8,"addcondition");
            LookactionDict.Add(9,"addquest");
            LookactionDict.Add(10,"cleardungeon");
            LookactionDict.Add(11,"leavedungeon");
            LookactionDict.Add(12,"additem");
            LookactionDict.Add(13,"killactor");
            LookactionDict.Add(14,"addloot");
            LookactionDict.Add(15, "setcondition");
            LookactionDict.Add(16, "removeitem");
            LookactionDict.Add(17,"removeallitem");
            LookactionDict.Add(19,"reply");
            LookactionDict.Add(20, "allhostile");
            LookactionDict.Add(21, "addconq");
            LookactionDict.Add(22, "incond");
            LookactionDict.Add(23,"add2con");
            LookactionDict.Add(24, "incondq");
            LookactionDict.Add(25, "loadmap");
            LookactionDict.Add(26, "shopscreen");
            LookactionDict.Add(27, "pancamera");
            LookactionDict.Add(28, "setcamera");
            LookactionDict.Add(29, "decCondition");
            LookactionDict.Add(30, ">=");
            LookactionDict.Add(31, "<=");
            LookactionDict.Add(32, "fail");
            LookactionDict.Add(33, "addally");
            LookactionDict.Add(34, "addquestq");
            LookactionDict.Add(35,"damageplayer");
            LookactionDict.Add(36,"additemq");
            LookactionDict.Add(0,"none");
            
            //lock dictionary
            lockDict.Add("none", -1);
            lockDict.Add("stat",0);
            lockDict.Add("skill", 1);
            lockDict.Add("condition", 2);
            lockDict.Add("quest", 3);
            lockDict.Add("item", 4);
            lockDict.Add("metbefore",5);
            lockDict.Add("listen",6);
            lockDict.Add("congreater",7);
            lockDict.Add("conlesser", 8);

            LooklockDict.Add(-1, "none");
            LooklockDict.Add(0,"stat");
            LooklockDict.Add(1,"skill");
            LooklockDict.Add(2,"condition");
            LooklockDict.Add(3,"quest");
            LooklockDict.Add(4,"item");
            LooklockDict.Add(5,"metbefore");
            LooklockDict.Add(6,"listen");
            LooklockDict.Add(7, "congreater");
            LooklockDict.Add(8, "conlesser");

            //stat dictionary
            statDict.Add("strength",0);
            statDict.Add("dexterity", 1);
            statDict.Add("intelligence", 2);
            statDict.Add("charisma", 3);
            statDict.Add("endurance", 4);
            statDict.Add("agility", 5);

            LookstatDict.Add(0,"strength");
            LookstatDict.Add(1,"dexterity");
            LookstatDict.Add(2,"intelligence");
            LookstatDict.Add(3,"charisma");
            LookstatDict.Add(4,"endurance");
            LookstatDict.Add(5,"agility");

            //skill dictionary
            skillDict.Add("melee",0);
            skillDict.Add("scavenge", 1);
            skillDict.Add("lockpick", 2);
            skillDict.Add("barter", 3);
            skillDict.Add("health", 4);

            LookskillDict.Add(0,"melee");
            LookskillDict.Add(1,"scavenge");
            LookskillDict.Add(2,"lockpick");
            LookskillDict.Add(3,"barter");
            LookskillDict.Add(4,"health");


            //set the locktypeBox
            locktypeBox.Items.Add("none");
            locktypeBox.Items.Add("stat");
            locktypeBox.Items.Add("skill");
            locktypeBox.Items.Add("condition");
            locktypeBox.Items.Add("conditionactive");
            locktypeBox.Items.Add("quest");
            locktypeBox.Items.Add("item");
            locktypeBox.Items.Add("metbefore");
            locktypeBox.Items.Add("listen");
            locktypeBox.Items.Add("congreater");
            locktypeBox.Items.Add("conlesser");
            locktypeBox.SelectedIndex = 0;


            //set the actiontype

            actiontypeBox.Items.Add("fight");
            actiontypeBox.Items.Add("allhostile");
            actiontypeBox.Items.Add("bribe");
            actiontypeBox.Items.Add("melee");
            actiontypeBox.Items.Add("scavenge");
            actiontypeBox.Items.Add("lockpick");
            actiontypeBox.Items.Add("barter");
            actiontypeBox.Items.Add("heal");
            actiontypeBox.Items.Add("addcondition");
            actiontypeBox.Items.Add("addquest");
            actiontypeBox.Items.Add("addquestq");
            actiontypeBox.Items.Add("cleardungeon");
            actiontypeBox.Items.Add("leavedungeon");
            actiontypeBox.Items.Add("additem");
            actiontypeBox.Items.Add("additemq");
            actiontypeBox.Items.Add("killactor");
            actiontypeBox.Items.Add("addloot");
            actiontypeBox.Items.Add("setcondition");
            actiontypeBox.Items.Add("removeitem");
            actiontypeBox.Items.Add("removeallitem");
            actiontypeBox.Items.Add("reply");
            actiontypeBox.Items.Add("addconq");
            actiontypeBox.Items.Add("add2con");
            actiontypeBox.Items.Add("incond");
            actiontypeBox.Items.Add("decCondition");
            actiontypeBox.Items.Add("incondq");
            actiontypeBox.Items.Add("loadmap");
            actiontypeBox.Items.Add("shopscreen");
            actiontypeBox.Items.Add("pancamera");
            actiontypeBox.Items.Add("setcamera");
            actiontypeBox.Items.Add(">=");
            actiontypeBox.Items.Add("<=");
            actiontypeBox.Items.Add("addally");
            actiontypeBox.Items.Add("damageplayer");
            actiontypeBox.Items.Add("fail");
            actiontypeBox.Items.Add("none");
            actiontypeBox.Items.Add("quit");
            
            actiontypeBox.SelectedIndex = 0;

            //set grouplocktypeBox
            grouplocktypeBox.Items.Add("none");
            grouplocktypeBox.Items.Add("stat");
            grouplocktypeBox.Items.Add("skill");
            grouplocktypeBox.Items.Add("condition");
            grouplocktypeBox.Items.Add("conditionactive");
            grouplocktypeBox.Items.Add("quest");
            grouplocktypeBox.Items.Add("item");
            grouplocktypeBox.Items.Add("metbefore");
            grouplocktypeBox.Items.Add("listen");
            grouplocktypeBox.SelectedIndex = 0;
            

            refreshGroupSort();
            groupBox.SelectedIndex = 0;
            refreshDisplay();
            refreshGroupDisplay();
        }

        private void refreshGroupSort() {//FIXME: old entries of group are still being added
            groupsortList.Clear();
            groupBox.Items.Clear();
            foreach(BrokenLands.Dialog d in dialogList){
                int group = d.group;
                bool found = false;
                foreach(int g in groupsortList){
                    found = g == group;
                    if (found)
                        break;
                }

                if (!found)
                    groupsortList.Add(group);
            }

           foreach (int g in groupsortList)
                groupBox.Items.Add(g);

           if (dialogList.Count > 0)
               groupBox.SelectedIndex = currentgroup;
           else
               groupBox.Text = "";
        }

        private void updateDisplayBuffer() {
            displayBufferList.Clear();

            int currentGroup = groupBox.SelectedIndex;

            string valuestring = "";

            for (int i = 0; i < dialogList.Count; i++)
            {
                if (dialogList[i].group == currentGroup)
                {

                    switch (dialogList[i].locK.type)
                    {
                        case 0://stat
                            valuestring = LookstatDict[dialogList[i].locK.value2];
                            break;
                        case 1://skill
                            valuestring = LookskillDict[dialogList[i].locK.value2];
                            break;
                        default://cond
                            valuestring = dialogList[i].locK.value2.ToString();
                            break;
                    }

                    displayBufferList.Add("Index:" + i.ToString() + "   Message:" + dialogList[i].message + "   Group:" + dialogList[i].group.ToString()
                        + "   Next:" + dialogList[i].getNextGroup().ToString() + "   LockType:" + LooklockDict[dialogList[i].locK.type] + "   Req:"
                        + dialogList[i].locK.value.ToString() + "   LockValue:" + valuestring +
                        "   Met:" + dialogList[i].locK.getMet() + "   ActionType:" + LookactionDict[dialogList[i].getAction().action] + "   ActionValue:" +
                        dialogList[i].getAction().value + "   Teller:" + dialogList[i].teller);

                }
            }
        }

        private void refreshDisplay() {
           
            updateDisplayBuffer();

            //refresh the display items
            displayBufferText.Text = "";

            foreach (string s in displayBufferList)
            {
                displayBufferText.AppendText(s);
                displayBufferText.AppendText(Environment.NewLine);
                displayBufferText.AppendText(Environment.NewLine);

            }
        }

        private void refreshGroupDisplay() {
            updateGroupDisplay();

            groupDisplayText.Text="";

            foreach (string s in groupdisplayBufferList)
            {
                groupDisplayText.AppendText(s);
                groupDisplayText.AppendText(Environment.NewLine);
                groupDisplayText.AppendText(Environment.NewLine);

            }
        }

        private void updateGroupDisplay() {
            
            groupdisplayBufferList.Clear();

            List<string> list = new List<string>();

            
            
            for (int i = 0; i < groupList.Count; i++)
            {
                string valuestring = "";

                switch (groupList[i].locK.type)
                {
                    case 0://stat
                        valuestring = LookstatDict[groupList[i].locK.value2];
                        break;
                    case 1://skill
                        valuestring = LookskillDict[groupList[i].locK.value2];
                        break;
                    default://cond
                        valuestring = groupList[i].locK.value2.ToString();
                        break;
                }

                list.Add(valuestring);
            }



            for (int i = 0; i < groupList.Count;i++ ) {
                groupdisplayBufferList.Add("Index:" + i.ToString() + "   Group:" + groupList[i].getGroup().ToString() + "   LockType:" +
                   LooklockDict[groupList[i].locK.type]+"   Req:"+groupList[i].locK.value+"   LockValue:"+list[i]+"   Met:"+
                    groupList[i].locK.getMet());
            }
        }

        private void writeFile() {
            try { }
            catch (Exception ex) { }
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            try {
                string message = messageText.Text;
                int locktype = lockDict[locktypeBox.SelectedItem.ToString()];
                int requirement = int.Parse(requirementType.Text);

                bool met = trueMetCheck.Checked;

                int lockvalue = 0;
                switch(locktype){
                    case 0://stat
                        lockvalue = statDict[lockvalueBox.SelectedItem.ToString()];
                        break;
                    case 1://skill
                        lockvalue = skillDict[lockvalueBox.SelectedItem.ToString()];
                        System.Diagnostics.Debug.WriteLine(lockvalueBox.SelectedItem.ToString());
                        System.Diagnostics.Debug.WriteLine("Value:" + lockvalue.ToString());
                        System.Diagnostics.Debug.WriteLine("Looktable:"+LookskillDict[lockvalue]);
                        break;
                    default://cond
                        lockvalue = int.Parse(lockvalueBox.Text);
                        break;
                }

                int group = int.Parse(groupText.Text);
                int next = int.Parse(nextText.Text);

                int actiontype = actionDict[actiontypeBox.SelectedItem.ToString()];
                int actionvalue = int.Parse(actionvalueText.Text);
                bool teller = trueTellerCheck.Checked;

                dialogList.Add(new BrokenLands.Dialog(message,new BrokenLands.Lock(locktype,requirement,met,lockvalue),group,next,new BrokenLands.Action(actiontype,actionvalue),teller));

                refreshGroupSort();
                refreshDisplay();
            }
            catch(Exception ex){
                MessageBox.Show("Error"+ex);
            }
        }

        private void removeButton_Click(object sender, EventArgs e)
        {
            try {
                int index = int.Parse(indexText.Text);
                dialogList.RemoveAt(index);
                refreshGroupSort();
                refreshDisplay();
                //groupBox.SelectedIndex = 0;
            }
            catch (Exception ex) {
                MessageBox.Show("Error:" + ex);
            }
        }

        private void addGroupButton_Click(object sender, EventArgs e)
        {
            try {
                int locktype=lockDict[grouplocktypeBox.SelectedItem.ToString()];

                int lockvalue = 0;
                switch (locktype)
                {
                    case 0://stat
                        lockvalue = statDict[grouplockvalueBox.SelectedItem.ToString()];
                        break;
                    case 1://skill
                        lockvalue = skillDict[grouplockvalueBox.SelectedItem.ToString()];
                        break;
                    default://cond
                        lockvalue = int.Parse(grouplockvalueBox.Text);
                        break;
                }

                int requirement=int.Parse(groupreqText.Text);
                bool met=groupmettrueButton.Checked;
                int group=int.Parse(groupgroupText.Text);

                groupList.Add(new BrokenLands.Group(new BrokenLands.Lock(locktype,requirement,met,lockvalue),group));
                refreshGroupDisplay();
            }
            catch (Exception ex) {
                MessageBox.Show("Error:"+ex.ToString());
            }
        }

        private void groupremoveButton_Click(object sender, EventArgs e)
        {
            try {
                int index = int.Parse(groupindexText.Text);
                groupList.RemoveAt(index);
                refreshGroupDisplay();
            }
            catch (Exception ex) {
                MessageBox.Show("Error:"+ex.ToString());
            }
        }

        private void groupBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            currentgroup = groupBox.SelectedIndex;
            refreshDisplay();
        }


        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            lockvalueBox.Items.Clear();

            switch (lockDict[locktypeBox.SelectedItem.ToString()])
            {
                case 0:
                    lockvalueBox.Items.Add("strength");
                    lockvalueBox.Items.Add("dexterity");
                    lockvalueBox.Items.Add("intelligence");
                    lockvalueBox.Items.Add("charisma");
                    lockvalueBox.Items.Add("endurance");
                    lockvalueBox.Items.Add("agility");
                    break;
                case 1:
                    lockvalueBox.Items.Add("melee");
                    lockvalueBox.Items.Add("scavenge");
                    lockvalueBox.Items.Add("lockpick");
                    lockvalueBox.Items.Add("barter");
                    lockvalueBox.Items.Add("health");
                    break;

                default:
                    lockvalueBox.Items.Add("");
                    break;

            }
            lockvalueBox.SelectedIndex = 0;

        }


        private void grouplocktypeBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            grouplockvalueBox.Items.Clear();

            switch (lockDict[grouplocktypeBox.SelectedItem.ToString()])
            {
                case 0:
                    grouplockvalueBox.Items.Add("strength");
                    grouplockvalueBox.Items.Add("dexterity");
                    grouplockvalueBox.Items.Add("intelligence");
                    grouplockvalueBox.Items.Add("charisma");
                    grouplockvalueBox.Items.Add("endurance");
                    grouplockvalueBox.Items.Add("agility");
                    break;
                case 1:
                    grouplockvalueBox.Items.Add("melee");
                    grouplockvalueBox.Items.Add("scavenge");
                    grouplockvalueBox.Items.Add("lockpick");
                    grouplockvalueBox.Items.Add("barter");
                    grouplockvalueBox.Items.Add("health");
                    break;

                default:
                    grouplockvalueBox.Items.Add("");
                    break;

            }
            grouplockvalueBox.SelectedIndex = 0;
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                using (StreamWriter writer = new StreamWriter(targetpath))
                {
                    //write dialog count
                    writer.WriteLine(dialogList.Count);

                    foreach (BrokenLands.Dialog d in dialogList)
                    {
                        //write message
                        writer.WriteLine(d.message);
                        //locktype
                        writer.WriteLine(d.locK.type);
                        //req
                        writer.WriteLine(d.locK.value);
                        //met
                        writer.WriteLine(d.locK.getMet());
                        //value
                        writer.WriteLine(d.locK.value2);
                        //group
                        writer.WriteLine(d.group);
                        //next group
                        writer.WriteLine(d.getNextGroup());
                        //action type
                        writer.WriteLine(d.getAction().action);
                        //action value
                        writer.WriteLine(d.getAction().value);
                        //teller
                        writer.WriteLine(d.teller);
                    }

                    //write group count
                    writer.WriteLine(groupList.Count);

                    foreach (BrokenLands.Group g in groupList)
                    {
                        //locktype
                        writer.WriteLine(g.locK.type);
                        //req
                        writer.WriteLine(g.locK.value);
                        //met
                        writer.WriteLine(g.locK.getMet());
                        //value
                        writer.WriteLine(g.locK.value2);
                        //group
                        writer.WriteLine(g.getGroup());
                    }
                }

                MessageBox.Show("Save successful");
            }
            catch(Exception ex){
                MessageBox.Show("Error:"+ex.ToString());
            }
        }

        private void swapButton_Click(object sender, EventArgs e)
        {
            int from = int.Parse(fromText.Text);
            int to = int.Parse(toText.Text);

            BrokenLands.Dialog fromhandle = dialogList[from];
            BrokenLands.Dialog tohandle = dialogList[to];

            dialogList[to] = fromhandle;
            dialogList[from] = tohandle;

            refreshGroupSort();
            refreshDisplay();

        }


        private void groupSwapButton_Click(object sender, EventArgs e)
        {
            int from = int.Parse(groupFromText.Text);
            int to = int.Parse(groupToText.Text);

            BrokenLands.Group fromhandle = groupList[from];
            BrokenLands.Group tohandle = groupList[to];

            groupList[to] = fromhandle;
            groupList[from] = tohandle;

            refreshGroupDisplay();
        }

        private void loadButton_Click(object sender, EventArgs e)
        {
            try
            {
               BrokenLands.Dialog handle = dialogList[int.Parse(loadText.Text)];

               messageText.Text = handle.message;
               if (handle.locK.getMet())
                   trueMetCheck.Checked = true;
               else
                   falseMetCheck.Checked = true;
               locktypeBox.SelectedItem = LooklockDict[handle.locK.type];

               switch (handle.locK.type)
               {
                   case 0://stat
                       lockvalueBox.SelectedItem = LookstatDict[handle.locK.value2];
                       break;
                   case 1://skill
                       lockvalueBox.SelectedItem = LookskillDict[handle.locK.value2];
                       break;
                   default://cond
                       lockvalueBox.Text = handle.locK.value2.ToString();
                       break;
               }

               requirementType.Text = handle.locK.value.ToString();
               groupText.Text = handle.group.ToString();
               nextText.Text = handle.getNextGroup().ToString();
               actiontypeBox.SelectedItem = LookactionDict[handle.getAction().action];
               actionvalueText.Text = handle.getAction().value.ToString();

               if (handle.teller)
                   trueTellerCheck.Checked = true;
               else
                   falseTellerCheck.Checked = true;
            }
            catch(Exception ex){
                MessageBox.Show(ex.ToString());
            }

        }

        private void replaceButton_Click(object sender, EventArgs e)
        {
            try
            {
                string message = messageText.Text;
                int locktype = lockDict[locktypeBox.SelectedItem.ToString()];
                int requirement = int.Parse(requirementType.Text);

                bool met = trueMetCheck.Checked;

                int lockvalue = 0;
                switch (locktype)
                {
                    case 0://stat
                        lockvalue = statDict[lockvalueBox.SelectedItem.ToString()];
                        break;
                    case 1://skill
                        lockvalue = skillDict[lockvalueBox.SelectedItem.ToString()];
                        System.Diagnostics.Debug.WriteLine(lockvalueBox.SelectedItem.ToString());
                        System.Diagnostics.Debug.WriteLine("Value:" + lockvalue.ToString());
                        System.Diagnostics.Debug.WriteLine("Looktable:" + LookskillDict[lockvalue]);
                        break;
                    default://cond
                        lockvalue = int.Parse(lockvalueBox.Text);
                        break;
                }

                int group = int.Parse(groupText.Text);
                int next = int.Parse(nextText.Text);

                int actiontype = actionDict[actiontypeBox.SelectedItem.ToString()];
                int actionvalue = int.Parse(actionvalueText.Text);
                bool teller = trueTellerCheck.Checked;

                dialogList[int.Parse(loadText.Text)] = new BrokenLands.Dialog(message, new BrokenLands.Lock(locktype, requirement, met, lockvalue), group, next, new BrokenLands.Action(actiontype, actionvalue), teller);
                //dialogList.Add(new BrokenLands.Dialog(message, new BrokenLands.Lock(locktype, requirement, met, lockvalue), group, next, new BrokenLands.Action(actiontype, actionvalue), teller));

                refreshGroupSort();
                refreshDisplay();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error" + ex);
            }
        }

        //Useless shit ahoy
        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void grouplockvalueBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        

        

        

        

        

        

       

        

        

        


    }
}
