﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using BrokenLands;

namespace DialogEditor
{
    public partial class LoadTree : Form
    {

        string path = "../../../../brokenland/brokenland/brokenlandcontent/";
        public bool failed = false;
        public string targetpath;

        BrokenLands.DialogTree handle;
        public LoadTree(BrokenLands.DialogTree dialogtree)
        {
            InitializeComponent();
            handle = dialogtree;
        }

        private void loadTree(string filename) {
            //path go back 3 BrokenLands  Content  ObjectData  Dialog
            System.Diagnostics.Debug.WriteLine(filename+" loaded");


            try{

            using (StreamReader reader = new StreamReader(path+"ObjectData/Dialog/"+filename+".dialog")) {
                targetpath = path + "ObjectData/Dialog/" + filename + ".dialog";
                //read the number of dialogs

                int dialogcount = int.Parse(reader.ReadLine());
                //for loop
                for (int i = 0; i < dialogcount; i++)
                {
                    //read message
                    string msg = reader.ReadLine();
                    //locktype
                    int locktype = int.Parse(reader.ReadLine());
                    //req
                    int req = int.Parse(reader.ReadLine());
                    //met
                    bool met = bool.Parse(reader.ReadLine());
                    //value
                    int value = int.Parse(reader.ReadLine());
                    //group
                    int group = int.Parse(reader.ReadLine());
                    //next group
                    int next = int.Parse(reader.ReadLine());
                    //action type
                    int actiontype = int.Parse(reader.ReadLine());
                    //action value
                    int actionvalue = int.Parse(reader.ReadLine());
                    //teller
                    bool teller = bool.Parse(reader.ReadLine());

                    //add the dialog to the tree
                    handle.dialogList.Add(new BrokenLands.Dialog(msg, new BrokenLands.Lock(locktype, req, met, value), group, next, new BrokenLands.Action(actiontype, actionvalue), teller));
                }

                //read the number of default groups
                int groupcount = int.Parse(reader.ReadLine());

                for (int i = 0; i < groupcount; i++)
                {
                    //locktype
                    int locktype = int.Parse(reader.ReadLine()); //4
                    //req
                    int req = int.Parse(reader.ReadLine()); //1
                    //met
                    bool met = bool.Parse(reader.ReadLine()); //False
                    //value
                    int value = int.Parse(reader.ReadLine());  //14
                    //group
                    int group = int.Parse(reader.ReadLine());  //13


                    





                    handle.defaultgroupList.Add(new BrokenLands.Group(new BrokenLands.Lock(locktype,req,met,value),group));
                }

                

            }

        }

            catch(Exception ex){
                MessageBox.Show("Failed:"+ex.ToString());
                failed = true;
            }

        }

        private void submitButton_Click(object sender, EventArgs e)
        {
            string file = dialogfiletext.Text;
            loadTree(file);
            this.Close();
        }
    }
}
