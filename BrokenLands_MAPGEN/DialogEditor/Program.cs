﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DialogEditor
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //generic dialogtree object
            BrokenLands.DialogTree tree = new BrokenLands.DialogTree();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            LoadTree handle = new LoadTree(tree);

            Application.Run(handle); //pass dialog to load tree


            if(!handle.failed)
            Application.Run(new TreeEditor(tree,handle.targetpath));

            //run editor form
        }
    }
}
