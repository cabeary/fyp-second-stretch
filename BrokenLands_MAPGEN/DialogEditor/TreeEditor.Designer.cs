﻿namespace DialogEditor
{
    partial class TreeEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.displayBufferText = new System.Windows.Forms.TextBox();
            this.groupBox = new System.Windows.Forms.ComboBox();
            this.messageText = new System.Windows.Forms.TextBox();
            this.requirementType = new System.Windows.Forms.TextBox();
            this.groupText = new System.Windows.Forms.TextBox();
            this.nextText = new System.Windows.Forms.TextBox();
            this.actionvalueText = new System.Windows.Forms.TextBox();
            this.addButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.indexText = new System.Windows.Forms.TextBox();
            this.removeButton = new System.Windows.Forms.Button();
            this.groupDisplayText = new System.Windows.Forms.TextBox();
            this.groupreqText = new System.Windows.Forms.TextBox();
            this.groupgroupText = new System.Windows.Forms.TextBox();
            this.addGroupButton = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.trueMetCheck = new System.Windows.Forms.RadioButton();
            this.falseMetCheck = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.trueTellerCheck = new System.Windows.Forms.RadioButton();
            this.falseTellerCheck = new System.Windows.Forms.RadioButton();
            this.locktypeBox = new System.Windows.Forms.ComboBox();
            this.actiontypeBox = new System.Windows.Forms.ComboBox();
            this.lockvalueBox = new System.Windows.Forms.ComboBox();
            this.label50 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.groupremoveButton = new System.Windows.Forms.Button();
            this.groupindexText = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupmettrueButton = new System.Windows.Forms.RadioButton();
            this.groupmetfalseButton = new System.Windows.Forms.RadioButton();
            this.grouplocktypeBox = new System.Windows.Forms.ComboBox();
            this.grouplockvalueBox = new System.Windows.Forms.ComboBox();
            this.saveButton = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.fromText = new System.Windows.Forms.TextBox();
            this.toText = new System.Windows.Forms.TextBox();
            this.swapButton = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.loadText = new System.Windows.Forms.TextBox();
            this.loadButton = new System.Windows.Forms.Button();
            this.replaceButton = new System.Windows.Forms.Button();
            this.groupSwapButton = new System.Windows.Forms.Button();
            this.groupToText = new System.Windows.Forms.TextBox();
            this.groupFromText = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(405, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Dialog";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(405, 509);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Default Groups";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // displayBufferText
            // 
            this.displayBufferText.Location = new System.Drawing.Point(55, 84);
            this.displayBufferText.Multiline = true;
            this.displayBufferText.Name = "displayBufferText";
            this.displayBufferText.Size = new System.Drawing.Size(765, 338);
            this.displayBufferText.TabIndex = 2;
            // 
            // groupBox
            // 
            this.groupBox.FormattingEnabled = true;
            this.groupBox.Location = new System.Drawing.Point(55, 441);
            this.groupBox.Name = "groupBox";
            this.groupBox.Size = new System.Drawing.Size(121, 21);
            this.groupBox.TabIndex = 3;
            this.groupBox.SelectedIndexChanged += new System.EventHandler(this.groupBox_SelectedIndexChanged);
            // 
            // messageText
            // 
            this.messageText.Location = new System.Drawing.Point(878, 66);
            this.messageText.Multiline = true;
            this.messageText.Name = "messageText";
            this.messageText.Size = new System.Drawing.Size(286, 74);
            this.messageText.TabIndex = 4;
            // 
            // requirementType
            // 
            this.requirementType.Location = new System.Drawing.Point(910, 249);
            this.requirementType.Name = "requirementType";
            this.requirementType.Size = new System.Drawing.Size(100, 20);
            this.requirementType.TabIndex = 6;
            // 
            // groupText
            // 
            this.groupText.Location = new System.Drawing.Point(910, 274);
            this.groupText.Name = "groupText";
            this.groupText.Size = new System.Drawing.Size(100, 20);
            this.groupText.TabIndex = 8;
            // 
            // nextText
            // 
            this.nextText.Location = new System.Drawing.Point(910, 301);
            this.nextText.Name = "nextText";
            this.nextText.Size = new System.Drawing.Size(100, 20);
            this.nextText.TabIndex = 9;
            // 
            // actionvalueText
            // 
            this.actionvalueText.Location = new System.Drawing.Point(910, 355);
            this.actionvalueText.Name = "actionvalueText";
            this.actionvalueText.Size = new System.Drawing.Size(100, 20);
            this.actionvalueText.TabIndex = 11;
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(859, 426);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(75, 23);
            this.addButton.TabIndex = 14;
            this.addButton.Text = "Add";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(52, 58);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 16;
            this.label3.Text = "Dialog View";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 444);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "Group";
            // 
            // indexText
            // 
            this.indexText.Location = new System.Drawing.Point(473, 441);
            this.indexText.Name = "indexText";
            this.indexText.Size = new System.Drawing.Size(100, 20);
            this.indexText.TabIndex = 19;
            // 
            // removeButton
            // 
            this.removeButton.Location = new System.Drawing.Point(485, 472);
            this.removeButton.Name = "removeButton";
            this.removeButton.Size = new System.Drawing.Size(75, 23);
            this.removeButton.TabIndex = 20;
            this.removeButton.Text = "Remove";
            this.removeButton.UseVisualStyleBackColor = true;
            this.removeButton.Click += new System.EventHandler(this.removeButton_Click);
            // 
            // groupDisplayText
            // 
            this.groupDisplayText.Location = new System.Drawing.Point(145, 555);
            this.groupDisplayText.Multiline = true;
            this.groupDisplayText.Name = "groupDisplayText";
            this.groupDisplayText.Size = new System.Drawing.Size(581, 147);
            this.groupDisplayText.TabIndex = 21;
            // 
            // groupreqText
            // 
            this.groupreqText.Location = new System.Drawing.Point(828, 574);
            this.groupreqText.Name = "groupreqText";
            this.groupreqText.Size = new System.Drawing.Size(100, 20);
            this.groupreqText.TabIndex = 23;
            // 
            // groupgroupText
            // 
            this.groupgroupText.Location = new System.Drawing.Point(828, 673);
            this.groupgroupText.Name = "groupgroupText";
            this.groupgroupText.Size = new System.Drawing.Size(100, 20);
            this.groupgroupText.TabIndex = 26;
            // 
            // addGroupButton
            // 
            this.addGroupButton.Location = new System.Drawing.Point(838, 710);
            this.addGroupButton.Name = "addGroupButton";
            this.addGroupButton.Size = new System.Drawing.Size(75, 23);
            this.addGroupButton.TabIndex = 27;
            this.addGroupButton.Text = "Add";
            this.addGroupButton.UseVisualStyleBackColor = true;
            this.addGroupButton.Click += new System.EventHandler(this.addGroupButton_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(432, 448);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 13);
            this.label6.TabIndex = 28;
            this.label6.Text = "Index";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(145, 536);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 13);
            this.label7.TabIndex = 29;
            this.label7.Text = "Groups";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(826, 86);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 13);
            this.label8.TabIndex = 30;
            this.label8.Text = "Message";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(831, 252);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(67, 13);
            this.label9.TabIndex = 31;
            this.label9.Text = "Requirement";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(831, 200);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(51, 13);
            this.label10.TabIndex = 32;
            this.label10.Text = "Locktype";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(831, 225);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(57, 13);
            this.label11.TabIndex = 33;
            this.label11.Text = "Lockvalue";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(831, 277);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(36, 13);
            this.label12.TabIndex = 34;
            this.label12.Text = "Group";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(831, 304);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(29, 13);
            this.label13.TabIndex = 35;
            this.label13.Text = "Next";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(831, 331);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(57, 13);
            this.label14.TabIndex = 36;
            this.label14.Text = "Actiontype";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(831, 358);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(62, 13);
            this.label15.TabIndex = 37;
            this.label15.Text = "ConditionID";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.trueMetCheck);
            this.groupBox1.Controls.Add(this.falseMetCheck);
            this.groupBox1.Location = new System.Drawing.Point(834, 148);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(182, 35);
            this.groupBox1.TabIndex = 38;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Met";
            // 
            // trueMetCheck
            // 
            this.trueMetCheck.AutoSize = true;
            this.trueMetCheck.Location = new System.Drawing.Point(99, 12);
            this.trueMetCheck.Name = "trueMetCheck";
            this.trueMetCheck.Size = new System.Drawing.Size(43, 17);
            this.trueMetCheck.TabIndex = 1;
            this.trueMetCheck.TabStop = true;
            this.trueMetCheck.Text = "true";
            this.trueMetCheck.UseVisualStyleBackColor = true;
            // 
            // falseMetCheck
            // 
            this.falseMetCheck.AutoSize = true;
            this.falseMetCheck.Location = new System.Drawing.Point(7, 12);
            this.falseMetCheck.Name = "falseMetCheck";
            this.falseMetCheck.Size = new System.Drawing.Size(47, 17);
            this.falseMetCheck.TabIndex = 0;
            this.falseMetCheck.TabStop = true;
            this.falseMetCheck.Text = "false";
            this.falseMetCheck.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.trueTellerCheck);
            this.groupBox2.Controls.Add(this.falseTellerCheck);
            this.groupBox2.Location = new System.Drawing.Point(834, 387);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(182, 35);
            this.groupBox2.TabIndex = 39;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Teller";
            // 
            // trueTellerCheck
            // 
            this.trueTellerCheck.AutoSize = true;
            this.trueTellerCheck.Location = new System.Drawing.Point(97, 12);
            this.trueTellerCheck.Name = "trueTellerCheck";
            this.trueTellerCheck.Size = new System.Drawing.Size(43, 17);
            this.trueTellerCheck.TabIndex = 1;
            this.trueTellerCheck.TabStop = true;
            this.trueTellerCheck.Text = "true";
            this.trueTellerCheck.UseVisualStyleBackColor = true;
            // 
            // falseTellerCheck
            // 
            this.falseTellerCheck.AutoSize = true;
            this.falseTellerCheck.Location = new System.Drawing.Point(6, 12);
            this.falseTellerCheck.Name = "falseTellerCheck";
            this.falseTellerCheck.Size = new System.Drawing.Size(47, 17);
            this.falseTellerCheck.TabIndex = 0;
            this.falseTellerCheck.TabStop = true;
            this.falseTellerCheck.Text = "false";
            this.falseTellerCheck.UseVisualStyleBackColor = true;
            // 
            // locktypeBox
            // 
            this.locktypeBox.FormattingEnabled = true;
            this.locktypeBox.Location = new System.Drawing.Point(910, 192);
            this.locktypeBox.Name = "locktypeBox";
            this.locktypeBox.Size = new System.Drawing.Size(100, 21);
            this.locktypeBox.TabIndex = 40;
            this.locktypeBox.SelectedIndexChanged += new System.EventHandler(this.comboBox3_SelectedIndexChanged);
            // 
            // actiontypeBox
            // 
            this.actiontypeBox.FormattingEnabled = true;
            this.actiontypeBox.Location = new System.Drawing.Point(910, 323);
            this.actiontypeBox.Name = "actiontypeBox";
            this.actiontypeBox.Size = new System.Drawing.Size(100, 21);
            this.actiontypeBox.TabIndex = 41;
            // 
            // lockvalueBox
            // 
            this.lockvalueBox.FormattingEnabled = true;
            this.lockvalueBox.Location = new System.Drawing.Point(910, 222);
            this.lockvalueBox.Name = "lockvalueBox";
            this.lockvalueBox.Size = new System.Drawing.Size(100, 21);
            this.lockvalueBox.TabIndex = 42;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(754, 550);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(51, 13);
            this.label50.TabIndex = 43;
            this.label50.Text = "Locktype";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(754, 575);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(67, 13);
            this.label16.TabIndex = 44;
            this.label16.Text = "Requirement";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(754, 604);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(57, 13);
            this.label18.TabIndex = 46;
            this.label18.Text = "Lockvalue";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(754, 678);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(36, 13);
            this.label19.TabIndex = 47;
            this.label19.Text = "Group";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(432, 713);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(33, 13);
            this.label20.TabIndex = 50;
            this.label20.Text = "Index";
            // 
            // groupremoveButton
            // 
            this.groupremoveButton.Location = new System.Drawing.Point(588, 706);
            this.groupremoveButton.Name = "groupremoveButton";
            this.groupremoveButton.Size = new System.Drawing.Size(75, 23);
            this.groupremoveButton.TabIndex = 49;
            this.groupremoveButton.Text = "Remove";
            this.groupremoveButton.UseVisualStyleBackColor = true;
            this.groupremoveButton.Click += new System.EventHandler(this.groupremoveButton_Click);
            // 
            // groupindexText
            // 
            this.groupindexText.Location = new System.Drawing.Point(473, 706);
            this.groupindexText.Name = "groupindexText";
            this.groupindexText.Size = new System.Drawing.Size(100, 20);
            this.groupindexText.TabIndex = 48;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.groupmettrueButton);
            this.groupBox3.Controls.Add(this.groupmetfalseButton);
            this.groupBox3.Location = new System.Drawing.Point(757, 626);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(182, 35);
            this.groupBox3.TabIndex = 39;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Met";
            // 
            // groupmettrueButton
            // 
            this.groupmettrueButton.AutoSize = true;
            this.groupmettrueButton.Location = new System.Drawing.Point(99, 12);
            this.groupmettrueButton.Name = "groupmettrueButton";
            this.groupmettrueButton.Size = new System.Drawing.Size(43, 17);
            this.groupmettrueButton.TabIndex = 1;
            this.groupmettrueButton.TabStop = true;
            this.groupmettrueButton.Text = "true";
            this.groupmettrueButton.UseVisualStyleBackColor = true;
            // 
            // groupmetfalseButton
            // 
            this.groupmetfalseButton.AutoSize = true;
            this.groupmetfalseButton.Location = new System.Drawing.Point(7, 12);
            this.groupmetfalseButton.Name = "groupmetfalseButton";
            this.groupmetfalseButton.Size = new System.Drawing.Size(47, 17);
            this.groupmetfalseButton.TabIndex = 0;
            this.groupmetfalseButton.TabStop = true;
            this.groupmetfalseButton.Text = "false";
            this.groupmetfalseButton.UseVisualStyleBackColor = true;
            // 
            // grouplocktypeBox
            // 
            this.grouplocktypeBox.FormattingEnabled = true;
            this.grouplocktypeBox.Location = new System.Drawing.Point(829, 546);
            this.grouplocktypeBox.Name = "grouplocktypeBox";
            this.grouplocktypeBox.Size = new System.Drawing.Size(99, 21);
            this.grouplocktypeBox.TabIndex = 51;
            this.grouplocktypeBox.SelectedIndexChanged += new System.EventHandler(this.grouplocktypeBox_SelectedIndexChanged);
            // 
            // grouplockvalueBox
            // 
            this.grouplockvalueBox.FormattingEnabled = true;
            this.grouplockvalueBox.Location = new System.Drawing.Point(829, 603);
            this.grouplockvalueBox.Name = "grouplockvalueBox";
            this.grouplockvalueBox.Size = new System.Drawing.Size(99, 21);
            this.grouplockvalueBox.TabIndex = 52;
            this.grouplockvalueBox.SelectedIndexChanged += new System.EventHandler(this.grouplockvalueBox_SelectedIndexChanged);
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(32, 16);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(75, 23);
            this.saveButton.TabIndex = 53;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(617, 444);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(30, 13);
            this.label5.TabIndex = 54;
            this.label5.Text = "From";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(735, 444);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(20, 13);
            this.label17.TabIndex = 55;
            this.label17.Text = "To";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // fromText
            // 
            this.fromText.Location = new System.Drawing.Point(653, 442);
            this.fromText.Name = "fromText";
            this.fromText.Size = new System.Drawing.Size(73, 20);
            this.fromText.TabIndex = 57;
            // 
            // toText
            // 
            this.toText.Location = new System.Drawing.Point(761, 441);
            this.toText.Name = "toText";
            this.toText.Size = new System.Drawing.Size(70, 20);
            this.toText.TabIndex = 58;
            // 
            // swapButton
            // 
            this.swapButton.Location = new System.Drawing.Point(706, 472);
            this.swapButton.Name = "swapButton";
            this.swapButton.Size = new System.Drawing.Size(75, 23);
            this.swapButton.TabIndex = 59;
            this.swapButton.Text = "Swap";
            this.swapButton.UseVisualStyleBackColor = true;
            this.swapButton.Click += new System.EventHandler(this.swapButton_Click);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(835, 28);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(60, 13);
            this.label21.TabIndex = 60;
            this.label21.Text = "Load Index";
            // 
            // loadText
            // 
            this.loadText.Location = new System.Drawing.Point(904, 25);
            this.loadText.Name = "loadText";
            this.loadText.Size = new System.Drawing.Size(100, 20);
            this.loadText.TabIndex = 61;
            // 
            // loadButton
            // 
            this.loadButton.Location = new System.Drawing.Point(1010, 23);
            this.loadButton.Name = "loadButton";
            this.loadButton.Size = new System.Drawing.Size(75, 23);
            this.loadButton.TabIndex = 62;
            this.loadButton.Text = "Load";
            this.loadButton.UseVisualStyleBackColor = true;
            this.loadButton.Click += new System.EventHandler(this.loadButton_Click);
            // 
            // replaceButton
            // 
            this.replaceButton.Location = new System.Drawing.Point(941, 426);
            this.replaceButton.Name = "replaceButton";
            this.replaceButton.Size = new System.Drawing.Size(75, 23);
            this.replaceButton.TabIndex = 63;
            this.replaceButton.Text = "Replace";
            this.replaceButton.UseVisualStyleBackColor = true;
            this.replaceButton.Click += new System.EventHandler(this.replaceButton_Click);
            // 
            // groupSwapButton
            // 
            this.groupSwapButton.Location = new System.Drawing.Point(1033, 575);
            this.groupSwapButton.Name = "groupSwapButton";
            this.groupSwapButton.Size = new System.Drawing.Size(75, 23);
            this.groupSwapButton.TabIndex = 68;
            this.groupSwapButton.Text = "Swap";
            this.groupSwapButton.UseVisualStyleBackColor = true;
            this.groupSwapButton.Click += new System.EventHandler(this.groupSwapButton_Click);
            // 
            // groupToText
            // 
            this.groupToText.Location = new System.Drawing.Point(1088, 544);
            this.groupToText.Name = "groupToText";
            this.groupToText.Size = new System.Drawing.Size(70, 20);
            this.groupToText.TabIndex = 67;
            // 
            // groupFromText
            // 
            this.groupFromText.Location = new System.Drawing.Point(980, 545);
            this.groupFromText.Name = "groupFromText";
            this.groupFromText.Size = new System.Drawing.Size(73, 20);
            this.groupFromText.TabIndex = 66;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(1062, 547);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(20, 13);
            this.label22.TabIndex = 65;
            this.label22.Text = "To";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(944, 547);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(30, 13);
            this.label23.TabIndex = 64;
            this.label23.Text = "From";
            // 
            // TreeEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1178, 733);
            this.Controls.Add(this.groupSwapButton);
            this.Controls.Add(this.groupToText);
            this.Controls.Add(this.groupFromText);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.replaceButton);
            this.Controls.Add(this.loadButton);
            this.Controls.Add(this.loadText);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.swapButton);
            this.Controls.Add(this.toText);
            this.Controls.Add(this.fromText);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.grouplockvalueBox);
            this.Controls.Add(this.grouplocktypeBox);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.groupremoveButton);
            this.Controls.Add(this.groupindexText);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label50);
            this.Controls.Add(this.lockvalueBox);
            this.Controls.Add(this.actiontypeBox);
            this.Controls.Add(this.locktypeBox);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.addGroupButton);
            this.Controls.Add(this.groupgroupText);
            this.Controls.Add(this.groupreqText);
            this.Controls.Add(this.groupDisplayText);
            this.Controls.Add(this.removeButton);
            this.Controls.Add(this.indexText);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.actionvalueText);
            this.Controls.Add(this.nextText);
            this.Controls.Add(this.groupText);
            this.Controls.Add(this.requirementType);
            this.Controls.Add(this.messageText);
            this.Controls.Add(this.groupBox);
            this.Controls.Add(this.displayBufferText);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "TreeEditor";
            this.Text = "TreeEditor";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox displayBufferText;
        private System.Windows.Forms.ComboBox groupBox;
        private System.Windows.Forms.TextBox messageText;
        private System.Windows.Forms.TextBox requirementType;
        private System.Windows.Forms.TextBox groupText;
        private System.Windows.Forms.TextBox nextText;
        private System.Windows.Forms.TextBox actionvalueText;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox indexText;
        private System.Windows.Forms.Button removeButton;
        private System.Windows.Forms.TextBox groupDisplayText;
        private System.Windows.Forms.TextBox groupreqText;
        private System.Windows.Forms.TextBox groupgroupText;
        private System.Windows.Forms.Button addGroupButton;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton trueMetCheck;
        private System.Windows.Forms.RadioButton falseMetCheck;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton trueTellerCheck;
        private System.Windows.Forms.RadioButton falseTellerCheck;
        private System.Windows.Forms.ComboBox locktypeBox;
        private System.Windows.Forms.ComboBox actiontypeBox;
        private System.Windows.Forms.ComboBox lockvalueBox;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button groupremoveButton;
        private System.Windows.Forms.TextBox groupindexText;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton groupmettrueButton;
        private System.Windows.Forms.RadioButton groupmetfalseButton;
        private System.Windows.Forms.ComboBox grouplocktypeBox;
        private System.Windows.Forms.ComboBox grouplockvalueBox;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.TextBox fromText;
        private System.Windows.Forms.TextBox toText;
        private System.Windows.Forms.Button swapButton;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox loadText;
        private System.Windows.Forms.Button loadButton;
        private System.Windows.Forms.Button replaceButton;
        private System.Windows.Forms.Button groupSwapButton;
        private System.Windows.Forms.TextBox groupToText;
        private System.Windows.Forms.TextBox groupFromText;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
    }
}