﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace IndentMap
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> fileBuffer = new List<string>();
            List<string> writeBuffer = new List<string>();
            string path = "../../../../brokenland/brokenland/brokenlandcontent/";
            string mapsize = "";

            //load file to a buffer

            using (StreamReader reader = new StreamReader(path + "ObjectData/Map/temp.txt"))
            {
                fileBuffer.Add(reader.ReadLine()); // mapsize 

                //read file
                string input = "";

                while (input != "end")
                {
                    input = reader.ReadLine();

                    if (input != "end")
                    {
                        int lineNo = int.Parse(input);

                        int type = int.Parse(reader.ReadLine());

                        int X = int.Parse(reader.ReadLine());
                        int Y = int.Parse(reader.ReadLine());

                        if (type == 2)
                        {

                            switch (lineNo)
                            {
                                case 4:
                                    lineNo = 5;
                                    break;
                                case 7:
                                    lineNo = 9;
                                    break;
                                case 10:
                                    lineNo = 13;
                                    break;
                                case 13:
                                    lineNo = 17;
                                    break;
                                case 16:
                                    lineNo = 21;
                                    break;
                                case 19:
                                    lineNo = 25;
                                    break;
                                case 22:
                                    lineNo = 29;
                                    break;
                                case 25:
                                    lineNo = 33;
                                    break;
                                case 28:
                                    lineNo = 37;
                                    break;
                                case 31:
                                    lineNo = 41;
                                    break;
                                case 34:
                                    lineNo = 45;
                                    break;
                                case 37:
                                    lineNo = 49;
                                    break;
                                case 40:
                                    lineNo = 53;
                                    break;
                                case 43:
                                    lineNo = 57;
                                    break;
                                case 46:
                                    lineNo = 61;
                                    break;
                                case 49:
                                    lineNo = 65;
                                    break;
                                case 52:
                                    lineNo = 69;
                                    break;
                                case 55:
                                    lineNo = 73;
                                    break;


                            }

                        }

                        fileBuffer.Add(lineNo.ToString());
                        fileBuffer.Add(type.ToString());
                        fileBuffer.Add(X.ToString());
                        fileBuffer.Add(Y.ToString());

                    }
                }
                writeBuffer = fileBuffer;
                writeBuffer.Add("end");

                foreach (string s in writeBuffer)
                    Console.WriteLine(s);

                using (StreamWriter writer = new StreamWriter(path + "ObjectData/Map/parsed.txt"))
                {
                    //writer.WriteLine(mapsize);

                    foreach (string s in writeBuffer)
                    {
                        writer.WriteLine(s);
                    }
                }
                Console.WriteLine("Process done");
                Console.ReadKey();
            }
        }
    }
}
