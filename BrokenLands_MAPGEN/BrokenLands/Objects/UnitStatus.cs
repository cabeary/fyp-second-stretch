﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrokenLands
{
    class UnitStatus : UnitStats
    {


        int _STR; // strength modifier
        int _DEX; // dex modifier
        int _INT; // int modifier
        int _CHA; // charisma modifier
        int _END; // endurance modifier
        int _AGI; // agi modifier
        public int Strength { get { return base.strength + _STR; } set { _STR = value - base.strength; } }
        public int Dexterity { get { return base.dexterity + _DEX; } set { _DEX = value - base.dexterity; } }
        public int Intelligence { get { return base.intelligence + _INT; } set { _INT = value - base.intelligence; } }
        public int Charisma { get { return base.charisma + _CHA; } set { _CHA = value - base.charisma; } }
        public int Endurance { get { return base.endurance + _END; } set { _END = value - base.endurance; } }
        public int Agility { get { return base.agility + _AGI; } set { _AGI = value - base.agility; } }

        //read only
        public int BaseStr { get { return base.strength; } }
        public int BaseDex { get { return base.dexterity; } }
        public int BaseInt { get { return base.intelligence; } }
        public int BaseCha { get { return base.charisma; } }
        public int BaseEnd { get { return base.endurance; } }
        public int BaseAgi { get { return base.agility; } }
        public int Level { get { return level; } }

        float currentHealth;
        int currentAP;
        int currentInitiative;
        int initiativeTimer;
        int range;
        UnitState state;
        public UnitState State { get { return state; } set { state = value; } }
        public float CurrentHealth { get { return currentHealth; } set { currentHealth = value; } }
        public int CurrentAP { get { return currentAP; } set { currentAP = value; } }
        public int CurrentInitiative { get { return currentInitiative; } set { currentInitiative = value; } }
        public int InitiativeTimer { get { return initiativeTimer; } set { initiativeTimer = value; } }
        public int Range { get { return range; } set { range = value; } }
        //public int attack; //attack modified
        //public int defence; // defence modified


        public UnitStatus(int STR, int DEX, int INT, int CHA, int END, int AGI)
        {

            strength = STR;
            dexterity = DEX;
            intelligence = INT;
            charisma = CHA;
            endurance = END;
            agility = AGI;
            range = 1;
            currentHealth = calcHP();
            currentAP = calcAP();
            initiativeTimer = 0;

        }
        /// <summary>
        /// calculate AP!
        /// </summary>
        /// <returns></returns>
        public int calcAP() { return dexterity / 2; }
        public float calcHP() { return (float)(endurance * 1.5); }
        public float calcInitiativeTimer() { return endurance * 1 + agility * 2; }
        public float calcRangedAttack() { return (float)dexterity * 3; }
        public float calcMeleeAttack() { return (float)strength * 3; }
        public float calcDefence() { return (float)endurance * 2; }
        //public float calcEvade() { return (float)dexterity * 2 ; }
        //public float calcAccuracy() { return (float)agility;}
        public void LevelUp()
        {
            level += 1;
            experience -= 100;
            //TODO: ADD STATS!
        }

        //Is it my turn or am i dead ?
        public enum UnitState
        {
            Inactive,
            Active,
            Dead
        }

        public void Update()
        {
            if (experience >= 100)
            {
                LevelUp();
            }
        }


    }
}
