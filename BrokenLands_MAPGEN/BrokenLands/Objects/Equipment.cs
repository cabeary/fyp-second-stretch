﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;


namespace BrokenLands
{

    class Equipment : ICloneable
    {


        float durability = 100;
        Vector2 offset = Vector2.Zero;
        Texture2D tex;
        public float shopValue = 0;
        public float baseAttack = 0;
        public float baseDefence = 0;
        public float baseMagic = 0;
        public float baseWeight = 0;
        public float baseSpeed = 0;
        public EquipType type;
        public string name, prefix, postfix;
        public string splash_t, sprite_t;
        public string desc;
        public int itemID;
        public object Clone()
        {
            Equipment newEquip = (Equipment)this.MemberwiseClone();
            return newEquip;
        }



        public Equipment(EquipType eType, float attack, float def, float magic, float weight, float speed, string name)
        {
            this.type = eType;
            this.baseAttack = attack;
            this.baseDefence = def;
            this.baseMagic = magic;
            this.baseWeight = weight;
            this.baseSpeed = speed;
            this.name = name;
        }

        public Equipment(EquipType eType, float attack, float def, float magic, float weight, float speed, string name, string prefix, string postfix)
        {
            this.type = eType;
            this.baseAttack = attack;
            this.baseDefence = def;
            this.baseMagic = magic;
            this.baseWeight = weight;
            this.baseSpeed = speed;
            this.name = name;
            this.prefix = prefix;
            this.postfix = postfix;
        }

        public Equipment() { }
        public void SetType(string type)
        {
            switch (type)
            {
                case "Helmet":
                    this.type = EquipType.Helmet;
                    break;
                case "Armor":
                    this.type = EquipType.Armor;
                    break;
                case "Legs":
                    //this.type = EquipType.Legs;
                    break;
                case "Gloves":
                    //this.type = EquipType.Gloves;
                    break;
                case "Shoes":
                    //this.type = EquipType.Shoes;
                    break;
                case "Weapon":
                    this.type = EquipType.Weapon;
                    break;
                case "Accessory":
                    this.type = EquipType.Accessory;
                    break;
                default:
                    throw new Exception("WTF BRO? TYPE DOES NOT EXIST!!!");

            }
        }
        public enum EquipType
        {
            Helmet,
            Armor,
            Weapon,
            Accessory
        }



    }
}
