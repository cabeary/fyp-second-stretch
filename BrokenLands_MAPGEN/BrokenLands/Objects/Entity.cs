﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace BrokenLands
{
    class Entity
    {
        //Model model;
        protected Texture2D texture;
        protected Vector2 position;
        public  int rotation;
        public int lineNo = 0;
        public float health;
        public string _NAME;
        public MapCell MyLocation;

        protected Vector2 textureOffset;

        public Vector2 Size;

        public uType MyType { get { return myType; } set { myType = value; } }
        protected uType myType;
        public enum uType
        {
            Player, Enemy, Obstacle, Actor, Loot

        }


        public Entity() { }
        public Entity(Texture2D tex, MapCell position, int lineNo)
        {
            //this.model = model;
            this.texture = tex;
            this.position = position.Location;
            this.MyLocation = position;
            this.lineNo = lineNo;

            MyType = uType.Obstacle;

          //  textureOffset = new Vector2(tex.Width/2,tex.Height/2);
            textureOffset = new Vector2(540,806);

        }

        public void UpdateLocation(Viewport viewport, CameraObj camera)
        {
            //test the world
            Vector3 projected = viewport.Project(MyLocation.getPositon(), camera.proj, camera.view, Matrix.Identity);
            position.X = projected.X;
            position.Y = projected.Y;
        }

        public Vector2 getPosition()
        {
            return position;
        }

        /*   public void setStats(int strength,int dexterity,int intelligence,int charisma,int endurance,int agility) {
               this.strength = strength;
               this.dexterity = dexterity;
               this.intelligence = intelligence;
               this.charisma = charisma;
               this.endurance = endurance;
               this.agility = agility;

               System.Diagnostics.Debug.WriteLine(strength.ToString());
               System.Diagnostics.Debug.WriteLine(endurance.ToString());

           }*/


        public virtual void Draw(SpriteBatch spriteBatch, Viewport viewport, CameraObj camera)
        {
           // spriteBatch.Draw(texture, position, Color.White);
            float scale = 0.2f;
            spriteBatch.Draw(texture, position - textureOffset * scale, null, Color.White, 0, Vector2.Zero, scale, SpriteEffects.None, 0.0f);
        }

        public virtual void Draw(SpriteBatch spriteBatch, Viewport viewport, CameraObj camera,float opacity)
        {
            // spriteBatch.Draw(texture, position, Color.White);
            float scale = 0.2f;
            spriteBatch.Draw(texture, position - textureOffset * scale, null, new Color(1 * opacity, 1 * opacity, 1 * opacity, opacity), 0, Vector2.Zero, scale, SpriteEffects.None, 0.0f);
        }

        public virtual void Update(GameTime gameTime)
        {
            if (health <= 0)
            {
                System.Diagnostics.Debug.WriteLine(_NAME + " IS DEAD!!!");
            }


        }




    }
}
