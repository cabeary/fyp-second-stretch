﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BrokenLands;

namespace BrokenLands
{
    class QuestObject
    {
        public int lineNo = 0;
        public string title;
        public string desc="";

        public bool complete = false;

        public List<Condition> conditionList = new List<Condition>();
        List<Condition> inactiveConditionList = new List<Condition>();


        public QuestObject(string title,int lineNo)
        {
            this.title = title;
            this.lineNo = lineNo;
            //prepare condition
            //usually will be loaded. values will be hard coded for now
            //inactiveConditionList.Add(new Condition("Make the bandits go away"));
            //inactiveConditionList.Add(new Condition("Get money from deadbeat from the bandit"));

            //conditionList.Add(inactiveConditionList[0]);
            //inactiveConditionList.RemoveAt(0);
        }

        public void questComplete()
        {
            Console.WriteLine("Quest comepleted: " + title);
            complete = true;
        }

        public void printQuest() {
            System.Diagnostics.Debug.WriteLine(title);
            System.Diagnostics.Debug.WriteLine(desc);

            foreach(Condition c in conditionList){
                System.Diagnostics.Debug.WriteLine(c.name);
            }
        }


        public void setCondition(int conditionID, int value)
        {


            foreach (Condition c in conditionList)
            {
                c.condition = value;
                Console.WriteLine("Method resolved:" + c.condition);
            }
        }

        public void addCondition(int conditionNo)
        {
            for (int i = 0; i < inactiveConditionList.Count; i++)
            {
                if (inactiveConditionList[i].conditionID == conditionNo)
                {
                    conditionList.Add(inactiveConditionList[i]);
                    inactiveConditionList.RemoveAt(i);
                    break;
                }
            }
        }

    }
}
