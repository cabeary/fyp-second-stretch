﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrokenLands
{
    static class QuestManager
    {
        public static List<QuestObject> questList = new List<QuestObject>();
        public static List<QuestObject> activeList = new List<QuestObject>();


        public static void addQuest(int questNo)
        {
            int no = -1;
            foreach(QuestObject q in questList){
                if(q.lineNo == questNo){
                    no = questNo;
                    break;
                }
            }

            if (no >= 0) {
                //adds are quest to the active list
                activeList.Add(questList[no]);
                questList.RemoveAt(no);
            }

        }

        public static void setCondition(int questNo, int ConditionNo, int value)
        {

            if (questNo >= 0)
            {

                foreach (QuestObject q in questList)
                {
                    if (q.lineNo == questNo)
                    {
                        q.setCondition(ConditionNo, value);
                        break;
                    }
                }
            }
        }

        

        public static void isComplete()
        {
            foreach (QuestObject q in questList)
            {
                int count = 0;
                foreach (Condition c in q.conditionList)
                {
                    if (c.condition > 0)
                        count++;

                }

                if (count == q.conditionList.Count)
                    q.questComplete();
            }
        }

        public static void addCondition(int questNo, int conditionNo)
        {

            if (questNo >= 0)
            {

                foreach (QuestObject q in questList)
                {
                    if (q.lineNo == questNo)
                    {
                        q.addCondition(conditionNo);
                        break;
                    }
                }
            }
        }


    }
}
