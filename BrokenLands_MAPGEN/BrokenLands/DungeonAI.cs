﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace BrokenLands
{
    class DungeonAI : SceneObject
    {
        public string map;

        public DungeonAI(Texture2D texture,int lineNo,MapCell MyLocation,string map) : base(texture,lineNo,MyLocation) {
            //this one gets sent to the dungeonAI List
            this.map = map;
        }
    }
}
