﻿using System;
using System.Windows.Forms;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace BrokenLands
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SpriteFont font;
        CameraObj TestCamera;
        KeyboardState currentState, prevState;

        //*************************************
        MapGenerator generator;
        Map map;
        UI ui;
        Texture2D bg;
        Texture2D bluetile;

        public string mapName="smiley";
        public bool newmap = false;
        public int gridSize = 12;

        Model CubeModel,PlaneModel,SpikeModel;
        Model Tile;

        MouseState prevMouseState;

        Vector3 centre = Vector3.Zero;

        //*************************************

        public Game1()
            : base()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.IsFullScreen = false;
            graphics.PreferredBackBufferHeight = 720;
            graphics.PreferredBackBufferWidth = 1280;

            Content.RootDirectory = "Content";
            this.IsMouseVisible = true;

            Application.EnableVisualStyles();
            Application.Run(new MapOptions(this));
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            //Vector3 camPos = new Vector3(30, 18, -3);
            Vector3 camPos = new Vector3(1, 100, 0);
            TestCamera = new CameraObj(this, new Vector3(0, 59.6f, 84.288f), new Vector3(0, 0, 0), Vector3.Up, 1, 1000f, 1f, true);
            prevMouseState = Mouse.GetState();
            base.Initialize();

        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            font = Content.Load<SpriteFont>("defaultFont");

            CubeModel = Content.Load<Model>("Cube");
            PlaneModel = Content.Load<Model>("Tile");
            SpikeModel = Content.Load<Model>("alienSpike");
            Tile = Content.Load<Model>("Tile");
            bluetile = Content.Load<Texture2D>("Blue-Tile");

            map = new Map(bluetile);
            generator = new MapGenerator(this,PlaneModel,map);
           // generator.NewMap(4f, 16);

            if(newmap)
                generator.NewMap(13, gridSize);
            else
            generator.ReadMap(ObjectLoader.path + "ObjectData/Map/" + mapName + ".map");

            generator.debugExits();
            generator.debugPlayerNodes();


            bg = Content.Load<Texture2D>("menuBack");
            ui = new UI(bg,generator,font);
            //map.NewMap(4f, 3);

            centre = map.gridNodes[(int)map.gridNodes.GetLength(0) / 2, (int)map.gridNodes.GetLength(0) / 2].getPositon();
            centre -= new Vector3(1.1f, 0.1f, 0.8f);

            // TODO: use this.Content to load your game content here
            //generator.loader.loadObject(1,5,Vector2.Zero);

        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == Microsoft.Xna.Framework.Input.ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Microsoft.Xna.Framework.Input.Keys.Escape))
                Exit();
            

            TestCamera.CreateLookAt();
            currentState = Keyboard.GetState();


            if (Keyboard.GetState().IsKeyDown(Microsoft.Xna.Framework.Input.Keys.Up))
                TestCamera.PanCamera(new Vector3(0, 0, -1));
            if (Keyboard.GetState().IsKeyDown(Microsoft.Xna.Framework.Input.Keys.Down))
                TestCamera.PanCamera(new Vector3(0, 0, 1));
            if (Keyboard.GetState().IsKeyDown(Microsoft.Xna.Framework.Input.Keys.Left))
                TestCamera.PanCamera(new Vector3(-1, 0, 0));
            if (Keyboard.GetState().IsKeyDown(Microsoft.Xna.Framework.Input.Keys.Right))
                TestCamera.PanCamera(new Vector3(1, 0, 0));
            if (Keyboard.GetState().IsKeyDown(Microsoft.Xna.Framework.Input.Keys.E))
                TestCamera.VCameraPosition += Vector3.Up;
            if (Keyboard.GetState().IsKeyDown(Microsoft.Xna.Framework.Input.Keys.R))
                TestCamera.VCameraPosition -= Vector3.Up;


            if (Keyboard.GetState().IsKeyDown(Microsoft.Xna.Framework.Input.Keys.Z))
            {
                TestCamera.rot += 0.01f;
                System.Diagnostics.Debug.WriteLine(TestCamera.rot);
            }

            if (Keyboard.GetState().IsKeyDown(Microsoft.Xna.Framework.Input.Keys.X))
            {
                TestCamera.rot -= 0.01f;
                System.Diagnostics.Debug.WriteLine(TestCamera.rot);
            }


            
            
            for (int x = 0; x < map.gridNodes.GetLength(0); x++)
            {
                for (int z = 0; z < map.gridNodes.GetLength(0); z++)
                {
                    if (MouseController.Selected(map.getTileBounds(new Vector2(x, z))))
                    {

                        if (MouseController.LeftMouseClick() && !MouseController.Selected(ui.backgroundsize))
                        {
                            int lineNo = 0;
                            if (ui.selectedObject == "PlayerNode" || ui.selectedObject == "MapNode")
                            {
                                if (ui.selectedObject == "PlayerNode")
                                generator.loader.loadObject(0,3,new Vector2(x,z));
                                else
                                    generator.loader.loadObject(0, -1, new Vector2(x, z));

                                //debug
                                map.debugList.Clear();
                                generator.debugExits();
                                generator.debugPlayerNodes();
                            }

                            else
                            {

                                switch (ui.objectType)
                                {
                                    case 0://enemy
                                        try
                                        {
                                            lineNo = ui.enemyDict[ui.selectedObject];
                                            generator.loader.loadObject(lineNo, 0, new Vector2(x, z));
                                        }
                                        catch (Exception ex)
                                        {
                                            System.Diagnostics.Debug.WriteLine(ex.ToString());
                                        }
                                        break;
                                    //case 1:
                                    //    try
                                    //    {
                                    //        lineNo = ui.obstacleDict[ui.selectedObject];
                                    //        generator.loader.loadObject(lineNo, 0, new Vector2(x, z));
                                    //    }
                                    //    catch (Exception ex)
                                    //    {
                                    //        System.Diagnostics.Debug.WriteLine("Object not found");
                                    //    }
                                    //    break;

                                    case 2://scene objects
                                        try
                                        {
                                            lineNo = ui.sceneDict[ui.selectedObject];
                                            generator.loader.loadObject(lineNo, 2, new Vector2(x, z));
                                        }
                                        catch (Exception ex)
                                        {
                                            System.Diagnostics.Debug.WriteLine(ex.ToString());
                                        }
                                        break;

                                    case 4://actor
                                        try
                                        {
                                            System.Diagnostics.Debug.WriteLine("Loading actor object");
                                            lineNo = ui.actorDict[ui.selectedObject];
                                            generator.loader.loadObject(lineNo, 4, new Vector2(x, z));
                                        }
                                        catch (Exception ex)
                                        {
                                            System.Diagnostics.Debug.WriteLine(ex);
                                        }
                                        break;
                                        
                                    case 7:// ai
                                        try
                                        {
                                            lineNo = ui.aiDict[ui.selectedObject];
                                            generator.loader.loadObject(lineNo, 7, new Vector2(x, z));
                                        }
                                        catch (Exception ex)
                                        {
                                            System.Diagnostics.Debug.WriteLine("Object not found");
                                        }
                                        break;

                                    case 9:// overlay
                                        try
                                        {
                                            lineNo = ui.overlayDict[ui.selectedObject];
                                            generator.loader.loadObject(lineNo, 9, new Vector2(x, z));
                                        }
                                        catch (Exception ex)
                                        {
                                            System.Diagnostics.Debug.WriteLine("Object not found");
                                        }
                                        break;


                                    case 10: // underlay
                                        try
                                        {
                                            lineNo = ui.underlayDict[ui.selectedObject];
                                            System.Diagnostics.Debug.WriteLine("Line No:"+lineNo);
                                            generator.loader.loadObject(lineNo, 10, new Vector2(x, z));
                                        }
                                        catch (Exception ex)
                                        {
                                            System.Diagnostics.Debug.WriteLine("Underlay Object not found");
                                        }
                                        break;

                                    case 11: //loot
                                        try
                                        {
                                            lineNo = ui.lootDict[ui.selectedObject];
                                            System.Diagnostics.Debug.WriteLine("Line No:" + lineNo);
                                            generator.loader.loadObject(lineNo, 11, new Vector2(x, z));
                                        }
                                        catch (Exception ex)
                                        {
                                            System.Diagnostics.Debug.WriteLine("Loot Object not found");
                                        }
                                        break;

                                    //default:
                                    //    try
                                    //    {
                                    //        lineNo = ui.sceneDict[ui.selectedObject];
                                    //        generator.loader.loadObject(lineNo, 0, new Vector2(x, z));
                                    //    }
                                    //    catch (Exception ex)
                                    //    {
                                    //        System.Diagnostics.Debug.WriteLine("Object not found");
                                    //    }
                                       // break;
                                }


                            }


                        }

                        if(MouseController.RightMouseClick()){
                            for (int i = 0; i < map.enemList.Count; i++)
                            {
                                if (map.enemList[i].MyLocation.Location == new Vector2(x, z))
                                {
                                    map.enemList.RemoveAt(i);

                                    //debug
                                    map.debugList.Clear();
                                    generator.debugExits();
                                    generator.debugPlayerNodes();
                                }



                                //remove map/playernodes
                            }



                            for (int i = 0; i < map.actorList.Count; i++)
                            {
                                if (map.actorList[i].MyLocation.Location == new Vector2(x, z))
                                {
                                    map.actorList.RemoveAt(i);

                                    //debug
                                    map.debugList.Clear();
                                    generator.debugExits();
                                    generator.debugPlayerNodes();
                                }



                                //remove map/playernodes
                            }

                            //remove scene objects

                            for (int i = 0; i < map.sceneList.Count; i++)
                            {
                                if (map.sceneList[i].MyLocation.Location == new Vector2(x, z))
                                {
                                    map.sceneList.RemoveAt(i);

                                    //debug
                                    map.debugList.Clear();
                                    generator.debugExits();
                                    generator.debugPlayerNodes();
                                }

                            }


                            //remove overlay
                            for (int i = 0; i < map.overlayList.Count; i++)
                            {
                                if (map.overlayList[i].MyLocation.Location == new Vector2(x, z))
                                {
                                    map.overlayList.RemoveAt(i);

                                    //debug
                                    map.debugList.Clear();
                                    generator.debugExits();
                                    generator.debugPlayerNodes();
                                }

                            }


                            //remove underlay
                            for (int i = 0; i < map.underlayList.Count; i++)
                            {
                                if (map.underlayList[i].MyLocation.Location == new Vector2(x, z))
                                {
                                    map.underlayList.RemoveAt(i);

                                    //debug
                                    map.debugList.Clear();
                                    generator.debugExits();
                                    generator.debugPlayerNodes();
                                }

                            }

                            //remove ai
                            for (int i = 0; i < map.aiList.Count; i++)
                            {
                                if (map.aiList[i].MyLocation.Location == new Vector2(x, z))
                                {
                                    map.aiList.RemoveAt(i);

                                    //debug
                                    map.debugList.Clear();
                                    generator.debugExits();
                                    generator.debugPlayerNodes();
                                }

                            }




                            for (int i = 0; i < map.sectionNodes.Count; i++)
                            {
                                if (map.sectionNodes[i] == new Vector2(x, z))
                                {
                                    map.sectionNodes.RemoveAt(i);
                                    generator.borderNodes.RemoveAt(i);

                                    map.debugList.Clear();
                                    generator.debugExits();
                                }
                            }

                            //remove playernodes from player nodeList

                            for (int i = 0; i < generator.playerNode.Count; i++)
                            {
                                if (generator.playerNode[i] == new Vector2(x, z))
                                {
                                    generator.playerNode.RemoveAt(i);

                                    //debug
                                    map.debugList.Clear();
                                    generator.debugExits();
                                    generator.debugPlayerNodes();
                                }
                            }

                            //remove border


                        }

                      
                    }
                }
            }
            //

            if (currentState.IsKeyDown(Microsoft.Xna.Framework.Input.Keys.S) && !prevState.IsKeyDown(Microsoft.Xna.Framework.Input.Keys.S))
            {
                generator.WriteMap(ObjectLoader.path +"ObjectData/Map/" + mapName + ".map");
            }



            prevState = currentState;

            ui.Update();

            MouseController.Update(this.GraphicsDevice.Viewport, TestCamera);
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.LimeGreen);

           // *******3D DRAW SETTINGS**********************
            GraphicsDevice.BlendState = BlendState.Opaque;
            GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            GraphicsDevice.SamplerStates[0] = SamplerState.LinearWrap;
            // *******3D DRAW SETTINGS**********************

           // DrawModel(Tile, Matrix.Identity, TestCamera);

            map.debugDraw(spriteBatch, TestCamera, this.GraphicsDevice);
            spriteBatch.Begin();

            
            spriteBatch.End();

            ui.Draw(spriteBatch);

            base.Draw(gameTime);
        }

        private void DrawModel(Model model, Matrix world, CameraObj camera)
        {//16.0f
            world = Matrix.Identity * Matrix.CreateTranslation(centre) * Matrix.CreateScale(16);
            Matrix[] transforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(transforms);

            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.EnableDefaultLighting();
                    effect.Texture =map.floorTexture;
                    effect.DiffuseColor = new Vector3(0.7f, 0.7f, 0.7f);
                    effect.World = transforms[mesh.ParentBone.Index] * world;
                    effect.View = camera.view;
                    effect.Projection = camera.proj;
                }
                mesh.Draw();
            }
        }
        

    }
}
