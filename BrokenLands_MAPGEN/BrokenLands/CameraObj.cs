﻿
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrokenLands
{
    //object culling
    public class CameraObj
    {
        public Vector3 VCameraOffset, VCameraPosition, VCameraDirection, VCameraUp;
        public float FCameraSpeed;
        public Matrix world, view, proj;
        public float FCameraNearPlane, FCameraFarPlane;
        public bool orthographic;
        public Vector3 target;
        public Vector3 distance;


        public float rot = 0;

        public Vector3 RotatedCameraView
        {
            get
            {

                Vector3 rotated = Vector3.Transform(VCameraPosition, Matrix.CreateRotationY(-MathHelper.PiOver4));
                // VCameraPosition = rotated;

                return rotated;
            }
        }

        public bool inView(BoundingBox boundingBox) {
            return new BoundingFrustum(view * proj).Intersects(boundingBox);
        }

        public void PanCamera(Vector3 vec)
        {
            VCameraPosition += vec;
            target += vec;
        }
        public CameraObj(Game game, Vector3 pos, Vector3 target, Vector3 up, float Near, float Far, float speed, bool orthographic)
        {
            this.orthographic = orthographic;
            this.target = target;
            VCameraPosition = pos;
            if (orthographic)
                VCameraDirection = target - RotatedCameraView;
            else
                VCameraDirection = target - pos;
            VCameraDirection.Normalize();
            VCameraUp = up;
            FCameraNearPlane = Near;
            FCameraFarPlane = Far;
            FCameraSpeed = speed;
            world = Matrix.Identity;


            distance = target - VCameraPosition;

            if (orthographic)
            {
                view = Matrix.CreateLookAt(RotatedCameraView, target, VCameraUp);
                proj = Matrix.CreateOrthographic(game.GraphicsDevice.Viewport.Width * 0.11f, game.GraphicsDevice.Viewport.Height * 0.11f, FCameraNearPlane, FCameraFarPlane);

            }
            else
            {
                view = Matrix.CreateLookAt(VCameraPosition, target, VCameraUp);
                proj = Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4, game.GraphicsDevice.Viewport.AspectRatio, FCameraNearPlane, FCameraFarPlane);
            }
        }
        public void CreateLookAt()
        {

            if (orthographic)
            {
                // maintain the camera angle at 30 degree, 45 degree


                //VCameraPosition.Z = (float)((VCameraPosition.Y - target.Y) / (Math.Tan(MathHelper.ToRadians(30))));

                view = Matrix.CreateLookAt(RotatedCameraView, RotatedCameraView + VCameraDirection, VCameraUp);
            }
            else
            {
                view = Matrix.CreateLookAt(VCameraPosition, VCameraPosition + VCameraDirection, VCameraUp);
            }
        }



    }


}
