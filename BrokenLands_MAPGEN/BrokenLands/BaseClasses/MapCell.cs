﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
namespace BrokenLands
{
    class MapCell : Node
    {
        //cost to move over
        int cost = 0;
        Vector2 location;
        //entity that is occupying the mapcell. if none, = null
        Entity ent;

        //pathfinding variables
        bool marked;
        MapCell parentLocation;
        int distanceMoved, distanceLeft, distanceTotal;
        //pathfinding end
        public Entity Ent { get { return ent; } set { ent = value; } }
        public Vector2 Location { get { return location; } set { location = value; } }
        public int Cost { get { return cost; } set { cost = value; } }

        public bool Marked { get { return marked; } set { marked = value; } }
        public MapCell ParentLocation { get { return parentLocation; } set { parentLocation = value; } }
        public int DistanceTotal { get { return distanceTotal; } }
        public int DistanceMoved { get { return distanceMoved; } }
        public int DistanceLeft { get { return distanceLeft; } }
        
        public MapCell(int cost, Vector2 location) { }
        public MapCell(int cost, Vector2 location, Vector3 position,Model model, float scale) : base(position,model,scale)
        {
            this.cost = cost;
            this.location = location;
        }

        public void updateDistance(int dm, int dl)
        {
            distanceMoved = dm;
            distanceLeft = dl;
            distanceTotal = dm + dl;
        }


    }
}
