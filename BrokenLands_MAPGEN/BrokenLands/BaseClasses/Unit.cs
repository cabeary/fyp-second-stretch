﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
namespace BrokenLands
{
    class Unit : Entity
    {
        string name;
        //UnitStats myStats;
        UnitStatus myStatus;
        float initiative;
        float actionPoints;
        bool isAlive;
        bool isActive;
        bool isSelected;
        MapCell myLocation;

        public MapCell MyLocation { get { return myLocation; } set { myLocation = value; base.position = value.Location; } }

        //List<Equipment> equips;
        public Unit(int STR, int DEX, int INT, int CHA, int END, int AGI, MapCell loc, Texture2D tex, int lineNo, string Name)
            : base(tex, loc.Location, lineNo)
        {
            myStatus = new UnitStatus(STR, DEX, INT, CHA, END, AGI);
            //name = "Ara";
            base._NAME = Name;
            MyLocation = loc;
            loc.Ent = this;
            /*
            myStatus.Agility = 10;
            myStatus.Charisma = 10;
            myStatus.Dexterity = 10;
            myStatus.Endurance = 10;
            myStatus.Intelligence = 10;
            myStatus.Strength = 10;
            */
            base.health = myStatus.calcHP();
            myStatus.CurrentAP = myStatus.calcAP();
            myStatus.CurrentInitiative = 100;
        }

        public string Name { get { return name; } set { name = value; } }
        //public UnitStats myStatus { get { return myStatus; } set { myStatus = value; } }
        public UnitStatus MyStatus { get { return myStatus; } set { myStatus = value; } }
        public float Initiative { get { return initiative; } set { initiative = value; } }
        public float ActionPoints { get { return actionPoints; } set { actionPoints = value; } }
        public bool IsAlive { get { return isAlive; } set { isAlive = value; } }
        public bool IsActive { get { return isActive; } set { isActive = value; } }
        public bool IsSelected { get { return isSelected; } set { isSelected = value; } }

        public void Move(MapCell destination)
        {

        }
        public String Describe_Self()
        {

            return "------------------------------------Name: " + base._NAME
                + "-------------------------------\nTexture: " + base.texture
                + "\nPosition: " + base.position
                + "\nStrength: " + myStatus.Strength
                + "\nDexterity: " + myStatus.Dexterity
                + "\nIntelligence: " + myStatus.Intelligence
                + "\nCharisma: " + myStatus.Charisma
                + "\nEndurance: " + myStatus.Endurance
                + "\nAgility: " + myStatus.Agility
                + "\nCurrentAP: " + myStatus.CurrentAP
                + "/MaxAP: " + myStatus.calcAP()
                + "\nCurrentInitiative: " + myStatus.CurrentInitiative
                + "\nMyInitiativeStats: " + myStatus.calcInitiativeTimer()
                + "\nBase Melee Attack: " + myStatus.calcMeleeAttack()
                + "\nBase Ranged Attack: " + myStatus.calcRangedAttack()
                + "\nDefence: " + myStatus.calcDefence()
                // + "\nEvasion: " + myStatus.calcEvade()                
                + "\nCurrentHealth: " + base.health
                + "/MaxHealth: " + myStatus.calcHP() + "\n---------------------------------------------------------------------------------------";
        }

    }
}
