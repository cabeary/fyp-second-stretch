﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace BrokenLands
{
    class Entity
    {
        //Model model;
        protected Texture2D texture;
        protected Vector2 position;
        public Vector3 posOffset = Vector3.Zero;
        protected int rotation;
        public int lineNo = 0;
        public float health;
        public string _NAME;
        public static Texture2D pointer;
        protected Vector2 pointerPos = Vector2.Zero;

        public bool isActive = true;
        Vector2 textureOffset;
        protected uType myType = uType.Obstacle;
        public uType MyType { get { return myType; } }
        public enum uType

        {
        Player, Enemy, Obstacle

        }

        public Entity() { }
        public Entity(Texture2D tex, Vector2 position, int lineNo)
        {
            //this.model = model;
            this.texture = tex;
            this.position = position;
            this.lineNo = lineNo;


            textureOffset = new Vector2(540, 806);

        }

        public Vector2 getPosition()
        {
            return position;
        }
        public void setPosition(Vector2 vec)
        {
            position = vec;
        }

        /*   public void setStats(int strength,int dexterity,int intelligence,int charisma,int endurance,int agility) {
               this.strength = strength;
               this.dexterity = dexterity;
               this.intelligence = intelligence;
               this.charisma = charisma;
               this.endurance = endurance;
               this.agility = agility;

               System.Diagnostics.Debug.WriteLine(strength.ToString());
               System.Diagnostics.Debug.WriteLine(endurance.ToString());

           }*/

      

        public virtual void Draw(SpriteBatch spriteBatch, Viewport viewport, CameraObj camera)
        {
            // spriteBatch.Draw(texture, position, Color.White);
            float scale = 0.2f;
            spriteBatch.Draw(texture, position - textureOffset * scale, null, Color.White, 0, Vector2.Zero, scale, SpriteEffects.None, 0.0f);
           
        }
        public void DrawPointer(SpriteBatch spriteBatch)
        {
                spriteBatch.Draw(pointer, pointerPos - new Vector2(pointer.Width / 2, pointer.Height / 2) * 0.5f, null, Color.White, 0, Vector2.Zero, 0.5f, SpriteEffects.None, 0.0f);
        }
        
        public virtual void Update(GameTime gameTime)
        {
            if (health <= 0)
            {
                System.Diagnostics.Debug.WriteLine(_NAME + " IS DEAD!!!");
            }


        }




    }
}
