﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace BrokenLands
{
    class SceneObject
    {
        Texture2D texture;
        Vector2 position;
        public int lineNo=0;

        public SceneObject(Texture2D texture,int lineNo) {
           
            this.texture = texture;
            position = Vector2.Zero;
           this.lineNo = lineNo;
           position.Y -= 350;
           position.X -= 10;
        }

        public void Draw(SpriteBatch spritebatch) {
            float scale = 1.2f;
            spritebatch.Draw(texture, position, null, Color.White, 0, Vector2.Zero, scale, SpriteEffects.None, 0.0f);
        }

        public void Update(GameTime gametime) { }




    }
    }

