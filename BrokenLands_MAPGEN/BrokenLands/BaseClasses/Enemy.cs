﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;



namespace BrokenLands
{
    class Enemy : Unit
    {
        
       public Enemy(int STR, int DEX, int INT, int CHA, int END, int AGI, MapCell loc,Texture2D tex,int lineNo,string Name) : base(STR, DEX, INT, CHA, END, AGI, loc,tex,lineNo,Name) { 
        
        }
       
        /*
        //location on map
        MapLocation nodeLoc;
        //model offset
        Vector3 modelPos; 
        //direction we're facing
        Vector3 rotation;
        //level of enemy
        float level;
        //initiative of enemy
        float initiativeTimer;
        //attack range (maybe moving to combatengine later on)
        Vector3 attackRange;
        //stats (maybe moving to combatengine)
        float currentHealth, attack, defence, actionPoints;

       
        
        public enum EnemyState {
            Patrolling,
            Idle,
            Alerted,
            Flinching,
            Attacking            
        }
        
        EnemyState enemyState = EnemyState.Idle;
        

     

        //EnemyState enemyState = EnemyState.Idle;

        public Enemy() {
            on_create();
        }

        void play_turn() {
            update_enemy_location();
            update_goals();
            set_target();
            execute_move_action();
            execute_attack_action();

        }
        void on_create() {
            update_enemy_location();
            //nodePos = new Vector2(0, 0);
            modelPos = new Vector3(0, 0, 1);
            rotation = new Vector3(0, 0, 0);
            //speed = 0;
            //aggroRange = new Vector3(10, 10, 0);
            attackRange = new Vector3(1, 1, 10);
            currentHealth = 100;
            attack = 1;
            defence = 1;
            actionPoints = 3;
            initiativeTimer = 10;
        }

        void get_hit() { }
        
        bool check_in_range() { return false; }
        void execute_attack_action() {}
        void execute_move_action() { }
        void execute_death_action() { }
        //Enemy return() { return this; }
        void add_goal() { }
        void update_goals() { }
        void add_target() { }
        void update_enemy_location() { }
        void set_target() { }
        public void Update() { }
        */
    }
}
