﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BrokenLands
{
    public partial class MapOptions : Form
    {

        Game1 handle;

        public MapOptions(Game1 game)
        {
            InitializeComponent();
            handle = game;
            sizeText.Enabled = false;
        }

        private void submitButton_Click(object sender, EventArgs e)
        {
            handle.mapName = nameText.Text;
            handle.newmap = newCheck.Checked;

            if(newCheck.Checked)
            handle.gridSize = int.Parse(sizeText.Text);

            this.Close();
        }

        private void newCheck_CheckedChanged(object sender, EventArgs e)
        {
            sizeText.Enabled = newCheck.Checked;
        }
    }
}
