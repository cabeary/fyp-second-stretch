﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
namespace BrokenLands
{
    class MapLocations
    {
        //cost to move over
        int cost;
        Vector2 location;

        //pathfinding variables
        bool occupied, marked;
        //Entity ent;
        MapLocations parentLocation;
        int distanceMoved, distanceLeft, distanceTotal;
        //pathfinding end


        public MapLocations(int cost, Vector2 location)
        {
            this.cost = cost;
            this.location = location;
        }

        public Vector2 getLocation()
        {
            return location;
        }

        public int getCost()
        {
            return cost;
        }

        public void updateDistance(int dm, int dl)
        {
            distanceMoved = dm;
            distanceLeft = dl;
            distanceTotal = dm + dl;
        }

        public void setParentLocation(MapLocations parentNode)
        {
            parentLocation = parentNode;
        }

        public MapLocations getParentLocation()
        {
            return parentLocation;
        }

        public void setMark(bool mark)
        {
            marked = mark;
        }

        public bool isOccupied()
        {
            return occupied;
        }
        public bool isMarked()
        {
            return marked;
        }
        public int getDistanceMoved()
        {
            return distanceMoved;
        }
        public int getDistanceLeft()
        {
            return distanceLeft;
        }
        public int getDistanceTotal()
        {
            return distanceTotal;
        }


    }
}
