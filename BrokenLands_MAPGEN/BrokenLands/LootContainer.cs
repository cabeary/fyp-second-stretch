﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace BrokenLands
{
    class LootContainer : SceneObject
    {
        public List<Item> itemList = new List<Item>();
        public bool showGUI =false;

        //include container UI here

        public LootContainer(Texture2D text, int lineno, MapCell Location) : base(text,lineno,Location) {
            MyType = uType.Loot;

        }

        public void toggleLoot() {

        }

        public void debugLoot() {
            foreach(Item i in itemList){
                System.Diagnostics.Debug.WriteLine("Item:"+i.name);
            }
        }

    }
}
