﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrokenLands
{
    static class QuestReactor
    {
        //public int currenttalker = 0;
        //takes in an action and creates the appropriate response
        //action, outcome, value,condition
        //quest complete should be an action
        public static void reaction(Action action, int questID)
        {

            switch (action.action)
            {
                case 1: //fight -> fight leads to load level
                    QuestManager.setCondition(questID, action.value, 1);
                    break;
                case 2: //bribe
                    //deduct funds
                    //player.money -= action.value;
                    QuestManager.setCondition(questID, action.value, 2);

                    break;
                case 3: //melee
                    QuestManager.setCondition(questID, action.value, 3);
                    break;
                case 4: // scavenge
                    QuestManager.setCondition(questID, action.value, 4);
                    break;

                case 5: //lockpick
                    QuestManager.setCondition(questID, action.value, 5);
                    break;

                case 6://barter
                    QuestManager.setCondition(questID, action.value, 6);
                    break;
                case 7://heal
                    QuestManager.setCondition(questID, action.value, 7);
                    break;

                case 8: //help 
                    //add next condition
                    QuestManager.addCondition(questID, action.value);
                    break;
                case 9:// add quest
                    QuestManager.addQuest(questID);
                    break;

                case 10://clear dungeon
                    QuestManager.addCondition(questID, action.value);
                    break;
                case 11:// leave the dungeon
                    break;
                default: // no action
                    break;
            }

            QuestManager.isComplete(); //checks if a quest has been
            //everytime we do an action
        }
    }
}
