﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
namespace BrokenLands
{
    static class MouseController
    {
        public static Ray raycast;
        static MouseState state;
        static MouseState prevstate = new MouseState();
        public static int mouseScrollValue = 0;


        public static void Update(Viewport viewport,CameraObj camera) {
            mouseScrollValue = Mouse.GetState().ScrollWheelValue;
            
            prevstate = state;
            state = Mouse.GetState();

            //get mouse mouse position
            Point mousePos = Mouse.GetState().Position;

            //mouse position in 3d space (at clipping plane)
            Vector3 pos1 = viewport.Unproject(new Vector3(mousePos.X, mousePos.Y, 0), camera.proj, camera.view,Matrix.Identity);


            ////mouse position + 1. project the position forward. Like camera.forward
            Vector3 pos2 = viewport.Unproject(new Vector3(mousePos.X, mousePos.Y, 1), camera.proj, camera.view, Matrix.Identity);
            //// direction to cast to
            Vector3 dir = Vector3.Normalize(pos2 - pos1);

            raycast = new Ray(pos1,dir);

        }

        public static bool Selected(BoundingBox box) {
            //if ray intersects w box
            return raycast.Intersects(box)>0;
        }

        public static bool Selected(Rectangle box) {
            return box.Contains(state.Position);
        }

        public static Vector2 getPosition() {
            return new Vector2(state.Position.X,state.Position.Y);
        }

        public static bool RightMouseClick() {
            return (state.RightButton == ButtonState.Pressed && prevstate.RightButton == ButtonState.Released);
        }

        public static bool LeftMouseClick()
        {
            return (state.LeftButton == ButtonState.Pressed && prevstate.LeftButton == ButtonState.Released);
        }
       
    }
}
