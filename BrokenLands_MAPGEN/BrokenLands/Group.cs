﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrokenLands
{
    public class Group
    {
            public Lock locK;
            int group;

            public Group(Lock locK, int group) {
                this.locK = locK;
                this.group = group;
            }

            public bool getLock()
            {
                return locK.getMet();
            }

            public int getGroup()
            {
                return group;
            }


            public void requirementCheck()
            {

                switch (locK.type)
                {

                    case 0: //cases for stat check
                        break;
                    case 1://cases for skill check
                        break;
                    case 2://cases for condition check
                        bool found = false;
                        foreach (QuestObject q in QuestManager.questList)
                        {

                            foreach (Condition c in q.conditionList)
                            {
                                if (locK.value2 == c.conditionID) //need to fetch a conditionid
                                {
                                    found = true;
                                    if (locK.value == c.condition)
                                    {
                                        locK.toggleMet();
                                        break;
                                    }
                                }

                                if (found)
                                    break;
                            }

                        }
                        break;
                    case 3://case check for quest
                        foreach (QuestObject q in QuestManager.questList)
                        {
                            if (locK.value2 == q.lineNo)
                            { //checks for q id

                                if (locK.value == (Convert.ToInt32(q.complete)))
                                {
                                    locK.toggleMet();
                                }


                                break;
                            }
                        }
                        break;
                    default: //do nothing
                        break;
                }
            }
        }
    }

