﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
using BrokenLands;

namespace BrokenLands
{
    class Actor : Entity
    {
        //TODO: Remove condition and conditionID

        //protected Condition condition;
        public DialogTree dialogtree = new DialogTree();
        public int questID;
        public Entity loadObject;
        protected string name;
        //public MapCell MyLocation;
        private bool chat = false;
        //protected List<Dialogue> dialogue = new List<Dialogue>();
        //list of possible actions

        public Actor(string name, int questID, Entity loadObject, Texture2D texture, MapCell location, int lineNo)
            : base(texture, location, lineNo)
        {
            this.name = name;
            this.questID = questID;
            this.loadObject = loadObject;
            this.MyLocation = location;
            this.position = location.Location;
            //myType = uType.Actor;
            System.Diagnostics.Debug.WriteLine("Actor spawned");
        }

        public bool getChat()
        {
            return chat;
        }

        public void printDialog()
        {
            foreach (Dialog d in dialogtree.dialogList)
            {
                System.Diagnostics.Debug.WriteLine(d.message);
            }
        }

        public void toggleChat()
        {
            chat = !chat;
            dialogtree.getDefaultGroup(); // set the default group
        }


        public void unlockSlef()
        {
            //if actor is a door, it can remove itself from the tile.

        }

        //public void UpdateChat(MouseController controller)
        //{
        //    dialogtree.fetchConversattion(); // loads active list and display

        //    //detect input for each action
        //    int nextgroup = dialogtree.currentGroup;
        //    Action action = new Action(0, 0);

        //    foreach (Dialog d in dialogtree.activeList)
        //    {
        //        //if within bounds and click, set the action and set the next group
        //        if (controller.Selected(d.box) && controller.LeftMouseClick())
        //        {
        //            action = d.getAction();
        //            nextgroup = d.getNextGroup();
        //        }
        //    }

        //    if (action.action >= 0)
        //    {
        //        dialogtree.currentGroup = nextgroup;
        //        QuestReactor.reaction(action, questID);
        //    }
        //    else
        //    {
        //        toggleChat();

        //    }

        //}

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public void DrawChat(SpriteBatch batch, SpriteFont font)
        {
            if (chat)
            {
                Vector2 start = new Vector2(250, 400);
                float offsety = 20;
                batch.DrawString(font, name + ":" + dialogtree.displayMessage, start, Color.White);

                foreach (Dialog d in dialogtree.activeList)
                {
                    Vector2 heightWidth = font.MeasureString(d.message);
                    start.Y += offsety + heightWidth.Y;
                    d.setBounds(heightWidth, start);
                    batch.DrawString(font, d.message, start, Color.White);
                }
            }
        }

        public void UpdateLocation(Viewport viewport, CameraObj camera)
        {
            //test the world
            Vector3 projected = viewport.Project(MyLocation.getPositon(), camera.proj, camera.view, Matrix.Identity);
            position.X = projected.X;
            position.Y = projected.Y;
        }

    }
}
