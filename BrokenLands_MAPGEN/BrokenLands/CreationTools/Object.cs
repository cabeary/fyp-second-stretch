﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.IO;

namespace BrokenLands
{
    //objects to load into game - enemies and obstacles
    struct Object
    {
        public string name;
        public int nodeIndex;
        public int type;
        public Properties properties;

        public void loadProperties() { }
        public void setProperties(string name)
        {

        }


    }

    struct Properties {
        string name;
        int objectCode;
        string modelName;
        Vector2 dimensions;
    }
}
