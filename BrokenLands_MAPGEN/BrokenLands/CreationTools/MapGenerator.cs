﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.IO;
using BrokenLands;

namespace BrokenLands
{
    //TODO: build players and exits from node positons

    //split this to map and map generator
    class MapGenerator
    {
        Model defaultTile;



        //Node[,] gridNodes; //nodes that hold the actual grid. nodes here will be used for 2x2 objects



        //************************************************************************************
        public ObjectLoader loader; //takes in metadata and spits out objects ->list files

        List<Object> scene;




        //List<Object> obstacleList = new List<Object>();

        public List<Vector2> playerNode = new List<Vector2>();
        public List<Vector2> borderNodes = new List<Vector2>();
        public Map handle;

        public Game game;
        //************************************************************************************

        public MapGenerator(Game game, Model model, Map map)
        {
            this.handle = map;
            defaultTile = model;
            loader = new ObjectLoader(this);
            this.game = game;



        }

        //spacing between each node. size of the whole map: in squares
        public void NewMap(float spacing, int size)
        {
            spacing = spacing / 2.0f;
            //this is isometric map for now
            //plane will come later
            MapCell[,] gridNodes = new MapCell[size, size]; //list of each of our nodes

            Vector3 pos = Vector3.Zero; //position for each node
            Vector3 nodePos = Vector3.Zero;

            pos.X = -1 * spacing * (size / 2);
            pos.Z = -1 * spacing * (size / 2);
            float resetZ = pos.Z;
            for (int x = 0; x < size; x++)
            {
                pos.Z = resetZ;
                for (int z = 0; z < size; z++)
                {
                    //int cost, Vector2 location, Vector3 position,Model model, float scale
                    gridNodes[x, z] = new MapCell(1, new Vector2(x, z), pos, defaultTile, 0.15f);//0.05f

                    pos.Z += spacing;
                }
                pos.X += spacing;
            }

            handle.gridNodes = gridNodes;
        }


        public void AddPlayers(List<Player> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                list[i].MyLocation.Ent = null;
                list[i].MyLocation = handle.gridNodes[(int)playerNode[i].X, (int)playerNode[i].Y];
                list[i].MyLocation.Ent = list[i];
            }
            handle.playerList = list;
        }

        public void  addtoDraw()
        {
            foreach (Unit en in handle.enemList)
            {
                handle.drawList.Add(en);
            }

            foreach (Unit p in handle.playerList)
            {
                handle.drawList.Add(p);
            }

            foreach (Actor a in handle.actorList)
            {
                 handle.drawList.Add(a);
                //add actor to the draw
            }

            foreach(DungeonAI d in handle.aiList){
                handle.drawList.Add(d);
            }

            foreach (SceneObject s in handle.sceneList)
            {
                handle.drawList.Add(s);
            }

            foreach(SceneObject s in handle.overlayList){
                handle.drawList.Add(s);
            }

            foreach(SceneObject s in handle.underlayList){
                handle.drawList.Add(s);
            }
        }


        public void setTile(Vector2 nodeDimension, Model model)
        {
            handle.gridNodes[(int)nodeDimension.X, (int)nodeDimension.Y].setTile(model);
        }

        public void debugPlayerNodes()
        {
            //add player to objectList w default texture
            Texture2D defaultPlayer = game.Content.Load<Texture2D>("Anvil"); //default player model
            foreach (Vector2 player in playerNode)
            {

                MapCell location = handle.gridNodes[(int)player.X, (int)player.Y];

                handle.debugList.Add(new SceneObject(defaultPlayer, 0, location));

            }
        }
        public void debugExits()
        {

            Texture2D defaultExit = game.Content.Load<Texture2D>("Bones"); //default exit model
            handle.debugList.Clear();
            foreach (Vector2 exit in borderNodes)
            {
                MapCell location = handle.gridNodes[(int)exit.X,(int)exit.Y];

                handle.debugList.Add(new SceneObject(defaultExit, 0, location));

            }
        }
        //add exits to obstacle list with exit params

        private void createPlayers()
        {
            //add player with player defined texture
        }



        //loading
        public void ReadMap(string filename)
        {
            int lineNo = 0, type = 0;
            Vector2 nodePos = Vector2.Zero;
            using (StreamReader reader = new StreamReader(filename))
            {

                //call the object loader

                //read grid dimensions
               // NewMap(12f, int.Parse(reader.ReadLine()));
                NewMap(13f, int.Parse(reader.ReadLine()));

                //read file
                string input = "";

                while (input != "end")
                {
                    input = reader.ReadLine();

                    if (input != "end")
                    {
                        lineNo = int.Parse(input);

                        type = int.Parse(reader.ReadLine());

                        nodePos.X = int.Parse(reader.ReadLine());
                        nodePos.Y = int.Parse(reader.ReadLine());

                        loader.loadObject(lineNo, type, nodePos);
                    }

                }
            }


        }

        public void WriteMap(string filename)
        {
            System.Diagnostics.Debug.WriteLine(filename);
            using (StreamWriter write = new StreamWriter(filename))
            {
                //int lineNo, int type, Vector2 nodePos

                //write gridsize
                write.WriteLine(handle.gridNodes.GetLength(0));

                //enemy
                foreach (Unit o in handle.enemList)
                {
                    write.WriteLine(o.lineNo); //lineNo
                    write.WriteLine(0); //type
                    Vector2 nodePos = Vector2.Zero;
                    bool stop = false;
                    for (int x = 0; x < handle.gridNodes.GetLength(0); x++)
                    {

                        for (int z = 0; z < handle.gridNodes.GetLength(1); z++)
                        {
                            if (new Vector2(x, z) == o.MyLocation.Location)
                            {
                                nodePos = new Vector2(x, z);
                                stop = true;
                                break;
                            }
                        }

                        if (stop)
                            break;
                    }

                    //write node pos
                    write.WriteLine(nodePos.X.ToString());
                    write.WriteLine(nodePos.Y.ToString());
                }

                //write obstacle
                foreach (testObject o in handle.obstacleList)
                {

                    write.WriteLine(o.lineNo);
                    write.WriteLine(1);
                    Vector2 nodePos = Vector2.Zero;
                    bool stop = false;
                    for (int x = 0; x < handle.gridNodes.GetLength(0); x++)
                    {

                        for (int z = 0; z < handle.gridNodes.GetLength(1); z++)
                        {
                            if (handle.gridNodes[x, z].getPositon() == o.getPosition())
                            {
                                nodePos = new Vector2(x, z);
                                stop = true;
                                break;
                            }
                        }

                        if (stop)
                            break;
                    }

                    //write node pos
                    write.WriteLine(nodePos.X.ToString());
                    write.WriteLine(nodePos.Y.ToString());


                }

                //write scene
                foreach (SceneObject o in handle.sceneList)
                {

                    write.WriteLine(o.lineNo);
                    if(o.MyType == Entity.uType.Loot)
                    write.WriteLine(11);
                    else
                        write.WriteLine(2);
                    Vector2 nodePos = Vector2.Zero;

                    bool stop = false;
                    for (int x = 0; x < handle.gridNodes.GetLength(0); x++)
                    {

                        for (int z = 0; z < handle.gridNodes.GetLength(1); z++)
                        {
                            if (new Vector2(x, z) == o.MyLocation.Location)
                            {
                                nodePos = new Vector2(x, z);
                                stop = true;
                                break;
                            }
                        }

                        if (stop)
                            break;
                    }

                    //write node pos
                    write.WriteLine(nodePos.X.ToString());
                    write.WriteLine(nodePos.Y.ToString());
                }


                //write ai
                foreach(DungeonAI d in handle.aiList){

                    write.WriteLine(d.lineNo);
                    write.WriteLine(7);
                    Vector2 nodePos = Vector2.Zero;

                    bool stop = false;
                    for (int x = 0; x < handle.gridNodes.GetLength(0); x++)
                    {

                        for (int z = 0; z < handle.gridNodes.GetLength(1); z++)
                        {
                            if (new Vector2(x, z) == d.MyLocation.Location)
                            {
                                nodePos = new Vector2(x, z);
                                stop = true;
                                break;
                            }
                        }

                        if (stop)
                            break;
                    }

                    //write node pos
                    write.WriteLine(nodePos.X.ToString());
                    write.WriteLine(nodePos.Y.ToString());

                }
                //write floor
                write.WriteLine(handle.floorObject.lineNo);
                write.WriteLine(8);
                write.WriteLine(0);
                write.WriteLine(0);

                //write overlay
                foreach(SceneObject s in handle.overlayList){
                    write.WriteLine(s.lineNo);
                    write.WriteLine(9);
                    Vector2 nodePos = Vector2.Zero;

                    bool stop = false;
                    for (int x = 0; x < handle.gridNodes.GetLength(0); x++)
                    {

                        for (int z = 0; z < handle.gridNodes.GetLength(1); z++)
                        {
                            if (new Vector2(x, z) == s.MyLocation.Location)
                            {
                                nodePos = new Vector2(x, z);
                                stop = true;
                                break;
                            }
                        }

                        if (stop)
                            break;
                    }

                    //write node pos
                    write.WriteLine(nodePos.X.ToString());
                    write.WriteLine(nodePos.Y.ToString());
                }


                foreach(Actor a in handle.actorList){

                    write.WriteLine(a.lineNo);
                    write.WriteLine(4);
                    Vector2 nodePos = Vector2.Zero;

                    bool stop = false;
                    for (int x = 0; x < handle.gridNodes.GetLength(0); x++)
                    {

                        for (int z = 0; z < handle.gridNodes.GetLength(1); z++)
                        {
                            if (new Vector2(x, z) == a.MyLocation.Location)
                            {
                                nodePos = new Vector2(x, z);
                                stop = true;
                                break;
                            }
                        }

                        if (stop)
                            break;
                    }

                    //write node pos
                    write.WriteLine(nodePos.X.ToString());
                    write.WriteLine(nodePos.Y.ToString());
                }

                //write underlay
                foreach (SceneObject s in handle.underlayList)
                {
                    write.WriteLine(s.lineNo);
                    write.WriteLine(10);
                    Vector2 nodePos = Vector2.Zero;

                    bool stop = false;
                    for (int x = 0; x < handle.gridNodes.GetLength(0); x++)
                    {

                        for (int z = 0; z < handle.gridNodes.GetLength(1); z++)
                        {
                            if (new Vector2(x, z) == s.MyLocation.Location)
                            {
                                nodePos = new Vector2(x, z);
                                stop = true;
                                break;
                            }
                        }

                        if (stop)
                            break;
                    }

                    //write node pos
                    write.WriteLine(nodePos.X.ToString());
                    write.WriteLine(nodePos.Y.ToString());
                }

                //write region

                //write player
                //lineNo, int type, Vector2 nodePos
                foreach (Vector2 p in playerNode)
                {
                    write.WriteLine(0); //lineNo -> player has no lineNo
                    write.WriteLine(3); //type
                    write.WriteLine(p.X.ToString()); //nodepos
                    write.WriteLine(p.Y.ToString());
                }

                //write exits
                foreach (Vector2 e in borderNodes)
                {
                    write.WriteLine(0); //lineNo -> player has no lineNo
                    write.WriteLine(-1); //type
                    write.WriteLine(e.X.ToString()); //nodepos
                    write.WriteLine(e.Y.ToString());
                }

                write.WriteLine("end");
            }


        }



        //deprecated

    };
};
