﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.IO;

namespace BrokenLands
{
    class Screen
    {
        protected List<Buttons> buttonList = new List<Buttons>();
        public UI handle;
        public Screen(UI handle) {
            this.handle = handle;
        }

        public void Draw(SpriteBatch batch, SpriteFont font) {
            foreach(Buttons b in buttonList){
                b.Draw(batch,font);
            }
        }

        public virtual void Update() {
           
        }
        
    }
}
