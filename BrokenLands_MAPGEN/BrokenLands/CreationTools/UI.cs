﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.IO;

namespace BrokenLands
{
    class UI
    {
        //UI properties
        Texture2D background;
        List<Screen> screenList = new List<Screen>();
        int screenIndex = 0;
        public MapGenerator handle;
        public string selectedObject = "";
        public int objectType=0;
        SpriteFont font;
        public Rectangle backgroundsize = new Rectangle(0, 0, 10000, 50);

        //display holders TODO: replace with dictionary/map <string name,int lineNo>
        public Dictionary<string,int> enemyDict = new Dictionary<string,int>();
        public Dictionary<string, int> obstacleDict = new Dictionary<string, int>();
        public Dictionary<string, int> sceneDict = new Dictionary<string, int>();
        public Dictionary<string, int> actorDict = new Dictionary<string, int>();
        public Dictionary<string, int> floorDict = new Dictionary<string, int>();
        public Dictionary<string, int> overlayDict = new Dictionary<string, int>();
        public Dictionary<string, int> aiDict = new Dictionary<string, int>();
        public Dictionary<string, int> underlayDict = new Dictionary<string, int>();
        public Dictionary<string, int> lootDict = new Dictionary<string, int>();


        //export holders --> load selected enemy from dictionary reference into here/handle
        

        public UI(Texture2D background, MapGenerator handle,SpriteFont font) {
            this.background = background;
            this.font = font;
            this.handle = handle;

            loadDictionaries();

            //add screens
            screenList.Add(new MainMenu(this));
            screenList.Add(new EnemyScreen(this,this.font));
            screenList.Add(new ObstacleScreen(this, this.font));
            screenList.Add(new SceneScreen(this, this.font));
            screenList.Add(new ActorScreen(this,this.font));
            screenList.Add(new AIScreen(this,this.font));
            screenList.Add(new FloorScreen(this, this.font));
            screenList.Add(new OverlayScreen(this, this.font));
            screenList.Add(new UnderlayScreen(this, this.font));
            screenList.Add(new LootScreen(this,this.font));

        }


        public void Draw(SpriteBatch batch) {
            batch.Begin();
            batch.DrawString(font, "Currently Selected: " + selectedObject, new Vector2(20, 70), Color.White);
            batch.Draw(background, backgroundsize,Color.White);
            screenList[screenIndex].Draw(batch,font);
            batch.End();
        }

        public void Update() {
            screenList[screenIndex].Update();
        }

        public void loadDictionaries() {
            //add the necessary objects into the corresponding lists
            //name,lineNo,type

            using (StreamReader reader = new StreamReader(ObjectLoader.path + "ObjectData/database.base"))
            {

                string input = "";

                do
                {

                    input = reader.ReadLine();

                    if (input != "end")
                    {

                        int lineNo = 0;
                        int type = 0;

                        lineNo = int.Parse(reader.ReadLine());
                        type = int.Parse(reader.ReadLine());

                        switch (type)
                        {
                            case 0:
                                enemyDict.Add(input, lineNo);
                                break;
                            case 1:
                                obstacleDict.Add(input, lineNo);
                                break;
                            case 2:
                                sceneDict.Add(input, lineNo);
                                break;
                            case 4:
                                actorDict.Add(input,lineNo);
                                break;
                            case 7:
                                aiDict.Add(input,lineNo);
                                break;
                            case 8:
                                floorDict.Add(input, lineNo);
                                break;
                            case 9:
                                overlayDict.Add(input, lineNo);
                                break;
                            case 10:
                                underlayDict.Add(input, lineNo);
                                break;

                            case 11:
                                lootDict.Add(input,lineNo);
                                break;
                        }


                    }

                } while (input != "end");


            }

        }

        public void emptyDictionaries() {
            enemyDict.Clear();
            obstacleDict.Clear();
            sceneDict.Clear();
        }

        public void setScreen(int index) {
            screenIndex = index;
        }



        
    }
}
