﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace BrokenLands
{
     partial class ImportForm : Form
    {
        string name;
        string model;
        int type;
        Screen handle;

        
         List<string> databaseList = new List<string>();
         List<string> targetList = new List<string>();


        public ImportForm(Screen handle)
        {
            this.handle = handle;
            loadDatabase();
            InitializeComponent();
            enemybutton.Checked = true;
        }

        private void ImportForm_Load(object sender, EventArgs e)
        {

        }

        private void submitButton_Click(object sender, EventArgs e)
        {//note: add objects -> decor
            name = nameText.Text;
            model = modelText.Text;
            //type = int.Parse(typeText.Text);

            if(enemybutton.Checked){
                type =0;
            }
            if(obstaclebutton.Checked){
                type =1;
            }
            if(scenebutton.Checked){
                type =2;
            }
            if(actorbutton.Checked){
                type = 4;
            }

            if(dungeonCheck.Checked){
                type = 9;
            }

            if (floorcCheck.Checked)
            {
                type = 8;
            }

            if(overlayCheck.Checked){
                type = 9;
            }

            if (underlayCheck.Checked)
            {
                type = 10;
            }

            if(lootCheck.Checked){
                type = 11;
            }

            writeFiles(name,model,type);

            //TODO: empty and reload the buttons in each list as well
           // handle.handle.emptyDictionaries();
           // handle.handle.loadDictionaries();

            

        }

        // 'C:\Users\Student-I\Documents\fyp\BrokenLands\BrokenLands\bin\Content\ObjectData\database.base

        private void loadDatabase()
        {
            using (StreamReader reader = new StreamReader(ObjectLoader.path + "ObjectData/database.base"))
            {
                string input = "";

                do
                {
                    input = reader.ReadLine();

                    if (input != "end")
                    {
                        databaseList.Add(input);
                    }

                } while (input != "end");




            }

            //debugging only
            foreach(string s in databaseList){
                System.Diagnostics.Debug.WriteLine(s);
            }
        }

         private void loadTarget(string path){
             targetList.Clear();

             ///load target
             using (StreamReader reader = new StreamReader(path))
             {


                 string input = "";

                 do
                 {
                     input = reader.ReadLine();  //FIXME: Textreader closes

                     if (input != "end")
                     {
                         targetList.Add(input);
                     }

                     //reader.Close();

                 } while (input != "end");

             }

             //debugging only
             foreach (string s in targetList)
             {
                 System.Diagnostics.Debug.WriteLine(s);
             }


         }


         private void writeTarget(string path,string model, int type)
         {
             ///load target
             using (StreamWriter writer = new StreamWriter(path))
             {

                 targetList.Add(model);

                 switch(type){ // write to target
                         
                     case 0:
                         //
                         targetList.Add(rotationText.Text);

                         //add the dimesions
                         targetList.Add(XText.Text);
                         targetList.Add(YText.Text);

                         //write stats
                         targetList.Add(strText.Text);
                         targetList.Add(dextext.Text);
                         targetList.Add(intText.Text);
                         targetList.Add(charText.Text);
                         targetList.Add(endText.Text);
                         targetList.Add(agText.Text);
                         //write aitype
                         targetList.Add(aitypeText.Text);
                         break;
                     case 1:
                         targetList.Add(rotationText.Text);

                         //add the dimesions
                         targetList.Add(XText.Text);
                         targetList.Add(YText.Text);

                         targetList.Add(hpText.Text);
                         targetList.Add(defText.Text);
                         break;

                     case 2:
                         //add the dimesions
                         targetList.Add(rotationText.Text);
                         targetList.Add(XText.Text);
                         targetList.Add(YText.Text);
                         break;

                     case 4: // write actors
                         //format: texture questid conditionid dialogfile
                         targetList.Add(rotationText.Text);

                         //add the dimesions
                         targetList.Add(XText.Text);
                         targetList.Add(YText.Text);

                         targetList.Add(questIDtext.Text);
                         targetList.Add(conditionIDtext.Text);
                         targetList.Add(loadtypeText.Text);
                         targetList.Add(dialognametext.Text);

                         //create dialogtree file for actor
                         using (File.Create(ObjectLoader.path + "ObjectData/Dialog/" + dialognametext.Text + ".dialog")) { }

                         break;

                     case 7://dungeon AI
                         targetList.Add(rotationText.Text);

                         //add the dimesions
                         targetList.Add(XText.Text);
                         targetList.Add(YText.Text);

                         targetList.Add(mapText.Text);
                         break;
                     case 8://floor
                         break;

                     case 11: // loot
                         targetList.Add(lootfileText.Text); //the name of the loot file
                         using (File.Create(ObjectLoader.path + "ObjectData/Loot/" + lootfileText.Text + ".loot")) { }
                         break;
                 }


                 targetList.Add("end");

                 //debugging only
                 foreach (string s in targetList)
                 {
                     writer.WriteLine(s);
                 }

             }// writer.Close();
         }

        private void writeFiles(string name,string model,int type) {

            string target = "";

            //half measure. should do something abt this. FIXME
            for (int i = 0; i < targetList.Count();i++ )
            {
                if (targetList[i] == "end")
                {
                    targetList.RemoveAt(i);
                }
            }

            for (int i = 0; i < databaseList.Count(); i++)
            {
                if (databaseList[i] == "end")
                {
                    databaseList.RemoveAt(i);
                }
            }

            using (StreamWriter writer = new StreamWriter(ObjectLoader.path + "ObjectData/database.base"))
            {



                switch (type)
                {
                    case 0:
                        target = ObjectLoader.path + "ObjectData/enemies.data";
                        break;
                    case 1:
                        target = ObjectLoader.path + "ObjectData/objects.data";
                        break;
                    case 2:
                        target = ObjectLoader.path + "ObjectData/scenes.data";
                        break;

                    case 4:
                        target = ObjectLoader.path + "ObjectData/actors.data";
                        break;

                    case 7:
                        target = ObjectLoader.path + "ObjectData/ais.data";
                        break;

                    case 8:
                        target = ObjectLoader.path + "ObjectData/floors.data";
                        break;

                    case 9:
                        target = ObjectLoader.path + "ObjectData/overlays.data";
                        break;

                    case 10:
                        target = ObjectLoader.path + "ObjectData/underlays.data";
                        break;

                    case 11:
                        target = ObjectLoader.path + "ObjectData/loot.data";
                        break;

                }

                loadTarget(target);

                

                bool check = false;
                foreach(string s in targetList){
                    check = name == s;
                    if (check)
                        break;
                }


                //write to target
                if (!check)
                {
                    databaseList.Add(name); //name of object
                    databaseList.Add((targetList.Count() + 1).ToString()); //lineNo
                    databaseList.Add(type.ToString());  //object type
                    writeTarget(target, model, type);
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine("Name already exists. Nothing written");
                }


                databaseList.Add("end");

                foreach (string s in databaseList)
                {
                    writer.WriteLine(s);
                }

            }

        }




        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void nameText_TextChanged(object sender, EventArgs e)
        {

        }

        private void modelText_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void ItemType_Enter(object sender, EventArgs e)
        {

        }

        private void rotationText_TextChanged(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void strText_TextChanged(object sender, EventArgs e)
        {

        }

        private void dextext_TextChanged(object sender, EventArgs e)
        {

        }

        private void intText_TextChanged(object sender, EventArgs e)
        {

        }

        private void charText_TextChanged(object sender, EventArgs e)
        {

        }

        private void endText_TextChanged(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void label21435_Click(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void label13_Click(object sender, EventArgs e)
        {

        }

        private void agText_TextChanged(object sender, EventArgs e)
        {

        }

        private void aitypeText_TextChanged(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void hpText_TextChanged(object sender, EventArgs e)
        {

        }

        private void defText_TextChanged(object sender, EventArgs e)
        {

        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

    }
}
