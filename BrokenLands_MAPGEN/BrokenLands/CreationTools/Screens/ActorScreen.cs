﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace BrokenLands
{
    class ActorScreen : Screen
    {
        Vector2 buttonposition = new Vector2(20, 20);
        public ActorScreen(UI handle, SpriteFont font) : base(handle) {
            addButtons(font);
        }

        private void addButtons(SpriteFont font)
        {

            for (int i = 0; i < handle.actorDict.Count(); i++)
            {
                buttonList.Add(new Buttons(handle.actorDict.ElementAt(i).Key, buttonposition));
                buttonposition.X += font.MeasureString(handle.actorDict.ElementAt(i).Key).X + 20;
            }

            buttonList.Add(new Buttons("Back", buttonposition));
        }


        public override void Update()
        {

            foreach (Buttons b in buttonList)
            {
                if (Mouse.GetState().ScrollWheelValue > MouseController.mouseScrollValue)
                {
                    b.position.X += 15f;
                }
                else if (Mouse.GetState().ScrollWheelValue < MouseController.mouseScrollValue)
                {
                    b.position.X -= 15f;
                }

                if (b.box.Contains(Mouse.GetState().Position) && MouseController.LeftMouseClick())
                {
                    if (b.title == "Back")
                    {
                        handle.setScreen(0);
                    }
                    else
                    {

                        handle.selectedObject = b.title;
                        handle.objectType = 4;

                    }
                }
            }
            base.Update();
        }
    }
}
