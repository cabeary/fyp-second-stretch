﻿namespace BrokenLands
{
    partial class ImportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nameText = new System.Windows.Forms.TextBox();
            this.submitButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.modelText = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.rotationText = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label21435 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.strText = new System.Windows.Forms.TextBox();
            this.dextext = new System.Windows.Forms.TextBox();
            this.intText = new System.Windows.Forms.TextBox();
            this.agText = new System.Windows.Forms.TextBox();
            this.charText = new System.Windows.Forms.TextBox();
            this.endText = new System.Windows.Forms.TextBox();
            this.hpText = new System.Windows.Forms.TextBox();
            this.defText = new System.Windows.Forms.TextBox();
            this.aitypeText = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.enemybutton = new System.Windows.Forms.RadioButton();
            this.obstaclebutton = new System.Windows.Forms.RadioButton();
            this.scenebutton = new System.Windows.Forms.RadioButton();
            this.actorbutton = new System.Windows.Forms.RadioButton();
            this.ItemType = new System.Windows.Forms.GroupBox();
            this.lootCheck = new System.Windows.Forms.RadioButton();
            this.underlayCheck = new System.Windows.Forms.RadioButton();
            this.overlayCheck = new System.Windows.Forms.RadioButton();
            this.dungeonCheck = new System.Windows.Forms.RadioButton();
            this.floorcCheck = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.conditionIDtext = new System.Windows.Forms.TextBox();
            this.questIDtext = new System.Windows.Forms.TextBox();
            this.dialognametext = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.mapText = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.loadtypeText = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.lootfileText = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.XText = new System.Windows.Forms.TextBox();
            this.YText = new System.Windows.Forms.TextBox();
            this.ItemType.SuspendLayout();
            this.SuspendLayout();
            // 
            // nameText
            // 
            this.nameText.Location = new System.Drawing.Point(80, 5);
            this.nameText.Name = "nameText";
            this.nameText.Size = new System.Drawing.Size(174, 20);
            this.nameText.TabIndex = 0;
            this.nameText.Text = "Name";
            this.nameText.TextChanged += new System.EventHandler(this.nameText_TextChanged);
            // 
            // submitButton
            // 
            this.submitButton.Location = new System.Drawing.Point(187, 638);
            this.submitButton.Name = "submitButton";
            this.submitButton.Size = new System.Drawing.Size(75, 23);
            this.submitButton.TabIndex = 1;
            this.submitButton.Text = "Submit";
            this.submitButton.UseVisualStyleBackColor = true;
            this.submitButton.Click += new System.EventHandler(this.submitButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Name";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Texture";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // modelText
            // 
            this.modelText.Location = new System.Drawing.Point(80, 50);
            this.modelText.Name = "modelText";
            this.modelText.Size = new System.Drawing.Size(174, 20);
            this.modelText.TabIndex = 4;
            this.modelText.Text = "TextureName";
            this.modelText.TextChanged += new System.EventHandler(this.modelText_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(197, 180);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Enemy";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(184, 329);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Obstacles";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(18, 158);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "Rotation";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // rotationText
            // 
            this.rotationText.Location = new System.Drawing.Point(80, 158);
            this.rotationText.Name = "rotationText";
            this.rotationText.Size = new System.Drawing.Size(37, 20);
            this.rotationText.TabIndex = 11;
            this.rotationText.Text = "0";
            this.rotationText.TextChanged += new System.EventHandler(this.rotationText_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(24, 209);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "STR";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(24, 237);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(29, 13);
            this.label9.TabIndex = 13;
            this.label9.Text = "DEX";
            this.label9.Click += new System.EventHandler(this.label9_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(24, 266);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(25, 13);
            this.label10.TabIndex = 14;
            this.label10.Text = "INT";
            this.label10.Click += new System.EventHandler(this.label10_Click);
            // 
            // label21435
            // 
            this.label21435.AutoSize = true;
            this.label21435.Location = new System.Drawing.Point(24, 292);
            this.label21435.Name = "label21435";
            this.label21435.Size = new System.Drawing.Size(37, 13);
            this.label21435.TabIndex = 15;
            this.label21435.Text = "CHAR";
            this.label21435.Click += new System.EventHandler(this.label21435_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(284, 209);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(30, 13);
            this.label12.TabIndex = 16;
            this.label12.Text = "END";
            this.label12.Click += new System.EventHandler(this.label12_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(284, 239);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(22, 13);
            this.label13.TabIndex = 17;
            this.label13.Text = "AG";
            this.label13.Click += new System.EventHandler(this.label13_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(26, 360);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(22, 13);
            this.label14.TabIndex = 18;
            this.label14.Text = "HP";
            this.label14.Click += new System.EventHandler(this.label14_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(26, 391);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(28, 13);
            this.label15.TabIndex = 19;
            this.label15.Text = "DEF";
            this.label15.Click += new System.EventHandler(this.label15_Click);
            // 
            // strText
            // 
            this.strText.Location = new System.Drawing.Point(80, 206);
            this.strText.Name = "strText";
            this.strText.Size = new System.Drawing.Size(37, 20);
            this.strText.TabIndex = 20;
            this.strText.Text = "0";
            this.strText.TextChanged += new System.EventHandler(this.strText_TextChanged);
            // 
            // dextext
            // 
            this.dextext.Location = new System.Drawing.Point(80, 234);
            this.dextext.Name = "dextext";
            this.dextext.Size = new System.Drawing.Size(37, 20);
            this.dextext.TabIndex = 21;
            this.dextext.Text = "0";
            this.dextext.TextChanged += new System.EventHandler(this.dextext_TextChanged);
            // 
            // intText
            // 
            this.intText.Location = new System.Drawing.Point(80, 263);
            this.intText.Name = "intText";
            this.intText.Size = new System.Drawing.Size(37, 20);
            this.intText.TabIndex = 22;
            this.intText.Text = "0";
            this.intText.TextChanged += new System.EventHandler(this.intText_TextChanged);
            // 
            // agText
            // 
            this.agText.Location = new System.Drawing.Point(340, 236);
            this.agText.Name = "agText";
            this.agText.Size = new System.Drawing.Size(37, 20);
            this.agText.TabIndex = 23;
            this.agText.Text = "0";
            this.agText.TextChanged += new System.EventHandler(this.agText_TextChanged);
            // 
            // charText
            // 
            this.charText.Location = new System.Drawing.Point(80, 289);
            this.charText.Name = "charText";
            this.charText.Size = new System.Drawing.Size(37, 20);
            this.charText.TabIndex = 24;
            this.charText.Text = "0";
            this.charText.TextChanged += new System.EventHandler(this.charText_TextChanged);
            // 
            // endText
            // 
            this.endText.Location = new System.Drawing.Point(340, 206);
            this.endText.Name = "endText";
            this.endText.Size = new System.Drawing.Size(37, 20);
            this.endText.TabIndex = 25;
            this.endText.Text = "0";
            this.endText.TextChanged += new System.EventHandler(this.endText_TextChanged);
            // 
            // hpText
            // 
            this.hpText.Location = new System.Drawing.Point(82, 357);
            this.hpText.Name = "hpText";
            this.hpText.Size = new System.Drawing.Size(37, 20);
            this.hpText.TabIndex = 26;
            this.hpText.Text = "0";
            this.hpText.TextChanged += new System.EventHandler(this.hpText_TextChanged);
            // 
            // defText
            // 
            this.defText.Location = new System.Drawing.Point(82, 388);
            this.defText.Name = "defText";
            this.defText.Size = new System.Drawing.Size(37, 20);
            this.defText.TabIndex = 27;
            this.defText.Text = "0";
            this.defText.TextChanged += new System.EventHandler(this.defText_TextChanged);
            // 
            // aitypeText
            // 
            this.aitypeText.Location = new System.Drawing.Point(340, 275);
            this.aitypeText.Name = "aitypeText";
            this.aitypeText.Size = new System.Drawing.Size(37, 20);
            this.aitypeText.TabIndex = 28;
            this.aitypeText.Text = "0";
            this.aitypeText.TextChanged += new System.EventHandler(this.aitypeText_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(286, 278);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 13);
            this.label6.TabIndex = 29;
            this.label6.Text = "AIType";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // enemybutton
            // 
            this.enemybutton.AutoSize = true;
            this.enemybutton.Location = new System.Drawing.Point(15, 29);
            this.enemybutton.Name = "enemybutton";
            this.enemybutton.Size = new System.Drawing.Size(57, 17);
            this.enemybutton.TabIndex = 30;
            this.enemybutton.TabStop = true;
            this.enemybutton.Text = "Enemy";
            this.enemybutton.UseVisualStyleBackColor = true;
            // 
            // obstaclebutton
            // 
            this.obstaclebutton.AutoSize = true;
            this.obstaclebutton.Location = new System.Drawing.Point(78, 29);
            this.obstaclebutton.Name = "obstaclebutton";
            this.obstaclebutton.Size = new System.Drawing.Size(67, 17);
            this.obstaclebutton.TabIndex = 31;
            this.obstaclebutton.TabStop = true;
            this.obstaclebutton.Text = "Obstacle";
            this.obstaclebutton.UseVisualStyleBackColor = true;
            // 
            // scenebutton
            // 
            this.scenebutton.AutoSize = true;
            this.scenebutton.Location = new System.Drawing.Point(151, 29);
            this.scenebutton.Name = "scenebutton";
            this.scenebutton.Size = new System.Drawing.Size(56, 17);
            this.scenebutton.TabIndex = 32;
            this.scenebutton.TabStop = true;
            this.scenebutton.Text = "Scene";
            this.scenebutton.UseVisualStyleBackColor = true;
            // 
            // actorbutton
            // 
            this.actorbutton.AutoSize = true;
            this.actorbutton.Location = new System.Drawing.Point(228, 29);
            this.actorbutton.Name = "actorbutton";
            this.actorbutton.Size = new System.Drawing.Size(50, 17);
            this.actorbutton.TabIndex = 33;
            this.actorbutton.TabStop = true;
            this.actorbutton.Text = "Actor";
            this.actorbutton.UseVisualStyleBackColor = true;
            // 
            // ItemType
            // 
            this.ItemType.Controls.Add(this.lootCheck);
            this.ItemType.Controls.Add(this.underlayCheck);
            this.ItemType.Controls.Add(this.overlayCheck);
            this.ItemType.Controls.Add(this.dungeonCheck);
            this.ItemType.Controls.Add(this.floorcCheck);
            this.ItemType.Controls.Add(this.enemybutton);
            this.ItemType.Controls.Add(this.actorbutton);
            this.ItemType.Controls.Add(this.obstaclebutton);
            this.ItemType.Controls.Add(this.scenebutton);
            this.ItemType.Location = new System.Drawing.Point(12, 94);
            this.ItemType.Name = "ItemType";
            this.ItemType.Size = new System.Drawing.Size(665, 54);
            this.ItemType.TabIndex = 34;
            this.ItemType.TabStop = false;
            this.ItemType.Text = "Type";
            this.ItemType.Enter += new System.EventHandler(this.ItemType_Enter);
            // 
            // lootCheck
            // 
            this.lootCheck.AutoSize = true;
            this.lootCheck.Location = new System.Drawing.Point(589, 28);
            this.lootCheck.Name = "lootCheck";
            this.lootCheck.Size = new System.Drawing.Size(46, 17);
            this.lootCheck.TabIndex = 38;
            this.lootCheck.TabStop = true;
            this.lootCheck.Text = "Loot";
            this.lootCheck.UseVisualStyleBackColor = true;
            // 
            // underlayCheck
            // 
            this.underlayCheck.AutoSize = true;
            this.underlayCheck.Location = new System.Drawing.Point(514, 29);
            this.underlayCheck.Name = "underlayCheck";
            this.underlayCheck.Size = new System.Drawing.Size(67, 17);
            this.underlayCheck.TabIndex = 37;
            this.underlayCheck.TabStop = true;
            this.underlayCheck.Text = "Underlay";
            this.underlayCheck.UseVisualStyleBackColor = true;
            // 
            // overlayCheck
            // 
            this.overlayCheck.AutoSize = true;
            this.overlayCheck.Location = new System.Drawing.Point(446, 29);
            this.overlayCheck.Name = "overlayCheck";
            this.overlayCheck.Size = new System.Drawing.Size(61, 17);
            this.overlayCheck.TabIndex = 36;
            this.overlayCheck.TabStop = true;
            this.overlayCheck.Text = "Overlay";
            this.overlayCheck.UseVisualStyleBackColor = true;
            // 
            // dungeonCheck
            // 
            this.dungeonCheck.AutoSize = true;
            this.dungeonCheck.Location = new System.Drawing.Point(360, 29);
            this.dungeonCheck.Name = "dungeonCheck";
            this.dungeonCheck.Size = new System.Drawing.Size(79, 17);
            this.dungeonCheck.TabIndex = 35;
            this.dungeonCheck.TabStop = true;
            this.dungeonCheck.Text = "DungeonAI";
            this.dungeonCheck.UseVisualStyleBackColor = true;
            // 
            // floorcCheck
            // 
            this.floorcCheck.AutoSize = true;
            this.floorcCheck.Location = new System.Drawing.Point(296, 29);
            this.floorcCheck.Name = "floorcCheck";
            this.floorcCheck.Size = new System.Drawing.Size(48, 17);
            this.floorcCheck.TabIndex = 34;
            this.floorcCheck.TabStop = true;
            this.floorcCheck.Text = "Floor";
            this.floorcCheck.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(184, 422);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 35;
            this.label3.Text = "Actor";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(27, 451);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(42, 13);
            this.label11.TabIndex = 36;
            this.label11.Text = "LoadID";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(27, 478);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(46, 13);
            this.label16.TabIndex = 37;
            this.label16.Text = "QuestID";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(30, 501);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(81, 13);
            this.label17.TabIndex = 38;
            this.label17.Text = "DialogFileName";
            // 
            // conditionIDtext
            // 
            this.conditionIDtext.Location = new System.Drawing.Point(150, 443);
            this.conditionIDtext.Name = "conditionIDtext";
            this.conditionIDtext.Size = new System.Drawing.Size(27, 20);
            this.conditionIDtext.TabIndex = 39;
            this.conditionIDtext.Text = "0";
            // 
            // questIDtext
            // 
            this.questIDtext.Location = new System.Drawing.Point(149, 471);
            this.questIDtext.Name = "questIDtext";
            this.questIDtext.Size = new System.Drawing.Size(28, 20);
            this.questIDtext.TabIndex = 40;
            this.questIDtext.Text = "0";
            // 
            // dialognametext
            // 
            this.dialognametext.Location = new System.Drawing.Point(149, 501);
            this.dialognametext.Name = "dialognametext";
            this.dialognametext.Size = new System.Drawing.Size(120, 20);
            this.dialognametext.TabIndex = 41;
            this.dialognametext.Text = "NameofFile";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(187, 553);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(64, 13);
            this.label18.TabIndex = 42;
            this.label18.Text = "Dungeon AI";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(27, 591);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(58, 13);
            this.label19.TabIndex = 43;
            this.label19.Text = "Battle Map";
            // 
            // mapText
            // 
            this.mapText.Location = new System.Drawing.Point(149, 584);
            this.mapText.Name = "mapText";
            this.mapText.Size = new System.Drawing.Size(120, 20);
            this.mapText.TabIndex = 44;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(187, 446);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(55, 13);
            this.label20.TabIndex = 45;
            this.label20.Text = "LoadType";
            // 
            // loadtypeText
            // 
            this.loadtypeText.Location = new System.Drawing.Point(249, 443);
            this.loadtypeText.Name = "loadtypeText";
            this.loadtypeText.Size = new System.Drawing.Size(100, 20);
            this.loadtypeText.TabIndex = 46;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(554, 180);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(73, 13);
            this.label21.TabIndex = 47;
            this.label21.Text = "LootContainer";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(470, 233);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(47, 13);
            this.label22.TabIndex = 48;
            this.label22.Text = "LootFile:";
            // 
            // lootfileText
            // 
            this.lootfileText.Location = new System.Drawing.Point(523, 230);
            this.lootfileText.Name = "lootfileText";
            this.lootfileText.Size = new System.Drawing.Size(100, 20);
            this.lootfileText.TabIndex = 49;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(308, 12);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(58, 13);
            this.label23.TabIndex = 50;
            this.label23.Text = "Dimesions:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(386, 32);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(17, 13);
            this.label24.TabIndex = 51;
            this.label24.Text = "X:";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(499, 32);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(17, 13);
            this.label25.TabIndex = 52;
            this.label25.Text = "Y:";
            // 
            // XText
            // 
            this.XText.Location = new System.Drawing.Point(410, 30);
            this.XText.Name = "XText";
            this.XText.Size = new System.Drawing.Size(72, 20);
            this.XText.TabIndex = 53;
            this.XText.Text = "1";
            // 
            // YText
            // 
            this.YText.Location = new System.Drawing.Point(521, 29);
            this.YText.Name = "YText";
            this.YText.Size = new System.Drawing.Size(72, 20);
            this.YText.TabIndex = 54;
            this.YText.Text = "1";
            // 
            // ImportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(738, 686);
            this.Controls.Add(this.YText);
            this.Controls.Add(this.XText);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.lootfileText);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.loadtypeText);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.mapText);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.dialognametext);
            this.Controls.Add(this.questIDtext);
            this.Controls.Add(this.conditionIDtext);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.ItemType);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.aitypeText);
            this.Controls.Add(this.defText);
            this.Controls.Add(this.hpText);
            this.Controls.Add(this.endText);
            this.Controls.Add(this.charText);
            this.Controls.Add(this.agText);
            this.Controls.Add(this.intText);
            this.Controls.Add(this.dextext);
            this.Controls.Add(this.strText);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label21435);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.rotationText);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.modelText);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.submitButton);
            this.Controls.Add(this.nameText);
            this.Name = "ImportForm";
            this.Text = "ImportForm";
            this.Load += new System.EventHandler(this.ImportForm_Load);
            this.ItemType.ResumeLayout(false);
            this.ItemType.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox nameText;
        private System.Windows.Forms.Button submitButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox modelText;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox rotationText;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label21435;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox strText;
        private System.Windows.Forms.TextBox dextext;
        private System.Windows.Forms.TextBox intText;
        private System.Windows.Forms.TextBox agText;
        private System.Windows.Forms.TextBox charText;
        private System.Windows.Forms.TextBox endText;
        private System.Windows.Forms.TextBox hpText;
        private System.Windows.Forms.TextBox defText;
        private System.Windows.Forms.TextBox aitypeText;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.RadioButton enemybutton;
        private System.Windows.Forms.RadioButton obstaclebutton;
        private System.Windows.Forms.RadioButton scenebutton;
        private System.Windows.Forms.RadioButton actorbutton;
        private System.Windows.Forms.GroupBox ItemType;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox conditionIDtext;
        private System.Windows.Forms.TextBox questIDtext;
        private System.Windows.Forms.TextBox dialognametext;
        private System.Windows.Forms.RadioButton dungeonCheck;
        private System.Windows.Forms.RadioButton floorcCheck;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox mapText;
        private System.Windows.Forms.RadioButton overlayCheck;
        private System.Windows.Forms.RadioButton underlayCheck;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox loadtypeText;
        private System.Windows.Forms.RadioButton lootCheck;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox lootfileText;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox XText;
        private System.Windows.Forms.TextBox YText;
    }
}