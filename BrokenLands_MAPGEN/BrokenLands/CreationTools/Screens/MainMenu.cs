﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Windows.Forms;
using System.Threading.Tasks;
namespace BrokenLands
{
    //add overlay, ai, floor
    class MainMenu : Screen
    {
        public MainMenu(UI handle) :  base(handle) {
            buttonList.Add(new Buttons("PlayerNode",new Vector2(70,20)));

            buttonList.Add(new Buttons("Enemies", new Vector2(160, 20)));

            buttonList.Add(new Buttons("Obstacles", new Vector2(230, 20)));

            buttonList.Add(new Buttons("SceneObjects", new Vector2(330, 20)));

            buttonList.Add(new Buttons("Import", new Vector2(430, 20)));

            buttonList.Add(new Buttons("MapNode", new Vector2(530, 20)));

            buttonList.Add(new Buttons("Actor", new Vector2(630, 20)));

            buttonList.Add(new Buttons("Floors", new Vector2(730, 20)));

            buttonList.Add(new Buttons("DungeonAI", new Vector2(830, 20)));

            buttonList.Add(new Buttons("Overlay", new Vector2(930, 20)));

            buttonList.Add(new Buttons("Underlay", new Vector2(1030, 20)));

            buttonList.Add(new Buttons("Lootcontainer", new Vector2(1130, 20)));
        }

        public override void Update()
        {
            foreach (Buttons b in buttonList)
            {
                if (b.box.Contains(Mouse.GetState().Position) && MouseController.LeftMouseClick())
                {
                    switch (b.title)
                    {
                        case "Enemies":
                            handle.setScreen(1);
                            break;
                        case "Obstacles":
                            handle.setScreen(2);
                            break;
                        case "SceneObjects":
                            handle.setScreen(3);
                            break;
                        case "Import":
                            Application.EnableVisualStyles();
                            Application.Run(new ImportForm(this));
                            break;

                        case "PlayerNode":
                            handle.selectedObject = "PlayerNode";
                            break;

                        case "MapNode":
                            handle.selectedObject = "MapNode";
                            break;

                        case "Actor":
                            handle.setScreen(4);
                            break;

                        case "Floors":
                            handle.setScreen(6);
                            break;
                        case "DungeonAI":
                            handle.setScreen(5);
                            break;
                        case "Overlay":
                            handle.setScreen(7);
                            break;

                        case "Underlay":
                            handle.setScreen(8);
                            break;

                        case "Lootcontainer":
                            handle.setScreen(9);
                            break;

                    }
                }
            }
            base.Update();
        } 







    }
}
