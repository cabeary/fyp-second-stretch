﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using BrokenLands;

namespace BrokenLands
{
    class Map
    {
        //replace mapexit wiht bordernodes

        //public Node[,] gridNodes;
        public MapCell[,] gridNodes;
        public Texture2D floorTexture;
        public FloorObject floorObject;
        // public List<testObject> objectList = new List<testObject>(); //stores enemies and players
        //contains a list of entities - enemies and player. sort by initiative
        //list of obstacles
        public List<testObject> obstacleList = new List<testObject>();

        //public List<testObject> mapexitList = new List<testObject>();
        public List<Vector2> sectionNodes = new List<Vector2>();

        public List<SceneObject> debugList = new List<SceneObject>();
        //current character


        //actual values
        public List<Enemy> enemList = new List<Enemy>();
        public List<Player> playerList = new List<Player>();
        public List<Entity> drawList = new List<Entity>();//t ake in entities
        public List<SceneObject> sceneList = new List<SceneObject>();
        public List<Actor> actorList = new List<Actor>();
        public List<DungeonAI> aiList = new List<DungeonAI>();
        public List<SceneObject> overlayList = new List<SceneObject>();
        public List<SceneObject> underlayList = new List<SceneObject>();
        //public List<Vector2> borderNodes = new List<Vector2>();


        //takes in the input of player
        //public Map(bool debug) {
        //    if(debug)
        //        //apply default playertexture. add to list
        //    else
        //    //load passed player texture
        //}

        public MapCell[,] getZone(Player p) {
            MapCell[,] zone = new MapCell[0,0];

            for (int i = 0; i < sectionNodes.Count; i += 2)
            {

                if ((p.MyLocation.Location.X > sectionNodes[i].X && p.MyLocation.Location.X < sectionNodes[i + 1].X) &&
                    (p.MyLocation.Location.Y > sectionNodes[i].Y && p.MyLocation.Location.Y < sectionNodes[i + 1].Y)
                    ) 
                {
                    Vector2 length = Vector2.Zero;
                    length.X = sectionNodes[i + 1].X - sectionNodes[i].X;
                    length.Y = sectionNodes[i + 1].Y - sectionNodes[i].Y;
                    zone = new MapCell[(int)length.X, (int)length.Y];
                    List<MapCell> cellList = new List<MapCell>();


                    for (int x = (int)sectionNodes[i].X; i < sectionNodes[i + 1].X; x++)
                    {
                        for (int z = (int)sectionNodes[i].Y; i < sectionNodes[i + 1].Y; z++)
                        {
                            cellList.Add(gridNodes[x,z]);
                        }
                    }

                    for (int x = 0; x < zone.GetLength(0);x++ ) {
                        for (int z = 0; z < zone.GetLength(1); z++) {
                            zone[x,z] = cellList[x*(zone.GetLength(0))+z];
                        }
                    }

                        break;
                }
            }

            return zone;
        }

        public Map(Texture2D defaultFloor) {
            floorObject = new FloorObject(defaultFloor,0);
            floorTexture = defaultFloor;
        }

        public void Update() { }

        public void sortDraw() {
            drawList.Sort(delegate(Entity a, Entity b)
            {
                float sumA = -a.MyLocation.getPositon().X + a.MyLocation.getPositon().Z;
                float sumB = -b.MyLocation.getPositon().X + b.MyLocation.getPositon().Z;


                if (sumA < sumB)
                    return -1;
                else
                    return 0;
            });
        }

        public void debugDraw(SpriteBatch spritebatch, CameraObj camera, GraphicsDevice device)
        {

            //draw grid

            device.DepthStencilState = DepthStencilState.Default;


            for (int x = 0; x < gridNodes.GetLength(0); x++)
            {

                for (int z = 0; z < gridNodes.GetLength(1); z++)
                {
                    if (camera.inView(gridNodes[x, z].getBox()))
                    {
                        gridNodes[x, z].Draw(camera, device);
                    }
                }
            }

            

            // sort the enemies by depth
            enemList.Sort(delegate(Enemy a, Enemy b)
            {
               float sumA = -a.MyLocation.getPositon().X + a.MyLocation.getPositon().Z;
               float sumB = -b.MyLocation.getPositon().X + b.MyLocation.getPositon().Z; 


                if (sumA < sumB)
                    return -1;
                else
                    return 0;



            });
            actorList.Sort(delegate(Actor a, Actor b)
            {
                float sumA = -a.MyLocation.getPositon().X + a.MyLocation.getPositon().Z;
                float sumB = -b.MyLocation.getPositon().X + b.MyLocation.getPositon().Z;


                if (sumA < sumB)
                    return -1;
                else
                    return 0;



            });


            sceneList.Sort(delegate(SceneObject a, SceneObject b)
            {
                float sumA = -a.MyLocation.getPositon().X + a.MyLocation.getPositon().Z;
                float sumB = -b.MyLocation.getPositon().X + b.MyLocation.getPositon().Z;


                if (sumA < sumB)
                    return -1;
                else
                    return 0;



            });






            //draw players and enemy
            spritebatch.Begin();


            foreach(SceneObject s in underlayList){
                s.Draw(spritebatch, device.Viewport, camera);
                s.UpdateLocation(device.Viewport, camera);
            }

            foreach (Unit t in enemList)
            {
                t.Draw(spritebatch, device.Viewport, camera);
                t.UpdateLocation(device.Viewport, camera);
            }

            foreach(Actor a in actorList){
                a.UpdateLocation(device.Viewport, camera);
                a.Draw(spritebatch, device.Viewport, camera);
            }

            

            //draw scene

            foreach (SceneObject s in sceneList)
            {
                s.UpdateLocation(device.Viewport, camera);

                if (Vector2.Distance(s.getPosition(),MouseController.getPosition()) <300 )
                {
                    s.Draw(spritebatch, device.Viewport, camera, 0.3f);
                }

                else
                {
                    s.Draw(spritebatch, device.Viewport, camera);
                    
                }
            }

            foreach(SceneObject s in overlayList){
                s.Draw(spritebatch, device.Viewport, camera);
                s.UpdateLocation(device.Viewport, camera);
            }


            //draw debug
            foreach (SceneObject t in debugList)
            {
                t.Draw(spritebatch, device.Viewport, camera);
                t.UpdateLocation(device.Viewport, camera);
            }

            spritebatch.End();

           
        }




        public void Draw(SpriteBatch spritebatch, CameraObj camera, GraphicsDevice device)
        {
            //draw grid

            device.DepthStencilState = DepthStencilState.Default;


            for (int x = 0; x < gridNodes.GetLength(0); x++)
            {

                for (int z = 0; z < gridNodes.GetLength(1); z++)
                {
                    if (camera.inView(gridNodes[x, z].getBox()))
                    {
                        gridNodes[x, z].Draw(camera, device);
                    }
                }
            }




            //draw players and enemy
            spritebatch.Begin();
            foreach (SceneObject s in underlayList)
            {
                s.Draw(spritebatch, device.Viewport, camera);
                s.UpdateLocation(device.Viewport, camera);
            }

            foreach (Entity t in drawList)
            {
                if (camera.inView(gridNodes[(int)t.MyLocation.Location.X, (int)t.MyLocation.Location.Y].getBox()))
                {
                t.Draw(spritebatch, device.Viewport, camera);
                t.UpdateLocation(device.Viewport, camera);
                }

            }

           
            spritebatch.End();
        }


        public BoundingBox getTileBounds(Vector2 nodeDimension)
        {
            return gridNodes[(int)nodeDimension.X, (int)nodeDimension.Y].getBox();
        }

        public Vector3 getTilePosition(Vector2 nodeDimension)
        {
            return gridNodes[(int)nodeDimension.X, (int)nodeDimension.Y].getPositon();
        }


    }
}
