﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.IO;


//TODO: Write an AI number
namespace BrokenLands
{
    class ObjectLoader
    {
        //replace the exit nodes with border nodes

        //HIW-> HOW IT WORKS

        //****Description****
        //takes in metadata
        //METADATA: INT TYPE, INT LINE NO,VEC2 NODEPOSITION
        //HIW(Type):Tells it the number of lines to retrieve and which file to retrieve from (enemy.data,etc)
        //HIW(Line):Which line to start at (object property position)
        //HIW(Node positon):The position to place the object in the map (NA to scenes)
        //spits out object classes to the handle
        MapGenerator handle;
        public static string path = "../../../../../brokenland/brokenland/brokenlandcontent/";

        public ObjectLoader(MapGenerator handle)
        {
            this.handle = handle;
        }

        public Entity fetchObject(int lineNo, int type, MapCell Location)
        {
            if (type == 0)
            {
                //return enemy 
                //load enemies
                //format texture, rotattion, strength, dexterity, intelligence,charisma, endurance, agility, node
                //skip linoNo,take x number of lines,order->first
                 string modelName = File.ReadLines(path + "ObjectData/enemies.data").Skip(lineNo - 1).Take(1).First();
                int rotation = int.Parse(File.ReadLines(path + "ObjectData/enemies.data").Skip(lineNo).Take(1).First());

                int XSize = int.Parse(File.ReadLines(path + "ObjectData/enemies.data").Skip(lineNo + 1).Take(1).First());
                int YSize = int.Parse(File.ReadLines(path + "ObjectData/enemies.data").Skip(lineNo + 2).Take(1).First());

                int strength = int.Parse(File.ReadLines(path + "ObjectData/enemies.data").Skip(lineNo + 3).Take(1).First());
                int dext = int.Parse(File.ReadLines(path + "ObjectData/enemies.data").Skip(lineNo + 4).Take(1).First());
                int intelligence = int.Parse(File.ReadLines(path + "ObjectData/enemies.data").Skip(lineNo + 5).Take(1).First());
                int charisma = int.Parse(File.ReadLines(path + "ObjectData/enemies.data").Skip(lineNo + 6).Take(1).First());
                int endurance = int.Parse(File.ReadLines(path + "ObjectData/enemies.data").Skip(lineNo + 7).Take(1).First());
                int agility = int.Parse(File.ReadLines(path + "ObjectData/enemies.data").Skip(lineNo + 8).Take(1).First());
                int AItype = int.Parse(File.ReadLines(path + "ObjectData/enemies.data").Skip(lineNo + 9).Take(1).First());

                System.Diagnostics.Debug.WriteLine(modelName);

                //model = handle.game.Content.Load<Model>(modelName);
                Texture2D texture = handle.game.Content.Load<Texture2D>(modelName);
                MapCell node = Location;

                Vector3 position = node.getPositon();
                Enemy enemy = new Enemy(strength, dext, intelligence, charisma, endurance, agility, node, texture, lineNo, modelName, AItype);
                enemy.Size = new Vector2(XSize, YSize);
                return enemy;

            }
            else if (type > 0)
            {
                //return underlay   string modelName = File.ReadLines(path + "ObjectData/underlays.data").Skip(lineNo - 1).Take(1).First();
                string modelName = File.ReadLines(path + "ObjectData/overlays.data").Skip(lineNo - 1).Take(1).First();
                System.Diagnostics.Debug.WriteLine(modelName);

                Texture2D texture2 = handle.game.Content.Load<Texture2D>(modelName);

                //get middleNode
                MapCell node = Location;

                //vect position2d = node.getPositon();
                return new SceneObject(texture2, lineNo, node);
            }
            else
            {
                return null;
            }

        }

        public void loadObject(int lineNo, int type, Vector2 nodePos)
        {
            string modelName = "";
            MapCell node;
            Vector3 position;
            Model model;


            switch (type)
            {
                //scenes take in vec2 while everything else uses nodes

                case 0:
                    //load enemies
                    //format texture, rotattion, strength, dexterity, intelligence,charisma, endurance, agility, node
                    //skip linoNo,take x number of lines,order->first


                    modelName = File.ReadLines(path + "ObjectData/enemies.data").Skip(lineNo - 1).Take(1).First();
                    int rotation = int.Parse(File.ReadLines(path + "ObjectData/enemies.data").Skip(lineNo).Take(1).First());

                    //string test = File.ReadLines(path + "ObjectData/enemies.data").Skip(lineNo + 1).Take(1).First();


                    int XSize = int.Parse(File.ReadLines(path + "ObjectData/enemies.data").Skip(lineNo+1).Take(1).First());
                    int YSize = int.Parse(File.ReadLines(path + "ObjectData/enemies.data").Skip(lineNo + 2).Take(1).First());

                    int strength = int.Parse(File.ReadLines(path + "ObjectData/enemies.data").Skip(lineNo + 3).Take(1).First());
                    int dext = int.Parse(File.ReadLines(path + "ObjectData/enemies.data").Skip(lineNo + 4).Take(1).First());
                    int intelligence = int.Parse(File.ReadLines(path + "ObjectData/enemies.data").Skip(lineNo + 5).Take(1).First());
                    int charisma = int.Parse(File.ReadLines(path + "ObjectData/enemies.data").Skip(lineNo + 6).Take(1).First());
                    int endurance = int.Parse(File.ReadLines(path + "ObjectData/enemies.data").Skip(lineNo + 7).Take(1).First());
                    int agility = int.Parse(File.ReadLines(path + "ObjectData/enemies.data").Skip(lineNo + 8).Take(1).First());
                    int AItype = int.Parse(File.ReadLines(path + "ObjectData/enemies.data").Skip(lineNo + 9).Take(1).First());

                    System.Diagnostics.Debug.WriteLine(modelName);

                    //model = handle.game.Content.Load<Model>(modelName);
                    Texture2D texture = handle.game.Content.Load<Texture2D>(modelName);
                    node = handle.handle.gridNodes[(int)nodePos.X, (int)nodePos.Y];

                    position = node.getPositon();
                    Enemy enemy = new Enemy(strength, dext, intelligence, charisma, endurance, agility, node, texture, lineNo, modelName, AItype);
                    enemy.Size = new Vector2(XSize, YSize);
                    handle.handle.enemList.Add(enemy);
                    break;
                case 1:
                    //load obstacles

                    modelName = File.ReadLines(path + "ObjectData/objects.data").Skip(lineNo - 1).Take(1).First();
                    System.Diagnostics.Debug.WriteLine(modelName);

                    int health = int.Parse(File.ReadLines(path + "ObjectData/objects.data").Skip(lineNo).Take(1).First());
                    int defence = int.Parse(File.ReadLines(path + "ObjectData/objects.data").Skip(lineNo + 1).Take(1).First());

                    model = handle.game.Content.Load<Model>(modelName);
                    node = handle.handle.gridNodes[(int)nodePos.X, (int)nodePos.Y];

                    position = node.getPositon();
                    handle.handle.obstacleList.Add(new testObject(model, position, lineNo));
                    //replace with obstacle once that is up
                    break;


                case 2:
                    //load scene. Scene now refers to deco object in a map

                    modelName = File.ReadLines(path + "ObjectData/scenes.data").Skip(lineNo - 1).Take(1).First();
                    int rot = int.Parse(File.ReadLines(path + "ObjectData/scenes.data").Skip(lineNo).Take(1).First());
                    int XSize2 = int.Parse(File.ReadLines(path + "ObjectData/scenes.data").Skip(lineNo + 1).Take(1).First());
                    int YSize2 = int.Parse(File.ReadLines(path + "ObjectData/scenes.data").Skip(lineNo + 2).Take(1).First());
                    System.Diagnostics.Debug.WriteLine(modelName);

                    Texture2D Scenetexture = handle.game.Content.Load<Texture2D>(modelName);

                    //get middleNode
                    node = node = handle.handle.gridNodes[(int)nodePos.X, (int)nodePos.Y];

                    //vect position2d = node.getPositon();
                    SceneObject scene = new SceneObject(Scenetexture, lineNo, node);
                    scene.Size = new Vector2(XSize2, YSize2);
                    scene.rotation = rot;
                    handle.handle.sceneList.Add(scene);
                    break;

                case 3:
                    //load playernodes
                    if (handle.playerNode.Count() < 3)
                    {
                        handle.playerNode.Add(nodePos);
                        System.Diagnostics.Debug.WriteLine("Player added:" + nodePos);
                    }
                    else
                    {
                        System.Diagnostics.Debug.WriteLine("Max number of players achieved");
                    }
                    break;

                case 4://load actor -> dialogtree -> dialogs ->groups

                    //load actor texture and position

                    modelName = File.ReadLines(path + "ObjectData/actors.data").Skip(lineNo - 1).Take(1).First();

                    System.Diagnostics.Debug.WriteLine(modelName);

                    //model = handle.game.Content.Load<Model>(modelName);
                    Texture2D texture2 = handle.game.Content.Load<Texture2D>(modelName);
                    node = handle.handle.gridNodes[(int)nodePos.X, (int)nodePos.Y];

                    position = node.getPositon();

                    rotation = int.Parse(File.ReadLines(path + "ObjectData/actors.data").Skip(lineNo - 1 + 1).Take(1).First());

                    int XSize1 =int.Parse(File.ReadLines(path + "ObjectData/actors.data").Skip(lineNo - 1 + 2).Take(1).First());
                    int YSize1 = int.Parse(File.ReadLines(path + "ObjectData/actors.data").Skip(lineNo - 1 + 3).Take(1).First());

                    int questID = int.Parse(File.ReadLines(path + "ObjectData/actors.data").Skip(lineNo - 1 + 4).Take(1).First());
                    int loadID = int.Parse(File.ReadLines(path + "ObjectData/actors.data").Skip(lineNo - 1 + 5).Take(1).First());
                    //string test = File.ReadLines(path + "ObjectData/actors.data").Skip(lineNo - 1 + 4).Take(1).First();
                    int loadtype = int.Parse(File.ReadLines(path + "ObjectData/actors.data").Skip(lineNo - 1 + 6).Take(1).First());

                    System.Diagnostics.Debug.WriteLine("Condition:" + loadID + " Quest:" + questID);
                    Entity loadObject = fetchObject(loadID, loadtype, node);  //temp value 0. replace w true/false
                    Actor actor = new Actor(modelName, questID, loadObject, texture2, node, lineNo);
                    actor.Size = new Vector2(XSize1,YSize1);

                    //string name,int questID,int conditionID,Texture2D texture, Vector2 position,int lineNo
                    //load dialogs -> no of messages -> rest of the stuff
                    //Dialog("What do you want,punk? ", new Lock(-1, 0, true, 0), 0, 1, new Action(0, 0, 0, 0), true)
                    string dialogfile = File.ReadLines(path + "ObjectData/actors.data").Skip(lineNo - 1 + 7).Take(1).First();
                    System.Diagnostics.Debug.WriteLine("Dialogfile:" + dialogfile);
                    using (StreamReader reader = new StreamReader(path + "ObjectData/Dialog/" + dialogfile + ".dialog"))
                    {
                        int dialogcount = int.Parse(reader.ReadLine());

                        for (int i = 0; i < dialogcount; i++)
                        {

                            //    //load message
                            string message = reader.ReadLine();

                            //    //load lock
                            //type req met value
                            int locktype = int.Parse(reader.ReadLine());
                            int requirement = int.Parse(reader.ReadLine());
                            bool met = bool.Parse(reader.ReadLine());
                            int value = int.Parse(reader.ReadLine());

                            //    //load group and next group
                            int group = int.Parse(reader.ReadLine());
                            int nextgroup = int.Parse(reader.ReadLine());

                            //    //load action
                            //Action(int action, int outcome, int value, int condition)
                            int action = int.Parse(reader.ReadLine());
                            int value1 = int.Parse(reader.ReadLine());


                            //    //load is teller
                            bool teller = bool.Parse(reader.ReadLine());

                            //    //add dialog 
                            actor.dialogtree.dialogList.Add(new Dialog(message, new Lock(locktype, requirement, met, value), group, nextgroup, new Action(action, value1), teller));

                        }
                        actor.printDialog();
                        //load group
                        int groupcount = int.Parse(reader.ReadLine());
                        for (int i = 0; i < groupcount; i++)
                        {
                            //    //load lock
                            //type req met value
                            int locktype = int.Parse(reader.ReadLine());
                            int requirement = int.Parse(reader.ReadLine());
                            bool met = bool.Parse(reader.ReadLine());
                            int value = int.Parse(reader.ReadLine());

                            //load group no
                            int group = int.Parse(reader.ReadLine());
                            //add group
                            actor.dialogtree.defaultgroupList.Add(new Group(new Lock(locktype, requirement, met, value), group));
                        }

                    }



                    //add actor to list
                    handle.handle.actorList.Add(actor);
                    break;

                case 5:
                    //refine the quest objects for desc and additional info
                    //load quest -> load conditions QuestObject(string title,int lineNo)

                    //load quest name
                    string questName = File.ReadLines(path + "ObjectData/quests.data").Skip(lineNo - 1).Take(1).First();
                    //quest number = lineNo

                    QuestObject quest = new QuestObject(questName, lineNo);

                    //open file by name
                    using (StreamReader reader = new StreamReader(path + "ObjectData/Quest/" + questName + ".quest"))
                    {
                        //load quest desc
                        string desc = reader.ReadLine();
                        quest.desc = desc;

                        //fetch condition count
                        int conditionCount = int.Parse(reader.ReadLine());
                        //loop condition
                        for (int i = 0; i < conditionCount; i++)
                        {
                            string conditionName = reader.ReadLine();
                            //conditionID will be assigned by list index
                            Condition condition = new Condition(conditionName);
                            //add condition to quest                            
                            quest.conditionList.Add(condition);
                        }
                    }

                    //add in section for rewards(weapons,reward,experience,etc)

                    //add quest
                    quest.printQuest();
                    QuestManager.questList.Add(quest);
                    break;

                case 6: //load active quests
                    break;
                case 7: // dungeon AI
                    modelName = File.ReadLines(path + "ObjectData/ais.data").Skip(lineNo - 1).Take(1).First();
                    string map = File.ReadLines(path + "ObjectData/ais.data").Skip(lineNo).Take(1).First();

                    System.Diagnostics.Debug.WriteLine(modelName);

                    //model = handle.game.Content.Load<Model>(modelName);
                    texture2 = handle.game.Content.Load<Texture2D>(modelName);
                    node = handle.handle.gridNodes[(int)nodePos.X, (int)nodePos.Y];

                    handle.handle.aiList.Add(new DungeonAI(texture2, lineNo, node, map));
                    break;
                case 8: //floor
                    modelName = File.ReadLines(path + "ObjectData/floors.data").Skip(lineNo - 1).Take(1).First();

                    System.Diagnostics.Debug.WriteLine(modelName);

                    //model = handle.game.Content.Load<Model>(modelName);
                    texture2 = handle.game.Content.Load<Texture2D>(modelName);

                    handle.handle.floorTexture = texture2;
                    handle.handle.floorObject = new FloorObject(texture2, lineNo);
                    break;

                case 9:
                    modelName = File.ReadLines(path + "ObjectData/overlays.data").Skip(lineNo - 1).Take(1).First();
                    System.Diagnostics.Debug.WriteLine(modelName);

                    texture2 = handle.game.Content.Load<Texture2D>(modelName);

                    //get middleNode
                    node = node = handle.handle.gridNodes[(int)nodePos.X, (int)nodePos.Y];

                    //vect position2d = node.getPositon();
                    handle.handle.overlayList.Add(new SceneObject(texture2, lineNo, node));
                    break;

                case 10: // underlay
                    modelName = File.ReadLines(path + "ObjectData/underlays.data").Skip(lineNo - 1).Take(1).First();
                    System.Diagnostics.Debug.WriteLine(modelName);

                    texture2 = handle.game.Content.Load<Texture2D>(modelName);

                    //get middleNode
                    node = node = handle.handle.gridNodes[(int)nodePos.X, (int)nodePos.Y];

                    //vect position2d = node.getPositon();
                    handle.handle.underlayList.Add(new SceneObject(texture2, lineNo, node));
                    break;
                case 11://load loot
                    modelName = File.ReadLines(path + "ObjectData/loot.data").Skip(lineNo - 1).Take(1).First();
                    string lootFile = File.ReadLines(path + "ObjectData/loot.data").Skip(lineNo).Take(1).First();
                    System.Diagnostics.Debug.WriteLine(modelName);

                    texture2 = handle.game.Content.Load<Texture2D>(modelName);

                    //get middleNode
                    node = node = handle.handle.gridNodes[(int)nodePos.X, (int)nodePos.Y];

                    //vect position2d = node.getPositon();
                    LootContainer loot = new LootContainer(texture2,lineNo,node);
 
                    //load the loot items
                    using (StreamReader reader = new StreamReader(path + "ObjectData/Loot/" + lootFile + ".loot")) {
                        //will have to use chu's item loader
                        int count =int.Parse(reader.ReadLine());

                        for (int i = 0; i < count;i++ ) {
                            // Item(int itemID, String itemName, String desc, int itemType, float val, Texture2D itemTexture = null)

                            int itemID = int.Parse(reader.ReadLine());



                           Item item =EquipmentLoader.fetchItem(itemID);


                           if (item != null)
                               loot.itemList.Add(item);

                           else {
                               System.Diagnostics.Debug.WriteLine("Item is not found");
                           }
                        }
                    }
                    handle.handle.sceneList.Add(loot);
                    
                    break;
                default:
                    //load exit nodes  ---> replace it to grid nodes. Actors can replace this
                    handle.handle.sectionNodes.Add(nodePos);
                    handle.borderNodes.Add(nodePos);
                    handle.debugExits();
                    System.Diagnostics.Debug.WriteLine("Exit added");
                    break;
            }
        }
    }
}
