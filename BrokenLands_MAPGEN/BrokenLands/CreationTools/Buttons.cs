﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace BrokenLands
{
    class Buttons
    {
        public string title;
        public Rectangle box;
        public Vector2 position;

        public Buttons(string title,Vector2 position) {
            this.title = title;
            this.position = position;
            
        }

        public void Draw(SpriteBatch batch, SpriteFont font) {
            Vector2 widthHeight=font.MeasureString(title);
            box = new Rectangle((int)position.X,(int)position.Y,(int)widthHeight.X,(int)widthHeight.Y);
            batch.DrawString(font,title,position,Color.White);

        }
    }
}
