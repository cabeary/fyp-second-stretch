﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace BrokenLands
{
    public class Dialog
    {
        //jump group should exist
        public string message;
        Action action;
        public int group; //group the message belongs to
        public Lock locK;
        public bool teller;
        int nextGroup; //the group to go to next
        public Rectangle box;

        public Dialog(string message, Lock lk, int group, int nextgroup, Action action, bool teller)
        {
            this.message = message;
            locK = lk;
            this.group = group;
            this.nextGroup = nextgroup;
            this.action = action;
            this.teller = teller;
        }

        public void setBounds(Vector2 heightWidth,Vector2 pos) {
            box = new Rectangle((int)pos.X, (int)pos.Y, (int)heightWidth.X, (int)heightWidth.Y);
        }

        public int getNextGroup()
        {
            return nextGroup;
        }

        public Action getAction()
        {
            return action;
        }



        public void requirementCheck()
        {
            //Player player = GameSession.players[0];
            switch (locK.type)
            {
                case 0: //cases for stat check
                    switch (locK.value2)
                    {
                        //case 0:// str
                        //    if (player.MyStatus.Strength > locK.value)
                        //        locK.toggleMet();
                        //    break;
                        //case 1://dex
                        //    if (player.MyStatus.Dexterity > locK.value)
                        //        locK.toggleMet();
                        //    break;
                        //case 2://int
                        //    if (player.MyStatus.Intelligence > locK.value)
                        //        locK.toggleMet();
                        //    break;
                        //case 3://cha
                        //    if (player.MyStatus.Charisma > locK.value)
                        //        locK.toggleMet();
                        //    break;
                        //case 4://end
                        //    if (player.MyStatus.Endurance > locK.value)
                        //        locK.toggleMet();
                        //    break;
                        //case 5://agi
                        //    if (player.MyStatus.Agility > locK.value)
                        //        locK.toggleMet();
                        //    break;
                    }
                    break;
                case 1://cases for skill check
                    switch (locK.value2)
                    {

                        //case 0://melee
                        //    if (player.MyStatus.melee > locK.value)
                        //        locK.toggleMet();
                        //    break;
                        //case 1://scavenge
                        //    if (player.MyStatus.scavenge > locK.value)
                        //        locK.toggleMet();
                        //    break;
                        //case 2://lockpick
                        //    if (player.MyStatus.lockpick > locK.value)
                        //        locK.toggleMet();
                        //    break;
                        //case 3://barter
                        //    if (player.MyStatus.barter > locK.value)
                        //        locK.toggleMet();
                        //    break;
                        //case 4://heal
                        //    if (player.MyStatus.heal > locK.value)
                        //        locK.toggleMet();
                        //    break;
                    }
                    break;
                case 2://cases for condition check
                    bool found = false;
                    foreach (QuestObject q in QuestManager.questList)
                    {

                        foreach (Condition c in q.conditionList)
                        {
                            if (locK.value2 == c.conditionID) //need to fetch a conditionid
                            {
                                found = true;
                                if (locK.value == c.condition)
                                {
                                    locK.toggleMet();
                                    break;
                                }
                            }

                            if (found)
                                break;
                        }

                    }
                    break;
                case 3://case check for quest
                    foreach (QuestObject q in QuestManager.questList)
                    {
                        if (locK.value2 == q.lineNo)
                        { //checks for q id

                            if (locK.value == (Convert.ToInt32(q.complete)))
                            {
                                locK.toggleMet();
                            }


                            break;
                        }
                    }
                    break;
                default: //do nothing
                    break;
            }
        }
    }
}
